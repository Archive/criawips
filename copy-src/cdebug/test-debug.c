/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#define CDEBUG_TYPE -1
#include <cdebug/cdebug.h>

gint
main(gint argc, gchar** argv) {
	GObject* object = NULL;
	
	g_type_init();

	object = g_object_new(G_TYPE_OBJECT, NULL);
	cdebug("demo()", "just a demo to demonstrate cdebug without CDEBUG_TYPE");
	cdebugc(G_OBJECT_GET_CLASS(object), "demo()", "just a demo to demonstrate cdebugc");
	cdebugo(object, "demo()", "just a demo to demonstrate cdebugo");
	cdebugt(G_TYPE_OBJECT, "demo()", "just a demo to demonstrate cdebugt");
	g_object_unref(object);
	object = NULL;
	
	return 0;
}
