#include "gnome-office-helpers.h"

gboolean
go_rect_equals(const GORect* rect1, const GORect* rect2) {
	if(!rect1 || !rect2) {
		return FALSE;
	}

	if(rect1 == rect2) {
		return TRUE;
	}

	if(rect1->top == rect2->top &&
	   rect1->bottom == rect2->bottom &&
	   rect1->left == rect2->left &&
	   rect1->right == rect2->right) {
		return TRUE;
	} else {
		return FALSE;
	}
}

static GORect*
go_rect_copy(GORect* rect) {
	GORect* retval = NULL;
	g_return_val_if_fail(rect, retval);
	
	retval = g_new0(GORect, 1);
	*retval = *rect;
	return retval;
}

GType
go_rect_get_type(void) {
	static GType type = 0;

	if(!type) {
		type = g_boxed_type_register_static("GORect", (GBoxedCopyFunc)go_rect_copy, g_free);
	}
	
	return type;
}

