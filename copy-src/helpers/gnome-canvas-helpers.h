#include <libgnomecanvas/libgnomecanvas.h>

#ifndef GNOME_CANVAS_HELPERS_H
#define GNOME_CANVAS_HELPERS_H

void
gnome_canvas_text_set_zoom_size (GnomeCanvasText *item,
				 gdouble          pixels);
void
gnome_canvas_set_zoom (GnomeCanvas *canvas,
		       gdouble      pixels_per_unit);
#endif /* !GNOME_CANVAS_HELPERS_H */
