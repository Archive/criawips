#include <string.h>

#include <glib.h>

#include "gnome-canvas-helpers.h"

static void g_object_set_double(gpointer obj, const char* name, gdouble val);
static gdouble g_object_get_double(gpointer obj, const char* name);

/**
 * g_object_set_double
 * @object: a valid GObject
 * @name:   name of the double value to set
 * @v_double: the actual value
 * 
 * Convenience variant of g_object_set_data() to set
 * a double instead of a pointer.
 */
static void
g_object_set_double (gpointer     object,
		const gchar *name,
		gdouble      v_double)
{
	gdouble zero = 0;
	
	g_return_if_fail (G_IS_OBJECT (object));
	
	if (memcmp (&v_double, &zero, sizeof (zero)) == 0)
		g_object_set_data (object, name, NULL);
	else
	{
		gdouble *dp = g_new0 (gdouble, 1);
		*dp = v_double;
		g_object_set_data_full (object, name, dp, g_free);
	}
}

/**
 * g_object_get_double
 * @object:  a valid GObject
 * @name:    name of the double value to retrieve
 * @RETURNS: the actual value
 * 
 * Convenience variant of g_object_get_data() to retrieve
 * a double instead of a pointer.
 */
static gdouble
g_object_get_double (gpointer     object,
		const gchar *name)
{
	gdouble *dp;
	
	g_return_val_if_fail (G_IS_OBJECT (object), 0);
	
	dp = g_object_get_data (object, name);
	return dp ? *dp : 0;
}


/**
 * gnome_canvas_text_set_zoom_size
 * @item:   canvas text item
 * @pixels: default font size
 *
 * Set the default font size in pixels of a canvas text
 * item. The size will be adjusted when the canvas zoomed
 * via gnome_canvas_set_zoom().
 */
void
gnome_canvas_text_set_zoom_size (GnomeCanvasText *item,
				 gdouble          pixels)
{
  g_return_if_fail (GNOME_IS_CANVAS_TEXT (item));

  g_object_set (item, "size_points", pixels * GNOME_CANVAS_ITEM(item)->canvas->pixels_per_unit, NULL);
  g_object_set_double (item, "zoom_size", pixels);
}

static void
canvas_adjust_text_zoom (GnomeCanvasGroup *group,
			 gdouble           pixels_per_unit)
{
  GList *list;
  for (list = group->item_list; list; list = list->next)
    {
      GnomeCanvasItem *item = list->data;
      if (GNOME_IS_CANVAS_TEXT (item))
	{
	  gdouble zoom_size = g_object_get_double (item, "zoom_size");
	  if (zoom_size != 0)
	    g_object_set (item, "size_points", zoom_size * pixels_per_unit, NULL);
	}
      else if (GNOME_IS_CANVAS_GROUP (item))
	canvas_adjust_text_zoom (GNOME_CANVAS_GROUP (item), pixels_per_unit);
    }
}

/**
 * gnome_canvas_set_zoom
 * @canvas:          valid #GnomeCanvas
 * @pixels_per_unit: zoom factor (defaults to 1.0)
 *
 * This function calls gnome_canvas_set_pixels_per_unit()
 * with its arguments and in addition adjusts the font sizes
 * of all canvas text items where gnome_canvas_text_set_zoom_size()
 * was used before.
 */
void
gnome_canvas_set_zoom (GnomeCanvas *canvas,
		       gdouble      pixels_per_unit)
{
  g_return_if_fail (GNOME_IS_CANVAS (canvas));

  /* adjust all text items */
  canvas_adjust_text_zoom (GNOME_CANVAS_GROUP (canvas->root), pixels_per_unit);
  /* perform the actual zoom */
  gnome_canvas_set_pixels_per_unit (canvas, pixels_per_unit);
}


