#ifndef CRIA_GNOME_OFFICE_HELPERS
#define CRIA_GNOME_OFFICE_HELPERS

#include <glib-object.h>
#include <goffice/utils/go-units.h>

G_BEGIN_DECLS

#define GO_TYPE_RECT (go_rect_get_type())
gboolean go_rect_equals(const GORect* rect1, const GORect* rect2);
GType    go_rect_get_type(void);

G_END_DECLS

#endif /* !CRIA_GNOME_OFFICE_HELPERS */
