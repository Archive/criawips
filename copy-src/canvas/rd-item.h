/* This file is part of criawips
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2007  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef RD_CANVAS_ITEM_H
#define RD_CANVAS_ITEM_H

#include <glib/gmacros.h>
#include <gtk/gtkuimanager.h>

G_BEGIN_DECLS

typedef struct _RdItem        RdItem;
typedef struct _RdItemPrivate RdItemPrivate;
typedef struct _RdItemClass   RdItemClass;

G_END_DECLS

#include <canvas/rd-canvas.h>

G_BEGIN_DECLS

#define RD_TYPE_ITEM         (cria_item_get_type())
#define RD_ITEM(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), RD_TYPE_ITEM, RdItem))
#define RD_ITEM_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), RD_TYPE_ITEM, RdItemClass))
#define RD_IS_ITEM(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), RD_TYPE_ITEM))
#define RD_IS_ITEM_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), RD_TYPE_ITEM))
#define RD_ITEM_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), RD_TYPE_ITEM, RdItemClass))

GType		cria_item_get_type	 (void);
void		cria_item_grab_focus     (RdItem	  * self);
gboolean        cria_item_is_interactive (RdItem	  * self);
void		cria_item_set_interactive(RdItem	  * self,
					  gboolean	    interactive);

/* grouping */
void     rd_item_construct       (RdItem      * self,
				  RdItem      * parent);
RdCanvas*rd_item_get_canvas      (RdItem const* self);
gboolean rd_item_is_constructed  (RdItem const* self);
gboolean rd_item_is_focused      (RdItem const* self);

/* visibility */
void     rd_item_hide		 (RdItem* self);
void     rd_item_show		 (RdItem* self);

/* stacking */
void     rd_item_lower_to_bottom (RdItem* self);
void     rd_item_raise_to_top    (RdItem* self);

typedef enum {
	RD_CAN_FOCUS   = 1 << 0,
	RD_INTERACTIVE = 1 << 1,
} RdItemFlags;

struct _RdItem {
	GnomeCanvasGroup  group;
	RdItem          * focused_child;
	RdItemFlags       item_flags;
};

struct _RdItemClass {
	GnomeCanvasGroupClass  group_class;

	/* signals */
	void     (*build_actions)      (RdItem          * self,
					GtkUIManager    * ui_manager,
					guint             merge_id,
					gchar const     * path);
	gboolean (*button_press_event) (RdItem          * self,
					GdkEventButton  * event);
	gboolean (*button_release_event) (RdItem          * self,
					  GdkEventButton  * event);
	gboolean (*enter_notify_event)   (RdItem          * self,
					  GdkEventCrossing* ev);
	gboolean (*focus)	       (RdItem          * self,
					GtkDirectionType  dir);
	gboolean (*focus_in_event)     (RdItem          * self,
					GdkEventFocus   * event);
	gboolean (*focus_out_event)    (RdItem          * self,
					GdkEventFocus   * event);
	void     (*grab_focus)	       (RdItem          * self);
	gboolean (*key_press_event)    (RdItem        * self,
					GdkEventKey     * event);
	gboolean (*leave_notify_event)   (RdItem          * self,
					  GdkEventCrossing* ev);
};

#define RD_ITEM_FLAGS(item)            (RD_ITEM(item)->item_flags)
#define RD_ITEM_CAN_FOCUS(item)        ((RD_ITEM_FLAGS(item) & RD_CAN_FOCUS) != 0)
#define RD_ITEM_IS_INTERACTIVE(item)   ((RD_ITEM_FLAGS(item) & RD_INTERACTIVE) != 0)
#define RD_ITEM_SET_FLAG(item, flag)   G_STMT_START{ (RD_ITEM_FLAGS(item) |= (flag)); }G_STMT_END
#define RD_ITEM_UNSET_FLAG(item, flag) G_STMT_START{ (RD_ITEM_FLAGS(item) &= ~(flag)); }G_STMT_END

G_END_DECLS

#endif /* !RD_CANVAS_ITEM_H */
