/* This file is part of criawips
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2007  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "rd-line.h"

#include <libgnomecanvas/gnome-canvas-line.h>
#include <canvas/rd-gobject.h>

struct _RdLinePrivate {
	GnomeCanvasItem* line;
};

enum {
	PROP_0,
	PROP_FILL_COLOR,
	PROP_POINTS
};

G_DEFINE_TYPE (RdLine, rd_line, RD_TYPE_PROXY);

RdItem*
rd_line_new (RdItem* parent)
{
	RdItem* result;

	g_return_val_if_fail (RD_IS_ITEM (parent), NULL);

	result = g_object_new (RD_TYPE_LINE, NULL);
	gnome_canvas_item_construct (GNOME_CANVAS_ITEM (result),
				     GNOME_CANVAS_GROUP (parent),
				     NULL, NULL);
	gnome_canvas_item_construct (GNOME_CANVAS_ITEM (RD_LINE (result)->_private->line),
				     GNOME_CANVAS_GROUP (result),
				     NULL, NULL);

	return result;
}

static void
rd_line_init (RdLine* self)
{
	self->_private = G_TYPE_INSTANCE_GET_PRIVATE (self, RD_TYPE_LINE, RdLinePrivate);

	self->_private->line = g_object_new (GNOME_TYPE_CANVAS_LINE, NULL);
}

static void
line_get_property (GObject   * object,
		   guint       prop_id,
		   GValue    * value,
		   GParamSpec* pspec)
{
	RdLine* self = RD_LINE (object);

	switch (prop_id) {
	case PROP_FILL_COLOR:
	case PROP_POINTS:
		g_object_get_property (G_OBJECT (self->_private->line),
				       g_param_spec_get_name (pspec),
				       value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
line_set_property (GObject     * object,
		   guint         prop_id,
		   GValue const* value,
		   GParamSpec  * pspec)
{
	RdLine* self = RD_LINE (object);

	switch (prop_id) {
	case PROP_FILL_COLOR:
	case PROP_POINTS:
		g_object_set_property (G_OBJECT (self->_private->line),
				       g_param_spec_get_name (pspec),
				       value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
rd_line_class_init (RdLineClass* self_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS (self_class);
	GObjectClass* target_class = g_type_class_ref (GNOME_TYPE_CANVAS_LINE);

	object_class->get_property = line_get_property;
	object_class->set_property = line_set_property;

	rd_gobject_class_proxy_property (object_class,
					 PROP_FILL_COLOR,
					 target_class,
					 "fill-color");
	rd_gobject_class_proxy_property (object_class,
					 PROP_POINTS,
					 target_class,
					 "points");

	g_type_class_unref (target_class);

	g_type_class_add_private (self_class, sizeof (RdLinePrivate));
}

