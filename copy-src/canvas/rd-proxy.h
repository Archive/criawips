/* This file is part of criawips
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2007  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef RD_PROXY_H
#define RD_PROXY_H

#include <canvas/rd-item.h>

G_BEGIN_DECLS

typedef struct _RdProxy        RdProxy;
typedef struct _RdProxyPrivate RdProxyPrivate;
typedef struct _RdProxyClass   RdProxyClass;

#define RD_TYPE_PROXY         (rd_proxy_get_type ())
#define RD_PROXY(i)           (G_TYPE_CHECK_INSTANCE_CAST ((i), RD_TYPE_PROXY, RdProxy))
#define RD_PROXY_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST ((c), RD_TYPE_PROXY, RdProxyClass))
#define RD_IS_PROXY(i)        (G_TYPE_CHECK_INSTANCE_TYPE ((i), RD_TYPE_PROXY))
#define RD_IS_PROXY_CLASS(c)  (G_TYPE_CHECL_CLASS_TYPE ((c), RD_TYPE_PROXY))
#define RD_PROXY_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS ((i), RD_TYPE_PROXY, RdProxyClass))

GType rd_proxy_get_type (void);

struct _RdProxy {
	RdItem          base_instance;
	RdProxyPrivate* _private;
};

struct _RdProxyClass {
	RdItemClass     base_class;
};

G_END_DECLS

#endif /* !RD_PROXY_H */
