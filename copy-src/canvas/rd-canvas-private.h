/* This file is part of criawips
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005,2007 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CCC_CANVAS_PRIV_H
#define CCC_CANVAS_PRIV_H

#include <canvas/rd-canvas.h>

#include <gtk/gtkuimanager.h>

G_BEGIN_DECLS

struct _RdCanvas {
	GnomeCanvas	  canvas;
	CcRectangle	* extents;
	gdouble		  zoom;
	guint64		  padding;

	RdCanvasPrivate* _private;
};

struct _RdCanvasClass {
	GnomeCanvasClass  canvas_class;

	gboolean (*focus_changed)      (RdCanvas* self,
					RdItem	* new_focus);
};

G_END_DECLS

#endif /* !CCC_CANVAS_PRIV_H */
