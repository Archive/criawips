/* This file is part of criawips
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2007  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <canvas/rd-pixbuf.h>

#include <libgnomecanvas/gnome-canvas-pixbuf.h>
#include <canvas/rd-gobject.h>

struct _RdPixbufPrivate {
	GnomeCanvasPixbuf* pixbuf;
};

enum {
	PROP_0,
	PROP_HEIGHT,
	PROP_HEIGHT_SET,
	PROP_PIXBUF,
	PROP_WIDTH,
	PROP_WIDTH_SET
};

G_DEFINE_TYPE (RdPixbuf, rd_pixbuf, RD_TYPE_ITEM);

RdItem*
rd_pixbuf_new (RdItem* parent)
{
	RdItem* result;

	g_return_val_if_fail (RD_IS_ITEM (parent), NULL);

	result = g_object_new (RD_TYPE_PIXBUF, "parent", parent, NULL);

	gnome_canvas_item_construct (GNOME_CANVAS_ITEM (result),
				     GNOME_CANVAS_GROUP (parent),
				     NULL, NULL);
	gnome_canvas_item_construct (GNOME_CANVAS_ITEM (RD_PIXBUF (result)->_private->pixbuf),
				     GNOME_CANVAS_GROUP (result),
				     NULL, NULL);

	return result;
}

static void
rd_pixbuf_init (RdPixbuf* self)
{
	self->_private = G_TYPE_INSTANCE_GET_PRIVATE (self, RD_TYPE_PIXBUF, RdPixbufPrivate);

	self->_private->pixbuf = g_object_new (GNOME_TYPE_CANVAS_PIXBUF, NULL);
#warning "we should have a proper construction interface in CcItem"
}

static void
pixbuf_get_property (GObject   * object,
		     guint       prop_id,
		     GValue    * value,
		     GParamSpec* pspec)
{
	RdPixbuf* self = RD_PIXBUF (object);

	switch (prop_id) {
	case PROP_HEIGHT:
	case PROP_HEIGHT_SET:
	case PROP_PIXBUF:
	case PROP_WIDTH:
	case PROP_WIDTH_SET:
		/* proxying for the real item */
		g_object_get_property (G_OBJECT (self->_private->pixbuf),
				       g_param_spec_get_name (pspec),
				       value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
pixbuf_set_property (GObject     * object,
		     guint         prop_id,
		     GValue const* value,
		     GParamSpec  * pspec)
{
	RdPixbuf* self = RD_PIXBUF (object);

	switch (prop_id) {
	case PROP_HEIGHT:
	case PROP_HEIGHT_SET:
	case PROP_PIXBUF:
	case PROP_WIDTH:
	case PROP_WIDTH_SET:
		/* proxying for the real item */
		g_object_set_property (G_OBJECT (self->_private->pixbuf),
				       g_param_spec_get_name (pspec),
				       value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
rd_pixbuf_class_init (RdPixbufClass* self_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS (self_class);
	GObjectClass* pixbuf_class = g_type_class_ref (GNOME_TYPE_CANVAS_PIXBUF);

	object_class->get_property = pixbuf_get_property;
	object_class->set_property = pixbuf_set_property;

	rd_gobject_class_proxy_property (object_class,
					 PROP_HEIGHT,
					 pixbuf_class,
					 "height");
	rd_gobject_class_proxy_property (object_class,
					 PROP_HEIGHT_SET,
					 pixbuf_class,
					 "height-set");
	rd_gobject_class_proxy_property (object_class,
					 PROP_PIXBUF,
					 pixbuf_class,
					 "pixbuf");
	rd_gobject_class_proxy_property (object_class,
					 PROP_WIDTH,
					 pixbuf_class,
					 "width");
	rd_gobject_class_proxy_property (object_class,
					 PROP_WIDTH_SET,
					 pixbuf_class,
					 "width-set");
	g_type_class_add_private (self_class, sizeof (RdPixbufPrivate));

	g_type_class_unref (pixbuf_class);
}

