/* This file is part of criawips
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005,2007 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <canvas/rd-text.h>

#include <string.h>
#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <gtk/gtkimcontext.h>
#include <libgnomecanvas/gnome-canvas-text.h>
#include <canvas/rd-gobject.h>

#define CDEBUG_TYPE cria_canvas_text_get_type
#include <cdebug/cdebug.h>

#include <helpers/gtk-helpers.h>
#include <helpers/gnome-canvas-helpers.h>

struct _RdTextPrivate {
	GnomeCanvasText* text;

	/* the input method */
	GtkIMContext   * im_context;

	/* the cursor position, for editing */
	gint             cursor;
	gint             cursor_trail;
};

G_DEFINE_TYPE (CriaCanvasText, cria_canvas_text, RD_TYPE_PROXY);

enum {
	PROP_0,
	PROP_ANCHOR,
	PROP_CLIP,
	PROP_CLIP_HEIGHT,
	PROP_CLIP_WIDTH,
	PROP_CURSOR_POS,
	PROP_CURSOR_TRAIL,
	PROP_FILL_COLOR_RGBA,
	PROP_FONT_DESCRIPTION,
	PROP_JUSTIFICATION,
	PROP_SIZE_SET,
	PROP_TEXT,
	PROP_TEXT_HEIGHT,
	PROP_TEXT_WIDTH
};

PangoLayout*
rd_text_get_layout (RdText const* self)
{
	g_return_val_if_fail (RD_IS_TEXT (self), NULL);

	return GNOME_CANVAS_TEXT (self->_private->text)->layout;
}

void
rd_text_set_zoom_size (RdText * self,
		       gdouble  zoom_size)
{
	g_return_if_fail (RD_IS_TEXT (self));

	gnome_canvas_text_set_zoom_size (GNOME_CANVAS_TEXT (self->_private->text), zoom_size);
}

static void
cct_move_cursor (CriaCanvasText* self,
		 gint            direction)
{
	PangoLayout* layout;
	gint         new_index, old_index;
	gint         new_trailing, old_trailing;

	layout = rd_text_get_layout (RD_TEXT (self));
	old_index = self->_private->cursor;
	old_trailing = self->_private->cursor_trail;

	pango_layout_move_cursor_visually(layout, TRUE,
					  self->_private->cursor, self->_private->cursor_trail,
					  direction,
					  &new_index, &new_trailing);

	if(new_index == G_MAXINT) {
		return;
	}

	if((new_index != self->_private->cursor && new_index >= 0) ||
	   (new_trailing != self->_private->cursor_trail)) {
		if(new_index != self->_private->cursor && new_index >= 0) {
			self->_private->cursor = new_index;
		}
	
		if(new_trailing != self->_private->cursor_trail) {
			self->_private->cursor_trail = new_trailing;
		}

		cdebug("moveCursor()", "moved from {%d+%d} into %d to {%d+%d}", old_index, old_trailing, direction, new_index, new_trailing);
		g_object_notify(G_OBJECT(self), "cursor");
	}
}

/**
 * cria_canvas_text_reset_cursor:
 * @self: a #CriaCanvasText
 *
 * Reset the cursor of a #CriaCanvasText. This call moves the cursor to the
 * position zero.
 */
void
cria_canvas_text_reset_cursor (RdText* self)
{
	g_return_if_fail (RD_IS_TEXT (self));

	if (!rd_text_get_layout (CRIA_CANVAS_TEXT (self))) {
		return;
	}

	while(self->_private->cursor || self->_private->cursor_trail) {
		cct_move_cursor(self, -1);
	}
}

static void
cct_insert(CriaCanvasText* self, gchar const* text, gsize position) {
	GString* str = g_string_new(pango_layout_get_text (rd_text_get_layout (CRIA_CANVAS_TEXT (self))));
	g_string_insert(str, position, text);
	g_object_set(self, "text", str->str, NULL);
	g_string_free(str, TRUE);
}

static void
cct_insert_and_move_cursor(CriaCanvasText* self, gchar const* text) {
	g_return_if_fail(CRIA_CANVAS_TEXT_GET_CLASS(self)->insert);
	
	gsize i = self->_private->cursor;

	if(self->_private->cursor_trail) {
		gchar const* old  = pango_layout_get_text (rd_text_get_layout (CRIA_CANVAS_TEXT (self)));
		gchar const* next = g_utf8_find_next_char(old + self->_private->cursor, NULL);
		i += (next - (old + self->_private->cursor));
	}
	
	CRIA_CANVAS_TEXT_GET_CLASS(self)->insert(self, text, i);
	
	/* move the cursor AFTER inserting text */
	for(i = text ? g_utf8_strlen(text, -1) : 0; i > 0; i--) {
		cct_move_cursor(self, 1);
	}
}

static void
cria_canvas_text_init (CriaCanvasText* self)
{
	self->_private = G_TYPE_INSTANCE_GET_PRIVATE (self, CRIA_TYPE_CANVAS_TEXT, RdTextPrivate);

	self->_private->text = g_object_new (GNOME_TYPE_CANVAS_TEXT, NULL);

	self->_private->im_context = gtk_im_multicontext_new();
#warning "FIXME: set the gdk window for the im context (eventually try to just use one context for the canvas)"
	//gtk_im_context_set_client_window(self->im_context, gdk_window_from_the_canvas);

	g_signal_connect_swapped(self->_private->im_context, "commit",
				 G_CALLBACK(cct_insert_and_move_cursor), self);
}

static void
cct_finalize(GObject* object) {
	CriaCanvasText* self = CRIA_CANVAS_TEXT(object);

	g_signal_handlers_disconnect_by_func(self->_private->im_context, cct_insert_and_move_cursor, self);
	g_object_unref(self->_private->im_context);

	G_OBJECT_CLASS (cria_canvas_text_parent_class)->finalize (object);
}

static void
text_get_property (GObject   * object,
		   guint       prop_id,
		   GValue    * value,
		   GParamSpec* pspec)
{
	CriaCanvasText* self = CRIA_CANVAS_TEXT(object);

	switch(prop_id) {
	case PROP_ANCHOR:
	case PROP_CLIP:
	case PROP_CLIP_HEIGHT:
	case PROP_CLIP_WIDTH:
	case PROP_FILL_COLOR_RGBA:
	case PROP_FONT_DESCRIPTION:
	case PROP_JUSTIFICATION:
	case PROP_SIZE_SET:
	case PROP_TEXT:
	case PROP_TEXT_HEIGHT:
	case PROP_TEXT_WIDTH:
		/* proxied properties */
		g_object_get_property (G_OBJECT (self->_private->text),
				       g_param_spec_get_name (pspec),
				       value);
		break;
	case PROP_CURSOR_POS:
		g_value_set_int(value, self->_private->cursor);
		break;
	case PROP_CURSOR_TRAIL:
		g_value_set_int(value, self->_private->cursor_trail);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void
text_set_property (GObject     * object,
		   guint         prop_id,
		   GValue const* value,
		   GParamSpec  * pspec)
{
	CriaCanvasText* self = CRIA_CANVAS_TEXT (object);

	if (G_UNLIKELY (!GNOME_CANVAS_ITEM (self->_private->text)->canvas)) {
		gnome_canvas_item_construct (GNOME_CANVAS_ITEM (self->_private->text),
					     GNOME_CANVAS_GROUP (self),
					     NULL, NULL);
	}

	switch (prop_id) {
	case PROP_ANCHOR:
	case PROP_CLIP:
	case PROP_CLIP_HEIGHT:
	case PROP_CLIP_WIDTH:
	case PROP_FILL_COLOR_RGBA:
	case PROP_FONT_DESCRIPTION:
	case PROP_JUSTIFICATION:
	case PROP_SIZE_SET:
	case PROP_TEXT:
	case PROP_TEXT_HEIGHT:
	case PROP_TEXT_WIDTH:
		/* proxied properties */
		g_object_set_property (G_OBJECT (self->_private->text),
				       g_param_spec_get_name (pspec),
				       value);
		break;
	case PROP_CURSOR_POS:
	case PROP_CURSOR_TRAIL:
		/* read only properties fall through */
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void
cct_move_backward_home(CriaCanvasText* self) {
	gchar const* text = pango_layout_get_text(rd_text_get_layout (CRIA_CANVAS_TEXT (self)));

	while(self->_private->cursor > 0 && text[self->_private->cursor - 1] != '\n') {
		cct_move_cursor(self, -1);
	}
}

static void
cct_move_backward_word(CriaCanvasText* self) {
	PangoLayout* layout = rd_text_get_layout (CRIA_CANVAS_TEXT (self));
	gchar const* text   = pango_layout_get_text(layout);
	gint new_pos = g_utf8_pointer_to_offset(text, text + self->_private->cursor + self->_private->cursor_trail),
	     n_attrs;
	PangoLogAttr* attrs;

	pango_layout_get_log_attrs(layout, &attrs, &n_attrs);
	
	cct_move_cursor(self, -1);
	for(new_pos--; new_pos > 0 && !attrs[new_pos].is_word_start; new_pos--) {
		cct_move_cursor(self, -1);
	}

	g_free(attrs);
}

static void
cct_move_forward_end(CriaCanvasText* self) {
	gchar const* text = pango_layout_get_text (rd_text_get_layout (CRIA_CANVAS_TEXT (self)));
	guint len = strlen(text);

#warning "FIXME: refactor 'cursor + cursor_trail * length_next' into a separate function"
	while((guint)(self->_private->cursor + self->_private->cursor_trail * g_utf8_length_next(text + self->_private->cursor)) < len &&
	  text[(self->_private->cursor + self->_private->cursor_trail * g_utf8_length_next(text + self->_private->cursor))] != '\n') {
		cct_move_cursor(self, 1);
	}
}

static void
cct_move_forward_word(CriaCanvasText* self) {
	PangoLayout* layout = rd_text_get_layout (CRIA_CANVAS_TEXT (self));
	gchar const* text   = pango_layout_get_text(layout);
	gint new_pos = g_utf8_pointer_to_offset(text, text + self->_private->cursor),
	     n_attrs;
	PangoLogAttr* attrs;

	pango_layout_get_log_attrs(layout, &attrs, &n_attrs);

	cct_move_cursor(self, 1);
	for(new_pos++; new_pos < n_attrs && !attrs[new_pos].is_word_end; new_pos++) {
		cct_move_cursor(self, 1);
	}

	g_free(attrs);
}

static void
cct_paste_received(GtkClipboard* clipboard, gchar const* text, CriaCanvasText* self) {
#warning "FIXME: do this like the GtkEntry does"
	cct_insert_and_move_cursor(self, text);
	g_object_unref(self);
}

static void
cct_paste(CriaCanvasText* self, GdkAtom selection) {
	g_object_ref(self);
	gtk_clipboard_request_text(gtk_widget_get_clipboard(GTK_WIDGET(GNOME_CANVAS_ITEM(self)->canvas), selection),
				   GTK_CLIPBOARD_TEXT_RECEIVED_FUNC(cct_paste_received), self);
}

static gboolean
cct_button_press_event (RdItem        * item,
			GdkEventButton* ev)
{
	CriaCanvasText * self = CRIA_CANVAS_TEXT (item);
	GnomeCanvasText* gct = self->_private->text;
	GnomeCanvasItem* gci = GNOME_CANVAS_ITEM (item);

	gdouble x, y, zero_x, zero_y;
	gint index = 0;
	gint trail = 0;

	gnome_canvas_world_to_window(gci->canvas, 0.0, 0.0, &zero_x, &zero_y);
	gnome_canvas_world_to_window(gci->canvas, ev->x, ev->y, &x, &y);
	x -= gct->cx + zero_x;
	y -= gct->cy + zero_y;

	pango_layout_xy_to_index(rd_text_get_layout (CRIA_CANVAS_TEXT (self)),
				 PANGO_SCALE * x, PANGO_SCALE * y,
				 &index, &trail);

	if(index > self->_private->cursor) {
		while(index > self->_private->cursor) {
			cct_move_cursor(self, 1);
		}
	} else if(index < self->_private->cursor) {
		while(index < self->_private->cursor) {
			cct_move_cursor(self, -1);
		}
	}

	// now the indices are equal
	while(self->_private->cursor_trail < trail && index == self->_private->cursor) {
		cct_move_cursor(self, 1);
	}

	if(ev->button == 2) {
		cct_paste(self, GDK_SELECTION_PRIMARY);
	}

	return FALSE;
}

static gboolean
cct_handle_key_press(CriaCanvasText* self, GdkEventKey* ev) {
	guint    offset = 0;
	gboolean retval = FALSE;

#ifdef USE_KEYBINDINGS
	if(gtk_im_context_filter_keypress(self->im_context, ev) ||
	   gtk_binding_set_activate(CRIA_CANVAS_TEXT_GET_CLASS(self)->bindings,
		   		    ev->keyval, ev->state, GTK_OBJECT(self)))
	{
		return TRUE;
	}
#else
	if(gtk_im_context_filter_keypress(self->_private->im_context, ev)) {
		return TRUE;
	}
#endif

	switch(ev->keyval) {
	case GDK_Left:
		if(ev->state & GDK_CONTROL_MASK) {
			cct_move_backward_word(self);
		} else {
			cct_move_cursor(self, -1);
		}
		retval = TRUE;
		break;
	case GDK_Right:
		if(ev->state & GDK_CONTROL_MASK) {
			cct_move_forward_word(self);
		} else {
			cct_move_cursor(self, 1);
		}
		retval = TRUE;
		break;
	case GDK_BackSpace:
		offset = 1;
		/* fall through */
	case GDK_Delete:
#warning "FIXME: make this if a g_return_if_fail"
		if(CRIA_CANVAS_TEXT_GET_CLASS(self)->delete) {
			gchar const* text     = pango_layout_get_text (rd_text_get_layout (CRIA_CANVAS_TEXT (self)));
			gsize        cursor   = self->_private->cursor +
						self->_private->cursor_trail * g_utf8_length_next(text + self->_private->cursor);
			gchar const* cursor_p = cursor + text;
			gsize length_of_deleted_char = 0;
			
			if(!offset) {
				length_of_deleted_char = (gsize)g_utf8_find_next_char(cursor_p, NULL);
			} else {
				length_of_deleted_char = (gsize)g_utf8_find_prev_char(text, cursor_p);
			}

			length_of_deleted_char = labs(length_of_deleted_char - (gsize)cursor_p);
			
			/* offset is 0 or 1, so we don't do evil stuff by multiplying */
			offset *= length_of_deleted_char;

			if(cursor >= offset && (cursor + 1 - offset) <= strlen(text)) {
				/* don't delete characters before or after the string */

				/* the cursor MUST be moved before the new text is set because
				 * the old cursor position might be invalid in the new text */
				if(offset) {
					cct_move_cursor(self, -offset);
				}
				
				cdebug("event()", "calling delete(%p, %li, %li)",
				       self,
				       cursor - offset,
				       length_of_deleted_char);
				CRIA_CANVAS_TEXT_GET_CLASS(self)->delete(self, cursor - offset, length_of_deleted_char);
				g_object_notify(G_OBJECT(self), "cursor");
			}
		}
		retval = TRUE;
		break;
	case GDK_Return:
	case GDK_KP_Enter:
		cct_insert_and_move_cursor(self, "\n");
		retval = TRUE;
		break;
	case GDK_Home:
	case GDK_KP_Home:
		cct_move_backward_home(self);
		retval = TRUE;
		break;
	case GDK_End:
	case GDK_KP_End:
		cct_move_forward_end(self);
		retval = TRUE;
		break;
	default:
		break;
	}

	return retval;
}

static gboolean
cct_event(GnomeCanvasItem* item, GdkEvent* ev) {
	gboolean        retval = FALSE;
	CriaCanvasText* self = CRIA_CANVAS_TEXT(item);

	switch(ev->type) {
	case GDK_KEY_PRESS:
		retval = cct_handle_key_press(self, &ev->key);
		break;
#ifdef DIRECT_TEXT
	case GDK_BUTTON_PRESS:
		retval = cct_button_press_event (self, &ev->button);
		break;
#endif
	default:
		break;
	}

	if(GNOME_CANVAS_ITEM_CLASS(cria_canvas_text_parent_class)->event) {
		retval |= GNOME_CANVAS_ITEM_CLASS(cria_canvas_text_parent_class)->event(item, ev);
	}

	return retval;
}

static void
cct_delete(CriaCanvasText* self, gsize offset, gsize num_deleted) {
	GString* str = g_string_new(pango_layout_get_text (rd_text_get_layout (CRIA_CANVAS_TEXT (self))));
	g_string_erase(str, offset, num_deleted);
	g_object_set(self, "text", str->str, NULL);
	g_string_free(str, TRUE);
}

static void
cria_canvas_text_class_init (CriaCanvasTextClass* self_class)
{
	GObjectClass        * object_class = G_OBJECT_CLASS (self_class);
	GObjectClass        * target_class = g_type_class_ref (GNOME_TYPE_CANVAS_TEXT);
	GnomeCanvasItemClass* gci_class;
	RdItemClass         * item_class = RD_ITEM_CLASS (self_class);

	/* GObjectClass */
	object_class->finalize     = cct_finalize;
	object_class->get_property = text_get_property;
	object_class->set_property = text_set_property;

	rd_gobject_class_proxy_property (object_class,
					 PROP_ANCHOR,
					 target_class,
					 "anchor");
	rd_gobject_class_proxy_property (object_class,
					 PROP_CLIP,
					 target_class,
					 "clip");
	rd_gobject_class_proxy_property (object_class,
					 PROP_CLIP_HEIGHT,
					 target_class,
					 "clip-height");
	rd_gobject_class_proxy_property (object_class,
					 PROP_CLIP_WIDTH,
					 target_class,
					 "clip-width");
	rd_gobject_class_proxy_property (object_class,
					 PROP_FILL_COLOR_RGBA,
					 target_class,
					 "fill-color-rgba");
	rd_gobject_class_proxy_property (object_class,
					 PROP_FONT_DESCRIPTION,
					 target_class,
					 "font-desc");
	rd_gobject_class_proxy_property (object_class,
					 PROP_JUSTIFICATION,
					 target_class,
					 "justification");
	rd_gobject_class_proxy_property (object_class,
					 PROP_SIZE_SET,
					 target_class,
					 "size-set");
	rd_gobject_class_proxy_property (object_class,
					 PROP_TEXT,
					 target_class,
					 "text");
	rd_gobject_class_proxy_property (object_class,
					 PROP_TEXT_WIDTH,
					 target_class,
					 "text-width");
	rd_gobject_class_proxy_property (object_class,
					 PROP_TEXT_HEIGHT,
					 target_class,
					 "text-height");
	g_object_class_install_property(object_class,
					PROP_CURSOR_POS,
					g_param_spec_int("cursor",
							 "cursor",
							 "The position of the cursor",
							 0, G_MAXINT,
							 0,
							 G_PARAM_READABLE));
	g_object_class_install_property(object_class,
					PROP_CURSOR_TRAIL,
					g_param_spec_int("cursor-trail",
							 "cursor-trail",
							 "BLURB",
							 0, G_MAXINT,
							 0,
							 G_PARAM_READABLE));

	/* GnomeCanvasItemClass */
	gci_class = GNOME_CANVAS_ITEM_CLASS(self_class);
	gci_class->event = cct_event;

	/* RdItemClass */
	item_class->button_press_event = cct_button_press_event;

	/* CriaCanvasTextClass */
	self_class->delete = cct_delete;
	self_class->insert = cct_insert;
#ifdef USE_KEYBINDINGS
	/* Key bindings */
	self_class->bindings = gtk_binding_set_by_class(self_class);

	/* add_move_binding(bindings, key, modifiers, movement, direction) */
	// GDK_Right, 0    => GTK_MOVEMENT_VISUAL_POSITIONS, 1
	// GDK_KP_Right, 0 => GTK_MOVEMENT_VISUAL_POSITIONS, 1
	// GDK_Left, 0     => GTK_MOVEMENT_VISUAL_POSITIONS, -1
	// GDK_KP_Left, 0  => GTK_MOVEMENT_VISUAL_POSITIONS, -1

	// GDK_Left, GDK_CONTROL_MASK    => GTK_MOVEMENT_WORDS, -1
	// GDK_KP_Left, GDK_CONTROL_MASK => GTK_MOVEMENT_WORDS, -1
	// GDK_Right, GDK_CONTROL_MASK    => GTK_MOVEMENT_WORDS, 1
	// GDK_KP_Right, GDK_CONTROL_MASK => GTK_MOVEMENT_WORDS, 1

	// GDK_Up, 0      => GTK_MOVEMENT_DISPLAY_LINES, -1
	// GDK_KP_Up, 0   => GTK_MOVEMENT_DISPLAY_LINES, -1
	// GDK_Down, 0    => GTK_MOVEMENT_DISPLAY_LINES, 1
	// GDK_KP_Down, 0 => GTK_MOVEMENT_DISPLAY_LINES, 1

	// GDK_Up, GDK_CONTROL_MASK      => GTK_MOVEMENT_PARAGRAPHS, -1
	// GDK_KP_Up, GDK_CONTROL_MASK   => GTK_MOVEMENT_PARAGRAPHS, -1
	// GDK_Down, GDK_CONTROL_MASK    => GTK_MOVEMENT_PARAGRAPHS, 1
	// GDK_KP_Down, GDK_CONTROL_MASK => GTK_MOVEMENT_PARAGRAPHS, 1

	// GDK_Home, 0    => GTK_MOVEMENT_DISPLAY_LINE_ENDS, -1
	// GDK_KP_Home, 0 => GTK_MOVEMENT_DISPLAY_LINE_ENDS, -1
	// GDK_End, 0     => GTK_MOVEMENT_DISPLAY_LINE_ENDS, 1
	// GDK_KP_End, 0  => GTK_MOVEMENT_DISPLAY_LINE_ENDS, 1

	// GDK_Home, GDK_CONTROL_MASK    => GTK_MOVEMENT_DISPLAY_BUFFER_ENDS, -1
	// GDK_KP_Home, GDK_CONTROL_MASK => GTK_MOVEMENT_DISPLAY_BUFFER_ENDS, -1
	// GDK_End, GDK_CONTROL_MASK     => GTK_MOVEMENT_DISPLAY_BUFFER_ENDS, 1
	// GDK_KP_End, GDK_CONTROL_MASK  => GTK_MOVEMENT_DISPLAY_BUFFER_ENDS, 1

	// GDK_Page_Up, 0    => GTK_MOVEMENT_PAGES, -1
	// GDK_KP_Page_Up, 0 => GTK_MOVEMENT_PAGES, -1
	// GDK_Page_Down, 0     => GTK_MOVEMENT_PAGES, 1
	// GDK_KP_Page_Down, 0  => GTK_MOVEMENT_PAGES, 1

	// GDK_Page_Up, GDK_CONTROL_MASK    => GTK_MOVEMENT_HORIZONTAL_PAGES, -1
	// GDK_KP_Page_Up, GDK_CONTROL_MASK => GTK_MOVEMENT_HORIZONTAL_PAGES, -1
	// GDK_Page_Down, GDK_CONTROL_MASK     => GTK_MOVEMENT_HORIZONTAL_PAGES, 1
	// GDK_KP_Page_Down, GDK_CONTROL_MASK  => GTK_MOVEMENT_HORIZONTAL_PAGES, 1

	/* select all */
	gtk_binding_entry_add_signal(self_class->bindings, GDK_a, GDK_CONTROL_MASK,
				     "select_all", 1,
				     G_TYPE_BOOLEAN, TRUE);
	gtk_binding_entry_add_signal(self_class->bindings, GDK_slash, GDK_CONTROL_MASK,
				     "select_all", 1,
				     G_TYPE_BOOLEAN, TRUE);

	/* unselect all */
	gtk_binding_entry_add_signal(self_class->bindings, GDK_backslash, GDK_CONTROL_MASK,
				     "select_all", 1,
				     G_TYPE_BOOLEAN, FALSE);

	/* delete text */
	gtk_binding_entry_add_signal(self_class->bindings, GDK_Delete, 0,
				     "delete_from_cursor", 2,
				     G_TYPE_ENUM, GTK_DELETE_CHARS,
				     G_TYPE_INT,  1);
	gtk_binding_entry_add_signal(self_class->bindings, GDK_KP_Delete, 0,
				     "delete_from_cursor", 2,
				     G_TYPE_ENUM, GTK_DELETE_CHARS,
				     G_TYPE_INT,  1);
	gtk_binding_entry_add_signal(self_class->bindings, GDK_BackSpace, 0,
				     "backspace", 0);
	// the next one prevents mistyping
	gtk_binding_entry_add_signal(self_class->bindings, GDK_BackSpace, GDK_SHIFT_MASK,
				     "backspace", 0);
	gtk_binding_entry_add_signal(self_class->bindings, GDK_Delete, GDK_CONTROL_MASK,
				     "delete_from_cursor", 2,
				     G_TYPE_ENUM, GTK_DELETE_WORD_ENDS,
				     G_TYPE_INT,  1);
	gtk_binding_entry_add_signal(self_class->bindings, GDK_KP_Delete, GDK_CONTROL_MASK,
				     "delete_from_cursor", 2,
				     G_TYPE_ENUM, GTK_DELETE_WORD_ENDS,
				     G_TYPE_INT,  1);
	gtk_binding_entry_add_signal(self_class->bindings, GDK_BackSpace, GDK_CONTROL_MASK,
				     "delete_from_cursor", 2,
				     G_TYPE_ENUM, GTK_DELETE_WORD_ENDS,
				     G_TYPE_INT,  -1);

	/* clipboard */
	gtk_binding_entry_add_signal(self_class->bindings, GDK_x, GDK_CONTROL_MASK,
				     "cut_clipboard", 0);
	gtk_binding_entry_add_signal(self_class->bindings, GDK_c, GDK_CONTROL_MASK,
				     "copy_clipboard", 0);
	gtk_binding_entry_add_signal(self_class->bindings, GDK_v, GDK_CONTROL_MASK,
				     "paste_clipboard", 0);
	gtk_binding_entry_add_signal(self_class->bindings, GDK_Delete, GDK_SHIFT_MASK,
				     "cut_clipboard", 0);
	gtk_binding_entry_add_signal(self_class->bindings, GDK_Insert, GDK_CONTROL_MASK,
				     "copy_clipboard", 0);
	gtk_binding_entry_add_signal(self_class->bindings, GDK_Insert, GDK_SHIFT_MASK,
				     "paste_clipboard", 0);

	/* overwrite */
	gtk_binding_entry_add_signal(self_class->bindings, GDK_Insert, 0,
				     "toggle_overwrite", 0);
	gtk_binding_entry_add_signal(self_class->bindings, GDK_KP_Insert, 0,
				     "toggle_overwrite", 0);

	/* control-tab focus motion */
	gtk_binding_entry_add_signal(self_class->bindings, GDK_Tab, GDK_CONTROL_MASK,
				     "move_focus", 1,
				     GTK_TYPE_DIRECTION_TYPE, GTK_DIR_TAB_FORWARD);
	gtk_binding_entry_add_signal(self_class->bindings, GDK_KP_Tab, GDK_CONTROL_MASK,
				     "move_focus", 1,
				     GTK_TYPE_DIRECTION_TYPE, GTK_DIR_TAB_FORWARD);
	gtk_binding_entry_add_signal(self_class->bindings, GDK_Tab, GDK_SHIFT_MASK | GDK_CONTROL_MASK,
				     "move_focus", 1,
				     GTK_TYPE_DIRECTION_TYPE, GTK_DIR_TAB_BACKWARD);
	gtk_binding_entry_add_signal(self_class->bindings, GDK_KP_Tab, GDK_SHIFT_MASK | GDK_CONTROL_MASK,
				     "move_focus", 1,
				     GTK_TYPE_DIRECTION_TYPE, GTK_DIR_TAB_BACKWARD);
#endif

	g_type_class_add_private (self_class, sizeof (RdTextPrivate));

	g_type_class_unref (target_class);
}

