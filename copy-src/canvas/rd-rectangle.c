/* This file is part of criawips
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2007  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <canvas/rd-rectangle.h>

#include <libgnomecanvas/gnome-canvas-rect-ellipse.h>
#include <canvas/rd-gobject.h>

struct _RdRectanglePrivate {
	GnomeCanvasItem* rectangle;
};

enum {
	PROP_0,
	PROP_FILL_COLOR_RGBA,
	PROP_OUTLINE_COLOR,
	PROP_OUTLINE_COLOR_GDK,
	PROP_OUTLINE_COLOR_RGBA,
	PROP_WIDTH_PIXELS,
	PROP_X1,
	PROP_X2,
	PROP_Y1,
	PROP_Y2
};

G_DEFINE_TYPE (RdRectangle, rd_rectangle, RD_TYPE_PROXY);

RdItem*
rd_rectangle_new (RdItem* parent)
{
	RdRectangle* result;

	g_return_val_if_fail (RD_IS_ITEM (parent), NULL);

	result = g_object_new (RD_TYPE_RECTANGLE, NULL);
	gnome_canvas_item_construct (GNOME_CANVAS_ITEM (result),
				     GNOME_CANVAS_GROUP (parent),
				     NULL, NULL);
	gnome_canvas_item_construct (GNOME_CANVAS_ITEM (result->_private->rectangle),
				     GNOME_CANVAS_GROUP (result),
				     NULL, NULL);

	return RD_ITEM (result);
}

static void
rd_rectangle_init (RdRectangle* self)
{
	self->_private = G_TYPE_INSTANCE_GET_PRIVATE (self, RD_TYPE_RECTANGLE, RdRectanglePrivate);

	self->_private->rectangle = g_object_new (GNOME_TYPE_CANVAS_RECT, NULL);
}

static void
rectangle_get_property (GObject   * object,
			guint       prop_id,
			GValue    * value,
			GParamSpec* pspec)
{
	RdRectangle* self = RD_RECTANGLE (object);

	switch (prop_id) {
	case PROP_FILL_COLOR_RGBA:
	case PROP_OUTLINE_COLOR:
	case PROP_OUTLINE_COLOR_GDK:
	case PROP_OUTLINE_COLOR_RGBA:
	case PROP_WIDTH_PIXELS:
	case PROP_X1:
	case PROP_X2:
	case PROP_Y1:
	case PROP_Y2:
		g_object_get_property (G_OBJECT (self->_private->rectangle),
				       g_param_spec_get_name (pspec),
				       value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
rectangle_set_property (GObject     * object,
			guint         prop_id,
			GValue const* value,
			GParamSpec  * pspec)
{
	RdRectangle* self = RD_RECTANGLE (object);

	switch (prop_id) {
	case PROP_FILL_COLOR_RGBA:
	case PROP_OUTLINE_COLOR:
	case PROP_OUTLINE_COLOR_GDK:
	case PROP_OUTLINE_COLOR_RGBA:
	case PROP_WIDTH_PIXELS:
	case PROP_X1:
	case PROP_X2:
	case PROP_Y1:
	case PROP_Y2:
		/* forward to the real rectangle */
		g_object_set_property (G_OBJECT (self->_private->rectangle),
				       g_param_spec_get_name (pspec),
				       value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
rd_rectangle_class_init (RdRectangleClass* self_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS (self_class);
	GObjectClass* target_class = g_type_class_ref (GNOME_TYPE_CANVAS_RECT);

	object_class->get_property = rectangle_get_property;
	object_class->set_property = rectangle_set_property;

	rd_gobject_class_proxy_property (object_class,
					 PROP_FILL_COLOR_RGBA,
					 target_class,
					 "fill-color-rgba");
	rd_gobject_class_proxy_property (object_class,
					 PROP_OUTLINE_COLOR,
					 target_class,
					 "outline-color");
	rd_gobject_class_proxy_property (object_class,
					 PROP_OUTLINE_COLOR_GDK,
					 target_class,
					 "outline-color-gdk");
	rd_gobject_class_proxy_property (object_class,
					 PROP_OUTLINE_COLOR_RGBA,
					 target_class,
					 "outline-color-rgba");
	rd_gobject_class_proxy_property (object_class,
					 PROP_WIDTH_PIXELS,
					 target_class,
					 "width-pixels");
	rd_gobject_class_proxy_property (object_class,
					 PROP_X1,
					 target_class,
					 "x1");
	rd_gobject_class_proxy_property (object_class,
					 PROP_X2,
					 target_class,
					 "x2");
	rd_gobject_class_proxy_property (object_class,
					 PROP_Y1,
					 target_class,
					 "y1");
	rd_gobject_class_proxy_property (object_class,
					 PROP_Y2,
					 target_class,
					 "y2");

	g_type_class_add_private (object_class, sizeof (RdRectanglePrivate));

	g_type_class_unref (target_class);
}

