/* This file is part of criawips
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005,2007 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIA_CANVAS_TEXT_H
#define CRIA_CANVAS_TEXT_H

#include <gtk/gtkbindings.h>
#include <canvas/rd-proxy.h>

G_BEGIN_DECLS

typedef struct _RdText        RdText;
typedef struct _RdTextPrivate RdTextPrivate;
typedef struct _RdTextClass   RdTextClass;
typedef struct _RdText        CriaCanvasText;
typedef struct _RdTextClass   CriaCanvasTextClass;

#define RD_TYPE_TEXT         (cria_canvas_text_get_type ())
#define RD_TEXT(i)           (G_TYPE_CHECK_INSTANCE_CAST ((i), RD_TYPE_TEXT, RdText))
#define RD_IS_TEXT(i)        (G_TYPE_CHECK_INSTANCE_TYPE ((i), RD_TYPE_TEXT))

#define CRIA_TYPE_CANVAS_TEXT         (cria_canvas_text_get_type())
#define CRIA_CANVAS_TEXT(o)           (G_TYPE_CHECK_INSTANCE_CAST((o), CRIA_TYPE_CANVAS_TEXT, CriaCanvasText))
#define CRIA_CANVAS_TEXT_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), CRIA_TYPE_CANVAS_TEXT, CriaCanvasTextClass))
#define CRIA_IS_CANVAS_TEXT(o)        (G_TYPE_CHECK_INSTANCE_TYPE((o), CRIA_TYPE_CANVAS_TEXT))
#define CRIA_IS_CANVAS_TEXT_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE((k), CRIA_TYPE_CANVAS_TEXT))
#define CRIA_CANVAS_TEXT_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS((o), CRIA_TYPE_CANVAS_TEXT, CriaCanvasTextClass))

GType cria_canvas_text_get_type(void);

void cria_canvas_text_reset_cursor(CriaCanvasText* self);
void         rd_text_set_zoom_size (RdText      * self,
				    gdouble       zoom_size);
PangoLayout* rd_text_get_layout    (RdText const* self);

struct _RdText {
	RdProxy        base_instance;
	RdTextPrivate* _private;
};

struct _RdTextClass {
	RdProxyClass   base_class;
#ifdef USE_KEYBINDINGS
	GtkBindingSet* bindings;
#endif

	/* vtable */
	void (*delete) (CriaCanvasText* self,
			guint           offset,
			gsize           length);
	void (*insert) (CriaCanvasText* self,
			gchar const   * text,
			gsize           offset);
};

G_END_DECLS

#endif

