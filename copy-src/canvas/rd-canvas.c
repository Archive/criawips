/* This file is part of criawips
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005,2007 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "rd-canvas-private.h"

#include <gdk/gdkenumtypes.h>

#define CDEBUG_TYPE cria_canvas_get_type
#include <cdebug/cdebug.h>
#include <helpers/gnome-canvas-helpers.h>

#include <canvas/rd-marshallers.h>

struct _RdCanvasPrivate {
	RdItem* root;
};

enum {
	CANVAS_PROP_0,
	CANVAS_PROP_PADDING,
	CANVAS_PROP_ZOOM
};

enum {
	CANVAS_FOCUS_CHANGED,
	CANVAS_N_SIGNALS
};

G_DEFINE_TYPE(RdCanvas, cria_canvas, GNOME_TYPE_CANVAS);

GType
rd_canvas_get_type (void)
{
	return cria_canvas_get_type ();
}

static void cria_canvas_get_property           (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static void cria_canvas_init		       (RdCanvas	* self);
static void cria_canvas_set_property           (GObject		* object,
						guint		  prop_id,
						const	GValue	* value,
						GParamSpec	* param_spec);
static void cria_canvas_update_zoom	       (RdCanvas       * self);
static void cria_canvas_update_extents	       (RdCanvas        * self);

/* enable these to add support for signals */
static guint cria_canvas_signals[CANVAS_N_SIGNALS] = { 0 };

RdItem*
rd_canvas_get_focus (RdCanvas const* self)
{
	g_return_val_if_fail (RD_IS_CANVAS (self), NULL);

	return RD_ITEM (GNOME_CANVAS (self)->focused_item);
}

RdItem*
rd_canvas_get_item_at (RdCanvas* self,
		       gdouble   world_x,
		       gdouble   world_y)
{
	GnomeCanvasItem* result;

	g_return_val_if_fail (RD_IS_CANVAS (self), NULL);

	result = gnome_canvas_get_item_at(GNOME_CANVAS(self), world_x, world_y);

	g_return_val_if_fail (!result || RD_IS_ITEM (result), NULL);

	return RD_ITEM (result);
}

static void
find_first_focusable_item (gpointer  item,
			   RdItem  **retval)
{
	if(retval && !*retval && RD_IS_ITEM(item) && RD_ITEM_CAN_FOCUS(item)) {
		*retval = RD_ITEM(item);
	}
}

static gboolean
cc_focus (GtkWidget       * widget,
	  GtkDirectionType  dir)
{
	gboolean retval = FALSE;

	if(GTK_WIDGET_CAN_FOCUS(widget)) {
		RdItem* item = NULL;
#if 1
		g_list_foreach(GNOME_CANVAS_GROUP (rd_canvas_get_root (RD_CANVAS(widget)))->item_list,
			       (GFunc)find_first_focusable_item,
			       &item);
#else
		item = rd_canvas_get_root (RD_CANVAS (widget));
#endif

		if (item) {
			if (!GTK_WIDGET_HAS_FOCUS(widget)) {
				gtk_widget_grab_focus(widget);

				if (GNOME_CANVAS(widget)->focused_item) {
					retval = TRUE;
				}
			}

			if (!retval && !GNOME_CANVAS (widget)->focused_item) {
				g_signal_emit_by_name (item, "focus", dir, &retval);
			}

			if (!retval) {
				g_signal_emit_by_name (item, "focus", dir, &retval);
			}
		}
	}

	return retval;
}

static gboolean
cc_focus_in_event(GtkWidget* widget, GdkEventFocus* ev) {
	gboolean retval = FALSE;
	if(GNOME_CANVAS(widget)->focused_item) {
		g_signal_emit_by_name (GNOME_CANVAS(widget)->focused_item,
				       "focus-in-event",
				       ev, &retval);
	}
	return retval;
}

static gboolean
cc_focus_out_event(GtkWidget* widget, GdkEventFocus* ev) {
	gboolean retval = FALSE;

	if(GNOME_CANVAS(widget)->focused_item) {
		gboolean result;
		g_signal_emit_by_name (GNOME_CANVAS (widget)->focused_item,
				       "focus-out-event",
				       ev, &result);
		GNOME_CANVAS(widget)->focused_item = NULL;
	}

	return retval;
}

static gboolean
cc_focus_changed (RdCanvas* self,
		  RdItem  * item)
{
	return FALSE;
}

static void
cria_canvas_class_init (RdCanvasClass* self_class)
{
	GObjectClass	* g_object_class;
	GtkWidgetClass  * widget_class;
	RdCanvasClass   * cria_canvas_class = self_class;

	/* setting up GObjectClass */
	g_object_class = G_OBJECT_CLASS(cria_canvas_class);
	g_object_class->set_property = cria_canvas_set_property;
	g_object_class->get_property = cria_canvas_get_property;

	cria_canvas_signals[CANVAS_FOCUS_CHANGED] = g_signal_new("focus-changed",
					CRIA_TYPE_CANVAS,
					G_SIGNAL_RUN_LAST,
					G_STRUCT_OFFSET(RdCanvasClass, focus_changed),
#warning "canvasClassInit(): FIXME: add accumulator"
					NULL, NULL,
					rd_marshal_BOOLEAN__OBJECT,
					G_TYPE_BOOLEAN,
					1,
					RD_TYPE_ITEM);
	g_object_class_install_property(g_object_class,
					CANVAS_PROP_PADDING,
					g_param_spec_uint64("padding",
							    "Padding",
							    "The padding between the slide and the widget edges.",
							    0,
							    G_MAXUINT64,
							    0,
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property(g_object_class,
					CANVAS_PROP_ZOOM,
					g_param_spec_double("zoom",
							    "Zoom",
							    "The zoom factor of this display: 1.0 means \"presentation size\"",
							    0.01,
							    G_MAXDOUBLE,
							    0.65,
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

	/* setting up the GtkWidget class */
	widget_class = GTK_WIDGET_CLASS(cria_canvas_class);
	widget_class->focus              = cc_focus;
	widget_class->focus_in_event     = cc_focus_in_event;
	widget_class->focus_out_event    = cc_focus_out_event;

	/* setting up CriaCanvasClass */
	cria_canvas_class->focus_changed = cc_focus_changed;

	g_type_class_add_private (self_class, sizeof (RdCanvasPrivate));
}

/**
 * cria_canvas_get_focused:
 * @self: a #CriaCanvas
 *
 * Get the currently selected renderer.
 *
 * Returns the renderer that's currently selected; NULL if there is none
 */
GnomeCanvasItem*
cria_canvas_get_focused (RdCanvas* self)
{
#warning "FIXME: this one could take a const argument"
	g_return_val_if_fail (CRIA_IS_CANVAS(self), NULL);

	return GNOME_CANVAS (self)->focused_item;
}

/**
 * cria_canvas_set_padding:
 * @self: a #CriaCanvas
 *
 * Get the padding of the canvas.
 * 
 * Returns the padding of the canvas.
 */
guint64
cria_canvas_get_padding (RdCanvas* self)
{
#warning "FIXME: this one could take a const argument"
	g_return_val_if_fail (CRIA_IS_CANVAS (self), 0);

	return self->padding;
}

static void
cria_canvas_get_property (GObject   * object,
			  guint       prop_id,
			  GValue    * value,
			  GParamSpec* param_spec)
{
	RdCanvas* self = CRIA_CANVAS(object);

	switch (prop_id) {
	case CANVAS_PROP_PADDING:
		g_value_set_uint64(value, cria_canvas_get_padding(self));
		break;
	case CANVAS_PROP_ZOOM:
		g_value_set_double(value, cria_canvas_get_zoom(self));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object,
						  prop_id,
						  param_spec);
		break;
	}
}

RdItem*
rd_canvas_get_root (RdCanvas const* self)
{
	g_return_val_if_fail (RD_IS_CANVAS (self), NULL);

	return self->_private->root;
}

/**
 * cria_canvas_get_zoom:
 * @self: a #CriaCanvas
 *
 * Get the current zoom value of this canvas
 *
 * Returns the zoom level of this canvas.
 */
gdouble
rd_canvas_get_zoom (RdCanvas const* self)
{
	g_return_val_if_fail (RD_IS_CANVAS (self), 1.0);

	return self->zoom;
}

static void
cria_canvas_init (RdCanvas* self)
{
	self->_private = G_TYPE_INSTANCE_GET_PRIVATE (self, RD_TYPE_CANVAS, RdCanvasPrivate);
	self->extents = g_new0(CcRectangle,1);

	self->_private->root = g_object_new (RD_TYPE_ITEM, NULL);
	//RD_ITEM_SET_FLAG (self->_private->root, RD_CAN_FOCUS);
	gnome_canvas_item_construct (GNOME_CANVAS_ITEM (self->_private->root),
				     gnome_canvas_root (GNOME_CANVAS (self)),
				     NULL, NULL);

	/* initialize the value with this value, so setting 1.0 in construct
	 * time will cause a redraw */
	self->zoom = 0.01;

	gtk_widget_add_events(GTK_WIDGET(self), GDK_KEY_PRESS_MASK);
}

/**
 * cria_canvas_new:
 *
 * Creates a new #CriaCanvas.
 *
 * Returns the newly created and canvas.
 */
RdCanvas*
cria_canvas_new (void)
{
	return g_object_new (CRIA_TYPE_CANVAS, "aa", TRUE, NULL);
}

/**
 * cria_canvas_set_padding:
 * @self: a #CriaCanvas
 * @padding: the new padding
 *
 * Set the padding of the canvas.
 */
void
cria_canvas_set_padding (RdCanvas* self,
			 guint64   padding)
{
	g_return_if_fail(CRIA_IS_CANVAS(self));

	if(padding != self->padding) {
		self->padding = padding;

		cria_canvas_update_extents(self);
		
		g_object_notify(G_OBJECT(self), "padding");
	}
}

static void
cria_canvas_set_property (GObject     * object,
			  guint         prop_id,
			  GValue const* value,
			  GParamSpec  * param_spec)
{
	RdCanvas* self = CRIA_CANVAS (object);
	
	switch(prop_id) {
	case CANVAS_PROP_PADDING:
		cria_canvas_set_padding(self, g_value_get_uint64(value));
		break;
	case CANVAS_PROP_ZOOM:
		cria_canvas_set_zoom(self, g_value_get_double(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

/**
 * cria_canvas_set_extents:
 * @self: a #CriaCanvas
 * @extents: a #CcRectangle (using canvas coordinates)
 *
 * Specify the visible area of this canvas.
 */
void
cria_canvas_set_extents (RdCanvas* self,
			 CcRectangle const* extents)
{
	g_return_if_fail(CRIA_IS_CANVAS(self));
	g_return_if_fail(self->extents);
	g_return_if_fail(extents);

	/* copy the whole structure at once */
	*(self->extents) = *extents;

	cria_canvas_update_extents(self);
	cria_canvas_update_zoom(self);
}

void
cria_canvas_set_zoom (RdCanvas* self,
		      gdouble   zoom)
{
	g_return_if_fail (CRIA_IS_CANVAS (self));

	rd_canvas_set_zoom (self, zoom);
}

void
rd_canvas_set_zoom (RdCanvas* self,
		    gdouble   zoom)
{
	g_return_if_fail(RD_IS_CANVAS(self));

	cdebug("setZoom()", "to %f", zoom);

	if(zoom != self->zoom) {
		self->zoom = zoom;

		cria_canvas_update_zoom (self);

		g_object_notify (G_OBJECT (self), "zoom");
	}
}

void
rd_canvas_window_to_world (RdCanvas* self,
			   gint      win_x,
			   gint      win_y,
			   gdouble * world_x,
			   gdouble * world_y)
{
	g_return_if_fail (RD_IS_CANVAS (self));

	gnome_canvas_window_to_world (GNOME_CANVAS (self),
				      win_x, win_y,
				      world_x, world_y);
}


static void
cria_canvas_update_extents (RdCanvas* self)
{
	gnome_canvas_set_scroll_region(GNOME_CANVAS(self),
#warning "updateExtents(): FIXME: make self->padding a gdouble"
				       self->extents->x - self->padding,
				       self->extents->y - self->padding,
				       self->extents->x + self->extents->w + self->padding,
				       self->extents->y + self->extents->h + self->padding);
	gnome_canvas_set_center_scroll_region(GNOME_CANVAS(self), TRUE);
}

static void
cria_canvas_update_zoom (RdCanvas* self)
{
	g_return_if_fail(CRIA_IS_CANVAS(self));
	g_return_if_fail(GNOME_IS_CANVAS(self));

	if(self->zoom > 0.0 && self->canvas.layout.hadjustment && self->canvas.layout.vadjustment) {
		gnome_canvas_set_zoom (GNOME_CANVAS (self), self->zoom);
	}
}


