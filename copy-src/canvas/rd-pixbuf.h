/* This file is part of criawips
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2007  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef RD_PIXBUF_H
#define RD_PIXBUF_H

#include <canvas/rd-proxy.h>

G_BEGIN_DECLS

typedef struct _RdPixbuf        RdPixbuf;
typedef struct _RdPixbufPrivate RdPixbufPrivate;
typedef struct _RdPixbufClass   RdPixbufClass;

#define RD_TYPE_PIXBUF         (rd_pixbuf_get_type ())
#define RD_PIXBUF(i)           (G_TYPE_CHECK_INSTANCE_CAST ((i), RD_TYPE_PIXBUF, RdPixbuf))
#define RD_PIXBUF_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST ((c), RD_TYPE_PIXBUF, RdPixbufClass))
#define RD_IS_PIXBUF(i)        (G_TYPE_CHECK_INSTANCE_TYPE ((i), RD_TYPE_PIXBUF))
#define RD_IS_PIXBUF_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE ((c), RD_TYPE_PIXBUF))
#define RD_PIXBUF_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS ((i), RD_TYPE_PIXBUF, RdPixbufClass))

GType   rd_pixbuf_get_type (void);
RdItem* rd_pixbuf_new      (RdItem* parent);

struct _RdPixbuf {
	RdProxy          base_instance;
	RdPixbufPrivate* _private;
};

struct _RdPixbufClass {
	RdProxyClass      base_class;
};

G_END_DECLS

#endif /* !RD_PIXBUF_H */
