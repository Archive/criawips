/* This file is part of criawips
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2007  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef RD_RECTANGLE_H
#define RD_RECTANGLE_H

#include <canvas/rd-proxy.h>

G_BEGIN_DECLS

typedef struct _RdRectangle        RdRectangle;
typedef struct _RdRectanglePrivate RdRectanglePrivate;
typedef struct _RdRectangleClass   RdRectangleClass;

#define RD_TYPE_RECTANGLE         (rd_rectangle_get_type ())
#define RD_RECTANGLE(i)           (G_TYPE_CHECK_INSTANCE_CAST ((i), RD_TYPE_RECTANGLE, RdRectangle))
#define RD_RECTANGLE_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST ((c), RD_TYPE_RECTANGLE, RdRectangleClass))
#define RD_IS_RECTANGLE(i)        (G_TYPE_CHECK_INSTANCE_TYPE ((i), RD_TYPE_RECTANGLE))
#define RD_IS_RECTANGLE_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE ((c), RD_TYPE_RECTANGLE))
#define RD_RECTANGLE_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS ((i), RD_TYPE_RECTANGLE, RdRectangleClass))

GType   rd_rectangle_get_type (void);
RdItem* rd_rectangle_new      (RdItem* parent);

struct _RdRectangle {
	RdProxy             base_instance;
	RdRectanglePrivate* _private;
};

struct _RdRectangleClass {
	RdProxyClass base_class;
};

G_END_DECLS

#endif /* !RD_RECTANGLE_H */
