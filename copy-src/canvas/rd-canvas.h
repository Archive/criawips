/* This file is part of criawips
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2007 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef RD_CANVAS_H
#define RD_CANVAS_H

#include <libgnomecanvas/gnome-canvas.h>
#include <canvas/rd-utils.h>

G_BEGIN_DECLS

typedef struct _RdCanvas        RdCanvas;
typedef struct _RdCanvasPrivate RdCanvasPrivate;
typedef struct _RdCanvasClass   RdCanvasClass;

G_END_DECLS

#include <canvas/rd-item.h>

G_BEGIN_DECLS

#define RD_TYPE_CANVAS         (rd_canvas_get_type ())
#define RD_CANVAS(i)           (G_TYPE_CHECK_INSTANCE_CAST ((i), RD_TYPE_CANVAS, RdCanvas))
#define RD_CANVAS_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST ((c), RD_TYPE_CANVAS, RdCanvasClass))
#define RD_IS_CANVAS(i)        (G_TYPE_CHECK_INSTANCE_TYPE ((i), RD_TYPE_CANVAS))
#define RD_IS_CANVAS_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE ((c), RD_TYPE_CANVAS))
#define RD_CANVAS_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS ((i), RD_TYPE_CANVAS, RdCanvasClass))

GType             rd_canvas_get_type           (void);
RdItem*           rd_canvas_get_focus          (RdCanvas const  * self);
RdItem*           rd_canvas_get_item_at        (RdCanvas	* self,
						gdouble		  world_x,
						gdouble		  world_y);
GnomeCanvasItem*  cria_canvas_get_focused      (RdCanvas	* self);
guint64		  cria_canvas_get_padding      (RdCanvas	* self);
RdItem*           rd_canvas_get_root           (RdCanvas const  * self);
GType		  cria_canvas_get_type	       (void);
gdouble           rd_canvas_get_zoom           (RdCanvas const  * self);
#warning "rd_canvas_new() should return a GtkWidget"
RdCanvas*	  cria_canvas_new	       (void);
void		  cria_canvas_set_padding      (RdCanvas	* self,
						guint64		  padding);
void		  cria_canvas_set_extents      (RdCanvas	* self,
						const CcRectangle*extents);
void		  cria_canvas_set_zoom	       (RdCanvas	* self,
						gdouble		  zoom);
void              rd_canvas_set_zoom           (RdCanvas        * self,
						gdouble           zoom);

void              rd_canvas_window_to_world    (RdCanvas        * self,
						gint              win_x,
						gint              win_y,
						gdouble         * world_x,
						gdouble         * world_y);

#warning "FIXME: remove aliases"
#define CRIA_TYPE_CANVAS		(cria_canvas_get_type ())
#define CRIA_CANVAS(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_CANVAS, RdCanvas))
#define CRIA_CANVAS_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_CANVAS, RdCanvasClass))
#define CRIA_IS_CANVAS(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_CANVAS))
#define CRIA_IS_CANVAS_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_CANVAS))
#define CRIA_CANVAS_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_CANVAS, RdCanvasClass))

#define cria_canvas_get_zoom(self) rd_canvas_get_zoom(self)

G_END_DECLS

#endif /* !RD_CANVAS_H */

