/* This file is part of criawips
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2007  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <canvas/rd-proxy.h>

G_DEFINE_ABSTRACT_TYPE (RdProxy, rd_proxy, RD_TYPE_ITEM);

static void
rd_proxy_init (RdProxy* self)
{}

static GObject*
proxy_constructor (GType                  type,
		   guint                  n_properties,
		   GObjectConstructParam* properties)
{
	GObject* result = G_OBJECT_CLASS (rd_proxy_parent_class)->constructor (type, n_properties, properties);
	return result;
}

static void
rd_proxy_class_init (RdProxyClass* self_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS (self_class);

	object_class->constructor = proxy_constructor;
}

