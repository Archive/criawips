/* This file is part of criawips
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2007  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "rd-item.h"

#include <canvas/rd-marshallers.h>

#define CDEBUG_TYPE cria_item_get_type
#include <cdebug/cdebug.h>

enum {
	ITEM_PROP_0,
	ITEM_PROP_INTERACTIVE,
};

enum {
	ITEM_BUILD_ACTIONS,
	ITEM_BUTTON_PRESS_EVENT,
	ITEM_BUTTON_RELEASE_EVENT,
	ITEM_ENTER_NOTIFY_EVENT,
	ITEM_FOCUS,
	ITEM_FOCUS_IN_EVENT,
	ITEM_FOCUS_OUT_EVENT,
	ITEM_GRAB_FOCUS,
	ITEM_KEY_PRESS_EVENT,
	ITEM_LEAVE_NOTIFY_EVENT,
	ITEM_SET_CANVAS,
	ITEM_N_SIGNALS
};

static guint cria_item_signals[ITEM_N_SIGNALS] = { 0 };

static void cria_item_get_property	       (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static void cria_item_set_property	       (GObject		* object,
						guint		  prop_id,
						const GValue	* value,
						GParamSpec	* param_spec);

G_DEFINE_TYPE(RdItem, cria_item, GNOME_TYPE_CANVAS_GROUP);

void
rd_item_construct (RdItem* self,
		   RdItem* parent)
{
	g_return_if_fail (RD_IS_ITEM (self));
	g_return_if_fail (RD_IS_ITEM (parent));

	if (rd_item_is_constructed (self)) {
		g_warning ("constructing the item %s twice",
			   G_OBJECT_TYPE_NAME (self));
	}

	gnome_canvas_item_construct (GNOME_CANVAS_ITEM (self),
				     GNOME_CANVAS_GROUP (parent),
				     NULL, NULL);
}

RdCanvas*
rd_item_get_canvas (RdItem const* self)
{
	g_return_val_if_fail (RD_IS_ITEM (self), NULL);

	return RD_CANVAS (GNOME_CANVAS_ITEM (self)->canvas);
}

void
rd_item_hide (RdItem* self)
{
	g_return_if_fail (RD_IS_ITEM (self));

	gnome_canvas_item_hide (GNOME_CANVAS_ITEM (self));
}

gboolean
rd_item_is_constructed (RdItem const* self)
{
	g_return_val_if_fail (RD_IS_ITEM (self), FALSE);

	return GNOME_CANVAS_ITEM (self)->canvas;
}

gboolean
rd_item_is_focused (RdItem const* self)
{
	g_return_val_if_fail (RD_IS_ITEM (self), FALSE);
	g_return_val_if_fail (RD_IS_CANVAS (rd_item_get_canvas (self)), FALSE);

	return self == rd_canvas_get_focus (rd_item_get_canvas (self));
}

void
rd_item_show (RdItem* self)
{
	g_return_if_fail (RD_IS_ITEM (self));

	gnome_canvas_item_show (GNOME_CANVAS_ITEM (self));
}

void
rd_item_lower_to_bottom (RdItem* self)
{
	gnome_canvas_item_lower_to_bottom (GNOME_CANVAS_ITEM (self));
}

void
rd_item_raise_to_top (RdItem* self)
{
	gnome_canvas_item_raise_to_top (GNOME_CANVAS_ITEM (self));
}

static gdouble
ci_point (GnomeCanvasItem* item,
	  double           x,
	  double           y,
	  int              cx,
	  int              cy,
	  GnomeCanvasItem**actual_item)
{
	gdouble distance = GNOME_CANVAS_ITEM_CLASS (cria_item_parent_class)->point
						(item, x, y, cx, cy, actual_item);

	while (GNOME_IS_CANVAS_ITEM (*actual_item) && !RD_IS_ITEM (*actual_item)) {
		*actual_item = (*actual_item)->parent;
	}

	return distance;
}

static void
find_first_focusable_item (gpointer  item,
			   RdItem  **retval)
{
	if(retval && !*retval && RD_IS_ITEM(item) && RD_ITEM_CAN_FOCUS(item)) {
		*retval = RD_ITEM(item);
	}
}

static gboolean
ci_focus (RdItem          * self,
	  GtkDirectionType  dir)
{
	gboolean retval = FALSE;

	if(!GTK_WIDGET_HAS_FOCUS(GTK_WIDGET(GNOME_CANVAS_ITEM(self)->canvas))) {
		retval = TRUE;
	}

	if(!retval && GNOME_CANVAS_ITEM(self)->canvas->focused_item != GNOME_CANVAS_ITEM(self)) {
		RdItem* current_focus = self->focused_child;

		/* try to focus a child, then focus ourselves */
		if(!current_focus) {
			/* focus the first child */
			RdItem* item = NULL;
			g_list_foreach(GNOME_CANVAS_GROUP(self)->item_list,
				       (GFunc)find_first_focusable_item,
				       &item);

			if(item) {
				g_signal_emit(item, cria_item_signals[ITEM_FOCUS], 0, dir, &retval);
			}
		}

		if(!retval && current_focus) {
			/* emit a focus event on the focused item, if this handler returns TRUE
			 * the sub-item seems to have another sub-item to focus */
			g_warning ("FIXME: make sure focused_child gets unset if it gets detached from the canvas");
			g_signal_emit(self->focused_child, cria_item_signals[ITEM_FOCUS], 0, dir, &retval);
		}

		if(!retval) {
			GList* focus = g_list_find(GNOME_CANVAS_GROUP(self)->item_list, current_focus);

			if(focus && focus->next) {
				do {
					/* just try to find the next item to focus */
					focus = focus->next;
				} while(focus != NULL && !(RD_IS_ITEM(focus->data) && RD_ITEM_CAN_FOCUS(focus->data)));
			}

			if(focus) {
				g_signal_emit(focus->data, cria_item_signals[ITEM_FOCUS], 0, dir, &retval);
			}
		}

		if(!retval && RD_ITEM_CAN_FOCUS(self)) {
			/* no next renderer found, focus ourselves */
			retval = TRUE;
			cria_item_grab_focus(self);
			self->focused_child = NULL;
		}
	}

	return retval;
}

static gboolean
ci_focus_in_event (RdItem       * self,
		   GdkEventFocus* ev)
{
	RD_ITEM(GNOME_CANVAS_ITEM(self)->parent)->focused_child = self;
	return FALSE;
}

static gboolean
ci_focus_out_event (RdItem       * self,
		    GdkEventFocus* ev)
{
	g_return_val_if_fail(GNOME_CANVAS_ITEM(self)->canvas->focused_item == GNOME_CANVAS_ITEM(self), TRUE);
	g_return_val_if_fail(RD_ITEM(GNOME_CANVAS_ITEM(self)->parent)->focused_child == RD_ITEM(self), TRUE);
	RD_ITEM(GNOME_CANVAS_ITEM(self)->parent)->focused_child = NULL;
	return FALSE;
}

static void
ci_grab_focus (RdItem* self)
{
	GnomeCanvasItem* it;
	gboolean         retval = FALSE;
	GdkEventFocus    ev;

	ev.type = GDK_FOCUS_CHANGE;
	ev.window = GTK_WIDGET(GNOME_CANVAS_ITEM(self)->canvas)->window;
	ev.send_event = FALSE;
	ev.in = FALSE;

	if(RD_IS_ITEM(GNOME_CANVAS_ITEM(self)->canvas->focused_item)) {
		g_signal_emit(GNOME_CANVAS_ITEM(self)->canvas->focused_item, cria_item_signals[ITEM_FOCUS_OUT_EVENT], 0, &ev, &retval);

		for(it = GNOME_CANVAS_ITEM(GNOME_CANVAS_ITEM(self)->parent); RD_IS_ITEM(it); it = GNOME_CANVAS_ITEM(it->parent)) {
			RD_ITEM(it)->focused_child = NULL;
		}
	}
	gnome_canvas_item_grab_focus(GNOME_CANVAS_ITEM(self));

	/* emit a focus-in-event on the current item */
	ev.in = TRUE;
	g_signal_emit(self, cria_item_signals[ITEM_FOCUS_IN_EVENT], 0, &ev, &retval);
	
	for(it = GNOME_CANVAS_ITEM(self); it && RD_IS_ITEM(it->parent); it = GNOME_CANVAS_ITEM(it->parent)) {
		RD_ITEM(it->parent)->focused_child = RD_ITEM(it);
	}

	g_signal_emit_by_name (GNOME_CANVAS_ITEM (self)->canvas,
			       "focus-changed",
			       self, &retval);
}

static gboolean
ci_event (GnomeCanvasItem* item,
	  GdkEvent       * ev)
{
	gboolean retval = FALSE;

	switch(ev->type) {
	case GDK_KEY_PRESS:
		g_signal_emit(item, cria_item_signals[ITEM_KEY_PRESS_EVENT], 0, ev, &retval);
		break;
	case GDK_BUTTON_PRESS:
		g_signal_emit(item, cria_item_signals[ITEM_BUTTON_PRESS_EVENT], 0, ev, &retval);
		break;
	case GDK_BUTTON_RELEASE:
		g_signal_emit (item,
			       cria_item_signals[ITEM_BUTTON_RELEASE_EVENT],
			       0,
			       ev,
			       &retval);
		break;
	case GDK_ENTER_NOTIFY:
		g_signal_emit (item,
			       cria_item_signals[ITEM_ENTER_NOTIFY_EVENT],
			       0,
			       ev,
			       &retval);
		break;
	case GDK_LEAVE_NOTIFY:
		g_signal_emit (item,
			       cria_item_signals[ITEM_LEAVE_NOTIFY_EVENT],
			       0,
			       ev,
			       &retval);
		break;
	default:
		{
			GTypeClass* c = g_type_class_ref(GDK_TYPE_EVENT_TYPE);
			cdebug("event()", "unhandled event of type %s", g_enum_get_value(G_ENUM_CLASS(c), ev->type)->value_name);
			g_type_class_unref(c);
		}
		break;
	}
	
	if(!retval && GNOME_CANVAS_ITEM_CLASS(cria_item_parent_class)->event) {
		retval = GNOME_CANVAS_ITEM_CLASS(cria_item_parent_class)->event(item, ev);
	}
	
	return retval;
}

static void
ci_build_actions (RdItem      * self,
		  GtkUIManager* ui_manager,
		  guint         merge_id,
		  gchar const * path)
{
	RdItem* parent;

	g_return_if_fail(RD_IS_ITEM(self));

	parent = (RdItem*)GNOME_CANVAS_ITEM(self)->parent;

	/* just emit the signal on the parent object (used to chain up events) */
	if(RD_IS_ITEM(parent)) {
		g_signal_emit(parent, cria_item_signals[ITEM_BUILD_ACTIONS], 0, ui_manager, merge_id, path);
	}
}

static void
cria_item_class_init (RdItemClass* cria_item_class)
{
	GObjectClass	    * g_object_class;
	GnomeCanvasItemClass* item_class;

	/* setting up the object class */
	g_object_class = G_OBJECT_CLASS(cria_item_class);
	g_object_class->set_property = cria_item_set_property;
	g_object_class->get_property = cria_item_get_property;

	g_object_class_install_property(g_object_class,
					ITEM_PROP_INTERACTIVE,
					g_param_spec_boolean("interactive",
							     "Interactive",
							     "Specifies whether this item should be able to "
							     "receive user events",
							     FALSE,
							     G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

	/* setting up the gnome canvas item class */
	item_class = GNOME_CANVAS_ITEM_CLASS(cria_item_class);
	item_class->event = ci_event;
	item_class->point = ci_point;
	
	/* setting up the cria canvas item class */
	cria_item_class->build_actions   = ci_build_actions;
	cria_item_class->focus           = ci_focus;
	cria_item_class->focus_in_event  = ci_focus_in_event;
	cria_item_class->focus_out_event = ci_focus_out_event;
	cria_item_class->grab_focus      = ci_grab_focus;

	/**
	 * CriaItem::button-press-event
	 * @self: the #CriaItem that received this event
	 * @ui_manager: a #GtkUIManager used to build the menu
	 * @merge_id: the merge id (take a look at #gtk_ui_manager_add_ui() for information)
	 * @path: the merge path
	 * 
	 * This signal gets emitted when an item should build menu entries for a popup
	 * menu.
	 */
	cria_item_signals[ITEM_BUILD_ACTIONS] =
		g_signal_new("build-actions", RD_TYPE_ITEM,
			     G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(RdItemClass, build_actions),
			     NULL, NULL,
			     rd_marshal_VOID__OBJECT_UINT_STRING,
			     G_TYPE_NONE, 3,
			     GTK_TYPE_UI_MANAGER,
			     G_TYPE_UINT,
			     G_TYPE_STRING);

	cria_item_signals[ITEM_BUTTON_PRESS_EVENT] = g_signal_new("button-press-event",
							RD_TYPE_ITEM,
							G_SIGNAL_RUN_LAST,
							G_STRUCT_OFFSET(RdItemClass, button_press_event),
							rd_accumulator_boolean, NULL,
							rd_marshal_BOOLEAN__BOXED,
							G_TYPE_BOOLEAN,
							1,
							GDK_TYPE_EVENT);
	cria_item_signals[ITEM_BUTTON_RELEASE_EVENT] =
		g_signal_new ("button-release-event",
			      RD_TYPE_ITEM,
			      0,
			      G_STRUCT_OFFSET (RdItemClass, button_release_event),
			      rd_accumulator_boolean, NULL,
			      rd_marshal_BOOLEAN__BOXED,
			      G_TYPE_BOOLEAN,
			      1,
			      GDK_TYPE_EVENT);
	cria_item_signals[ITEM_ENTER_NOTIFY_EVENT] =
		g_signal_new ("enter-notify-event",
			      RD_TYPE_ITEM,
			      0,
			      G_STRUCT_OFFSET (RdItemClass, enter_notify_event),
			      rd_accumulator_boolean, NULL,
			      rd_marshal_BOOLEAN__BOXED,
			      G_TYPE_BOOLEAN,
			      1,
			      GDK_TYPE_EVENT);

	/**
	 * CriaItem::focus:
	 * @dir: a #GtkDirectionType
	 *
	 * The ::focus signal is emitted when focus events happen.
	 *
	 * Returns %TRUE to stop other handlers from being invoked for this event.
	 *   %FALSE to porpagate the event further.
	 */
	cria_item_signals[ITEM_FOCUS] = g_signal_new("focus",
							RD_TYPE_ITEM,
							G_SIGNAL_RUN_LAST,
							G_STRUCT_OFFSET(RdItemClass, focus),
							rd_accumulator_boolean, NULL,
							rd_marshal_BOOLEAN__ENUM,
							G_TYPE_BOOLEAN,
							1,
							GTK_TYPE_DIRECTION_TYPE);
	cria_item_signals[ITEM_FOCUS_IN_EVENT] = g_signal_new("focus-in-event",
							RD_TYPE_ITEM,
							G_SIGNAL_RUN_LAST,
							G_STRUCT_OFFSET(RdItemClass, focus_in_event),
							rd_accumulator_boolean, NULL,
							rd_marshal_BOOLEAN__BOXED,
							G_TYPE_BOOLEAN,
							1,
							GDK_TYPE_EVENT);
	cria_item_signals[ITEM_FOCUS_OUT_EVENT] = g_signal_new("focus-out-event",
							RD_TYPE_ITEM,
							G_SIGNAL_RUN_LAST,
							G_STRUCT_OFFSET(RdItemClass, focus_out_event),
							rd_accumulator_boolean, NULL,
							rd_marshal_BOOLEAN__BOXED,
							G_TYPE_BOOLEAN,
							1,
							GDK_TYPE_EVENT);
	cria_item_signals[ITEM_GRAB_FOCUS] = g_signal_new("grab-focus",
							RD_TYPE_ITEM,
							G_SIGNAL_RUN_LAST,
							G_STRUCT_OFFSET(RdItemClass, grab_focus),
							NULL, NULL,
							g_cclosure_marshal_VOID__VOID,
							G_TYPE_NONE,
							0);
	cria_item_signals[ITEM_KEY_PRESS_EVENT] = g_signal_new("key-press-event",
							RD_TYPE_ITEM,
							G_SIGNAL_RUN_LAST,
							G_STRUCT_OFFSET(RdItemClass, key_press_event),
							rd_accumulator_boolean, NULL,
							rd_marshal_BOOLEAN__BOXED,
							G_TYPE_BOOLEAN,
							1,
							GDK_TYPE_EVENT);
	cria_item_signals[ITEM_LEAVE_NOTIFY_EVENT] =
		g_signal_new ("leave-notify-event",
			      RD_TYPE_ITEM,
			      0,
			      G_STRUCT_OFFSET (RdItemClass, leave_notify_event),
			      rd_accumulator_boolean, NULL,
			      rd_marshal_BOOLEAN__BOXED,
			      G_TYPE_BOOLEAN,
			      1,
			      GDK_TYPE_EVENT);
}

/**
 * cria_item_is_interactive:
 * @self: a #CriaItem
 *
 * Find out whether an item is interactive or not. Interactive items may
 * respond or react to events like user input etc.
 *
 * Returns TRUE if the item is interactive, FALSE if not
 */
gboolean
cria_item_is_interactive (RdItem* self)
{
	g_return_val_if_fail(RD_IS_ITEM(self), FALSE);

	return RD_ITEM_IS_INTERACTIVE(self);
}

static void
cria_item_get_property (GObject   * object,
			guint       prop_id,
			GValue    * value,
			GParamSpec* param_spec)
{
	RdItem* self = RD_ITEM(object);

	switch(prop_id) {
	case ITEM_PROP_INTERACTIVE:
		g_value_set_boolean(value, cria_item_is_interactive(self));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

/**
 * cria_item_grab_focus:
 * @self: a #CriaItem
 *
 * Grabs the keyboard focus to this renderer. This function should only be
 * called on renderers which can focus.
 */
void
cria_item_grab_focus (RdItem* self)
{
	g_return_if_fail(RD_IS_ITEM(self));

	if(!RD_ITEM_CAN_FOCUS(self)) {
		return;
	}

	if(RD_ITEM(GNOME_CANVAS_ITEM(self)->canvas->focused_item) == self) {
		/* just make sure the widget is focused */
		gtk_widget_grab_focus(GTK_WIDGET(GNOME_CANVAS_ITEM(self)->canvas));
	}

	/* emit a focus on the current item, if the result is non-FALSE,
	 * grab the focus, return if not */
	g_signal_emit(self, cria_item_signals[ITEM_GRAB_FOCUS], 0);
}

/**
 * cria_item_set_interactive:
 * @self: a #CriaItem
 * @interactive: a #gboolean
 *
 * Allow this item to get user events (TRUE) or not.
 */
void
cria_item_set_interactive (RdItem  * self,
			   gboolean  interactive)
{
	g_return_if_fail(RD_IS_ITEM(self));

	if(interactive == RD_ITEM_IS_INTERACTIVE(self)) {
		return;
	}

	if(interactive) {
		RD_ITEM_SET_FLAG(self, RD_INTERACTIVE);
	} else {
		RD_ITEM_UNSET_FLAG(self, RD_INTERACTIVE);
	}
	
	cdebug("setInteractive()", "set interactive to %s", interactive ? "true" : "false");
	g_object_notify(G_OBJECT(self), "interactive");
}

static void
cria_item_set_property (GObject     * object,
			guint         prop_id,
			GValue const* value,
			GParamSpec  * param_spec)
{
	RdItem* self = RD_ITEM(object);

	switch(prop_id) {
	case ITEM_PROP_INTERACTIVE:
		cria_item_set_interactive(self, g_value_get_boolean(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

static void
cria_item_init (RdItem* self)
{}

