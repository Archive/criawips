<INCLUDE>ccc/canvas.h</INCLUDE>

<SECTION>
<FILE>canvas</FILE>
CriaCanvas
cria_canvas_get_focused
cria_canvas_get_padding
cria_canvas_get_render_group
cria_canvas_get_top_group
cria_canvas_get_zoom
cria_canvas_new
cria_canvas_set_focused
cria_canvas_set_padding
cria_canvas_set_zoom
<TITLE>CriaCanvas</TITLE>
<SUBSECTION Standard>
CriaCanvasClass
CriaCanvasPrivate
CRIA_IS_CANVAS
CRIA_TYPE_CANVAS
cria_canvas_get_type
CRIA_IS_CANVAS_CLASS
CRIA_CANVAS
CRIA_CANVAS_CLASS
CRIA_CANVAS_GET_CLASS
</SECTION>

<SECTION>
<FILE>item</FILE>
CriaItem
cria_item_get_canvas
cria_item_get_type
cria_item_grab_focus
cria_item_is_editable
cria_item_set_canvas
<TITLE>CriaItem</TITLE>
<SUBSECTION Standard>
CriaItemClass
CriaItemPrivate
CRIA_TYPE_ITEM
CRIA_ITEM
CRIA_ITEM_CLASS
CRIA_IS_ITEM
CRIA_IS_ITEM_CLASS
CRIA_ITEM_GET_CLASS
</SECTION>
