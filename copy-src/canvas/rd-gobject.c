/* This file is part of criawips
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2007  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "rd-gobject.h"

static GParamSpec*
g_param_spec_new_proxy (GParamSpec* target)
{
	GParamSpec* result;

	g_return_val_if_fail (G_TYPE_CHECK_INSTANCE_TYPE (target, G_TYPE_PARAM), NULL);

	result = g_param_spec_internal (G_TYPE_INSTANCE_GET_CLASS(target,
								  G_TYPE_PARAM,
								  GTypeClass)->g_type,
					g_param_spec_get_name (target),
					g_param_spec_get_nick (target),
					g_param_spec_get_blurb (target),
					target->flags);

	result->value_type = target->value_type;

	if (G_IS_PARAM_SPEC_ENUM (target)) {
		GParamSpecEnum* result_e = G_PARAM_SPEC_ENUM (result);
		GParamSpecEnum* target_e = G_PARAM_SPEC_ENUM (target);

		result_e->enum_class    = g_type_class_ref (((GTypeClass*)target_e->enum_class)->g_type);
		result_e->default_value = target_e->default_value;
	}

	return result;
}

void
rd_gobject_class_proxy_property (GObjectClass* object_class,
				 guint         property_id,
				 GObjectClass* target_class,
				 gchar const * property_name)
{
	g_object_class_install_property (object_class,
					 property_id,
					 g_param_spec_new_proxy (g_object_class_find_property (target_class,
											       property_name)));
}

