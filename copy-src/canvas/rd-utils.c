/* This file is part of criawips
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2007  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <canvas/rd-utils.h>

/**
 * rd_accumulator_boolean:
 * @hint: a #GSignalInvocationHint
 * @return_accu: a #GValue for the emission
 * @handler_return: a #GValue
 * @data: unused
 *
 * Serves as a simple accumulator for signal emission. If you set
 * rd_accumulator_boolean() as the accumulator for a signal, the signal will
 * abort execution once a signal handler returns %TRUE.
 *
 * Returns %FALSE to stop, %TRUE to continue.
 */
gboolean
rd_accumulator_boolean (GSignalInvocationHint* hint,
			GValue               * return_accu,
			GValue const         * handler_return,
			gpointer               data)
{
	gboolean retval = g_value_get_boolean (handler_return);

	g_value_set_boolean (return_accu, retval);

	return !retval;
}

