/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIAWIPS_LEPTON_H
#define CRIAWIPS_LEPTON_H

#include <glib/gmacros.h>
#include <glib/gtypes.h>

G_BEGIN_DECLS

typedef gsize CriaLepton;

void        cria_lepton_init          (void);
void        cria_lepton_shutdown      (void);
void        cria_lepton_shutdown_debug(void);

CriaLepton  cria_lepton_ref           (gchar const     * string);
void	    cria_lepton_unref         (CriaLepton const  lepton);
const gchar*cria_lepton_get           (CriaLepton const  lepton);

guint       cria_lepton_refs          (CriaLepton const  lepton);

G_END_DECLS

#endif /* !CRIAWIPS_LEPTON_H */
