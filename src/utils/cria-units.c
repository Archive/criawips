/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <utils/cria-units.h>

#include <stdlib.h>
#include <string.h>
#include <goffice/utils/go-units.h>

#define CDEBUG_TYPE -1
#include <cdebug/cdebug.h>

GODistance
cria_unit_to_pixels(const gchar* size, GODistance size_reference) {
	gchar    * end = NULL;
	gdouble    retval = 0.0;

	if(!size || !*size) {
		return retval;
	}
	
#warning "setFont(): FIXME: Add more unit types"
	retval = size ? g_ascii_strtod(size, &end) : 0.0;
	cdebug("CriaUnit::toPixels()", "parsed '%s' to %f", size, retval);
	
	if(!retval || !end || !*end || !strcmp(end, "px")) {
		/* keep retval this way */
	} else if(!strcmp(end, "cm") || !strcmp(end, "mm")) {
		/* cm/mm */
		if(*end == 'c') {
			retval *= 10;
		}
		retval = GO_CM_TO_PT(retval*576);
	} else if(!strcmp(end, "in")) {
		/* inch */
		retval = GO_IN_TO_PT(retval);
	} else if(!strcmp(end, "%")) {
		gdouble exact = 0.01 * retval * size_reference;
		retval = exact;
	} else {
		/* keep retval this way (let it be pixels) */
	}

	return retval;
}
