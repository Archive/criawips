/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <utils/cria-lepton.h>

#include <string.h>
#include <glib/gmessages.h>
#include <glib/gslist.h>
#include <glib/gstrfuncs.h>
#include <glib/gtree.h>

#define ALLOC_CHUNK_SIZE 32

typedef struct {
	gchar* string;
	guint  refs;
} CriaLeptonData;

static CriaLeptonData*data_array   = NULL;
static gsize          initialized  = 0;
static gsize          max_lepton   = 0; /* the index of the next element, the associated lepton is ++max_lepton */
static GTree*         lookup_tree  = NULL;
static GSList*	      free_leptons = NULL;

/* initialize and finalize functions */

/* extend the array to be able to store new elements */
static void
cl_extend(void) {
	CriaLeptonData*new_data = g_new0(CriaLeptonData, initialized + ALLOC_CHUNK_SIZE);
	if(initialized) {
		memcpy(new_data, data_array, sizeof(CriaLeptonData) * initialized);
	}
	initialized += ALLOC_CHUNK_SIZE;
	g_free(data_array);
	data_array = new_data;
}

/**
 * cria_lepton_init:
 *
 * Initialize the lepton system.
 */
void
cria_lepton_init(void) {
	g_return_if_fail(!initialized);
	lookup_tree = g_tree_new((GCompareFunc)strcmp);
	cl_extend();
}

static void
cl_clear_tree(void) {
	g_tree_destroy(lookup_tree);
	lookup_tree = NULL;
}

static void
cl_clear_array(void) {
	g_free(data_array);
	g_slist_free(free_leptons);
	data_array = NULL;
	initialized = 0;
}

/**
 * cria_lepton_shutdown_debug:
 *
 * Shutdown the functionality of the lepton system, freeing all the memory that
 * has been used. Contrary to #cria_lepton_shutdown this method shows a warning
 * for any leaked reference.
 */
void
cria_lepton_shutdown_debug(void) {
	gsize i;

	g_return_if_fail(initialized);

	cl_clear_tree();

	for(i = 0; i < max_lepton; i++) {
		if G_UNLIKELY(data_array[i].refs) {
			g_warning("CriaLepton \"%s\" has %d leaked reference%s",
				  data_array[i].string,
				  data_array[i].refs,
				  data_array[i].refs <= 1 ? "" : "s");
		}
		
		if G_LIKELY(data_array[i].string) {
			g_free(data_array[i].string);
			data_array[i].string = NULL;
		}
	}
	
	cl_clear_array();
}

/**
 * cria_lepton_shutdown:
 *
 * Shutdown the functionality of the lepton system, freeing all the memory that
 * has been used.
 */
void
cria_lepton_shutdown(void) {
	gsize i;
	
	g_return_if_fail(initialized);
	
	cl_clear_tree();
	
	for(i = 0; i < max_lepton; i++) {
		if G_LIKELY(data_array[i].string) {
			g_free(data_array[i].string);
			data_array[i].string = NULL;
		}
	}

	cl_clear_array();
}

/**
 * cria_lepton_ref:
 * @string: the string to create a unique id (#CriaLepton) for
 *
 * Creates a new #CriaLepton if the string hasn't been registered yet. It it
 * has it just increases the reference count and returns the yet registered
 * #CriaLepton.
 *
 * Returns the #CriaLepton belonging to @string.
 */
CriaLepton
cria_lepton_ref(gchar const* string) {
	CriaLepton retval = 0;
	
	g_return_val_if_fail(initialized, 0);

	if G_UNLIKELY(!string) {
		return 0;
	}

	retval = GPOINTER_TO_SIZE(g_tree_lookup(lookup_tree, string));

	if G_LIKELY(retval) {
		/* just increase the reference count */
		data_array[retval - 1].refs += 1;
	} else {
		if G_UNLIKELY(free_leptons) {
			/* try to insert at a freed position */
			retval = GPOINTER_TO_SIZE(free_leptons->data);
			free_leptons = g_slist_delete_link(free_leptons, free_leptons);
		} else {
			/* append the data */
			if G_UNLIKELY(max_lepton == initialized) {
				cl_extend();
			}
			
			retval = ++max_lepton;
		}
		
		data_array[retval - 1].string = g_strdup(string);
		data_array[retval - 1].refs = 1;
		
		g_tree_insert(lookup_tree, data_array[retval - 1].string, GSIZE_TO_POINTER(retval));
	}
	
	g_return_val_if_fail(retval, retval);

	return retval;
}

/**
 * cria_lepton_unref:
 * @lepton: the #CriaLepton to be unreffed
 *
 * Unrefs the #CriaLepton specified by @lepton, frees allocated memory if
 * necessary.
 */
void
cria_lepton_unref(CriaLepton lepton) {
	g_return_if_fail(initialized);
	
	if G_UNLIKELY(!lepton) {
		return;
	}
	
	g_return_if_fail(data_array[lepton - 1].refs);

	data_array[lepton - 1].refs -= 1;

	if G_UNLIKELY(!data_array[lepton - 1].refs) {
		g_tree_remove(lookup_tree, data_array[lepton - 1].string);
		g_free(data_array[lepton - 1].string);
		data_array[lepton - 1].string = NULL;
		free_leptons = g_slist_prepend(free_leptons, GSIZE_TO_POINTER(lepton));
	}
}

/**
 * cria_lepton_get:
 * @lepton: a #CriaLepton
 *
 * Get the string associated to a #CriaLepton.
 *
 * Returns the string associated to a #CriaLepton; returns an empty string
 * (%"") if @lepton is %0.
 */
const gchar*
cria_lepton_get(CriaLepton lepton) {
	g_return_val_if_fail(initialized, "");
	g_return_val_if_fail(lepton <= initialized, "");
	
	return G_LIKELY(lepton) ? data_array[lepton - 1].string : "";
}

/**
 * cria_lepton_refs:
 * @lepton: a #CriaLepton
 *
 * Get the number of references on a #CriaLepton. This information might be
 * useful when you're looking for reference leaks. The lepton '0' has got
 * infinite (%G_MAXUINT) references.
 *
 * Returns the number of references on a #CriaLepton.
 */
guint
cria_lepton_refs(CriaLepton const lepton) {
	if(!lepton) {
		return G_MAXUINT;
	}

	if(lepton < max_lepton || !data_array[lepton - 1].string) {
		g_return_val_if_fail("%d is at the moment not a valid lepton.", 0);
	}

	return data_array[lepton - 1].refs;
}
