/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *       Adrien Beaucreux     <informancer@afturgurluk.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 * Copyright (C) 2004,2005 Adrien Beaucreux
 * Copyright (C) 2004 Keith Sharp
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdint.h>

#include <glib.h>
#include <glib-object.h>
#include <gconf/gconf-client.h>
#include <gtk/gtk.h>
#include <gnome.h>
#include <glade/glade.h>
#include <libgnomevfs/gnome-vfs-utils.h>

#define CDEBUG_TYPE cria_application_get_type
#include <cdebug/cdebug.h>
#include <helpers/gtk-helpers.h>

#include <widgets/cria-main-window.h>
#include <widgets/cria-slide-show.h>
#include <utils/cria-lepton.h>

#include "application.h"
#include "libglade-support.h"

enum {
	PROP_0,
	PROP_DEFAULT_FONT,
	PROP_DETACH_MENUBARS,
	PROP_DETACH_TOOLBARS,
	PROP_DISABLE_SCREENSAVER,
	PROP_GCONF_CLIENT,
	PROP_PGDOWN_NEW_SLIDE,
	PROP_SHOW_TEAROFF,
};

enum {
	CHANGED_DEFAULT_FONT_SIGNAL,
	N_SIGNALS
};

struct _CriaApplicationPriv {
	GList		     * windows;
	PangoFontDescription * default_font;
	GConfClient          * gconf_client;
	gboolean	       disable_screensaver;
	gboolean	       page_down_creates_slide;
};

/* method prototypes */

/*int x = -1, y, w = -1, h = -1;*/
/*char *debug_modules = NULL;	*/

gboolean	start_off = 0,
		debug     = 0;

static CriaApplication	* self = NULL;

static const struct poptOption options[] = {
	{"debug", '\0', POPT_ARG_NONE, &debug, 0, N_("Enable debugging"), NULL},
	{"start-off", 's', POPT_ARG_NONE, &start_off, 0,
		N_("Start a presentation (and open a file selection if no file "
			"is given at the command line)"), NULL},
/*	{NULL, 'x', POPT_ARG_INT, &x, 0, N_("X position of window"), N_("X")},
	{NULL, 'y', POPT_ARG_INT, &y, 0, N_("Y position of window"), N_("Y")},
	{NULL, 'w', POPT_ARG_INT, &w, 0, N_("width of window"), N_("WIDTH")},
	{NULL, 'h', POPT_ARG_INT, &h, 0, N_("height of window"), N_("WIDTH")},
	{"debug-modules", '\0', POPT_ARG_STRING, &debug_modules, 0,
		N_("Modules to enable debugging in"), N_("MODULES")},
*/	{NULL, '\0', 0, NULL, 0}
};

static void cria_application_get_property      (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static void cria_application_set_property      (GObject		* object,
						guint		  prop_id,
						const GValue	* value,
						GParamSpec	* param_spec);
static void cria_open_presentation_from_filename
					       (char const	* filename);
static void cria_application_init	       (CriaApplication	* self);

static void notify_default_font_changed        (GConfClient *client,
						guint cnxn_id,
						GConfEntry *entry,
						gpointer user_data);

static guint		  cria_application_signals[N_SIGNALS] = { 0 };

static void
cmw_about_uri_cb(GtkAboutDialog* d, const gchar* link, gpointer data) {
	gnome_url_show(link, NULL);
}

/**
 * criawips_init:
 * @argc: pointer to argc as delivered to the main() method
 * @argv: pointer to the argument vector as delivered to the main() method
 *
 * Initializes the application. #criawips_init parses the command line arguments
 * and strips those arguments it understood from argc and argv.
 */
void
criawips_init(int *argc, char ***argv) {
#warning "init(): FIXME: check for a running instance, delegate command line tasks to it"
	const gchar	**filenames;
	GnomeProgram	* gnome_program;
	GValue		* value;
	poptContext	  popt_context;

	g_set_prgname(PACKAGE);
	g_type_init();
	cdebug("criawips_init()", "start");

	/* initialize i18n */
#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif

	/* initializing libraries */
	  /* GTK+ */
	gtk_init (argc, argv);

	gtk_window_set_default_icon_from_file(PACKAGE_DATA_DIR "/pixmaps/criawips.png", NULL);
	gtk_about_dialog_set_url_hook(cmw_about_uri_cb, NULL, NULL);

	  /* libgnome(ui) */
	gnome_program = gnome_program_init(PACKAGE, PACKAGE_VERSION,
					   LIBGNOMEUI_MODULE,
					   *argc, *argv,
					   GNOME_PARAM_POPT_TABLE, options,
					   GNOME_PARAM_POPT_FLAGS, 0,
					   GNOME_PARAM_APP_DATADIR, PACKAGE_DATA_DIR,
					   GNOME_PARAM_HUMAN_READABLE_NAME, _("Criawips"),
					   NULL);

	  /* libglade */
	glade_init();
	glade_set_custom_handler(criawips_libglade_custom_handler, NULL);

	cria_lepton_init();

	value = g_new0(GValue, 1);
	g_value_init(value,
		     G_TYPE_POINTER);
	g_object_get_property(G_OBJECT(gnome_program),
			      GNOME_PARAM_POPT_CONTEXT,
			      value);

	popt_context = (poptContext) g_value_get_pointer(value);
	filenames = poptGetArgs(popt_context);
	g_free(value);

	cria_application_get_instance();

	if(start_off) {
		cdebug("criawips_init()", "opening presentation mode");

		if(filenames && filenames[0]) {
			if(filenames[1]) {
				GtkWidget* dialog = gtk_message_dialog_new_with_markup(NULL,
										      0,
										      GTK_MESSAGE_WARNING,
										      GTK_BUTTONS_CLOSE,
										      _("Criawips can only show one presentation"));
				gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(dialog),
									 _("You tried to show more than one presentation. Criawips cannot handle this yet."));
				gtk_widget_show(dialog);
				gtk_dialog_run(GTK_DIALOG(dialog));
				gtk_widget_hide_now(dialog);
				gtk_object_destroy(GTK_OBJECT(dialog));
			}

			cria_open_presentation_from_filename (filenames[0]);
		} else {
			GtkWidget	* dialog;
			GtkFileFilter	* filter;
			gchar		* filename;

#warning "init(): FIXME: extract the filechooser code into one code fragment"
			dialog = gtk_file_chooser_dialog_new(_("Select a Presentation"),
							     NULL,
							     GTK_FILE_CHOOSER_ACTION_OPEN,
							     GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
							     GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
							     NULL);
			filter = gtk_file_filter_new();
			gtk_file_filter_set_name(filter, _("Criawips Presentations"));
			gtk_file_filter_add_pattern(filter, "*.criawips");
			gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog),
						    filter);
			switch(gtk_dialog_run(GTK_DIALOG(dialog))) {
			case GTK_RESPONSE_ACCEPT:
				gtk_widget_hide_now(dialog);
				filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
				cria_open_presentation_from_filename(filename);
				g_free(filename);
				gtk_object_destroy(GTK_OBJECT(dialog));
				break;
			case GTK_RESPONSE_CANCEL:
				criawips_quit();
				break;
			default:
				g_warning("Should not be reached");
				break;
			};
		}
	} else {
		cdebug("criawips_init()", "opening main window(s)");

		if(!filenames) {
			/* open the criawips main window here */
			GtkWidget* mainwin = cria_main_window_new_default();
			cria_main_window_set_geometry(CRIA_MAIN_WINDOW(mainwin));
		} else {
			/* open each file in a new window */
			const gchar	**iterator;
			gchar		 *uri;
			cdebug("criawips_init()", "filenames is a char** at 0x%x", (uintptr_t)filenames);
			for(iterator = filenames; iterator && *iterator; iterator++) {
				cdebug("criawips_init()", "iterator is now 0x%x >> 0x%x (%s)", (uintptr_t)iterator, (uintptr_t)(*iterator), *iterator);
				GtkWidget* mainwin = cria_main_window_new(*iterator);
				cria_main_window_set_geometry(CRIA_MAIN_WINDOW(mainwin));

				uri = gnome_vfs_make_uri_from_shell_arg(*iterator);
				cria_main_window_add_to_recent(CRIA_MAIN_WINDOW(mainwin), uri, cria_main_window_get_presentation(CRIA_MAIN_WINDOW(mainwin)));
				g_free(uri);
			}
		}
	}

	cdebug("criawips_init()", "end");
}

/**
 * criawips_shutdown:
 *
 * Shutdown all initialized systems.
 */
void
criawips_shutdown(void) {
	cria_lepton_shutdown_debug();
	cdebug_shutdown();
}

/**
 * criawips_quit:
 *
 * Quit Criawips, close all application windows and then exit the application.
 */
void
criawips_quit(void) {
#warning "quit(): go through the windows an destroy one after the other"
#warning "criawips_quit(): FIXME: Ask open windows to save their content"

#warning "criawips_quit(): FIXME: should be freed in the application destructor"
        g_object_unref(G_OBJECT(self->priv->gconf_client));

	/* get the application object and quit all window instances */
	if(gtk_main_level() > 0) {
		gtk_main_quit ();
	} else {
		exit(0);
	}
}

/*****************************************************************************
 * file selection handling stuff                                             *
 *****************************************************************************/

static void
cria_open_presentation_from_filename(const gchar*filename) {
	CriaPresentation	* pres;
	GtkWidget		* show;
	GError			* error = NULL;

	g_return_if_fail(filename);
	cdebug("cria_open_presentation_from_filename()", "Opening file '%s'", filename);

	pres = cria_presentation_new_from_file(filename, &error);

	if(error) {
		char* error_message = g_strdup_printf("%s %s:\n<span style=\"italic\">%s</span>.",
						  _("For some reason the presentation you wanted to open could not be opened."),
						  _("The reason was:"),
						  error->message);
		cria_application_show_error_dialog(NULL,
						   _("The Presentation could not be opened"),
						   error_message);
		g_free(error_message);
		g_error_free(error);
		gtk_main_quit();
	} else {
		show = cria_slide_show_new(pres);
		g_signal_connect(G_OBJECT(show), "destroy",
				 G_CALLBACK(criawips_quit), NULL);
	}
}

/*---------------------------------------------------------------------------*/

static void
cria_application_class_init (CriaApplicationClass* cria_application_class) {
	GObjectClass	* g_object_class;

	g_object_class = G_OBJECT_CLASS(cria_application_class);

	/* setting up signal system */
	/* We do not have a default handler. */
	cria_application_class->changed_default_font = NULL;

	cria_application_signals[CHANGED_DEFAULT_FONT_SIGNAL] = g_signal_new("default-font-changed",
									     CRIA_TYPE_APPLICATION,
									     G_SIGNAL_RUN_LAST,
									     G_STRUCT_OFFSET(CriaApplicationClass,
											     changed_default_font),
									     NULL,
									     NULL,
									     g_cclosure_marshal_VOID__VOID,
									     G_TYPE_NONE,
									     0);

	/* setting up property system */
	g_object_class->set_property = cria_application_set_property;
	g_object_class->get_property = cria_application_get_property;

	g_object_class_install_property(g_object_class,
					PROP_DEFAULT_FONT,
					g_param_spec_boxed("default_font",
							   "Default_font",
							   "The default font for the slides",
							   PANGO_TYPE_FONT_DESCRIPTION, 
							   G_PARAM_READWRITE));
	g_object_class_install_property(g_object_class,
					PROP_DETACH_MENUBARS,
					g_param_spec_boolean("detach-menubars",
							     "Detach Menubars",
							     "Specify whether the menubars are detachable",
							     FALSE,
							     G_PARAM_READABLE));
	g_object_class_install_property(g_object_class,
					PROP_DETACH_TOOLBARS,
					g_param_spec_boolean("detach-toolbars",
							     "Detach Toolbars",
							     "Specify whether the toolbars are detachable",
							     FALSE,
							     G_PARAM_READABLE));
	g_object_class_install_property(g_object_class,
					PROP_DISABLE_SCREENSAVER,
					g_param_spec_boolean("disable-screensaver",
							     "Disable Screensaver",
							     "Specify whether the screensaver should be turned off for presentations",
							     FALSE,
							     G_PARAM_READWRITE));
	g_object_class_install_property(g_object_class,
					PROP_PGDOWN_NEW_SLIDE,
					g_param_spec_boolean("pagedown-new-slides",
							     "Page Down can create new slides",
							     "Specify whether pressing page down on the last slide creates a new one",
							     TRUE,
							     G_PARAM_READWRITE));
	g_object_class_install_property(g_object_class,
					PROP_GCONF_CLIENT,
					g_param_spec_object("gconf-client",
							    "GConf client",
							    "The Gconfclient for the application",
							    GCONF_TYPE_CLIENT,
							    G_PARAM_READABLE));
	g_object_class_install_property(g_object_class,
					PROP_SHOW_TEAROFF,
					g_param_spec_boolean("show-tearoff",
							     "Show Tearoff",
							     "Show Tearoff Menu Items",
							     FALSE,
							     G_PARAM_READABLE));
}

/**
 * cria_application_register_window:
 * @window: a main window to be registeres by the application
 *
 * Registers a #CriaMainWindow to the application. So it's being propted to
 * close the file before quitting.
 */
void
cria_application_register_window(CriaMainWindow	* window) {
	CriaApplication	* self;

	cdebugo(cria_application_get_instance(), "registerWindow()", "start");

	g_return_if_fail(CRIA_IS_MAIN_WINDOW(window));

	self = cria_application_get_instance();

	self->priv->windows = g_list_prepend(self->priv->windows, g_object_ref(window));

	/*g_object_notify(self, "windows");*/

	cdebugo(cria_application_get_instance(), "registerWindow()", "end");
}

/**
 * cria_application_unregister_window:
 * @window: a main window to be unregistered by the application
 * 
 * Unregisters a #CriaMainWindow from the application.
 */
void
cria_application_unregister_window(CriaMainWindow *window) {
	CriaApplication	* self;

	cdebugo(cria_application_get_instance(), "cria_application_unregister_window()", "start" );

	g_return_if_fail(CRIA_IS_MAIN_WINDOW(window));

	self = cria_application_get_instance();
	/* Remove the window from the windows list */
	if(g_list_find(self->priv->windows, window)) {
		self->priv->windows = g_list_remove(self->priv->windows, window);
		g_object_unref(window);
	} else {
		cdebugo(cria_application_get_instance(), "unregisterWindow()", "Trying to unregister an unregistered window.");
	}
		
	/* If the list is empty, quit the application */
	if(self->priv->windows == NULL) {
		criawips_quit();
	}

	cdebugo(cria_application_get_instance(), "cria_application_unregister_window()", "end" );
}

/**
 * cria_application_get_instance:
 *
 * As #CriaApplication is a singleton, this method delivers the single
 * instance.
 *
 * Return value: the instance of the application
 */
CriaApplication*
cria_application_get_instance (void) {
	if (!self) {
		self = g_object_new(CRIA_TYPE_APPLICATION, NULL);
	}

	return self;
}

gchar*
cria_application_get_default_geometry(void) {
	return gconf_client_get_string(self->priv->gconf_client, "/apps/criawips/ui/default_geometry", NULL);
}

gboolean
cria_application_get_detach_menubars(void) {
	return gconf_client_get_bool(self->priv->gconf_client, "/desktop/gnome/interface/menubar_detachable", NULL);
}

gboolean
cria_application_get_detach_toolbars(void) {
	return gconf_client_get_bool(self->priv->gconf_client, "/desktop/gnome/interface/toolbar_detachable", NULL);
}

gboolean
cria_application_get_disable_screensaver(void) {
	g_return_val_if_fail(CRIA_IS_APPLICATION(self), FALSE);
	g_return_val_if_fail(self->priv, FALSE);

	return self->priv->disable_screensaver;
}

gboolean
cria_application_get_page_down_creates_slide(void) {
	g_return_val_if_fail(CRIA_IS_APPLICATION(self), FALSE);
	g_return_val_if_fail(self->priv, FALSE);

	return self->priv->page_down_creates_slide;
}

gboolean
cria_application_get_show_sidebar(void) {
	return gconf_client_get_bool(self->priv->gconf_client, "/apps/criawips/ui/sidebar_visible", NULL);
}

gboolean
cria_application_get_show_tearoff(void) {
	return gconf_client_get_bool(self->priv->gconf_client, "/desktop/gnome/interface/menus_have_tearoff", NULL);
}

gint
cria_application_get_sidebar_width(void) {
	return gconf_client_get_int(self->priv->gconf_client, "/apps/criawips/ui/sidebar_width", NULL);
}

gboolean
cria_application_get_templates_visible(void) {
	return gconf_client_get_bool(self->priv->gconf_client, "/apps/criawips/ui/templates_visible", NULL);
}

gint
cria_application_get_templates_width(void) {
	return gconf_client_get_int(self->priv->gconf_client, "/apps/criawips/ui/templates_width", NULL);
}

const gchar*
cria_application_get_zoom(void) {
	return gconf_client_get_string(self->priv->gconf_client, "/apps/criawips/ui/default_zoom", NULL);
}

static void
cria_application_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaApplication	* self;

	self = CRIA_APPLICATION (object);

	switch(prop_id) {
	case PROP_DEFAULT_FONT:
		g_value_set_object(value, self->priv->default_font);
		break;
	case PROP_DISABLE_SCREENSAVER:
		g_value_set_boolean(value, cria_application_get_disable_screensaver());
		break;
	case PROP_PGDOWN_NEW_SLIDE:
		g_value_set_boolean(value, cria_application_get_page_down_creates_slide());
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

void
cria_application_set_disable_screensaver(gboolean disable_screensaver) {
	g_return_if_fail(CRIA_IS_APPLICATION(self));
	g_return_if_fail(self->priv);

	if(self->priv->disable_screensaver == disable_screensaver) {
		return;
	}

	self->priv->disable_screensaver = disable_screensaver;
	gconf_client_set_bool(self->priv->gconf_client,
			      PACKAGE_GCONF_PATH "/page_down_creates_slides",
			      disable_screensaver,
			      NULL);

	g_object_notify(G_OBJECT(self), "disable-screensaver");
}

static void
notify_disable_screensaver_changed(GConfClient* client, guint conxn_id, GConfEntry* entry, gpointer user_data) {
	GError  * error = NULL;
	gboolean  ds = gconf_client_get_bool(self->priv->gconf_client,
					     PACKAGE_GCONF_PATH "/disable_screensaver",
					     &error);

	if(error) {
		g_critical("%s", error->message);
		g_error_free(error);
	}
	
	cria_application_set_disable_screensaver(ds);
}

void
cria_application_set_page_down_creates_slide(gboolean pdcs) {
	g_return_if_fail(CRIA_IS_APPLICATION(self));
	g_return_if_fail(self->priv);

	if(self->priv->page_down_creates_slide == pdcs) {
		return;
	}

	self->priv->page_down_creates_slide = pdcs;
	gconf_client_set_bool(self->priv->gconf_client,
			      PACKAGE_GCONF_PATH "/page_down_creates_slides",
			      pdcs,
			      NULL);
	
	g_object_notify(G_OBJECT(self), "pagedown-new-slides");
}

static void
notify_page_down_creates_slides(GConfClient* client, guint conxn_id, GConfEntry* entry, gpointer user_data) {
	GError  * error = NULL;
	gboolean  val = gconf_client_get_bool(self->priv->gconf_client,
					      PACKAGE_GCONF_PATH "/page_down_creates_slides",
					      &error);

	if(error) {
		g_critical("%s", error->message);
		g_error_free(error);
	}

	cria_application_set_page_down_creates_slide(val);
}

static void
notify_show_tearoff(GConfClient* client, guint conxn_id, GConfEntry* entry, gpointer data) {
	g_object_notify(G_OBJECT(self), "show-tearoff");
}

static void
notify_detach_menubars(GConfClient* client, guint conxn_id, GConfEntry* entry, gpointer data) {
	g_object_notify(G_OBJECT(self), "detach-menubars");
}

static void
notify_detach_toolbars(GConfClient* client, guint conxn_id, GConfEntry* entry, gpointer data) {
	g_object_notify(G_OBJECT(self), "detach-toolbars");
}

static void
cria_application_init(CriaApplication* app) {
	GError * error = NULL;

	if (!self) {
		self = app;
	} else {
		g_critical("%s", "CriaApplication can only be instantiated once");
	}
	
	self->priv = g_new0(CriaApplicationPriv,1);
	self->priv->windows = NULL;

	/* Initialise the preferences */
	/* Initialise GConf */
	self->priv->gconf_client = gconf_client_get_default();

	gconf_client_add_dir(self->priv->gconf_client,
			     PACKAGE_GCONF_PATH,
			     GCONF_CLIENT_PRELOAD_NONE,
			     &error);
	if(error) {
		gchar	* message = g_strdup_printf(_("Couldn't open preferences directory: %s"), error->message);
		cria_application_show_error_dialog(NULL, _("The preferences system could not be initialized"), message);
		g_free(message);
		g_error_free(error);
		error = NULL;
	}

	/* Get the configuration */
	notify_default_font_changed(NULL, 0, NULL, NULL);
	notify_disable_screensaver_changed(NULL, 0, NULL, NULL);
	notify_page_down_creates_slides(NULL, 0, NULL, NULL);
	notify_show_tearoff(NULL, 0, NULL, NULL);
	notify_detach_menubars(NULL, 0, NULL, NULL);
	notify_detach_toolbars(NULL, 0, NULL, NULL);

	/* Set up notifications */
	gconf_client_notify_add(self->priv->gconf_client,
			        PACKAGE_GCONF_PATH "/default_fallback_font",
				notify_default_font_changed,
				NULL,
				NULL,
				&error);
	if(error) { 
		cdebugo(self, "init()", "Couldn't add gconf notification: '%s'", error->message);
		g_error_free(error);
		error = NULL;
	}
	
	gconf_client_notify_add(self->priv->gconf_client,
			        PACKAGE_GCONF_PATH "/disable_screensaver",
				notify_disable_screensaver_changed,
				NULL,
				NULL,
				&error);
	if(error) { 
		cdebugo(self, "init()", "Couldn't add gconf notification: '%s'", error->message);
		g_error_free(error);
		error = NULL;
	}
	
	gconf_client_notify_add(self->priv->gconf_client,
				PACKAGE_GCONF_PATH "/page_down_creates_slides",
				notify_page_down_creates_slides,
				NULL,
				NULL,
				&error);
	if(error) { 
		cdebugo(self, "init()", "Couldn't add gconf notification: '%s'", error->message);
		g_error_free(error);
		error = NULL;
	}

	gconf_client_notify_add(self->priv->gconf_client,
				"/desktop/gnome/interface/menus_have_tearoff",
				notify_show_tearoff,
				NULL,
				NULL,
				NULL);
	gconf_client_notify_add(self->priv->gconf_client,
				"/desktop/gnome/interface/menubar_detachable",
				notify_detach_menubars,
				NULL,
				NULL,
				NULL);
	gconf_client_notify_add(self->priv->gconf_client,
				"/desktop/gnome/interface/toolbar_detachable",
				notify_detach_toolbars,
				NULL,
				NULL,
				NULL);
}

void
cria_application_set_default_geometry(const gchar* geometry) {
	g_return_if_fail(geometry && strlen(geometry));

	gconf_client_set_string(self->priv->gconf_client, "/apps/criawips/ui/default_geometry", geometry, NULL);
}

void
cria_application_set_show_sidebar(gboolean ssb) {
	gconf_client_set_bool(self->priv->gconf_client, "/apps/criawips/ui/sidebar_visible", ssb, NULL);
}

void
cria_application_set_sidebar_width(gint sidebar_width) {
	gconf_client_set_int(self->priv->gconf_client, "/apps/criawips/ui/sidebar_width", sidebar_width, NULL);
}

void
cria_application_set_templates_visible(gboolean templates_visible) {
	gconf_client_set_bool(self->priv->gconf_client, "/apps/criawips/ui/templates_visible", templates_visible, NULL);
}

void
cria_application_set_templates_width(gint templates_width) {
	gconf_client_set_int(self->priv->gconf_client, "/apps/criawips/ui/templates_width", templates_width, NULL);
}

void
cria_application_set_zoom(const gchar* zoom) {
	gconf_client_set_string(self->priv->gconf_client, "/apps/criawips/ui/default_zoom", zoom, NULL);
}

static void
cria_application_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaApplication	* self;
	
	self = CRIA_APPLICATION(object);
	
	switch(prop_id) {
	case PROP_DEFAULT_FONT:
		cria_application_set_default_font_name(g_value_get_string(value));
		break;
	case PROP_DISABLE_SCREENSAVER:
		cria_application_set_disable_screensaver(g_value_get_boolean(value));
		break;
	case PROP_PGDOWN_NEW_SLIDE:
		cria_application_set_page_down_creates_slide(g_value_get_boolean(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

/**
 * cria_application_show_error_dialog:
 * @parent: a #GtkWindow or NULL
 * @primary: the primary dialog message, the short description
 * @secondary: the secondary dialog message, a detailed description; may be NULL
 *
 * Shows an error dialog. To be used when fatal things happen (e.g. loading the
 * ui definitions from the glade file failed). @parent is used to specify a
 * window that this dialog is transient to.
 */
void
cria_application_show_error_dialog(GtkWindow* parent, gchar const* primary, gchar const* secondary) {
	GtkWidget	* dialog;

	dialog = gtk_message_dialog_new(parent,
					GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_ERROR,
					GTK_BUTTONS_CLOSE,
					"%s", primary);
	if(secondary) {
		gtk_message_dialog_format_secondary_markup(GTK_MESSAGE_DIALOG(dialog), "%s", secondary);
	}
	
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_hide_now(dialog);
	gtk_object_destroy(GTK_OBJECT(dialog));
}

GType
cria_application_get_type (void) {
	static GType	type = 0;

	if (!type) {
		const GTypeInfo info = {
			sizeof (CriaApplicationClass),
			NULL,	/* base initializer */
			NULL,	/* base finalizer */
			(GClassInitFunc)cria_application_class_init,
			NULL,	/* class finalizer */
			NULL,	/* class data */
			sizeof (CriaApplication),
			0,
			(GInstanceInitFunc)cria_application_init,
			0
		};

		type = g_type_register_static (
				G_TYPE_OBJECT,
				"CriaApplication",
				&info,
				0);
	}

	return type;
}

PangoFontDescription *
cria_application_get_default_font(void) {
	return self->priv->default_font;
}

void
cria_application_set_default_font_name(const gchar* font) {
	g_return_if_fail(font && strlen(font));

	gconf_client_set_string(self->priv->gconf_client,
				PACKAGE_GCONF_PATH "/default_fallback_font",
				font,
				NULL);
	/* notify event will get triggered automatically */
}

static void
notify_default_font_changed(GConfClient *client,
			    guint cnxn_id,
			    GConfEntry *entry,
			    gpointer user_data)
{
	gchar  *font = NULL;
	GError *error = NULL;

	PangoFontDescription *old_default = NULL;
	
	cdebugo(cria_application_get_instance(), "notifyDefaultFontChanged()", "start");

	old_default = self->priv->default_font;

	font = gconf_client_get_string(self->priv->gconf_client,
				       PACKAGE_GCONF_PATH "/default_fallback_font",
				       &error);
	if(error) {  
		cdebugo(cria_application_get_instance(), "notifyDefaultFontChanged()", "Couldn't load default fallback font: '%s'", error->message);
		g_error_free(error);
		error = NULL;
		self->priv->default_font = pango_font_description_from_string("Sans 12");
	} else if(!font) {
		cdebugo(cria_application_get_instance(), "notifyDefaultFontChanged()", "Fallback font was null.");
		self->priv->default_font = pango_font_description_from_string("Sans 12");
	} else {
		self->priv->default_font = pango_font_description_from_string(font);
		if(old_default) {
			pango_font_description_free(old_default);
			old_default = NULL;
		}
		g_signal_emit(self, cria_application_signals[CHANGED_DEFAULT_FONT_SIGNAL], 0, NULL);
	}
	cdebugo(cria_application_get_instance(), "notifyDefaultFontChanged()", "end");
}

