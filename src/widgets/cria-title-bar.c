/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <widgets/cria-title-bar.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#define CDEBUG_TYPE cria_title_bar_get_type
#include <cdebug/cdebug.h>

#include <dom/cria-slide-view.h>

enum {
	PROP_0,
	PROP_SLIDE
};

enum {
	SIGNAL,
	N_SIGNALS
};

struct _CriaTitleBarPrivate {
	GtkWidget	* hbox,
			* button,
			* label,
			* combo;
	CriaSlide	* slide;
};

static void ctb_slide_view_init(CriaSlideViewIface* iface);

G_DEFINE_TYPE_WITH_CODE(CriaTitleBar, cria_title_bar, CRIA_TYPE_TEXT_PANE,
			G_IMPLEMENT_INTERFACE(CRIA_TYPE_SLIDE_VIEW, ctb_slide_view_init));

#if 0
/* enable these to add support for signals */
static	guint	cria_title_bar_signals[N_SIGNALS] = { 0 };

static	void	cria_title_bar_signal	       (CriaTitleBar	* self,
						const	gchar	* string);
#endif

static void
ctb_finalize(GObject* object) {
	CriaTitleBar* self = CRIA_TITLE_BAR(object);
	if(self->priv->slide) {
		cria_slide_view_unregister(CRIA_SLIDE_VIEW(self), self->priv->slide);
		cdebug("finalize()", "slide has %d references, removing one", G_OBJECT(self->priv->slide)->ref_count);
		g_object_unref(self->priv->slide);
		self->priv->slide = NULL;
	}
	
	G_OBJECT_CLASS(cria_title_bar_parent_class)->finalize(object);
}

static void
ctb_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaTitleBar	* self = CRIA_TITLE_BAR(object);

	switch(prop_id) {
	case PROP_SLIDE:
		g_value_set_object(value, cria_title_bar_get_slide(self));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

static void
ctb_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaTitleBar* self = CRIA_TITLE_BAR (object);
	
	switch(prop_id) {
	case PROP_SLIDE:
		cria_title_bar_set_slide(self, g_value_get_object(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

static void
cria_title_bar_class_init(CriaTitleBarClass* self_class) {
	GObjectClass* go_class;

	/* GObjectClass */
	go_class = G_OBJECT_CLASS(self_class);
	go_class->finalize     = ctb_finalize;
	go_class->set_property = ctb_set_property;
	go_class->get_property = ctb_get_property;

	g_object_class_install_property(go_class,
					PROP_SLIDE,
					g_param_spec_object("slide",
							    "Slide",
							    "The slide...",
							    CRIA_TYPE_SLIDE,
							    G_PARAM_READWRITE));
}

CriaSlide*
cria_title_bar_get_slide(CriaTitleBar* self) {
	g_return_val_if_fail(CRIA_IS_TITLE_BAR(self), NULL);
	g_return_val_if_fail(self->priv, NULL);
	g_return_val_if_fail(!self->priv->slide || CRIA_IS_SLIDE(self->priv->slide), NULL);
	
	return self->priv->slide;
}

static void
cria_title_bar_init(CriaTitleBar* self) {
	g_return_if_fail(CRIA_IS_TITLE_BAR(self));

	/* initialize the private data */
	self->priv = g_new0(CriaTitleBarPrivate,1);

	/* initialize the content of this widget * /
	 * add an hbox and pack these three in there:
	 *  1. a button to select between (SlideEditor;OutlineView;ThumbnailView;...)
	 *  2. a label to display a title [done]
	 *  3. a ComboBoxEntry to select the zoom level
	 */

/*	self->priv->button;
		set relief to none
		add an hbox as content (FALSE, 6)
			add a label ("Slide View"); FALSE, FALSE, 0
			add an arrow (down); FALSE, FALSE, 0
	connect signal
	pack start; FALSE, FALSE, 0 */

/*	self->priv->combo;
		request width (90)
	pack start; FALSE, FALSE, 0 */
}

GtkWidget*
cria_title_bar_new(CriaSlide* slide) {
	GtkWidget* retval;
	
	g_return_val_if_fail(slide == NULL || CRIA_IS_SLIDE(slide), NULL);

	retval = GTK_WIDGET(g_object_new(CRIA_TYPE_TITLE_BAR, NULL));

	if(slide) {
		cria_title_bar_set_slide(CRIA_TITLE_BAR(retval), slide);
	}

	return retval;
}

void
cria_title_bar_set_slide(CriaTitleBar* self, CriaSlide* slide) {
	g_return_if_fail(CRIA_IS_TITLE_BAR(self));
	g_return_if_fail(CRIA_IS_SLIDE(slide));

	if(self->priv->slide != NULL) {
		cria_slide_view_unregister(CRIA_SLIDE_VIEW(self), self->priv->slide);
		g_object_unref(self->priv->slide);
		self->priv->slide = NULL;
	}

	self->priv->slide = g_object_ref(slide);
	cria_slide_view_register(CRIA_SLIDE_VIEW(self), self->priv->slide);
	g_object_notify(G_OBJECT(self), "slide");
}

/* CriaSlideView interface */
static void
ctb_notify_title(CriaSlideView* view, CriaSlide* slide, gchar const* title) {
	CriaTitleBar* self = CRIA_TITLE_BAR(view);
	gchar	* message;

#warning "TitleBar::updateTitle(): FIXME: add some 'slide give me your presentation' stuff and then fix the display in here"
	message = g_strdup_printf(/* "<span weight=\"bold\">%s %d/%d \"%s\"</span>", */
			
				  /* translators: this is "Slide \"slide-title\"" */
				  _("Slide \"%s\""),
				  /* cria_presentation_get_slide_index(self->priv->presentation, cria_slide_editor_get_slide(slide_editor)) + 1, */
				  /* cria_presentation_n_slides(self->priv->presentation), */
				  title);
	cria_text_pane_set_text(CRIA_TEXT_PANE(self), message);
	
	g_free(message);
}

static void
ctb_slide_view_init(CriaSlideViewIface* iface) {
	iface->notify_title = ctb_notify_title;
}

