/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <widgets/cria-template-model-priv.h>

#include <gtk/gtktreemodel.h>

#define CDEBUG_TYPE cria_template_model_get_type
#include <cdebug/cdebug.h>

#include <dom/cria-theme-view.h>
#include <rendering/cria-slide-display.h>

enum {
	PROP_0,
	PROP_THEME
};

/* -- initialize the GtkTreeModel interface -- */

static gboolean
ctm_iter_is_valid(CriaTemplateModel* self, GtkTreeIter* iter) {
	return CRIA_IS_TEMPLATE_MODEL(self) && (iter) &&
	       (self->stamp == iter->stamp) &&
	       (CRIA_IS_SLIDE(iter->user_data));
#warning "FIXME: check whether the slide is part of the theme" 
} 

static GType
ctm_get_column_type(GtkTreeModel* model, gint column) {
	g_return_val_if_fail(0 <= column && column <= N_COLUMNS, G_TYPE_INVALID);

	switch(column) {
	case COLUMN_PREVIEW:
		return GDK_TYPE_PIXBUF;
	case COLUMN_TEXT:
		return G_TYPE_STRING;
	default:
		g_return_val_if_fail(FALSE, G_TYPE_INVALID);
		return G_TYPE_INVALID;
	}
}

static GtkTreeModelFlags
ctm_get_flags(GtkTreeModel* model) {
	return GTK_TREE_MODEL_LIST_ONLY | GTK_TREE_MODEL_ITERS_PERSIST;
}

static gboolean
ctm_get_iter(GtkTreeModel* model, GtkTreeIter* iter, GtkTreePath* path) {
	CriaTemplateModel* self = CRIA_TEMPLATE_MODEL(model);
	guint              slide_index;
	
	g_return_val_if_fail(path, FALSE);
	g_return_val_if_fail(gtk_tree_path_get_depth(path) == 1, FALSE);

	slide_index = gtk_tree_path_get_indices(path)[0];
	cdebug("getIter()", "index: %d", slide_index);

	if(!self->theme) {
		return FALSE;
	}

	if((guint)0 <= slide_index && slide_index < cria_theme_get_n_slides(self->theme)) {
		GList* slides = cria_theme_get_master_slides(self->theme);
		
		iter->stamp = self->stamp;
		iter->user_data = CRIA_SLIDE(g_list_nth(slides, slide_index)->data);

		g_list_free(slides);
		
		return TRUE;
	}

	return FALSE;
}

static GtkTreePath*
ctm_get_path(GtkTreeModel* model, GtkTreeIter* iter) {
	GtkTreePath      * retval;
	CriaTemplateModel* self = CRIA_TEMPLATE_MODEL(model);
	GList            * slides;
	
	g_return_val_if_fail(ctm_iter_is_valid(self, iter), NULL);
	
	slides = cria_theme_get_master_slides(self->theme);
	
	retval = gtk_tree_path_new();
	gtk_tree_path_append_index(retval, g_list_index(slides, iter->user_data));
	
	g_list_free(slides);
	return retval;
} 

static void
ctm_get_value(GtkTreeModel* model, GtkTreeIter* iter, gint column, GValue* value) {
	CriaTemplateModel* self = CRIA_TEMPLATE_MODEL(model);
	
	g_return_if_fail(ctm_iter_is_valid(self, iter));
	
	cdebug("getValue()", "column index is %d", column);
	g_value_init(value, ctm_get_column_type(model, column));

	switch(column) {
	case COLUMN_PREVIEW:
		g_value_set_object(value, cria_slide_display_get_preview(CRIA_SLIDE(iter->user_data)));
		break;
	case COLUMN_TEXT:
		g_value_set_string(value, cria_slide_get_title(CRIA_SLIDE(iter->user_data)));
		break;
	default:
		g_warning("Column index %d is invalid for %s", column, G_OBJECT_TYPE_NAME(model));
		break;
	}
}

static gboolean
ctm_iter_next(GtkTreeModel* model, GtkTreeIter* iter) {
	CriaTemplateModel* self = CRIA_TEMPLATE_MODEL(model);
	GList            * slides, * found;
	CriaSlide        * slide;
	gboolean           retval = FALSE;
	
	g_return_val_if_fail(ctm_iter_is_valid(self, iter), FALSE);

	slide = CRIA_SLIDE(iter->user_data);
	slides = cria_theme_get_master_slides(self->theme);
	
	found = g_list_find(slides, slide);

	if(found) {
		found = found->next;

		if(found) {
			iter->user_data = found->data;
			retval = TRUE;
		}
	}
	
	g_list_free(slides);
	
	return retval;
}

static void
ctm_ref_node(GtkTreeModel* model, GtkTreeIter* iter) {
	g_return_if_fail(ctm_iter_is_valid(CRIA_TEMPLATE_MODEL(model), iter)); 
	
	g_object_ref(iter->user_data);
} 

static void
ctm_unref_node(GtkTreeModel* model, GtkTreeIter* iter) {
	g_return_if_fail(ctm_iter_is_valid(CRIA_TEMPLATE_MODEL(model), iter)); 
	
	g_object_unref(iter->user_data);
}

static int
ctm_get_n_columns(GtkTreeModel* model) {
	return N_COLUMNS;
}

static void
ctm_implement_tree_model(GtkTreeModelIface* iclass) {
	iclass->get_column_type = ctm_get_column_type;
	iclass->get_flags       = ctm_get_flags;
	iclass->get_iter        = ctm_get_iter;
	iclass->get_n_columns   = ctm_get_n_columns;
	iclass->get_path        = ctm_get_path;
	iclass->get_value       = ctm_get_value;
	iclass->iter_next       = ctm_iter_next;
	iclass->ref_node        = ctm_ref_node;
	iclass->unref_node      = ctm_unref_node;
}

/* -- done initializing the GtkTreeModel interface -- */

G_DEFINE_TYPE_WITH_CODE(CriaTemplateModel, cria_template_model, G_TYPE_OBJECT,
			G_IMPLEMENT_INTERFACE(GTK_TYPE_TREE_MODEL, ctm_implement_tree_model);
			G_IMPLEMENT_INTERFACE(CRIA_TYPE_THEME_VIEW, NULL))

CriaTemplateModel*
cria_template_model_for_theme(CriaTheme* theme) {
	g_return_val_if_fail(theme, NULL);
	return g_object_new(CRIA_TYPE_TEMPLATE_MODEL, "theme", theme, NULL);
}

static void
ctm_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaTemplateModel* self = CRIA_TEMPLATE_MODEL(object);
	
	switch(prop_id) {
	case PROP_THEME:
		g_value_set_object(value, self->theme);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

static void
ctm_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaTemplateModel* self = CRIA_TEMPLATE_MODEL(object);
	
	switch(prop_id) {
	case PROP_THEME:
		g_return_if_fail(CRIA_IS_THEME(g_value_get_object(value)));
		if(self->theme != CRIA_THEME(g_value_get_object(value))) {
			if(self->theme) {
				cria_theme_view_unregister(CRIA_THEME_VIEW(self), self->theme);
				g_object_unref(self->theme);
				self->theme = NULL;
			}

			self->theme = g_object_ref(g_value_get_object(value));
			cria_theme_view_register(CRIA_THEME_VIEW(self), self->theme);
			g_object_notify(object, "theme");
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

static void
ctm_dispose(GObject* object) {
	CriaTemplateModel* self = CRIA_TEMPLATE_MODEL(object);

	if(self->disposed) {
		return;
	}

	self->disposed = TRUE;
	
	if(self->theme) {
		cria_theme_view_unregister(CRIA_THEME_VIEW(self), self->theme);
		cdebug("finalize()", "removing one of %d references on the theme", G_OBJECT(self->theme)->ref_count);
		g_object_unref(self->theme);
		self->theme = NULL;
	}
	
	G_OBJECT_CLASS(cria_template_model_parent_class)->dispose(object);
}

static void
cria_template_model_class_init(CriaTemplateModelClass* self_class) {
	GObjectClass* go_class;
	
	/* setting up gobject class */
	go_class = G_OBJECT_CLASS(self_class);
	go_class->get_property = ctm_get_property;
	go_class->set_property = ctm_set_property;
	go_class->dispose      = ctm_dispose;
	g_object_class_install_property(go_class,
					PROP_THEME,
					g_param_spec_object("theme",
							    "theme",
							    "Theme",
							    CRIA_TYPE_THEME,
							    G_PARAM_READWRITE));
}

static void
cria_template_model_init(CriaTemplateModel* self) {
	self->stamp = g_random_int();
}

