/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <gtk/gtksizegroup.h>
#include <glade/glade.h>

#include <dom/cria-presentation.h>
#include <widgets/cria-theme-actions.h>

G_BEGIN_DECLS

typedef struct _CriaMainWindow CriaMainWindow;
typedef struct _CriaMainWindowClass CriaMainWindowClass;
typedef struct _CriaMainWindowPriv CriaMainWindowPriv;

GtkWidget*		  cria_main_window_new		       (const gchar	* filename);
GtkWidget*		  cria_main_window_new_default	       (void);
GtkWidget*		  cria_main_window_new_uri	       (const gchar	* uri);
GType			  cria_main_window_get_type	       (void);
void			  cria_main_window_add_to_recent       (CriaMainWindow  * self,
							        const gchar     * text_uri, 
								CriaPresentation* presentation);
CriaPresentation*	  cria_main_window_get_presentation    (CriaMainWindow	* self);
void			  cria_main_window_set_geometry	       (CriaMainWindow	* self);
void			  cria_main_window_set_presentation    (CriaMainWindow	* self,
								CriaPresentation* presentation);
void			  cria_main_window_show_about	       (CriaMainWindow	* self);

#define CRIA_TYPE_MAIN_WINDOW			(cria_main_window_get_type ())
#define CRIA_MAIN_WINDOW(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_MAIN_WINDOW, CriaMainWindow))
#define CRIA_MAIN_WINDOW_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_MAIN_WINDOW, CriaMainWindowClass))
#define CRIA_IS_MAIN_WINDOW(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_MAIN_WINDOW))
#define CRIA_IS_MAIN_WINDOW_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_MAIN_WINDOW))
#define CRIA_MAIN_WINDOW_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_MAIN_WINDOW, CriaMainWindowClass))

struct _CriaMainWindow {
	GtkWindow		  base_instance;
	GtkSizeGroup		* text_pane_group;
	CriaSlide               * slide;
	CriaThemeActions        * theme_actions;
	CriaMainWindowPriv	* priv;
};

struct _CriaMainWindowClass {
	GtkWindowClass		  base_class;
};

G_END_DECLS

#endif /* MAIN_WINDOW_H */

