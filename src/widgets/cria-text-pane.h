/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <glib-object.h>
#include <gtk/gtkframe.h>

#ifndef TEXT_PANE_H
#define TEXT_PANE_H

G_BEGIN_DECLS

typedef struct _CriaTextPane CriaTextPane;
typedef struct _CriaTextPaneClass CriaTextPaneClass;
typedef struct _CriaTextPanePrivate CriaTextPanePrivate;

GType		cria_text_pane_get_type	       (void);
GtkWidget*	cria_text_pane_new	       (const gchar	* text);
void		cria_text_pane_set_text        (CriaTextPane	* self,
						const gchar	* text);
void		cria_text_pane_pack_end	       (CriaTextPane	* self,
						GtkWidget	* widget);

#define CRIA_TYPE_TEXT_PANE			(cria_text_pane_get_type ())
#define CRIA_TEXT_PANE(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_TEXT_PANE, CriaTextPane))
#define CRIA_TEXT_PANE_CLASS(klass)		(G_TYPE_CHACK_CLASS_CAST((klass), CRIA_TYPE_TEXT_PANE, CriaTextPaneClass))
#define CRIA_IS_TEXT_PANE(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_TEXT_PANE))
#define CRIA_IS_TEXT_PANE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_TEXT_PANE))
#define CRIA_TEXT_PANE_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_TEXT_PANE, CriaTextPaneClass))

struct _CriaTextPane {
	GtkFrame	  base_instance;
	CriaTextPanePrivate* priv;
};

struct _CriaTextPaneClass {
	GtkFrameClass	  base_class;

	/* signals */
	/*void (*signal)	       (CriaTextPane	* self,
				const	gchar	* string);*/
};

G_END_DECLS

#endif /* !TEXT_PANE_H */

