/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <widgets/cria-slide-properties-dialog-priv.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include <glade/glade.h>

#include <dom/cria-slide-view.h>

enum {
	PROP_0,
	PROP_SLIDE
};

enum {
	SIGNAL,
	N_SIGNALS
};

static void cspd_init_slide_view(CriaSlideViewIface* iface);
//static void cspd_init_background_view(CriaBackgroundViewIface* iface);

G_DEFINE_TYPE_WITH_CODE(CriaSlidePropertiesDialog, cria_slide_properties_dialog, GTK_TYPE_DIALOG,
			G_IMPLEMENT_INTERFACE(CRIA_TYPE_SLIDE_VIEW, cspd_init_slide_view);
/*			G_IMPLEMENT_INTERFACE(CRIA_TYPE_BACKGROUND_VIEW, cspd_init_background_view)*/);

static void
cria_slide_properties_dialog_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaSlidePropertiesDialog* self = CRIA_SLIDE_PROPERTIES_DIALOG(object);

	switch(prop_id) {
	case PROP_SLIDE:
		g_value_set_object(value, cria_slide_properties_dialog_get_slide(self));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

/**
 * cria_slide_properties_dialog_get_slide:
 * @self: a #CriaSlidePropertiesDialog
 *
 * Get the slide displayed by @self.
 *
 * Returns the slide displayed by @self.
 */
CriaSlide*
cria_slide_properties_dialog_get_slide(CriaSlidePropertiesDialog* self) {
	g_return_val_if_fail(CRIA_IS_SLIDE_PROPERTIES_DIALOG(self), NULL);
	
	return self->slide;
}

static void
cspd_color_check_toggled_cb(CriaSlidePropertiesDialog* self, GtkToggleButton* toggle) {
	GtkWidget* color_chooser = glade_xml_get_widget(self->xml, "slide_properties_colorbutton");
	gboolean   active = gtk_toggle_button_get_active(toggle);

	gtk_widget_set_sensitive(color_chooser, active);

	if(!active && self->background) {
		cria_background_set_color(self->background, NULL);
	} else {
#warning "FIXME: set the color from the chooser"
		//cria_background_set_color(self->background, );
	}
}

static void
cspd_update_text(CriaSlidePropertiesDialog* self, GParamSpec* pspec, GtkEntry* entry) {
	cria_slide_set_title(self->slide, gtk_entry_get_text(entry));
}

static void
cria_slide_properties_dialog_init(CriaSlidePropertiesDialog* self) {
	g_return_if_fail(CRIA_IS_SLIDE_PROPERTIES_DIALOG(self));

	/* setting up the dialog's content */
	self->xml = glade_xml_new(PACKAGE_DATA_DIR "/" PACKAGE "/data/criawips.glade", "slide_properties_notebook", NULL);
	gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(self)->vbox), glade_xml_get_widget(self->xml, "slide_properties_notebook"));

	/* setting up the background page */
	 // color
	g_signal_connect_swapped(glade_xml_get_widget(self->xml, "slide_properties_checkbutton_color"),
				 "toggled", G_CALLBACK(cspd_color_check_toggled_cb), self);

	/* setting up the gtk dialog */
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_box_set_spacing(GTK_BOX(GTK_DIALOG(self)->vbox), 5);
        gtk_dialog_add_buttons(GTK_DIALOG(self),
#warning "SlidePropertiesDialog::init(): add help button"
                               GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
                               NULL);
	g_signal_connect(self, "close",
			 G_CALLBACK(gtk_object_destroy), NULL);
	g_signal_connect(self, "response",
			 G_CALLBACK(gtk_object_destroy), NULL);

	g_signal_connect_swapped(glade_xml_get_widget(self->xml, "slide_properties_title"), "notify::text",
				 G_CALLBACK(cspd_update_text), self);

	/* setting up the gtk container */
	gtk_container_set_border_width(GTK_CONTAINER(self), 5);
}

/**
 * cria_slide_properties_dialog_new:
 * @slide: a #CriaSlide
 *
 * Creates a new slide property dialog connected to @slide.
 *
 * Returns a new slide property dialog.
 */
GtkWidget*
cria_slide_properties_dialog_new(CriaSlide* slide) {
	g_return_val_if_fail(CRIA_IS_SLIDE(slide), NULL);

	return g_object_new(CRIA_TYPE_SLIDE_PROPERTIES_DIALOG, "slide", slide, NULL);
}

static void
cria_slide_properties_dialog_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	CriaSlidePropertiesDialog* self = CRIA_SLIDE_PROPERTIES_DIALOG(object);
	
	switch(prop_id) {
	case PROP_SLIDE:
		cria_slide_properties_dialog_set_slide(self, g_value_get_object(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

/**
 * cria_slide_properties_dialog_set_slide:
 * @self: a #CriaSlidePropertiesDialog
 * @slide: a #CriaSlide
 *
 * Set the slide to be displayed in this properties dialog.
 */
void
cria_slide_properties_dialog_set_slide(CriaSlidePropertiesDialog* self, CriaSlide* slide) {
	g_return_if_fail(CRIA_IS_SLIDE_PROPERTIES_DIALOG(self));
	g_return_if_fail(CRIA_IS_SLIDE(slide));

	if(self->slide) {
		cria_slide_view_unregister(CRIA_SLIDE_VIEW(self), self->slide);
		g_object_unref(self->slide);
	}

	self->slide = g_object_ref(slide);
	cria_slide_view_register(CRIA_SLIDE_VIEW(self), self->slide);

	g_object_notify(G_OBJECT(self), "slide");
}

static void
cspd_finalize(GObject* object) {
	CriaSlidePropertiesDialog* self = CRIA_SLIDE_PROPERTIES_DIALOG(object);
	
	cria_slide_view_unregister(CRIA_SLIDE_VIEW(self), self->slide);
	g_object_unref(self->slide);
	self->slide = NULL;
	
	G_OBJECT_CLASS(cria_slide_properties_dialog_parent_class)->finalize(object);
}

static void
cria_slide_properties_dialog_class_init (CriaSlidePropertiesDialogClass	* cria_slide_properties_dialog_class) {
	GObjectClass* go_class;

	/* GObjectClass */
	go_class = G_OBJECT_CLASS(cria_slide_properties_dialog_class);
	go_class->finalize     = cspd_finalize;
	go_class->set_property = cria_slide_properties_dialog_set_property;
	go_class->get_property = cria_slide_properties_dialog_get_property;

	g_object_class_install_property(go_class,
					PROP_SLIDE,
					g_param_spec_object("slide",
							    "Slide",
							    "The slide this dialog is connected to",
							    CRIA_TYPE_SLIDE,
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
}

/* CriaBackgroundViewIface */
/*static void
cspd_init_background_view(CriaBackgroundViewIface* iface) {
	iface->notify_color = update the check button and the color chooser
}*/

/* CriaSlideViewIface */
static void
cspd_notify_title(CriaSlideView* view, CriaSlide* slide, gchar const* title) {
	CriaSlidePropertiesDialog* self = CRIA_SLIDE_PROPERTIES_DIALOG(view);
	gtk_entry_set_text(GTK_ENTRY(glade_xml_get_widget(self->xml, "slide_properties_title")), title);
}

static void
cspd_init_slide_view(CriaSlideViewIface* iface) {
	iface->notify_title = cspd_notify_title;
}

