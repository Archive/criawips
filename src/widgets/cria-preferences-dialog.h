/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *       Adrien Beaucreux     <informancer@afturgurluk.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <glib-object.h>
#include <gtk/gtk.h>

#ifndef PREFERENCES_DIALOG_H
#define PREFERENCES_DIALOG_H

G_BEGIN_DECLS

typedef struct _CriaPreferencesDialog CriaPreferencesDialog;
typedef struct _CriaPreferencesDialogClass CriaPreferencesDialogClass;
typedef struct _CriaPreferencesDialogPrivate CriaPreferencesDialogPrivate;

GType		cria_preferences_dialog_get_type	       (void);
GtkWidget*      cria_preferences_dialog_get_instance           (void);

const gchar*	cria_preferences_dialog_get_attribute       (CriaPreferencesDialog	* self);
void		cria_preferences_dialog_set_attribute       (CriaPreferencesDialog	* self,
							     const char	* attribute);

#define CRIA_TYPE_PREFERENCES_DIALOG			(cria_preferences_dialog_get_type ())
#define CRIA_PREFERENCES_DIALOG(object)			(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_PREFERENCES_DIALOG, CriaPreferencesDialog))
#define CRIA_PREFERENCES_DIALOG_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_PREFERENCES_DIALOG, CriaPreferencesDialogClass))
#define CRIA_IS_PREFERENCES_DIALOG(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_PREFERENCES_DIALOG))
#define CRIA_IS_PREFERENCES_DIALOG_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_PREFERENCES_DIALOG))
#define CRIA_PREFERENCES_DIALOG_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_PREFERENCES_DIALOG, CriaPreferencesDialogClass))

struct _CriaPreferencesDialog {
	GtkDialog		  base_instance;
	CriaPreferencesDialogPrivate
	                  * priv;
};

struct _CriaPreferencesDialogClass {
	GtkDialogClass	  base_class;

	/* signals */
	/*void (*signal)	       (CriaPreferencesDialog	* self,
	  const	gchar	* string);*/
};

G_END_DECLS

#endif /* !PREFERENCES_DIALOG_H */

