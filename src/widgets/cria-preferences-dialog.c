/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *       Adrien Beaucreux     <informancer@afturgurluk.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <widgets/cria-preferences-dialog.h>

#include <inttypes.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <glade/glade.h>

#define CDEBUG_TYPE cria_preferences_dialog_get_type
#include <cdebug/cdebug.h>

#include "application.h"

enum {
	PROP_0,
	/*	PROP_ATTRIBUTE*/
};

enum {
	N_SIGNALS
};

struct _CriaPreferencesDialogPrivate {
	GladeXML		* xml;
};

static	void	cria_preferences_dialog_get_property	       (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static	void	cria_preferences_dialog_set_property        (GObject		* object,
						guint		  prop_id,
						const	GValue	* value,
						GParamSpec	* param_spec);
static  void    cria_preferences_dialog_init(CriaPreferencesDialog* self);
static  void    cb_font_set(GtkFontButton *default_font, gpointer user_data);
static  void    cb_font_changed(void);

#if 0
/* enable these to add support for signals */
static	guint	cria_preferences_dialog_signals[N_SIGNALS] = { 0 };

static	void	cria_preferences_dialog_signal	       (CriaPreferencesDialog	* self,
						const	gchar	* string);
#endif

static void
cria_preferences_dialog_class_init (CriaPreferencesDialogClass	* cria_preferences_dialog_class) {
	GObjectClass	* g_object_class;

	g_object_class = G_OBJECT_CLASS(cria_preferences_dialog_class);
#if 0
	/* setting up signal system */
	cria_preferences_dialog_class->signal = cria_preferences_dialog_signal;

	cria_preferences_dialog_signals[SIGNAL] = g_signal_new (
			"signal",
			CRIA_TYPE_PREFERENCES_DIALOG,
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (
				CriaPreferencesDialogClass,
				signal),
			NULL,
			NULL,
			g_cclosure_marshal_VOID__STRING,
			G_TYPE_NONE,
			0);
#endif
	/* setting up property system */
	g_object_class->set_property = cria_preferences_dialog_set_property;
	g_object_class->get_property = cria_preferences_dialog_get_property;

	/*g_object_class_install_property(g_object_class,
					PROP_ATTRIBUTE,
					g_param_spec_string("attribute",
							    "Attribute",
							    "A simple unneccessary attribute that does nothing special except "
							    "being a demonstration for the correct implementation of a GObject "
							    "property",
							    "default_value",
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT));*/
}

static void
cria_preferences_dialog_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaPreferencesDialog	* self;

	self = CRIA_PREFERENCES_DIALOG(object);

	switch (prop_id) {
/*	case PROP_ATTRIBUTE:
		g_value_set_string(value, self->attribute);
		break;*/
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object,
						  prop_id,
						  param_spec);
		break;
	}
}

GType
cria_preferences_dialog_get_type(void) {
	static GType	type = 0;

	if(!type) {
		static const GTypeInfo info = {
			sizeof(CriaPreferencesDialogClass),
			NULL,	/* base initializer */
			NULL,	/* base finalizer */
			(GClassInitFunc)cria_preferences_dialog_class_init,
			NULL,	/* class finalizer */
			NULL,	/* class data */
			sizeof(CriaPreferencesDialog),
			0,
			(GInstanceInitFunc)cria_preferences_dialog_init,
			0
		};

		type = g_type_register_static(GTK_TYPE_DIALOG,
					      "CriaPreferencesDialog",
					      &info,
					      0);
	}

	return type;
}

/**
 * cria_preferences_dialog_get_instance:
 *
 * As #CriaPreferencesDialog is a singleton, this method delivers the single
 * instance.
 *
 * Return value: the instance of the preference dialog.
 */

GtkWidget*
cria_preferences_dialog_get_instance(void) {
	static CriaPreferencesDialog  * prefs = NULL;

	if(!prefs) {
		prefs = g_object_new(CRIA_TYPE_PREFERENCES_DIALOG, NULL);
	}

	return GTK_WIDGET(prefs);
}

static void
cria_preferences_dialog_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaPreferencesDialog	* self;
	
	self = CRIA_PREFERENCES_DIALOG(object);
	
	switch(prop_id) {
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

static void
cb_screensaver_toggled(GtkToggleButton *widget) {
	g_return_if_fail(GTK_IS_TOGGLE_BUTTON(widget));
	
	cria_application_set_disable_screensaver(gtk_toggle_button_get_active(widget));
}

static void
cb_screensaver_changed(void) {
	CriaPreferencesDialog *self = CRIA_PREFERENCES_DIALOG(cria_preferences_dialog_get_instance());

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(glade_xml_get_widget(self->priv->xml, "preferences_checkbutton_screensaver")),
				     cria_application_get_disable_screensaver());
}

static void
cb_pdcs_toggled(GtkToggleButton *widget) {
	g_return_if_fail(GTK_IS_TOGGLE_BUTTON(widget));
	
	cria_application_set_page_down_creates_slide(gtk_toggle_button_get_active(widget));
}

static void
cb_pdcs_changed(void) {
	CriaPreferencesDialog *self = CRIA_PREFERENCES_DIALOG(cria_preferences_dialog_get_instance());

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(glade_xml_get_widget(self->priv->xml, "preferences_checkbutton_pdcs")),
				     cria_application_get_page_down_creates_slide());
}

static void
cria_preferences_dialog_init(CriaPreferencesDialog* self) {
	g_return_if_fail(CRIA_IS_PREFERENCES_DIALOG(self));

	cdebugo(self, "init()", "start");

	gtk_window_set_title(GTK_WINDOW(self), _("Criawips Preferences"));

	gtk_container_set_border_width(GTK_CONTAINER(self), 5);
	
	self->priv = g_new0(CriaPreferencesDialogPrivate,1);

	self->priv->xml = glade_xml_new(PACKAGE_DATA_DIR "/" PACKAGE "/data/criawips.glade", "preferences_notebook", NULL);
	gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(self)->vbox), glade_xml_get_widget(self->priv->xml, "preferences_notebook"));

	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);

#warning "PreferencesDialog::init(): add help button"
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
			       NULL);	

	/*cb_font_changed(); can't  be called: infinte loop*/
	gtk_font_button_set_font_name(GTK_FONT_BUTTON(glade_xml_get_widget(self->priv->xml, "default_font")),
				      pango_font_description_to_string(cria_application_get_default_font()));
	cdebugo(self, "init()", "Font description 0x%x '%s'", (uintptr_t)cria_application_get_default_font(), (cria_application_get_default_font())?(pango_font_description_to_string(cria_application_get_default_font())):(NULL));

	g_signal_connect(self, "delete-event",
			 G_CALLBACK(gtk_widget_hide_on_delete), NULL);
	g_signal_connect(self, "response",
			 G_CALLBACK(gtk_widget_hide_on_delete), NULL);

	g_signal_connect(glade_xml_get_widget(self->priv->xml, "default_font"), "font_set",
			 G_CALLBACK(cb_font_set), NULL);
	g_signal_connect(cria_application_get_instance(), "default-font-changed",
			 G_CALLBACK(cb_font_changed), NULL);
	
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(glade_xml_get_widget(self->priv->xml, "preferences_checkbutton_screensaver")),
				     cria_application_get_disable_screensaver());
	g_signal_connect(glade_xml_get_widget(self->priv->xml, "preferences_checkbutton_screensaver"), "toggled",
			 G_CALLBACK(cb_screensaver_toggled), NULL);
	g_signal_connect(cria_application_get_instance(), "notify::disable-screensaver",
			 G_CALLBACK(cb_screensaver_changed), NULL);

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(glade_xml_get_widget(self->priv->xml, "preferences_checkbutton_pdcs")),
				     cria_application_get_page_down_creates_slide());
	g_signal_connect(glade_xml_get_widget(self->priv->xml, "preferences_checkbutton_pdcs"), "toggled",
			 G_CALLBACK(cb_pdcs_toggled), NULL);
	g_signal_connect(cria_application_get_instance(), "notify::pagedown-new_slides",
			 G_CALLBACK(cb_pdcs_changed), NULL);

	cdebugo(self, "init()", "end");
}

static void
cb_font_set(GtkFontButton *default_font, gpointer user_data) {
	g_return_if_fail(GTK_IS_FONT_BUTTON(default_font));
	
	cria_application_set_default_font_name(gtk_font_button_get_font_name(default_font));
}

static void
cb_font_changed(void) {
	CriaPreferencesDialog *self = CRIA_PREFERENCES_DIALOG(cria_preferences_dialog_get_instance());

	gtk_font_button_set_font_name(GTK_FONT_BUTTON(glade_xml_get_widget(self->priv->xml, "default_font")),
				      pango_font_description_to_string(cria_application_get_default_font()));
	
}

