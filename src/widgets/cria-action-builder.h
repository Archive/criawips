/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIAWIPS_ACTION_BUILDER_H
#define CRIAWIPS_ACTION_BUILDER_H

#include <gtk/gtkactiongroup.h>
#include <gtk/gtkuimanager.h>

G_BEGIN_DECLS

typedef struct _CriaActionBuilder CriaActionBuilder;
typedef struct _CriaActionBuilderClass CriaActionBuilderClass;

typedef enum _CriaActionBuilderType CriaActionBuilderType;

#define CRIA_TYPE_ACTION_BUILDER         (cria_action_builder_get_type())
#define CRIA_ACTION_BUILDER(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), CRIA_TYPE_ACTION_BUILDER, CriaActionBuilder))
#define CRIA_ACTION_BUILDER_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), CRIA_TYPE_ACTION_BUILDER, CriaActionBuilderClass))
#define CRIA_IS_ACTION_BUILDER(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), CRIA_TYPE_ACTION_BUILDER))
#define CRIA_IS_ACTION_BUILDER_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), CRIA_TYPE_ACTION_BUILDER))
#define CRIA_ACTION_BUILDER_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), CRIA_TYPE_ACTION_BUILDER, CriaActionBuilderClass))

GType cria_action_builder_get_type(void);

void cria_action_builder_rebuild(CriaActionBuilder* self);

G_END_DECLS

#endif /* !CRIAWIPS_ACTION_BUILDER_H */

