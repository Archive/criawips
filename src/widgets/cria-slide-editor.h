/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef SLIDE_EDITOR_H
#define SLIDE_EDITOR_H

#include <glib-object.h>
#include <gtk/gtk.h>

#include <dom/cria-slide.h>
#include <rendering/cria-slide-display.h>

G_BEGIN_DECLS

typedef struct _CriaSlideEditor CriaSlideEditor;
typedef struct _CriaSlideEditorClass CriaSlideEditorClass;

GType		  cria_slide_editor_get_type	       (void);
GtkWidget	* cria_slide_editor_new		       (CriaSlide	* slide);
void		  cria_slide_editor_set_zoom_auto      (CriaSlideEditor	* self,
							gboolean	  zoom_auto);
void		  cria_slide_editor_drag_create        (CriaSlideEditor * self,
							GType		  to_create,
							GFunc		  cancel_func,
							gpointer          cancel_data);

#define CRIA_TYPE_SLIDE_EDITOR			(cria_slide_editor_get_type ())
#define CRIA_SLIDE_EDITOR(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_SLIDE_EDITOR, CriaSlideEditor))
#define CRIA_SLIDE_EDITOR_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_SLIDE_EDITOR, CriaSlideEditorClass))
#define CRIA_IS_SLIDE_EDITOR(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_SLIDE_EDITOR))
#define CRIA_IS_SLIDE_EDITOR_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_SLIDE_EDITOR))
#define CRIA_SLIDE_EDITOR_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_SLIDE_EDITOR, CriaSlideEditorClass))

#define SLIDE_EDITOR_PADDING	 GO_PT_TO_IN(6*576)
#define SLIDE_EDITOR_SHADOW_OFFSET GO_PT_TO_IN(6*576)

G_END_DECLS

#endif /* SLIDE_EDITOR_H */

