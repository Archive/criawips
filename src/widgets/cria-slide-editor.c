/* This file is part of criawips
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005,2007 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <widgets/cria-slide-editor-priv.h>

#include <inttypes.h>
#include <string.h>

#include <glib.h>
#include <glib-object.h>
#include <glib/gi18n.h>

#include <canvas/rd-canvas.h>

#define CDEBUG_TYPE cria_slide_editor_get_type
#include <cdebug/cdebug.h>
#include <helpers/gtk-helpers.h>

#include <dom/cria-block.h>
#include <dom/cria-presentation.h>

enum {
	PROP_0,
};

static GObjectClass* parent_class = NULL;

static gboolean cria_slide_editor_focus_in	       (GtkWidget	* self,
							GdkEventFocus	* ev);
static gboolean cria_slide_editor_focus_out	       (GtkWidget	* self,
							GdkEventFocus	* ev);
static	void	cria_slide_editor_get_property         (GObject		* object,
							guint		  prop_id,
							GValue		* value,
							GParamSpec	* param_spec);
static	void	cria_slide_editor_init	               (CriaSlideEditor	* self);
static	void	cria_slide_editor_set_property         (GObject		* object,
						        guint		  prop_id,
						        const	GValue	* value,
						        GParamSpec	* param_spec);
static gboolean	cria_slide_editor_popup_menu	       (GtkWidget	* self);
static	void	cria_slide_editor_slide_changed        (CriaSlideEditor	* self,
						        gpointer	  data);

static void
cse_dispose(GObject* object) {
	parent_class->dispose(object);
}

static void
cria_slide_editor_class_init(CriaSlideEditorClass* cria_slide_editor_class) {
	GObjectClass	* g_object_class;
	GtkWidgetClass	* widget_class;

	/* setting up the object class */
	g_object_class = G_OBJECT_CLASS(cria_slide_editor_class);
	g_object_class->dispose = cse_dispose;
	g_object_class->set_property = cria_slide_editor_set_property;
	g_object_class->get_property = cria_slide_editor_get_property;

	/* setting up the widget class */
	widget_class = GTK_WIDGET_CLASS(cria_slide_editor_class);
	widget_class->focus_in_event = cria_slide_editor_focus_in;
	widget_class->focus_out_event = cria_slide_editor_focus_out;
	widget_class->popup_menu = cria_slide_editor_popup_menu;

	/* setting up the slide view class */
	parent_class = g_type_class_peek_parent(cria_slide_editor_class);
}

static gboolean
cria_slide_editor_focus_in(GtkWidget* self, GdkEventFocus* ev) {
	cdebugo(self, "focusIn()", "run");
	return GTK_WIDGET_CLASS(parent_class)->focus_in_event(self, ev);
}

static gboolean
cria_slide_editor_focus_out(GtkWidget* self, GdkEventFocus* ev) {
	cdebugo(self, "focusOut()", "run");
	return GTK_WIDGET_CLASS(parent_class)->focus_out_event(self, ev);
}

static void
cria_slide_editor_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaSlideEditor	* self;

	self = CRIA_SLIDE_EDITOR (object);

	switch(prop_id) {
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

GType
cria_slide_editor_get_type(void) {
	static GType	type = 0;

	if(!type) {
		static const GTypeInfo info = {
			sizeof (CriaSlideEditorClass),
			NULL,	/* base initializer */
			NULL,	/* base finalizer */
			(GClassInitFunc)cria_slide_editor_class_init,
			NULL,	/* class finalizer */
			NULL,	/* class data */
			sizeof (CriaSlideEditor),
			0,
			(GInstanceInitFunc)cria_slide_editor_init,
			0
		};

		type = g_type_register_static(CRIA_TYPE_SLIDE_DISPLAY,
					      "CriaSlideEditor",
					      &info,
					      0);
	}

	return type;
}

static void
cria_slide_editor_init(CriaSlideEditor* self) {
	/* connect neccessary signals */
	g_signal_connect(self, "notify::slide",
			 G_CALLBACK(cria_slide_editor_slide_changed), NULL);

	GTK_WIDGET_SET_FLAGS (self, GTK_CAN_FOCUS);
}

/**
 * cria_slide_editor_new:
 * @slide: a #CriaSlide
 *
 * Create a new slide editor, connected to @slide.
 *
 * Returns the new slide editor.
 */
GtkWidget*
cria_slide_editor_new(CriaSlide* slide) {
	GtkWidget* self = NULL;
	g_return_val_if_fail(slide == NULL || CRIA_IS_SLIDE(slide), self);

	self = g_object_new(CRIA_TYPE_SLIDE_EDITOR,
			    "slide-renderer", cria_slide_renderer_new(TRUE),
			    "aa", TRUE,
			    NULL);

	if(slide) {
		cria_slide_display_set_slide(CRIA_SLIDE_DISPLAY(self), slide);
	}

	cria_canvas_set_padding(CRIA_CANVAS(self), SLIDE_EDITOR_PADDING);

	return self;
}

struct cancel_data {
	CriaSlideEditor* self;        /* ref'd */
	GType            type;
	GFunc            cancel_func; /* function to be called after
					 cancelling or finishing the
					 operation */
	gpointer         cancel_data; /* data for cancel_func */
	gdouble          x, y, w, h;
};

static struct cancel_data*
cd_new(CriaSlideEditor* self, GType type, GFunc cancel_func, gpointer cancel_data) {
	struct cancel_data* d;

	g_return_val_if_fail(CRIA_IS_SLIDE_EDITOR(self), NULL);
	g_return_val_if_fail(cancel_func, NULL);

	d = g_new0(struct cancel_data, 1);
	d->self = g_object_ref(self);
	d->type = type;
	d->cancel_func = cancel_func;
	d->cancel_data = cancel_data;
	d->x = d->y = d->w = d->h = 0.0;

	return d;
}

static void
cd_free(struct cancel_data* d) {
	g_object_unref(d->self);
	g_free(d);
}

static void cse_drag_cancel_timeout_func(struct cancel_data* d);

static gboolean
cse_button_press_handler (CriaSlideEditor   * self,
			  GdkEventButton    * ev,
			  struct cancel_data* d)
{
#warning "FIXME: cancel with right button"
	gdouble x, y;
	rd_canvas_window_to_world (RD_CANVAS (self),
				   ev->x, ev->y,
				   &x, &y);
	d->x = x;
	d->y = y;
	return FALSE;
}

static gboolean
cse_button_release_handler (CriaSlideEditor   * self,
			    GdkEventButton    * ev,
			    struct cancel_data* d)
{
	gdouble x, y;
	rd_canvas_window_to_world (RD_CANVAS (self),
				   ev->x, ev->y,
				   &x, &y);

	if(x < d->x) {
		d->w = d->x - x;
		d->x = x;
	} else {
		d->w = x - d->x;
	}

	if(y < d->y) {
		d->h = d->h - y;
		d->y = y;
	} else {
		d->h = y - d->y;
	}

	if(d->type == CRIA_TYPE_BLOCK) {
		CriaSlide* slide = cria_slide_display_get_slide(CRIA_SLIDE_DISPLAY(d->self));
		CriaBlock* block;

		// find a free name
		gchar const* format_name = _("Text Element %u");
#warning "FIXME: make the format string a class property of the element class, then we can use this code more generically and also for other types..."
		gchar      * name;
		guint new_index = 0;
		GList* elements = cria_slide_get_elements(slide);
		GList* element;

		for(element = elements; element; element = element->next) {
			gchar const* name = cria_slide_element_get_name(CRIA_SLIDE_ELEMENT(element->data));
			guint this_index;

			if(sscanf(name, format_name, &this_index)) {
				new_index = MAX(new_index, this_index);
			}
		}

		name = g_strdup_printf(format_name, new_index + 1);
		block = cria_block_new(name);

		cria_slide_element_set_position(CRIA_SLIDE_ELEMENT(block), d->x, d->y, d->x + d->w, d->y + d->h);
		cria_slide_add_element(slide, CRIA_SLIDE_ELEMENT(block));

		g_free(name);
		g_list_free(elements);
	}

	cse_drag_cancel_timeout_func(d);
	return FALSE;
}

static void
cse_drag_cancel_timeout_func(struct cancel_data* d) {
	if(d->cancel_func) {
		d->cancel_func(d->cancel_data, NULL);
	}

	g_signal_handlers_disconnect_by_func(d->self, cse_button_press_handler, d);
	g_signal_handlers_disconnect_by_func(d->self, cse_button_release_handler, d);
	cd_free(d);
}

/**
 * cria_slide_editor_drag_create:
 * @self: a #CriaSlideEditor
 * @to_create: a GType of an item to create
 * @cancel_func: function to be called when the operation gets finished or
 * aborted
 *
 * Tell the slide editor to create an item by user interaction via drag and
 * drop.
 */
void
cria_slide_editor_drag_create(CriaSlideEditor* self, GType to_create, GFunc cancel_func, gpointer cancel_data) {
	struct cancel_data* d;

	g_return_if_fail(to_create == CRIA_TYPE_BLOCK);

	d = cd_new(self, to_create, cancel_func, cancel_data);
	g_signal_connect(self, "button-press-event",
			 G_CALLBACK(cse_button_press_handler), d);
	g_signal_connect(self, "button-release-event",
			 G_CALLBACK(cse_button_release_handler), d);
#warning "FIXME: cancel with ESC"
}

static gboolean
cria_slide_editor_popup_menu(GtkWidget* widget) {
#warning "SlideEditor::popupMenu(): find out where the cursor is and populate the menu"
#warning "SlideEditor::popupMenu(): show the menu"
	return FALSE;
}

static void
cria_slide_editor_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaSlideEditor	* self;

	self = CRIA_SLIDE_EDITOR(object);

	switch(prop_id) {
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

static void
cria_slide_editor_slide_changed(CriaSlideEditor* self, gpointer data) {
	g_return_if_fail(CRIA_IS_SLIDE_EDITOR(self));

	cria_slide_renderer_set_slide(CRIA_SLIDE_DISPLAY(self)->slide_renderer,
				      cria_slide_display_get_slide(CRIA_SLIDE_DISPLAY(self)));
}

