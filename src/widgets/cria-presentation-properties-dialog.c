/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <widgets/cria-presentation-properties-dialog.h>

#include <string.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <glade/glade.h>

#define CDEBUG_TYPE cria_presentation_properties_get_type
#include <cdebug/cdebug.h>

#include <dom/cria-presentation.h>

enum {
	PROP_0,
	PROP_PRESENTATION,
};

enum {
	N_SIGNALS
};

struct _CriaPresentationPropertiesDialogPrivate {
	GladeXML		* xml;
	CriaPresentation	* presentation;
};

static	void	cria_presentation_properties_dialog_get_property	       (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static void	cria_presentation_properties_dialog_init	       (CriaPresentationPropertiesDialog* self);
static	void	cria_presentation_properties_dialog_set_property        (GObject		* object,
						guint		  prop_id,
						const	GValue	* value,
						GParamSpec	* param_spec);

static void
cria_presentation_properties_dialog_class_init (CriaPresentationPropertiesDialogClass	* cria_presentation_properties_dialog_class) {
	GObjectClass	* g_object_class;

	g_object_class = G_OBJECT_CLASS(cria_presentation_properties_dialog_class);

	/* setting up property system */
	g_object_class->set_property = cria_presentation_properties_dialog_set_property;
	g_object_class->get_property = cria_presentation_properties_dialog_get_property;

	g_object_class_install_property(g_object_class,
					PROP_PRESENTATION,
					g_param_spec_object("presentation",
							    "Presentation",
							    "The Presentation that's being edited with this dialog.",
							    CRIA_TYPE_PRESENTATION,
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
}

CriaPresentation*
cria_presentation_properties_dialog_get_presentation(CriaPresentationPropertiesDialog* self) {
	g_return_val_if_fail(CRIA_IS_PRESENTATION_PROPERTIES_DIALOG(self), NULL);
	g_return_val_if_fail(self->priv, NULL);
	
	return self->priv->presentation;
}

static void
cria_presentation_properties_dialog_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaPresentationPropertiesDialog	* self;

	self = CRIA_PRESENTATION_PROPERTIES_DIALOG(object);

	switch (prop_id) {
	case PROP_PRESENTATION:
		g_value_set_object(value, self->priv->presentation);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object,
						  prop_id,
						  param_spec);
		break;
	}
}

GType
cria_presentation_properties_dialog_get_type(void) {
	static GType	type = 0;

	if(!type) {
		static const GTypeInfo info = {
			sizeof(CriaPresentationPropertiesDialogClass),
			NULL,	/* base initializer */
			NULL,	/* base finalizer */
			(GClassInitFunc)cria_presentation_properties_dialog_class_init,
			NULL,	/* class finalizer */
			NULL,	/* class data */
			sizeof(CriaPresentationPropertiesDialog),
			0,
			(GInstanceInitFunc)cria_presentation_properties_dialog_init,
			0
		};

		type = g_type_register_static(GTK_TYPE_DIALOG,
					      "CriaPresentationPropertiesDialog",
					      &info,
					      0);
	}

	return type;
}

GtkWidget*
cria_presentation_properties_dialog_new(CriaPresentation* presentation) {
	return g_object_new(CRIA_TYPE_PRESENTATION_PROPERTIES_DIALOG, "presentation", presentation, NULL);
}

static void
cb_properties_checkbutton_autoswitch_toggled(CriaPresentationPropertiesDialog* self) {
	g_return_if_fail(CRIA_IS_PRESENTATION_PROPERTIES_DIALOG(self));
	g_return_if_fail(self->priv);
	g_return_if_fail(CRIA_IS_PRESENTATION(self->priv->presentation));
	g_return_if_fail(GLADE_IS_XML(self->priv->xml));

	cria_presentation_set_auto_switch(self->priv->presentation, gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(glade_xml_get_widget(self->priv->xml, "properties_checkbutton_autoswitch"))));
}

static void
cb_properties_checkbutton_loop_toggled(CriaPresentationPropertiesDialog* self) {
	g_return_if_fail(CRIA_IS_PRESENTATION_PROPERTIES_DIALOG(self));
	g_return_if_fail(self->priv);
	g_return_if_fail(CRIA_IS_PRESENTATION(self->priv->presentation));
	g_return_if_fail(GLADE_IS_XML(self->priv->xml));

	cria_presentation_set_loop(self->priv->presentation, gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(glade_xml_get_widget(self->priv->xml, "properties_checkbutton_loop"))));
}

static void
cb_properties_entry_title_changed(CriaPresentationPropertiesDialog* self, GtkEntry* entry) {
	g_return_if_fail(CRIA_IS_PRESENTATION_PROPERTIES_DIALOG(self));
	g_return_if_fail(GTK_IS_ENTRY(entry));
	g_return_if_fail(self->priv);
	g_return_if_fail(CRIA_IS_PRESENTATION(self->priv->presentation));

	cria_presentation_set_title(self->priv->presentation, gtk_entry_get_text(entry));
}

static void
cb_properties_spinbutton_delay_changed(CriaPresentationPropertiesDialog* self, GtkSpinButton* spin) {
	gdouble delay;
	
	g_return_if_fail(CRIA_IS_PRESENTATION_PROPERTIES_DIALOG(self));
	g_return_if_fail(self->priv);
	g_return_if_fail(CRIA_IS_PRESENTATION(self->priv->presentation));
	g_return_if_fail(GTK_IS_SPIN_BUTTON(spin));

	delay = gtk_spin_button_get_value(spin);

	cria_presentation_set_slide_delay(self->priv->presentation, delay);
}

static void
cria_presentation_properties_dialog_init(CriaPresentationPropertiesDialog* self) {
	g_return_if_fail(CRIA_IS_PRESENTATION_PROPERTIES_DIALOG(self));

#warning "PresentationPropertiesDialog::init(): remove this one once the dialog has a good ratio"
	gtk_window_set_default_size(GTK_WINDOW(self), 400, 280);
	
	gtk_container_set_border_width(GTK_CONTAINER(self), 5);
	
	self->priv = g_new0(CriaPresentationPropertiesDialogPrivate,1);

	self->priv->xml = glade_xml_new(PACKAGE_DATA_DIR "/" PACKAGE "/data/criawips.glade", "properties_notebook", NULL);
	gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(self)->vbox), glade_xml_get_widget(self->priv->xml, "properties_notebook"));

	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
#warning "PresentationPropertiesDialog::init(): add help button"
			       GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
			       NULL);
	g_signal_connect(self, "close",
			 G_CALLBACK(gtk_object_destroy), NULL);
	g_signal_connect(self, "response",
			 G_CALLBACK(gtk_object_destroy), NULL);
	g_signal_connect_swapped(glade_xml_get_widget(self->priv->xml, "properties_checkbutton_autoswitch"), "toggled",
				 G_CALLBACK(cb_properties_checkbutton_autoswitch_toggled), self);
	g_signal_connect_swapped(glade_xml_get_widget(self->priv->xml, "properties_checkbutton_loop"), "toggled",
				 G_CALLBACK(cb_properties_checkbutton_loop_toggled), self);
	g_signal_connect_swapped(glade_xml_get_widget(self->priv->xml, "properties_entry_title"), "changed",
				 G_CALLBACK(cb_properties_entry_title_changed), self);
	g_signal_connect_swapped(glade_xml_get_widget(self->priv->xml, "properties_spinbutton_delay"), "value-changed",
				 G_CALLBACK(cb_properties_spinbutton_delay_changed), self);
}

static void
cria_presentation_properties_dialog_update_auto_switch(CriaPresentationPropertiesDialog* self) {
	GtkWidget* checkbutton;

	g_return_if_fail(CRIA_IS_PRESENTATION_PROPERTIES_DIALOG(self));
	g_return_if_fail(self->priv);
	g_return_if_fail(CRIA_IS_PRESENTATION(self->priv->presentation));
	g_return_if_fail(GLADE_IS_XML(self->priv->xml));

	checkbutton = glade_xml_get_widget(self->priv->xml, "properties_checkbutton_autoswitch");

	g_return_if_fail(GTK_IS_CHECK_BUTTON(checkbutton));

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbutton), cria_presentation_get_auto_switch(self->priv->presentation));
}

static void
cria_presentation_properties_dialog_update_loop(CriaPresentationPropertiesDialog* self) {
	GtkWidget* checkbutton;

	g_return_if_fail(CRIA_IS_PRESENTATION_PROPERTIES_DIALOG(self));
	g_return_if_fail(self->priv);
	g_return_if_fail(CRIA_IS_PRESENTATION(self->priv->presentation));
	g_return_if_fail(GLADE_IS_XML(self->priv->xml));

	checkbutton = glade_xml_get_widget(self->priv->xml, "properties_checkbutton_loop");

	g_return_if_fail(GTK_IS_CHECK_BUTTON(checkbutton));

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbutton), cria_presentation_get_loop(self->priv->presentation));
}

static void
cria_presentation_properties_dialog_update_slide_delay(CriaPresentationPropertiesDialog* self) {
	GtkWidget* spinbutton;
	
	g_return_if_fail(CRIA_IS_PRESENTATION_PROPERTIES_DIALOG(self));
	g_return_if_fail(self->priv);
	g_return_if_fail(CRIA_IS_PRESENTATION(self->priv->presentation));
	g_return_if_fail(GLADE_IS_XML(self->priv->xml));
	
	spinbutton = glade_xml_get_widget(self->priv->xml, "properties_spinbutton_delay");
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbutton), 1.0 * cria_presentation_get_slide_delay(self->priv->presentation));
}

static void
cria_presentation_properties_dialog_update_title(CriaPresentationPropertiesDialog* self) {
	GtkWidget  * entry;
	gchar      * title;
	const gchar* ptitle;

	g_return_if_fail(CRIA_IS_PRESENTATION_PROPERTIES_DIALOG(self));
	g_return_if_fail(GTK_IS_WINDOW(self));
	g_return_if_fail(self->priv);
	g_return_if_fail(CRIA_IS_PRESENTATION(self->priv->presentation));
	g_return_if_fail(GLADE_IS_XML(self->priv->xml));
	
	entry = glade_xml_get_widget(self->priv->xml, "properties_entry_title");

	g_return_if_fail(GTK_IS_ENTRY(entry));
	
	/* update the title */
	ptitle = cria_presentation_get_title(self->priv->presentation);
	cdebugo(self, "updateTitle(): presentation's title is \"%s\"", ptitle);
	title = g_strdup_printf(_("%s Properties"), ptitle);
	gtk_window_set_title(GTK_WINDOW(self), title);

	/* connect the title GtkEntry */
	if(strcmp(ptitle, gtk_entry_get_text(GTK_ENTRY(entry)))) {
		gtk_entry_set_text(GTK_ENTRY(entry), ptitle);
	}
	
	g_free(title);
}

void
cria_presentation_properties_dialog_set_presentation(CriaPresentationPropertiesDialog* self, CriaPresentation* presentation) {
	g_return_if_fail(CRIA_IS_PRESENTATION_PROPERTIES_DIALOG(self));
	g_return_if_fail(CRIA_IS_PRESENTATION(presentation));

	if(presentation == self->priv->presentation) {
		return;
	}

	if(self->priv->presentation != NULL) {
		g_signal_handlers_disconnect_by_func(self->priv->presentation,
						     cria_presentation_properties_dialog_update_auto_switch,
						     self);
		g_signal_handlers_disconnect_by_func(self->priv->presentation,
						     cria_presentation_properties_dialog_update_loop,
						     self);
		g_signal_handlers_disconnect_by_func(self->priv->presentation,
						     cria_presentation_properties_dialog_update_slide_delay,
						     self);
		g_signal_handlers_disconnect_by_func(self->priv->presentation,
						     cria_presentation_properties_dialog_update_title,
						     self);
		g_object_unref(self->priv->presentation);
	}

	self->priv->presentation = g_object_ref(presentation);

	g_signal_connect_swapped(self->priv->presentation, "notify::auto-switch",
				 G_CALLBACK(cria_presentation_properties_dialog_update_auto_switch), self);
	cria_presentation_properties_dialog_update_auto_switch(self);
	g_signal_connect_swapped(self->priv->presentation, "notify::loop",
				 G_CALLBACK(cria_presentation_properties_dialog_update_loop), self);
	cria_presentation_properties_dialog_update_loop(self);
	g_signal_connect_swapped(self->priv->presentation, "notify::slide-delay",
				 G_CALLBACK(cria_presentation_properties_dialog_update_slide_delay), self);
	cria_presentation_properties_dialog_update_slide_delay(self);
	g_signal_connect_swapped(self->priv->presentation, "notify::title",
				 G_CALLBACK(cria_presentation_properties_dialog_update_title), self);
	cria_presentation_properties_dialog_update_title(self);
	
	g_object_notify(G_OBJECT(self), "presentation");
}

static void
cria_presentation_properties_dialog_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaPresentationPropertiesDialog	* self;
	
	self = CRIA_PRESENTATION_PROPERTIES_DIALOG(object);
	
	switch (prop_id) {
	case PROP_PRESENTATION:
		cria_presentation_properties_dialog_set_presentation(self, g_value_get_object(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object,
						  prop_id,
						  param_spec);
		break;
	}
}

