/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <glib-object.h>
#include <widgets/cria-text-pane.h>

#ifndef TITLE_BAR_H
#define TITLE_BAR_H

#include <dom/cria-slide.h>

G_BEGIN_DECLS

typedef struct _CriaTitleBar CriaTitleBar;
typedef struct _CriaTitleBarClass CriaTitleBarClass;
typedef struct _CriaTitleBarPrivate CriaTitleBarPrivate;

GType		cria_title_bar_get_type	       (void);
CriaSlide*	cria_title_bar_get_slide       (CriaTitleBar	* self);
GtkWidget*	cria_title_bar_new	       (CriaSlide	* slide);
void		cria_title_bar_set_slide       (CriaTitleBar	* self,
						CriaSlide	* slide);

#define CRIA_TYPE_TITLE_BAR			(cria_title_bar_get_type ())
#define CRIA_TITLE_BAR(object)			(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_TITLE_BAR, CriaTitleBar))
#define CRIA_TITLE_BAR_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_TITLE_BAR, CriaTitleBarClass))
#define CRIA_IS_TITLE_BAR(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_TITLE_BAR))
#define CRIA_IS_TITLE_BAR_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_TITLE_BAR))
#define CRIA_TITLE_BAR_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_TITLE_BAR, CriaTitleBarClass))

struct _CriaTitleBar {
	CriaTextPane		  base_instance;
	CriaTitleBarPrivate	* priv;
};

struct _CriaTitleBarClass {
	CriaTextPaneClass	  base_class;

	/* signals */
	/*void (*signal)	       (CriaTitleBar	* self,
				const	gchar	* string);*/
};

G_END_DECLS

#endif /* !TITLE_BAR_H */

