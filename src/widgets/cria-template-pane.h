/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIAWIPS_TEMPLATE_PANE_H
#define CRIAWIPS_TEMPLATE_PANE_H

#include <gtk/gtkiconview.h>

#include <dom/cria-theme.h>

G_BEGIN_DECLS

typedef struct _GtkIconView CriaTemplatePane;
typedef struct _GtkIconViewClass CriaTemplatePaneClass;

#define CRIA_TYPE_TEMPLATE_PANE         (cria_template_pane_get_type())
#define CRIA_TEMPLATE_PANE(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), CRIA_TYPE_TEMPLATE_PANE, CriaTemplatePane))
#define CRIA_TEMPLATE_PANE_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), CRIA_TYPE_TEMPLATE_PANE, CriaTemplatePaneClass))
#define CRIA_IS_TEMPLATE_PANE(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), CRIA_TYPE_TEMPLATE_PANE))
#define CRIA_IS_TEMPLATE_PANE_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), CRIA_TYPE_TEMPLATE_PANE))
#define CRIA_TEMPLATE_PANE_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), CRIA_TYPE_TEMPLATE_PANE, CriaTemplatePaneClass))

GType      cria_template_pane_get_type        (void);
GtkWidget* cria_template_pane_new             (void);
void       cria_template_pane_set_theme       (CriaTemplatePane* self,
					       CriaTheme       * theme);

G_END_DECLS

#endif /* !CRIAWIPS_TEMPLATE_PANE_H */

