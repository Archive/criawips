/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *       Adrien Beaucreux     <informancer@afturgurluk.org>
 *       Ezequiel Pérez       <eazel7@yahoo.com.ar>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 * Copyright (C) 2004,2005 Adrien Beaucreux
 * Copyright (C) 2004 Keith Sharp
 * Copyright (C) 2004 Ezequiel Pérez
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <widgets/cria-main-window.h>

#include <string.h>
#include <stdint.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <libgnomeui/libgnomeui.h>
#include <libgnomevfs/gnome-vfs-ops.h>
#include <libgnomevfs/gnome-vfs-mime-utils.h>
#include <goffice/gtk/go-action-combo-text.h>

#define CDEBUG_TYPE cria_main_window_get_type
#include <cdebug/cdebug.h>
#include <helpers/gtk-helpers.h>

#include <dom/cria-format.h>
#include <dom/cria-presentation-view.h>
#include <dom/cria-slide-list.h>
#include <dom/cria-slide-view.h>
#include <io/cria-presentation-writer.h>
#include <rendering/cria-block-renderer.h>
#include <widgets/cria-presentation-properties-dialog.h>
#include <widgets/cria-preferences-dialog.h>
#include <widgets/cria-sidebar.h>
#include <widgets/cria-slide-editor.h>
#include <widgets/cria-slide-show.h>
#include <widgets/cria-template-pane.h>
#include <widgets/cria-title-bar.h>
#include <utils/cria-units.h>

#include "application.h"

enum {
	PROP_0,
	PROP_PRESENTATION,
	PROP_SLIDE,
	PROP_THEME
};

enum {
	SIGNAL,
	N_SIGNALS
};

static void cmw_init_slide_view_iface(CriaSlideViewIface* iface);
static void cmw_init_presentation_view_iface(CriaPresentationViewIface* iface);

G_DEFINE_TYPE_WITH_CODE(CriaMainWindow, cria_main_window, GTK_TYPE_WINDOW,
			G_IMPLEMENT_INTERFACE(CRIA_TYPE_SLIDE_VIEW, cmw_init_slide_view_iface);
			G_IMPLEMENT_INTERFACE(CRIA_TYPE_PRESENTATION_VIEW, cmw_init_presentation_view_iface));

struct _CriaMainWindowPriv {
	GtkWidget	* hpaned,
			* hpaned_templates,
			* vbox,
			* sidebar,
			* sidebar_templates,
			* slide_editor,
			* slide_list,
			* status_bar,
			* templates,
			* title_bar;
	GladeXML	* xml;
	GtkUIManager	* ui_manager;
	GtkActionGroup  * action_group;
	GtkAction	* font_family,
			* font_size,
			* zoom;
	CriaPresentation* presentation;
	GtkRecentManager* recent_manager;
	guint		  geometry_timeout_id,
			  paned_timeout_id,
			  paned_timeout_id_templates;
	gchar		* last_geometry;
	guint		  status_context,
			  status_drag_to_create;
};

static	void	cria_main_window_open		       (CriaMainWindow	* self,
							gpointer	  data);
static	void	cria_main_window_selected_slide_changed(CriaMainWindow	* self,
							GtkTreeSelection* selection);
static	void	cria_main_window_start_off	       (CriaMainWindow	* self);
static	void	cria_main_window_set_presentation_from_text_uri	(CriaMainWindow* self,
							const gchar	* text_uri);
static void     cria_main_window_open_presentations_from_uri_list(CriaMainWindow * self, 
							GSList         * uris);

#if 0
static	void	cria_main_window_signal		       (CriaMainWindow	* template,
							const	gchar	* string);

static	guint	cria_main_window_signals[N_SIGNALS] = { 0 };
#endif

static void cmw_init_actions(CriaMainWindow* self);
static GtkActionEntry entries[];
static GtkToggleActionEntry toggle_entries[];

/* Drag target types */
enum {
	TARGET_URI_LIST
};

static gboolean
save_window_geometry_timeout(CriaMainWindow* self) {
	cria_application_set_default_geometry(self->priv->last_geometry);
	self->priv->geometry_timeout_id = 0;

	return FALSE;
}

static gboolean
cmw_configure_event(GtkWidget* widget, GdkEventConfigure* ev) {
	CriaMainWindow* self;
	
	GTK_WIDGET_CLASS(cria_main_window_parent_class)->configure_event(widget, ev);

	self = CRIA_MAIN_WINDOW(widget);

	if(self->priv->geometry_timeout_id) {
		g_source_remove(self->priv->geometry_timeout_id);
	}
	if(GTK_WIDGET_VISIBLE(widget) && (gdk_window_get_state(widget->window) & GDK_WINDOW_STATE_MAXIMIZED) == 0) {
		gchar* geometry = gtk_window_get_geometry(GTK_WINDOW(widget));

		/* If the last geometry is NULL the window must have just
		 * appeared. No need to save the geometry to disk since
		 * it must have remained the same */
		if(self->priv->last_geometry == NULL) {
			self->priv->last_geometry = geometry;
			return FALSE;
		}

		/* don't save if nothing changed */
		if(!strcmp(self->priv->last_geometry, geometry)) {
			g_free(geometry);
			return FALSE;
		}

		g_free(self->priv->last_geometry);
		self->priv->last_geometry = geometry;

		self->priv->geometry_timeout_id = g_timeout_add(1000, (GSourceFunc)save_window_geometry_timeout, self);
	}
	
	return FALSE;
}

static gboolean
cmw_window_state_event(GtkWidget* widget, GdkEventWindowState* ev) {
	if(ev->changed_mask & GDK_WINDOW_STATE_MAXIMIZED) {
		gboolean fullscreen = (ev->new_window_state & GDK_WINDOW_STATE_MAXIMIZED) ? FALSE : TRUE;
		gtk_statusbar_set_has_resize_grip(GTK_STATUSBAR(CRIA_MAIN_WINDOW(widget)->priv->status_bar), fullscreen);
#warning "FIXME: store the maximized-status in GConf"
	}
	
	if(GTK_WIDGET_CLASS(cria_main_window_parent_class)->window_state_event) {
		return GTK_WIDGET_CLASS(cria_main_window_parent_class)->window_state_event(widget, ev);
	}

	return FALSE;
}

static void
cmw_finalize(GObject* object) {
	CriaMainWindow* self = CRIA_MAIN_WINDOW(object);

	g_object_unref(self->theme_actions);
	self->theme_actions = NULL;

	cria_slide_view_unregister(CRIA_SLIDE_VIEW(self), self->slide);
	cdebug("finalize()", "%p: Removing %d. reference on %p (%s)", self, G_OBJECT(self->slide)->ref_count, self->slide, G_OBJECT_TYPE_NAME(self->slide));
	g_object_unref(self->slide);
	self->slide = NULL;

	g_object_unref(self->priv->ui_manager);
	self->priv->ui_manager = NULL;

	if(self->priv->geometry_timeout_id) {
		g_source_remove(self->priv->geometry_timeout_id);
	}
	if(self->priv->paned_timeout_id) {
		g_source_remove(self->priv->paned_timeout_id);
	}

	g_free(self->priv->last_geometry);
	self->priv->last_geometry = NULL;

	g_object_unref(self->priv->xml);
	self->priv->xml = NULL;

	cria_presentation_view_unregister(CRIA_PRESENTATION_VIEW(self), self->priv->presentation);
	cdebug("finalize()", "presentation has %d references, removing one", G_OBJECT(self->priv->presentation)->ref_count);
	g_object_unref(self->priv->presentation);
	self->priv->presentation = NULL;

	G_OBJECT_CLASS(cria_main_window_parent_class)->finalize(object);
}

static void
cmw_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaMainWindow	* self;

	self = CRIA_MAIN_WINDOW(object);

	switch(prop_id) {
	case PROP_PRESENTATION:
		g_value_set_object(value, self->priv->presentation);
		break;
	case PROP_THEME:
		g_value_set_object(value, cria_presentation_get_theme(self->priv->presentation));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID( object, prop_id, param_spec);
		break;
	}
}

static void
cmw_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaMainWindow	* self;

	cdebugo(object, "setProperty()", "start");
	
	self = CRIA_MAIN_WINDOW(object);
	
	switch (prop_id) {
	case PROP_PRESENTATION:
		cria_main_window_set_presentation(self, g_value_get_object (value));
		break;
	case PROP_THEME:
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}

	cdebugo(object, "setProperty()", "end");
}

/*
 * cmw_drag_data_received:
 * @widget: The widget which received the drop
 * @drag_context: the drag context
 * @x,@y: Coordinates of the drop
 * @data: the received data
 * @info: the info that has been registered with the target in the GtkTargetList.
 * @time: the timestamp at which the data was received 
 *
 * cb_drag_data_received() is the function called when a drop occurs on
 * the main window.
 *
 * It acts like a dispatcher according to the type of the droped data.
 */
static void
cmw_drag_data_received(GtkWidget* widget, GdkDragContext* drag_context, gint x, gint y, GtkSelectionData* data, guint info, guint time) {
	gboolean success = FALSE;
	gboolean del = FALSE;

	gint i;
	gchar** uri_strv = NULL;
	gint uri_strv_len;
	GSList *uris = NULL;

	cdebug("cb_drag_data_received()", "start");
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(widget));

	switch(info) {
	case TARGET_URI_LIST:
		cdebug("cb_drag_data_received()", "URI list received");

		/* We need to make the interface between a string and a GList */
		uri_strv = g_strsplit((const gchar*)data->data, "\n", -1);
		uri_strv_len = g_strv_length(uri_strv);

		for(i=0; i < uri_strv_len; i++) {
			if(g_ascii_strncasecmp(uri_strv[i],"",1)>0) {
				gchar* new_uri = g_strstrip(uri_strv[i]);
				cdebug("cb_drag_data_received()","appending %s to uris",new_uri);
				uris = g_slist_append(uris, uri_strv[i]);
			}
		}
		cria_main_window_open_presentations_from_uri_list(CRIA_MAIN_WINDOW(widget), uris);
		/* Clean up */
		g_strfreev(uri_strv);
		uri_strv = NULL;
		g_slist_free(uris);
		uris = NULL;
		success = TRUE;
		break;
	default:		/* This should never happen */
		g_warning("Got unknown drag data.");
		break;
	}

	/* Tell the system that we are finished */
	gtk_drag_finish(drag_context, success, del, time);
	cdebug("cb_drag_data_received()", "end");

}

static void
cmw_close(CriaMainWindow* self) {
#warning "MainWindow::close(): FIXME: ask whether we need to save"
	cria_application_unregister_window(self);

#warning "MainWindow::close(): FIXME: is this neccessary or does the window get disposed automatically on unregister?"
	gtk_widget_destroy(GTK_WIDGET(self));
}

/* callback to close a window without quitting the application */
static gboolean
cmw_delete_event(GtkWidget *widget, GdkEventAny *event) {
	g_return_val_if_fail(CRIA_IS_MAIN_WINDOW(widget), FALSE);

	cmw_close(CRIA_MAIN_WINDOW(widget));

	return TRUE;
}

static void
main_window_recent_manager_changed(GtkRecentManager *recent_manager, CriaMainWindow *main_window) {
	GtkWidget       *menu,
			*recent_menu_item;
	gchar	       **uris;
	gsize		 length;

	g_return_if_fail(GTK_IS_RECENT_MANAGER(recent_manager));
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(main_window));

	recent_menu_item = gtk_ui_manager_get_widget(main_window->priv->ui_manager, "/menubar/FileMenu/RecentDocuments");
	menu = gtk_menu_item_get_submenu(GTK_MENU_ITEM(recent_menu_item));

	uris = gtk_recent_chooser_get_uris(GTK_RECENT_CHOOSER(menu), &length);
	g_strfreev(uris);

	gtk_widget_set_sensitive(recent_menu_item, length > 0);
}

static void
main_window_recent_item_activated(GtkRecentChooser *chooser, CriaMainWindow *main_window) {
	GtkRecentInfo *recent_info;
	const gchar   *text_uri;

	g_return_if_fail(GTK_IS_RECENT_CHOOSER(chooser));
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(main_window));

	recent_info = gtk_recent_chooser_get_current_item(chooser);
	text_uri = gtk_recent_info_get_uri(recent_info);
	gtk_recent_info_unref(recent_info);

	cria_main_window_set_presentation_from_text_uri(main_window, text_uri);
}

static void
main_window_update_recent_manager(CriaMainWindow *self, GdkScreen *screen) {
	GtkWidget       *menu,
			*recent_menu_item;
	GtkRecentFilter *filter;

	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(GDK_IS_SCREEN(screen));

	if (self->priv->recent_manager != NULL) {
		g_signal_handlers_disconnect_by_func(G_OBJECT (self->priv->recent_manager),
				                     G_CALLBACK (main_window_recent_manager_changed), self);
	}
	self->priv->recent_manager = gtk_recent_manager_get_for_screen(screen);
	g_signal_connect(G_OBJECT(self->priv->recent_manager), "changed",
			 G_CALLBACK(main_window_recent_manager_changed), self);

	menu = gtk_recent_chooser_menu_new_for_manager(self->priv->recent_manager);
	g_signal_connect(G_OBJECT(menu), "item-activated", G_CALLBACK(main_window_recent_item_activated), self);
	filter = gtk_recent_filter_new();
	gtk_recent_filter_add_application(filter, g_get_application_name());
	gtk_recent_chooser_set_filter(GTK_RECENT_CHOOSER(menu), filter);
	gtk_recent_chooser_set_sort_type(GTK_RECENT_CHOOSER(menu),GTK_RECENT_SORT_MRU);

	recent_menu_item = gtk_ui_manager_get_widget(self->priv->ui_manager, "/menubar/FileMenu/RecentDocuments");
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(recent_menu_item), menu);

	main_window_recent_manager_changed (self->priv->recent_manager, self);
}

static void
main_window_realize(GtkWidget *widget) {
	CriaMainWindow *self;
	GdkScreen      *screen;

	g_return_if_fail(CRIA_IS_MAIN_WINDOW(widget));

	GTK_WIDGET_CLASS(cria_main_window_parent_class)->realize(widget);

	self = CRIA_MAIN_WINDOW(widget);
	screen = gtk_widget_get_screen(widget);

	main_window_update_recent_manager(self, screen);
}

static void
main_window_screen_changed(GtkWidget *widget, GdkScreen *screen) {
	CriaMainWindow *self;

	g_return_if_fail(CRIA_IS_MAIN_WINDOW(widget));

	self = CRIA_MAIN_WINDOW(widget);

	main_window_update_recent_manager(self, screen);

	GTK_WIDGET_CLASS(cria_main_window_parent_class)->screen_changed(widget, screen);
}

static void
cria_main_window_class_init(CriaMainWindowClass* self_class) {
	GObjectClass	* go_class;
	GtkWidgetClass  * gw_class;

	/* setting up the object class */
	go_class = G_OBJECT_CLASS(self_class);
	go_class->finalize     = cmw_finalize;
	go_class->get_property = cmw_get_property;
	go_class->set_property = cmw_set_property;

	g_object_class_install_property(go_class,
					PROP_PRESENTATION,
					g_param_spec_object("presentation",
							    "Presentation",
							    "This is the presentation that's currently assotiated with this main window",
							    CRIA_TYPE_PRESENTATION,
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property(go_class,
					PROP_SLIDE,
					g_param_spec_object("slide",
							    "Slide",
							    "The slide being rendered/editable by this window",
							    CRIA_TYPE_SLIDE,
							    G_PARAM_READWRITE));

	/* setting up the widget class */
	gw_class = GTK_WIDGET_CLASS(self_class);
	gw_class->configure_event    = cmw_configure_event;
	gw_class->drag_data_received = cmw_drag_data_received;
	gw_class->delete_event       = cmw_delete_event;
	gw_class->realize            = main_window_realize;
	gw_class->screen_changed     = main_window_screen_changed;
	gw_class->window_state_event = cmw_window_state_event;
}

static void
cmw_set_slide(CriaMainWindow* self, CriaSlide* slide) {
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(CRIA_IS_SLIDE(slide));
	
	if(self->slide == slide) {
		return;
	}
	
	if(self->slide) {
		cria_slide_view_unregister(CRIA_SLIDE_VIEW(self), self->slide);
		g_object_unref(self->slide);
		self->slide = NULL;
	}

	if(slide) {
		self->slide = g_object_ref(slide);
		cria_slide_view_register(CRIA_SLIDE_VIEW(self), slide);

		cria_slide_display_set_slide(CRIA_SLIDE_DISPLAY(self->priv->slide_editor), slide);
		cria_title_bar_set_slide(CRIA_TITLE_BAR(self->priv->title_bar), slide);

		/* update the slide selection */
		cria_sidebar_set_selected(CRIA_SIDEBAR(self->priv->slide_list), slide);
	}

	g_object_notify(G_OBJECT(self), "slide");
}

CriaPresentation*
cria_main_window_get_presentation(CriaMainWindow* self) {
	g_return_val_if_fail (CRIA_IS_MAIN_WINDOW(self), NULL);
	
	return self->priv->presentation;
}

static void
cmw_zoom_activated(CriaMainWindow* self, GOActionComboText* act) {
	gdouble       zoom;
	const gchar * text;

	g_return_if_fail(IS_GO_ACTION_COMBO_TEXT(act));

	text = go_action_combo_text_get_entry(act);

	cdebugo(self, "zoomActivated()", "Combo: %s", text);
	zoom = strtod(text, NULL);
	cdebugo(self, "zoomActivated()", "Zoom: %f%%", zoom);
	zoom = zoom / 100.0;
	cdebugo(self, "zoomActivated()", "Zoom: %f", zoom);

	if(zoom >= 0.01) {
		cria_slide_display_set_slide_zoom(CRIA_SLIDE_DISPLAY(self->priv->slide_editor), zoom);
		cria_application_set_zoom(text);
	}
}

static void
cmw_init_zoom(CriaMainWindow* self) {
	static const gchar * const presets[] = {
		"100%",
		"75%",
		"50%",
		"25%",
		NULL
	};
	gint i = 0;
	self->priv->zoom = g_object_new(go_action_combo_text_get_type(),
					"name", "Zoom",
					"label", _("Zoom"),
					NULL);
	go_action_combo_text_set_width(GO_ACTION_COMBO_TEXT(self->priv->zoom), "10000%");
	for(i = 0; presets[i]; i++) {
		go_action_combo_text_add_item(GO_ACTION_COMBO_TEXT(self->priv->zoom), presets[i]);
	}
	g_signal_connect_swapped(self->priv->zoom, "activate",
				 G_CALLBACK(cmw_zoom_activated), self);
	gtk_action_group_add_action(self->priv->action_group, self->priv->zoom);
}

static void
cmw_font_selection_set_sensitive(CriaMainWindow* self, gboolean sensitive) {
	gtk_action_set_sensitive(self->priv->font_family, sensitive);
	gtk_action_set_sensitive(self->priv->font_size, sensitive);

	gtk_action_set_sensitive(gtk_action_group_get_action(self->priv->action_group, "FormatFontBold"), sensitive);
	gtk_action_set_sensitive(gtk_action_group_get_action(self->priv->action_group, "FormatFontItalic"), sensitive);
	/* disable underline until we can internally handle this */
	gtk_action_set_sensitive(gtk_action_group_get_action(self->priv->action_group, "FormatFontUnderline"), FALSE /*sensitive*/);
}

static void
cmw_enable_font_selection(CriaMainWindow* self) {
	cmw_font_selection_set_sensitive(self, TRUE);
}

static void
cmw_disable_font_selection(CriaMainWindow* self) {
	cmw_font_selection_set_sensitive(self, FALSE);
}

static CriaBlockRenderer*
get_focused_block_renderer (CriaMainWindow const* self)
{
	return cria_block_renderer_get_block (
			CRIA_BLOCK_RENDERER (
				rd_canvas_get_focus (RD_CANVAS (self->priv->slide_editor))
			));
}

static void
cmw_set_font_family(CriaMainWindow* self, GOActionComboText* act) {
	cria_block_set_font_family(get_focused_block_renderer (self),
				   go_action_combo_text_get_entry(act));
}

static void
cmw_set_font_size(CriaMainWindow* self, GOActionComboText* act) {
	gint size = atoi(go_action_combo_text_get_entry(act)) * MASTER_COORDINATES_PER_POINT;
	
	cria_block_set_font_size_int(get_focused_block_renderer (self),
				     size);
}

static void
cmw_init_font(CriaMainWindow* self) {
        GSList* list, *it;
        self->priv->font_family = g_object_new(go_action_combo_text_get_type(),
                                                "name", "SelectFontFamily",
                                                NULL);
	g_signal_connect_swapped(self->priv->font_family, "activate",
				 G_CALLBACK(cmw_set_font_family), self);

        list = go_fonts_list_families(gtk_widget_get_pango_context(GTK_WIDGET(self)));
        for(it = list; it; it = it->next) {
                go_action_combo_text_add_item(GO_ACTION_COMBO_TEXT(self->priv->font_family), it->data);
                g_free(it->data);
        }
        g_slist_free(list);

        self->priv->font_size = g_object_new(go_action_combo_text_get_type(),
                                                "name", "SelectFontSize",
                                                NULL);
        go_action_combo_text_set_width(GO_ACTION_COMBO_TEXT(self->priv->font_size), "1280");
	g_signal_connect_swapped(self->priv->font_size, "activate",
				 G_CALLBACK(cmw_set_font_size), self);

        list = go_fonts_list_sizes();
        for(it = list; it; it = it->next) {
                gchar* text = g_strdup_printf("%d", GPOINTER_TO_INT(it->data) / PANGO_SCALE);
                go_action_combo_text_add_item(GO_ACTION_COMBO_TEXT(self->priv->font_size), text);
                g_free(text);
        }
        g_slist_free(list);

        gtk_action_group_add_action(self->priv->action_group, self->priv->font_family);
        gtk_action_group_add_action(self->priv->action_group, self->priv->font_size);

	cmw_disable_font_selection(self);
}

static gboolean
cmw_store_hpaned_pos(CriaMainWindow* self) {
	cria_application_set_sidebar_width(gtk_paned_get_position(GTK_PANED(self->priv->hpaned)));
	return FALSE;
}

static void
cmw_hpaned_resized(CriaMainWindow* self) {
	if(self->priv->paned_timeout_id) {
		g_source_remove(self->priv->paned_timeout_id);
	}

	self->priv->paned_timeout_id = g_timeout_add(100, (GSourceFunc)cmw_store_hpaned_pos, self);
}

static gboolean
cmw_store_hpaned_templates_pos(CriaMainWindow* self) {
	cria_application_set_templates_width(gtk_paned_get_position(GTK_PANED(self->priv->hpaned_templates)));
	return FALSE;
}

static void
cmw_hpaned_templates_resized(CriaMainWindow* self) {
	if(self->priv->paned_timeout_id_templates) {
		g_source_remove(self->priv->paned_timeout_id_templates);
	}

	self->priv->paned_timeout_id_templates = g_timeout_add(100, (GSourceFunc)cmw_store_hpaned_templates_pos, self);
}

static gboolean
cmw_menu_enter(CriaMainWindow* self, GdkEventCrossing* ev, GtkWidget* widget) {
	if(!self->priv->status_context) {
		self->priv->status_context = gtk_statusbar_get_context_id(GTK_STATUSBAR(self->priv->status_bar), "main-window");
	}
	gtk_statusbar_push(GTK_STATUSBAR(self->priv->status_bar),
			   self->priv->status_context,
			   g_object_get_data(G_OBJECT(widget), "tooltip"));
	return FALSE;
}

static gboolean
cmw_menu_leave(CriaMainWindow* self, GdkEventCrossing* ev, GtkWidget* widget) {
	gtk_statusbar_pop(GTK_STATUSBAR(self->priv->status_bar), self->priv->status_context);
	return FALSE;
}

static void
cmw_action_group_connect_proxy(CriaMainWindow* self, GtkAction* action, GtkWidget* proxy, GtkActionGroup ag) {
	const gchar* tooltip;
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(GTK_IS_WIDGET(proxy));
	g_return_if_fail(GTK_IS_ACTION(action));

	if(GTK_IS_MENU_ITEM(proxy)) {
		g_object_get(action, "tooltip", &tooltip, NULL);
		if(tooltip) {
			g_object_set_data(G_OBJECT(proxy), "tooltip", g_strdup(tooltip));
			g_signal_connect_swapped(proxy, "enter-notify-event",
						 G_CALLBACK(cmw_menu_enter), self);
			g_signal_connect_swapped(proxy, "leave-notify-event",
						 G_CALLBACK(cmw_menu_leave), self);
		}
	}
}

static void
cmw_set_show_tearoff(CriaMainWindow* self) {
	gtk_ui_manager_set_add_tearoffs(self->priv->ui_manager, cria_application_get_show_tearoff());
}

static void
add_widget(CriaMainWindow* self, GtkWidget* widget, gboolean detachable) {
	if(detachable) {
		GtkWidget* handle = gtk_handle_box_new();
		gtk_container_add(GTK_CONTAINER(handle), widget);
		gtk_widget_show_all(handle);
		gtk_box_pack_start(GTK_BOX(self->priv->vbox), handle, FALSE, FALSE, 0);
	} else {
		gtk_box_pack_start(GTK_BOX(self->priv->vbox), widget, FALSE, FALSE, 0);
	}
}

static void
pack_menubar(CriaMainWindow* self) {
	add_widget(self, gtk_ui_manager_get_widget(self->priv->ui_manager, "/menubar"), cria_application_get_detach_menubars());
}

static void
pack_toolbar(CriaMainWindow* self, const gchar* toolbarname) {
	add_widget(self, gtk_ui_manager_get_widget(self->priv->ui_manager, toolbarname), cria_application_get_detach_toolbars());
}

static gboolean
cmw_se_focus_changed (CriaMainWindow* self,
		      RdItem        * focus,
		      RdCanvas      * canvas)
{
	if(CRIA_IS_BLOCK_RENDERER(focus)) {
		gchar   * text;
		gboolean  value;

		/* enable font selection */
		cmw_enable_font_selection(self);

		/* set font selection properly */
		text = cria_format_get(cria_block_get_format(cria_block_renderer_get_block(CRIA_BLOCK_RENDERER(focus))),
				       "font-family");
		if(text) {
			go_action_combo_text_set_entry(GO_ACTION_COMBO_TEXT(gtk_action_group_get_action(self->priv->action_group, "SelectFontFamily")),
					       text, GO_ACTION_COMBO_SEARCH_NEXT);
			g_free(text);
		}

		text = cria_format_get(cria_block_get_format(cria_block_renderer_get_block(CRIA_BLOCK_RENDERER(focus))),
				       "font-size");
		if(text) {
			gchar* it;
			for(it = text; it && g_unichar_isdigit(g_utf8_get_char(it)); it = g_utf8_next_char(it)) {}
			*it = '\0';
			go_action_combo_text_set_entry(GO_ACTION_COMBO_TEXT(gtk_action_group_get_action(self->priv->action_group, "SelectFontSize")),
					       text, GO_ACTION_COMBO_SEARCH_NEXT);
			g_free(text);
		}

		text = cria_format_get(cria_block_get_format(cria_block_renderer_get_block(CRIA_BLOCK_RENDERER(focus))),
				       "font-weight");
		value = FALSE;
		if(text) {
			if(atoi(text)) {
				value = atoi(text) >= PANGO_WEIGHT_BOLD;
			} else if(!strcmp(text, "bold")) {
				value = TRUE;
			}
			g_free(text);
		}

		gtk_toggle_action_set_active(GTK_TOGGLE_ACTION(gtk_action_group_get_action(self->priv->action_group, "FormatFontBold")),
					     value);

		text = cria_format_get(cria_block_get_format(cria_block_renderer_get_block(CRIA_BLOCK_RENDERER(focus))), "font-style");
		value = FALSE;
		if(text) {
			if(!strcmp(text, "italic")) {
				value = TRUE;
			} else if(!strcmp(text, "oblique")) {
				value = TRUE;
			} else if(!strcmp(text, "normal")) {
				/* keep value FALSE */
			}
			g_free(text);
		}
		gtk_toggle_action_set_active(GTK_TOGGLE_ACTION(gtk_action_group_get_action(self->priv->action_group, "FormatFontItalic")),
					     value);
#warning "focusChanged(): FIXME: enable underline"
/*		gtk_toggle_action_set_active(GTK_TOGGLE_ACTION(gtk_action_group_get_action(self->priv->action_group, "FormatFontUnderline")),
					     (pango_font_description_get_set_fields(desc) & PANGO_FONT_MASK_WEIGHT) &&
					     (pango_font_description_get_weight(desc) > PANGO_WEIGHT_LIGHT));*/
	} else {
		/* disable font selection */
		cmw_disable_font_selection(self);
	}
	return FALSE;
}

static void
cmw_set_master_slide(CriaMainWindow* self, CriaSlide* master_slide) {
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(!master_slide || CRIA_IS_SLIDE(master_slide));

	cria_slide_set_master_slide(self->slide, master_slide);
}

static void
cria_main_window_init(CriaMainWindow* self) {
	GtkWidget	* helper,
			* scrolled,
			* text_pane;
	GError		* error = NULL;

	static GtkTargetEntry drag_types[] = {
		{"text/uri-list", 0, TARGET_URI_LIST },
	};

	cdebugo(self, "init()", "start");

	/* creating the private data */
	self->priv = g_new0(CriaMainWindowPriv,1);

	/* glade user interface description */
	self->priv->xml = glade_xml_new(PACKAGE_DATA_DIR "/" PACKAGE "/" "data/criawips.glade", "main_vbox", NULL);

	if(!self->priv->xml) {
		GtkWidget* error_dialog = gtk_message_dialog_new(NULL, 0,
								 GTK_MESSAGE_ERROR,
								 GTK_BUTTONS_CLOSE,
								 _("Error inializing the Window"));
		gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(error_dialog),
							 _("The window could not be loaded. "
							   "Please make sure the installation of criawips is complete."));
		gtk_dialog_run(GTK_DIALOG(error_dialog));
		gtk_widget_destroy(error_dialog);
		exit(1);
	}

	/* GtkAction UI API */
	self->priv->ui_manager = gtk_ui_manager_new();
	self->priv->action_group = gtk_action_group_new("Actions");
	g_signal_connect_swapped(self->priv->action_group, "connect-proxy",
				 G_CALLBACK(cmw_action_group_connect_proxy), self);
	gtk_action_group_set_translation_domain(self->priv->action_group, GETTEXT_PACKAGE);

	cmw_init_actions(self);
	cmw_init_font(self);
	cmw_init_zoom(self);
	gtk_ui_manager_insert_action_group(self->priv->ui_manager, self->priv->action_group, 0);
	gtk_window_add_accel_group(GTK_WINDOW(self),
				   gtk_ui_manager_get_accel_group(self->priv->ui_manager));
	gtk_ui_manager_add_ui_from_file(self->priv->ui_manager, PACKAGE_DATA_DIR "/" PACKAGE "/data/criawips.ui", &error);

	if(error) {
#warning "init(): FIXME: add a critical mode to the error dialog to be able to quit the application"
		gchar* message = g_strdup_printf("%s\n\n<span style=\"italic\">%s</span>",
						 _("An important file could not be loaded. Maybe your installation is broken."),
						 error->message);

		cria_application_show_error_dialog(GTK_WINDOW(self),
						   _("Failed to load the User Interface"),
						   message);
		g_free(message);
		g_error_free(error);
		criawips_quit();
	}

	self->priv->vbox = glade_xml_get_widget(self->priv->xml, "main_vbox");
	gtk_container_add(GTK_CONTAINER(self), self->priv->vbox);

	/* master slide menus */
	self->theme_actions = cria_theme_actions_new(self->priv->ui_manager, "/menubar/MasterSlideMenu/MasterSlideApply");
	g_signal_connect_swapped(self->theme_actions, "selected",
				 G_CALLBACK(cmw_set_master_slide), self);

	/* build the ui */
	pack_menubar(self);
	pack_toolbar(self, "/toolbar1");
	pack_toolbar(self, "/toolbar2");
	
	self->priv->hpaned = glade_xml_get_widget(self->priv->xml, "main_hpaned");
	self->priv->hpaned_templates = glade_xml_get_widget(self->priv->xml, "main_hpaned_templates");

	/* the text pane over the slide list */
	self->text_pane_group = gtk_size_group_new(GTK_SIZE_GROUP_VERTICAL);
	text_pane = cria_text_pane_new(_("Slides"));
	gtk_size_group_add_widget(self->text_pane_group, text_pane);
	gtk_widget_show(text_pane);
	
	/* the slide list */
	self->priv->slide_list = cria_sidebar_new(NULL);
	gtk_widget_show(self->priv->slide_list);
	
	/* the scrolled window to put the slide list into */
	scrolled = glade_xml_get_widget(self->priv->xml, "main_scrolled_list");
	gtk_container_add(GTK_CONTAINER(scrolled), self->priv->slide_list);
	scrolled = NULL;

	/* the vbox containing the slide list and the text pane */
	self->priv->sidebar = glade_xml_get_widget(self->priv->xml, "main_vbox_list");
	gtk_box_pack_start(GTK_BOX(self->priv->sidebar), text_pane, FALSE, FALSE, 0);
	gtk_toggle_action_set_active(GTK_TOGGLE_ACTION(gtk_action_group_get_action(self->priv->action_group, "ViewSidebar")),
				     cria_application_get_show_sidebar());
	gtk_paned_pack1(GTK_PANED(self->priv->hpaned), self->priv->sidebar,
			TRUE, FALSE);
	text_pane = NULL;

	/* the templates list */
	text_pane = cria_text_pane_new(_("Templates"));
	gtk_size_group_add_widget(self->text_pane_group, text_pane);
	gtk_widget_show(text_pane);

	self->priv->templates = cria_template_pane_new();
	gtk_widget_show(self->priv->templates);

	self->priv->sidebar_templates = glade_xml_get_widget(self->priv->xml, "main_vbox_templates");
	gtk_box_pack_start(GTK_BOX(self->priv->sidebar_templates), text_pane, FALSE, FALSE, 0);
	gtk_toggle_action_set_active(GTK_TOGGLE_ACTION(gtk_action_group_get_action(self->priv->action_group, "ViewTemplates")),
				     cria_application_get_templates_visible());
	scrolled = glade_xml_get_widget(self->priv->xml, "main_scrolled_templates");
	gtk_container_add(GTK_CONTAINER(scrolled), self->priv->templates);

	/* the vpaned for the title bar and the editor */
	helper = glade_xml_get_widget(self->priv->xml, "main_vbox_editor");

	/* the title bar */
	/* we could to this by getting the selection of the SlideList */
	self->priv->title_bar = cria_title_bar_new(NULL);
	gtk_size_group_add_widget(self->text_pane_group, self->priv->title_bar);
	gtk_widget_show(self->priv->title_bar);
	gtk_box_pack_start(GTK_BOX(helper), self->priv->title_bar, FALSE, FALSE, 0);
	helper = NULL;

	/* slide editor */
	self->priv->slide_editor = cria_slide_editor_new(NULL);
	gtk_widget_show(self->priv->slide_editor);
	gtk_container_add(GTK_CONTAINER(glade_xml_get_widget(self->priv->xml, "main_scrolled_editor")),
			  self->priv->slide_editor);

	g_signal_connect_swapped(self->priv->slide_editor, "focus-changed",
				 G_CALLBACK(cmw_se_focus_changed), self);

	/* status bar */
	self->priv->status_bar = glade_xml_get_widget(self->priv->xml, "main_statusbar");

	/* initializing Drag'n'Drop */
	gtk_drag_dest_set(GTK_WIDGET(self), GTK_DEST_DEFAULT_ALL,
			  drag_types, G_N_ELEMENTS(drag_types), GDK_ACTION_COPY);

	/* connecting signals */
	/* slide list */
#warning "MainWindow::init(): FIXME: make this one nicer"
	g_signal_connect_swapped(gtk_tree_view_get_selection(GTK_TREE_VIEW(self->priv->slide_list)), "changed",
				 G_CALLBACK(cria_main_window_selected_slide_changed), self);

	/* Paneds */
	gtk_paned_set_position(GTK_PANED(self->priv->hpaned), cria_application_get_sidebar_width());
	g_signal_connect_swapped(self->priv->hpaned, "notify::position",
				 G_CALLBACK(cmw_hpaned_resized), self);

	gtk_paned_set_position(GTK_PANED(self->priv->hpaned_templates), cria_application_get_templates_width());
	g_signal_connect_swapped(self->priv->hpaned_templates, "notify::position",
				 G_CALLBACK(cmw_hpaned_templates_resized), self);

	/* slide view */
	g_signal_connect_swapped(cria_application_get_instance(), "notify::show-tearoff",
				 G_CALLBACK(cmw_set_show_tearoff), self);
	cmw_set_show_tearoff(self);

	/* update the last stuff */
#warning "MainWindow::init(): FIXME: set the zoom value from the presentation and zoom to auto-fit for new presentations"
	if(cria_application_get_zoom()) {
		go_action_combo_text_set_entry(GO_ACTION_COMBO_TEXT(self->priv->zoom), cria_application_get_zoom(), GO_ACTION_COMBO_SEARCH_NEXT);
	} else {
		go_action_combo_text_set_entry(GO_ACTION_COMBO_TEXT(self->priv->zoom), "75%", GO_ACTION_COMBO_SEARCH_NEXT);
	}
	cmw_zoom_activated(self, GO_ACTION_COMBO_TEXT(self->priv->zoom));

	self->priv->status_drag_to_create = gtk_statusbar_get_context_id(GTK_STATUSBAR(self->priv->status_bar), "drag-and-drop-commands");

	cdebugo(self, "init()", "end");
}

static void
cria_main_window_insert_slide (CriaMainWindow* self)
{
	CriaTheme* theme;
	CriaSlide* slide;
	CriaSlide* master_slide = NULL;
	gint	   pos = 0;

	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));

	/* get the current slide */
	slide = cria_slide_display_get_slide(CRIA_SLIDE_DISPLAY(self->priv->slide_editor));
	pos = cria_slide_list_index(CRIA_SLIDE_LIST(self->priv->presentation), slide) + 1;

	slide = cria_slide_new_pos(CRIA_SLIDE_LIST(self->priv->presentation), pos);

	theme = cria_presentation_get_theme (self->priv->presentation);
	if (theme) {
		master_slide = cria_theme_get_default_slide (theme);
	}

	if (master_slide) {
		cria_slide_set_master_slide (CRIA_SLIDE (slide),
					     master_slide);
	}
}

static GtkWidget*
cmw_new_presentation(CriaPresentation* presentation) {
	CriaMainWindow	* self;

	g_return_val_if_fail(CRIA_IS_PRESENTATION(presentation), NULL);

	self = g_object_new(CRIA_TYPE_MAIN_WINDOW, "presentation", presentation, NULL);
	cria_application_register_window(self);
	g_object_unref(presentation);

	return GTK_WIDGET(self);
}

/**
 * cria_main_window_new:
 * @filename: filename of the new window
 *
 * Creates a new main window displaying the presentation that @filename
 * points to. It automatically registers the window for the application
 * to make it managed by the application. @filename must not be NULL.
 *
 * Returns a new main window
 */
GtkWidget*
cria_main_window_new(const gchar* filename) {
	CriaPresentation* presentation;
	GType		  type = CRIA_TYPE_MAIN_WINDOW;
	GError		* error = NULL;

	g_return_val_if_fail(filename, NULL);

	cdebugt(type, "new()", "got filename 0x%x(\"%s\")", (uintptr_t)filename, filename);
	
	presentation = cria_presentation_new_from_file(filename, &error);

	if(error) {
		/* NOTE: keep these strings in sync with the ones from application.c */
		gchar* error_message = g_strdup_printf("%s %s\n<span style=\"italic\">%s</span>.",
						       _("For some reason the presentation you wanted to open could not be opened."),
						       _("The reason was:"),
						       error->message);

		cria_application_show_error_dialog(NULL, _("The Presentation could not be opened"), error_message);
		g_free(error_message);
		g_error_free(error);
	}

	return cmw_new_presentation(presentation);
}

GtkWidget*
cria_main_window_new_default(void) {
	return cmw_new_presentation(cria_presentation_new_default());
}

GtkWidget*
cria_main_window_new_uri(const gchar* uri) {
#warning "newUri(): FIXME: add error handling"
	return cmw_new_presentation(cria_presentation_new_from_text_uri(uri, NULL));
}

static void
cria_main_window_open(CriaMainWindow* self, gpointer d) {
	GtkFileFilter	* filter;
	GtkWidget	* dialog = gtk_file_chooser_dialog_new(_("Open Presentation"),
							       GTK_WINDOW(self),
							       GTK_FILE_CHOOSER_ACTION_OPEN,
							       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
							       GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
							       NULL);
	gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(dialog), TRUE);
	filter = gtk_file_filter_new();
	gtk_file_filter_add_pattern(filter, "*.criawips");
	gtk_file_filter_set_name(filter, _("Criawips Presentations"));
	gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog),
				    filter);

	if(gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
		GSList* uris;
		gtk_widget_hide_now(dialog);
		uris = gtk_file_chooser_get_uris(GTK_FILE_CHOOSER(dialog));

		cria_main_window_open_presentations_from_uri_list(self, uris);
	}

	gtk_object_destroy(GTK_OBJECT(dialog));
}

static void
cria_main_window_save_as(CriaMainWindow* self) {
	GtkWidget* dialog;
	gchar    * default_name;
	gboolean done = FALSE;

	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));

	dialog = gtk_file_chooser_dialog_new(_("Save Presentation"),
					     GTK_WINDOW(self),
					     GTK_FILE_CHOOSER_ACTION_SAVE,
					     GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					     GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
					     NULL);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog),
					 GTK_RESPONSE_ACCEPT);
	default_name = cria_presentation_get_filename(cria_main_window_get_presentation(self));
	gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), default_name);
	g_free(default_name);

	do {
		if(gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_CANCEL) {
			done = TRUE;
		} else {
			/* got GTK_RESPONSE_ACCEPT */
			GError     * error = NULL;
			GnomeVFSURI* uri;
			GnomeVFSURI* old_uri;
			gchar      * text_uri;
			gboolean     write = TRUE;

			gtk_widget_hide_now(dialog);

			text_uri = gtk_file_chooser_get_uri(GTK_FILE_CHOOSER(dialog));
			uri = gnome_vfs_uri_new(text_uri);
			g_free(text_uri);

			old_uri = cria_presentation_get_uri(cria_main_window_get_presentation(self));
			if(old_uri) {
				old_uri = gnome_vfs_uri_ref(old_uri);
			}

			if(write && gnome_vfs_uri_exists(uri) && (!old_uri || !gnome_vfs_uri_equal(uri, old_uri))) {
				GtkWidget* qdialog = gtk_message_dialog_new(GTK_WINDOW(self),
									    GTK_DIALOG_DESTROY_WITH_PARENT,
									    GTK_MESSAGE_WARNING,
									    GTK_BUTTONS_NONE,
									    _("The specified file already exists, you can replace it."));
				gchar* basename = gnome_vfs_uri_extract_short_name(uri);
				gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(qdialog),
#warning "FIXME: generate this message dynamically so that it always matches"
								      _("The file \"%s\" already exists. If you are sure you want to overwrite it "
									"press \"Replace\", if you are unsure press \"Cancel\" and select a new "
									"filename."),
								      basename);
				gtk_dialog_add_buttons(GTK_DIALOG(qdialog),
						       GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
						       _("Replace"), GTK_RESPONSE_ACCEPT,
						       NULL);
				switch(gtk_dialog_run(GTK_DIALOG(qdialog))) {
				case GTK_RESPONSE_ACCEPT:
					write = TRUE;
					break;
				case GTK_RESPONSE_REJECT:
					write = FALSE;
					done = FALSE;
					break;
				}
				gtk_widget_hide_now(qdialog);
				g_free(basename);
				gtk_object_destroy(GTK_OBJECT(qdialog));
			}

			if(write) {
				cria_presentation_save_to_file(cria_main_window_get_presentation(self), uri, &error);

				if(error) {
					gchar* message = g_strdup_printf("%s\n\n<span style=\"italic\">%s</span>",
									 _("The presentation could not be saved. This usually means that "
									   "there was an error with access permissions."),
									 error->message);
					cria_application_show_error_dialog(GTK_WINDOW(self),
									   _("Could not save the Presentation"),
									   message);
					g_error_free(error);
				} else {
					gchar* text_uri = gnome_vfs_uri_to_string(uri, GNOME_VFS_URI_HIDE_PASSWORD);
					cria_main_window_add_to_recent(self, text_uri, cria_main_window_get_presentation(self));
					cria_presentation_set_uri(cria_main_window_get_presentation(self), uri);
					g_free(text_uri);
				}

				done = TRUE;
			}

			gnome_vfs_uri_unref(uri);
			if(old_uri) {
				gnome_vfs_uri_unref(old_uri);
			}
		}
	} while (!done);

	gtk_object_destroy(GTK_OBJECT(dialog));
}

static void
cria_main_window_save(CriaMainWindow* self) {
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(CRIA_IS_PRESENTATION(cria_main_window_get_presentation(self)));
	
	if(!cria_presentation_get_uri(cria_main_window_get_presentation(self))) {
		/* we don't know a filename, so lets ask for one */
		cria_main_window_save_as(self);
	} else {
		GError* error = NULL;
		cria_presentation_save_to_file(cria_main_window_get_presentation(self), cria_presentation_get_uri(cria_main_window_get_presentation(self)), &error);

		if(error) {
#warning "save(): FIXME: add error handling"
			g_error_free(error);
		} else {
			gchar* text_uri = gnome_vfs_uri_to_string(cria_presentation_get_uri(cria_main_window_get_presentation(self)), GNOME_VFS_URI_HIDE_PASSWORD);
			cria_main_window_add_to_recent(self, text_uri, cria_main_window_get_presentation(self));
			g_free(text_uri);
		}
	}
}

static void
cria_main_window_selected_slide_changed(CriaMainWindow* self, GtkTreeSelection* selection) {
	CriaSlide	* slide;

	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));

	slide = cria_sidebar_get_selected(CRIA_SIDEBAR(self->priv->slide_list));
	if(slide) {
		cmw_set_slide(self, slide);
	}
}

void
cria_main_window_add_to_recent(CriaMainWindow *self, const gchar *text_uri, CriaPresentation *presentation) {
	GtkRecentData  data;
	gchar         *groups[] = { "Office", "Presentation", NULL };

	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(GTK_WIDGET_REALIZED(self));
	g_return_if_fail(text_uri != NULL);

	if (*cria_presentation_get_title(presentation)) {
		data.display_name = g_strdup(cria_presentation_get_title(presentation));
	} else {
		data.display_name = g_path_get_basename(text_uri);
	}
	data.description = NULL; // FIXME: use presentation description
	data.mime_type = gnome_vfs_get_mime_type(text_uri);
	data.app_name = g_strdup(g_get_application_name());
	data.app_exec = g_strjoin(" ", g_get_prgname(), "%u", NULL);
	data.groups = groups;

	gtk_recent_manager_add_full(self->priv->recent_manager, text_uri, &data);

	g_free(data.display_name);
	g_free(data.mime_type);
	g_free(data.app_name);
	g_free(data.app_exec);
}

/**
 * cria_main_window_set_geometry:
 * @self: a #CriaMainWindow
 *
 * Get the geometry from the application and apply it to the main window.
 */
void
cria_main_window_set_geometry(CriaMainWindow* self) {
	gchar* geometry;
	guint  w,h;

	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));

	geometry = cria_application_get_default_geometry();

	if(geometry && geometry[0] != '\0') {
		gtk_window_parse_geometry(GTK_WINDOW(self), geometry);
		g_free(geometry);
		gtk_widget_show(GTK_WIDGET(self));
	} else {
		w = 400;
		h = 300;
		gtk_window_set_default_size(GTK_WINDOW(self), w, h);
		gtk_widget_show(GTK_WIDGET(self));
	}
}

/**
 * cria_main_window_set_presentation:
 * @self: a #CriaMainWindow
 * @presentation: The presentation to connect to this window. Must not be NULL
 *
 * Specify the presentation to be displayed and edited within this window.
 */
void
cria_main_window_set_presentation(CriaMainWindow* self, CriaPresentation* presentation) {
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(CRIA_IS_PRESENTATION(presentation));
	
#warning "MainWindow::setPresentation(): connect to notify::uri signal"
	
	cdebugo(self, "setPresentation()", "start");
	
	if(self->priv->presentation != NULL) {
		cria_theme_actions_set_theme(self->theme_actions, NULL);
		cria_presentation_view_unregister(CRIA_PRESENTATION_VIEW(self), self->priv->presentation);
		g_object_unref(self->priv->presentation);
		self->priv->presentation = NULL;
	}

	cdebugo(self, "setPresentation()", "cleared the old value, now setting the new one");

	self->priv->presentation = g_object_ref(presentation);
	cria_presentation_view_register(CRIA_PRESENTATION_VIEW(self), self->priv->presentation);
	cria_sidebar_set_presentation(CRIA_SIDEBAR(self->priv->slide_list),
				      cria_main_window_get_presentation(self));
	cmw_set_slide(self, cria_slide_list_get(CRIA_SLIDE_LIST(cria_main_window_get_presentation(self)), 0));

	g_object_notify(G_OBJECT(self), "presentation");
	cdebugo(self, "setPresentation()", "end");
}

void
cria_main_window_show_about(CriaMainWindow* self) {
#warning "MainWindow::showAbout(): FIXME: this should go into application or an own class"

#include <credit-authors.h>
#include <credit-docs.h>

	GtkWidget* dialog = gtk_about_dialog_new();
	gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(self));
	gtk_about_dialog_set_name(GTK_ABOUT_DIALOG(dialog), PACKAGE);
	gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), VERSION);
	gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog), _("Copyright (C) 2004,2005,2006 - The criawips development team"));
	gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(dialog), _("Criawips is a presentation application for the GNOME "
				        "desktop environment which should integrate smoothly into "
					"the desktop"));
#warning "showAbout(): FIXME: enable this one for license stuff"
	/* gtk_about_dialog_set_license(GTK_ABOUT_DIALOG(dialog), "gpl short version"); */
	gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(dialog), "http://www.nongnu.org/criawips");
	gtk_about_dialog_set_authors(GTK_ABOUT_DIALOG(dialog), author_credits);
#warning "showAbout(): FIXME: enable this one for artists"
	/* gtk_about_dialog_set_artists(GTK_ABOUT_DIALOG(dialog), artists); */
	gtk_about_dialog_set_documenters(GTK_ABOUT_DIALOG(dialog), documentation_credits);
	/* TRANSLATORS: please translate 'translator-credits' with your name and the name of other
	 * translators that have been involved in this translation */
	gtk_about_dialog_set_translator_credits(GTK_ABOUT_DIALOG(dialog), _("translator-credits"));

	gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(self));

	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

static void
cria_main_window_start_off (CriaMainWindow* self) {
	GtkWidget	* slide_show;

	cdebug("startOff()", "start");
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(self->priv);
	g_return_if_fail(CRIA_IS_PRESENTATION(self->priv->presentation));

	slide_show = cria_slide_show_new(self->priv->presentation);
	gtk_widget_show(slide_show);

	cdebug("startOff()", "end");
}

static void
cria_main_window_set_presentation_from_text_uri(CriaMainWindow* self, const gchar* text_uri) {
	GError		* error = NULL;
	CriaPresentation* presentation;

	g_return_if_fail(GTK_IS_WIDGET(self));

	gtk_widget_set_cursor(GTK_WIDGET(self), GDK_WATCH);
	presentation = cria_presentation_new_from_text_uri(text_uri, &error);
	gtk_widget_set_cursor(GTK_WIDGET(self), GDK_LEFT_PTR);

	if(error) {
		/* NOTE: keep the string in sync with the ones from application.c */
		char            * error_message = g_strdup_printf("%s %s:\n<span style=\"italic\">%s</span>.",
								  _("For some reason the presentation you wanted to open could not be opened."),
								  _("The reason was:"),
								  error->message);

		cria_application_show_error_dialog(GTK_WINDOW(self),
						   _("The Presentation could not be opened"),
						   error_message);
		cdebugo(self, "setPresentationFromTextUri()", "uri was %s", text_uri);

		g_free(error_message);
		g_error_free(error);
	} else {
		cria_main_window_add_to_recent(self, text_uri, presentation);

		cria_main_window_set_presentation(self, presentation);
		g_object_unref(presentation);
	}
}

static void
cria_main_window_open_presentations_from_uri_list(CriaMainWindow *self, GSList *uris) {
	if(uris) {
		GSList	* it = NULL;

		cria_main_window_set_presentation_from_text_uri(self, (const gchar*)uris->data);

		for(it = uris->next; it; it = it->next) {
			gtk_widget_show(cria_main_window_new_uri((const gchar*)it->data));
		}
	}
}


/* GtkAction callbacks:
 *
 * data is always set to the main window, so just check the parameters and
 * call the correct method
 */

static void
cb_action_application_quit(GtkAction* action, CriaMainWindow* window) {
	criawips_quit();
}

static void
cb_action_about_info(GtkAction* action, CriaMainWindow* window) {
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(window));

	cria_main_window_show_about(window);
}

static void
cb_action_file_new(GtkAction* action, CriaMainWindow* self) {
	g_return_if_fail(GTK_IS_ACTION(action));
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));

	cria_main_window_set_presentation(self, cria_presentation_new_default());
	g_object_unref(self->priv->presentation);
}

static void
cb_action_file_open(GtkAction* action, CriaMainWindow* window) {
	g_return_if_fail(GTK_IS_ACTION(action));
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(window));

	cria_main_window_open(window, action);
}

static void
cb_action_file_save(GtkAction* action, CriaMainWindow* window) {
	g_return_if_fail(GTK_IS_ACTION (action));
	g_return_if_fail(CRIA_IS_MAIN_WINDOW (window));

	cria_main_window_save(window);
}

static void
cb_action_file_save_as(GtkAction* action, CriaMainWindow* window) {
	g_return_if_fail(GTK_IS_ACTION (action));
	g_return_if_fail(CRIA_IS_MAIN_WINDOW (window));

	cria_main_window_save_as(window);
}

static void
cmw_format_font_bold(GtkAction* action, CriaMainWindow* self) {
	g_return_if_fail(GTK_IS_ACTION(action));
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));

	if(gtk_toggle_action_get_active(GTK_TOGGLE_ACTION(action))) {
		cria_block_set_font_weight(get_focused_block_renderer (self),
				   	   "bold");
	} else {
		cria_block_set_font_weight(get_focused_block_renderer (self),
				   	   "normal");
	}
}

static void
cmw_format_font_italic(GtkAction* action, CriaMainWindow* self) {
	g_return_if_fail(GTK_IS_ACTION(action));
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));

	if(gtk_toggle_action_get_active(GTK_TOGGLE_ACTION(action))) {
		cria_block_set_font_style(get_focused_block_renderer (self),
				   	  "italic");
	} else {
		cria_block_set_font_style(get_focused_block_renderer (self),
				   	  "normal");
	}
}

static void
cmw_format_font_underline(GtkAction* action, CriaMainWindow* self) {
	g_return_if_fail(GTK_IS_ACTION(action));
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
#if 0
	if(gtk_toggle_action_get_active(GTK_TOGGLE_ACTION(action))) {
		cria_block_set_text_decoration(get_focused_block_renderer (self),
					       "underline");
	} else {
		cria_block_set_text_decoration(get_focused_block_renderer (self),
					       "none");
	}
#endif
}

static void
cb_action_help_bug_buddy(GtkAction* action, CriaMainWindow* window) {
	GError* error = NULL;
	gdk_spawn_command_line_on_screen(gtk_widget_get_screen(GTK_WIDGET(window)), "bug-buddy --product=criawips", &error);

	if(error){
		cria_application_show_error_dialog(GTK_WINDOW(window), _("Unable to start bug-buddy"),
				_("Bug-buddy could not be started; please visit http://bugzilla.gnome.org/ to file a bug report."));
		g_error_free(error);
	}
}

static void
cb_action_help_manual(GtkAction* action, CriaMainWindow* window) {
	GError* error = NULL;
	g_return_if_fail(GTK_IS_ACTION(action));
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(window));

	gnome_help_display("criawips-manual", NULL, &error);

	if(error) {
		gchar* msg;

		switch(error->code) {
		case GNOME_HELP_ERROR_NOT_FOUND:
			msg = g_strdup_printf(_("The specified help document could not be found. %s"), error->message);
			break;
		case GNOME_HELP_ERROR_INTERNAL:
		default:
			msg = g_strdup_printf(_("An internal error occurred. Your system may be out of resources, or you may "
				"have a broken installation. %s"), error->message);
			break;
		};
		cria_application_show_error_dialog(GTK_WINDOW(window),
						   _("Could not Display the Help Document"),
						   msg);
		g_error_free(error);
		error = NULL;
		g_free(msg);
	}
}

static void
cb_action_insert_slide(GtkAction* action, CriaMainWindow* self) {
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));

	cria_main_window_insert_slide(self);
}

/* finalize the dnd session */
static void
cmw_finalize_dnd_action(CriaMainWindow* self) {
	gtk_statusbar_pop(GTK_STATUSBAR(self->priv->status_bar),
			  self->priv->status_drag_to_create);
	gtk_widget_set_cursor(self->priv->slide_editor, GDK_LEFT_PTR);
}

static void
cb_action_insert_block(GtkAction* action, CriaMainWindow* self) {
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));

	gtk_statusbar_push(GTK_STATUSBAR(self->priv->status_bar),
			   self->priv->status_drag_to_create,
			   _("Drag the block onto the slide"));
	
	gtk_widget_set_cursor(self->priv->slide_editor, GDK_CROSS);
	cria_slide_editor_drag_create(CRIA_SLIDE_EDITOR(self->priv->slide_editor),
				      CRIA_TYPE_BLOCK,
				      G_FUNC(cmw_finalize_dnd_action),
				      self);
}

static void
cb_action_main_window_close(GtkAction* action, CriaMainWindow* self) {
	g_return_if_fail(GTK_IS_ACTION(action));
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));

	cmw_close(self);
}

static void
cb_action_master_slide_edit(GtkAction* action, CriaMainWindow* self) {
	CriaSlide* master_slide;
	
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(CRIA_IS_SLIDE(self->slide));

	master_slide = cria_slide_get_master_slide(self->slide);

	g_return_if_fail(CRIA_IS_SLIDE(master_slide));
	
	cdebug("cb_action_master_slide_edit()", "slide before: %p (master: %p)", self->slide, master_slide);
	cmw_set_slide(self, master_slide);
	cdebug("cb_action_master_slide_edit()", "slide after: %p (master: %p)", self->slide, cria_slide_get_master_slide(self->slide));
}

static void
cb_action_master_slide_create(GtkAction* action, CriaMainWindow* self) {
	CriaSlide* master = cria_slide_new(NULL);
	CriaTheme* theme  = cria_presentation_get_theme(self->priv->presentation);
	guint i;
	gchar* new_title;
	
	for(i = 1; i < G_MAXINT; i++) {
		new_title = g_strdup_printf(_("Master Slide %u"), i);
		if(!cria_theme_get_master_slide(theme, new_title)) {
			break;
		}
		g_free(new_title);
	}

	cria_slide_set_title(master, new_title);
	g_free(new_title);
	
	cria_theme_add_master_slide(cria_presentation_get_theme(self->priv->presentation), master);
	cdebug("cb_action_master_slide_create()", "master slide before: %p", cria_slide_get_master_slide(self->slide));
	cmw_set_master_slide(self, master);
	cdebug("cb_action_master_slide_create()", "master slide after: %p", cria_slide_get_master_slide(self->slide));

	cb_action_master_slide_edit(NULL, self);
}

static void
cb_action_presentation_properties(GtkAction* action, CriaMainWindow* self) {
	GtkWidget * properties = cria_presentation_properties_dialog_new(self->priv->presentation);
	gtk_widget_show(properties);
}

static void
cb_action_show_preferences(GtkAction* action, CriaMainWindow* self) {
	gtk_widget_show(cria_preferences_dialog_get_instance());
}

static void
cb_action_view_first(GtkAction* action, CriaMainWindow* self) {
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(self->priv != NULL);
	
	cria_sidebar_select_first(CRIA_SIDEBAR(self->priv->slide_list));
}

static void
cb_action_view_last(GtkAction* action, CriaMainWindow* self) {
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(self->priv != NULL);
	
	cria_sidebar_select_last(CRIA_SIDEBAR(self->priv->slide_list));
}

static void
cb_action_view_next(GtkAction* action, CriaMainWindow* self) {
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(self->priv != NULL);
	
	if(cria_application_get_page_down_creates_slide() &&
	   (cria_slide_list_index(CRIA_SLIDE_LIST(self->priv->presentation), cria_sidebar_get_selected(CRIA_SIDEBAR(self->priv->slide_list))) == (gint)cria_slide_list_n_slides(CRIA_SLIDE_LIST(self->priv->presentation)) - 1)) {
		cria_main_window_insert_slide(self);
	}
	cria_sidebar_select_next(CRIA_SIDEBAR(self->priv->slide_list));
}

static void
cb_action_view_presentation(GtkAction* action, CriaMainWindow* self) {
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	
	cria_main_window_start_off(self);
}

static void
cb_action_view_previous(GtkAction* action, CriaMainWindow* self) {
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(self->priv != NULL);
	
	cria_sidebar_select_previous(CRIA_SIDEBAR(self->priv->slide_list));
}

static void
cb_action_view_sidebar(GtkAction* action, CriaMainWindow* self) {
	gboolean visible;
	g_return_if_fail(GTK_IS_TOGGLE_ACTION(action));
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(self->priv);

	visible = gtk_toggle_action_get_active(GTK_TOGGLE_ACTION(action));
	if(visible) {
		gtk_widget_show(self->priv->sidebar);
	} else {
		gtk_widget_hide(self->priv->sidebar);
	}
	cria_application_set_show_sidebar(visible);
}

static void
cb_action_view_templates(GtkAction* action, CriaMainWindow* self) {
	gboolean visible;
	g_return_if_fail(GTK_IS_TOGGLE_ACTION(action));
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(self->priv);

	visible = gtk_toggle_action_get_active(GTK_TOGGLE_ACTION(action));
	if(visible) {
		gtk_widget_show(self->priv->sidebar_templates);
	} else {
		gtk_widget_hide(self->priv->sidebar_templates);
	}
	cria_application_set_templates_visible(visible);
}

static GtkActionEntry entries[] = {
	/* use this one when you add new items */
	{"FooBarDummy",		GTK_STOCK_MISSING_IMAGE,NULL},
	{"ApplicationQuit",	GTK_STOCK_QUIT,		N_("_Quit"),
	 NULL,			N_("Quit Criawips and close all Windows"),
	 G_CALLBACK(cb_action_application_quit)},
	{"FileMenu",		NULL, 			N_("_File")},
	{"FileNew",		GTK_STOCK_NEW,		N_("_New"),
	 "<control>N",		N_("Create a new Presentation"),
	 G_CALLBACK(cb_action_file_new)},
	{"FileOpen",		GTK_STOCK_OPEN,		N_("_Open"),
	 "<control>O",		N_("Open a Presentation"),
	 G_CALLBACK(cb_action_file_open)},
	{"RecentDocuments",     NULL,                   N_("_Recent Files")},
	{"FileSave",		GTK_STOCK_SAVE,		N_("_Save"),
	 "<control>S",		N_("Save the Presentation"),
	 G_CALLBACK(cb_action_file_save)},
	{"FileSaveAs",		GTK_STOCK_SAVE_AS,	N_("_Save As"),
	 "<control><shift>S",	N_("Save the Presentation under a specified filename"),
	 G_CALLBACK(cb_action_file_save_as)},
	{"EditMenu",            NULL,                   N_("_Edit")},
	{"PreferencesOpen",     GTK_STOCK_PREFERENCES,  N_("_Preferences"),
	 NULL,                  N_("Show Preferences"),
	 G_CALLBACK(cb_action_show_preferences)},
	{"HelpBugBuddy",        NULL,			N_("_Submit a Bug Report"),
	 NULL,                  N_("Submit a bug report or request a feature"),
	 G_CALLBACK(cb_action_help_bug_buddy)},
	{"HelpManual",		GTK_STOCK_HELP,		N_("_Contents"),
	 "F1",			N_("Open the manual"),
	 G_CALLBACK(cb_action_help_manual)},
	{"HelpMenu",		NULL,			N_("_Help")},
	{"HelpInfo",		GNOME_STOCK_ABOUT,	N_("_About"),
	 NULL,			N_("Show Information about Criawips"),
	 G_CALLBACK(cb_action_about_info)},
	{"InsertMenu",		NULL,			N_("_Insert")},
	{"InsertSlide",		GTK_STOCK_ADD,		N_("_Slide"),
	 NULL,			N_("Insert a new slide after this one"),
	 G_CALLBACK(cb_action_insert_slide)},
	{"InsertBlock",		GTK_STOCK_JUSTIFY_FILL,	N_("_Text Block"),
	 NULL,			N_("Insert a new text block into the slide"),
	 G_CALLBACK(cb_action_insert_block)},
	{"MainWindowClose",	GTK_STOCK_CLOSE,	N_("_Close"),
	 "<control>W",		N_("Close this window"),
	 G_CALLBACK(cb_action_main_window_close)},
	{"MasterSlideMenu",	NULL,			N_("_Master Slide")},
	{"MasterSlideEdit",	GTK_STOCK_EDIT,		N_("_Edit"),
	 NULL,			N_("Edit the master slide of this slide"),
	 G_CALLBACK(cb_action_master_slide_edit)},
	{"MasterSlideCreate",	GTK_STOCK_NEW,		N_("_Create"),
	 "",			N_("Create a new empty master slide and assign it to this slide"),
	 G_CALLBACK(cb_action_master_slide_create)},
	{"PresentationProperties", GTK_STOCK_PROPERTIES, N_("_Properties"),
	 NULL,			N_("View and edit Properties of the Presentation"),
	 G_CALLBACK(cb_action_presentation_properties)},
	{"FormatMenu",		NULL,			N_("For_mat")},
	{"ViewMenu",		NULL,			N_("_View")},
	{"ViewFirst",		GTK_STOCK_GOTO_FIRST,	N_("_First Slide"),
	 NULL,			N_("Display the first Slide of the Presentation"),
	 G_CALLBACK(cb_action_view_first)},
	{"ViewLast",		GTK_STOCK_GOTO_LAST,	N_("_Last Slide"),
	 NULL,			N_("Display the last Slide of the Presentation"),
	 G_CALLBACK(cb_action_view_last)},
	{"ViewNext",		GTK_STOCK_GO_FORWARD,	N_("_Next Slide"),
	 "Page_Down",		N_("Display the next Slide"),
	 G_CALLBACK(cb_action_view_next)},
	{"ViewPresentation",	GTK_STOCK_EXECUTE,	N_("S_tart Presentation"),
	 "F5",			N_("Show this Presentation"),
	 G_CALLBACK(cb_action_view_presentation)},
	{"ViewPrevious",	GTK_STOCK_GO_BACK,	N_("_Previous Slide"),
	 "Page_Up",		N_("Display the previous Slide"),
	 G_CALLBACK(cb_action_view_previous)}
};

static GtkToggleActionEntry toggle_entries[] = {
	{"FormatFontBold",	GTK_STOCK_BOLD,		N_("_Bold"),
	 "<control>B",		NULL,
	 G_CALLBACK(cmw_format_font_bold)},
	{"FormatFontItalic",	GTK_STOCK_ITALIC,	N_("_Italic"),
	 "<control>I",		NULL,
	 G_CALLBACK(cmw_format_font_italic)},
	{"FormatFontUnderline",	GTK_STOCK_UNDERLINE,	N_("_Underline"),
	 "<control>U",		NULL,
	 G_CALLBACK(cmw_format_font_underline)},
	{"ViewSidebar",		NULL,			N_("_Sidebar"),
	 "F9",			N_("Display the slide list sidebar"),
	 G_CALLBACK(cb_action_view_sidebar),		TRUE},
	{"ViewTemplates",	NULL,			N_("_Templates"),
	 NULL,			N_("Display the template list sidebar"),
	 G_CALLBACK(cb_action_view_templates),		TRUE}
};

static void
cmw_init_actions(CriaMainWindow* self) {
	gtk_action_group_add_actions(self->priv->action_group, entries, G_N_ELEMENTS(entries), self);
	gtk_action_group_add_toggle_actions(self->priv->action_group, toggle_entries, G_N_ELEMENTS(toggle_entries), self);

}

/* CriaSlideView interface */
static void
cmw_notify_master_slide(CriaSlideView* view, CriaSlide* master_slide) {
	CriaMainWindow* self = CRIA_MAIN_WINDOW(view);
	gtk_action_set_sensitive(gtk_action_group_get_action(self->priv->action_group, "MasterSlideEdit"),
				 master_slide ? TRUE : FALSE);

	cria_theme_actions_select(self->theme_actions, master_slide);
}

static void
cmw_init_slide_view_iface(CriaSlideViewIface* iface) {
	iface->notify_master_slide = cmw_notify_master_slide;
}

/* CriaPresentationViewIface */
static void
cmw_notify_theme(CriaPresentationView* view, CriaTheme* theme) {
	CriaMainWindow* self = CRIA_MAIN_WINDOW(view);

	cria_theme_actions_set_theme(self->theme_actions, theme);
	cria_template_pane_set_theme(CRIA_TEMPLATE_PANE(self->priv->templates), theme);
}

static void
cmw_notify_title(CriaPresentationView* view, gchar const* title) {
	CriaMainWindow* self = CRIA_MAIN_WINDOW(view);
	
	g_return_if_fail(CRIA_IS_MAIN_WINDOW(self));
	g_return_if_fail(self->priv != NULL);
	g_return_if_fail(CRIA_IS_PRESENTATION(self->priv->presentation));

#warning "titleChanged(): FIXME: update after a changed filename too"
#warning "titleChanged(): FIXME: update after a changed 'saved' property"
	
	cdebugo(self, "notifyTitle()", "we have a presentation");

	/* setting the window's title */
	if(!(title && *title && strcmp(title, _("Untitled")))) {
		cdebugo(self, "notifyTitle()", "we don't have a cool title, using the filename");
		title = cria_presentation_get_filename(self->priv->presentation);
	}
	
	cdebugo(self, "notifyTitle()", "setting title to %p (%s)", title, title);
	gtk_window_set_title(GTK_WINDOW(self), title);
}

static void
cmw_init_presentation_view_iface(CriaPresentationViewIface* iface) {
	iface->notify_theme = cmw_notify_theme;
	iface->notify_title = cmw_notify_title;
}

