/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <widgets/cria-text-pane.h>

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

enum {
	PROP_0,
};

enum {
	N_SIGNALS
};

struct _CriaTextPanePrivate {
	GtkWidget	* hbox,
			* label;
};

static void cria_text_pane_get_property	       (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static void cria_text_pane_init		       (CriaTextPane	* self);
static void cria_text_pane_set_property        (GObject		* object,
						guint		  prop_id,
						const	GValue	* value,
						GParamSpec	* param_spec);
#if 0
/* enable these to add support for signals */
static	guint	cria_text_pane_signals[N_SIGNALS] = { 0 };

static	void	cria_text_pane_signal	       (CriaTextPane	* self,
						const	gchar	* string);
#endif

static void
cria_text_pane_class_init (CriaTextPaneClass	* cria_text_pane_class) {
	GObjectClass	* g_object_class;

	g_object_class = G_OBJECT_CLASS(cria_text_pane_class);
#if 0
	/* setting up signal system */
	cria_text_pane_class->signal = cria_text_pane_signal;

	cria_text_pane_signals[SIGNAL] = g_signal_new (
			"signal",
			CRIA_TYPE_TEXT_PANE,
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (
				CriaTextPaneClass,
				signal),
			NULL,
			NULL,
			g_cclosure_marshal_VOID__STRING,
			G_TYPE_NONE,
			0);
#endif
	/* setting up property system */
	g_object_class->set_property = cria_text_pane_set_property;
	g_object_class->get_property = cria_text_pane_get_property;
#if 0
	g_object_class_install_property(g_object_class,
					PROP_ATTRIBUTE,
					g_param_spec_string("attribute",
							    "Attribute",
							    "A simple unneccessary attribute that does nothing special except "
							    "being a demonstration for the correct implementation of a GObject "
							    "property",
							    "default_value",
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
#endif
}

static void
cria_text_pane_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaTextPane	* self;

	self = CRIA_TEXT_PANE(object);

	switch (prop_id) {
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object,
						  prop_id,
						  param_spec);
		break;
	}
}

GType
cria_text_pane_get_type(void) {
	static GType	type = 0;

	if(!type) {
		static const GTypeInfo info = {
			sizeof(CriaTextPaneClass),
			NULL,	/* base initializer */
			NULL,	/* base finalizer */
			(GClassInitFunc)cria_text_pane_class_init,
			NULL,	/* class finalizer */
			NULL,	/* class data */
			sizeof(CriaTextPane),
			0,
			(GInstanceInitFunc)cria_text_pane_init,
			0
		};

		type = g_type_register_static(GTK_TYPE_FRAME,
					      "CriaTextPane",
					      &info,
					      0);
	}

	return type;
}

static void
cria_text_pane_init(CriaTextPane* self) {
	g_return_if_fail(CRIA_IS_TEXT_PANE(self));
	g_return_if_fail(GTK_IS_FRAME(self));

	/* the label */
	self->priv = g_new0(CriaTextPanePrivate, 1);
	self->priv->label = gtk_label_new("");
	gtk_label_set_ellipsize(GTK_LABEL(self->priv->label), PANGO_ELLIPSIZE_END);
	gtk_widget_show(self->priv->label);

	/* the hbox */
	self->priv->hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start_defaults(GTK_BOX(self->priv->hbox), self->priv->label);
	gtk_box_set_spacing(GTK_BOX(self->priv->hbox), 6);
	gtk_container_set_border_width(GTK_CONTAINER(self->priv->hbox), 6);
	gtk_widget_show(self->priv->hbox);

	/* self as a gtk frame */
	gtk_frame_set_shadow_type(GTK_FRAME(self), GTK_SHADOW_OUT);
	gtk_widget_show(self->priv->hbox);
	gtk_container_add(GTK_CONTAINER(self), self->priv->hbox);
}

/**
 * cria_text_pane_new:
 * @text: ...
 *
 * ...
 *
 * Returns ...
 */
GtkWidget*
cria_text_pane_new(const gchar* text) {
	CriaTextPane* self = g_object_new(CRIA_TYPE_TEXT_PANE, NULL);
	cria_text_pane_set_text(self, text);
	return GTK_WIDGET(self);
}

void
cria_text_pane_pack_end(CriaTextPane* self, GtkWidget* widget) {
	g_return_if_fail(CRIA_IS_TEXT_PANE(self));
	g_return_if_fail(self->priv);
	g_return_if_fail(GTK_IS_BOX(self->priv->hbox));
	g_return_if_fail(GTK_IS_WIDGET(widget));

	gtk_box_pack_start(GTK_BOX(self->priv->hbox), widget, FALSE, FALSE, 0);
	gtk_box_reorder_child(GTK_BOX(self->priv->hbox), widget, -1);
}

static void
cria_text_pane_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaTextPane	* self;
	
	self = CRIA_TEXT_PANE(object);
	
	switch(prop_id) {
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

void
cria_text_pane_set_text(CriaTextPane* self, const gchar* text) {
	gchar* message;
	
	g_return_if_fail(CRIA_IS_TEXT_PANE(self));
	g_return_if_fail(text != NULL);
	
	message = g_strdup_printf("<span weight=\"bold\">%s</span>", text);
	gtk_label_set_markup(GTK_LABEL(self->priv->label), message);
	g_free(message);
}

