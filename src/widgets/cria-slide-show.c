/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#warning "Rename to CriaPlayer"

#include <math.h>
#include <stdint.h>

#include <glib-object.h>
#include <glib.h>
#include <gdk/gdkkeysyms.h>
#include <libgnome/gnome-i18n.h>
#include <goffice/utils/go-color.h>
#include <cut-n-paste/gtkgesture.h>

#define CDEBUG_TYPE cria_slide_show_get_type
#include <cdebug/cdebug.h>
#include <helpers/gtk-helpers.h>

#include <dom/cria-block.h>
#include <rendering/cria-slide-display.h>
#include <widgets/cria-slide-show.h>

#include "application.h"

#define MILLIS_PER_SECOND 1000

struct _CriaSlideShowPrivate {
	CriaSlideDisplay	* display;
	GtkGestureHandler	* gesture_handler;
	guint			  slide_change_timer,
				  screensaver_notify_timer;
};

enum {
	PROP_0,
	PROP_PRESENTATION,
	PROP_SLIDE
};

enum {
	SIGNAL,
	N_SIGNALS
};

static GObjectClass* parent_class = NULL;

static	void	cria_slide_show_init	       (CriaSlideShow	* slide_show);
static	void	cria_slide_show_get_property   (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static void	cria_slide_show_next_page_event(GtkGestureHandler *gh,
						gpointer user_data_a,
						gpointer user_data_b);
static void	cria_slide_show_prev_page_event(GtkGestureHandler *gh,
						gpointer user_data_a,
						gpointer user_data_b);
static	void	cria_slide_show_render	       (CriaSlideShow	* slide_show);
static	void	cria_slide_show_set_property   (GObject		* object,
						guint		  prop_id,
						const	GValue	* value,
						GParamSpec	* param_spec);
#if 0
static	guint	cria_slide_show_signals[N_SIGNALS] = { 0 };

static	void	cria_slide_show_signal	       (CriaSlideShow	* template,
						const	gchar	* string);
#endif
typedef enum {
	KEYS_NONE,
	KEYS_FORWARD,
	KEYS_BACK,
	KEYS_PAUSE,
	KEYS_CLOSE
} CriaSlideEditorAction;

static CriaSlideEditorAction
cria_key_press_event_cb_next_slide(GtkWidget* widget, GdkEventKey* event, gpointer data) {
	guint	  action = KEYS_NONE;
	guint	  slide = 0;
	
	g_return_val_if_fail(CRIA_IS_SLIDE_SHOW(widget), KEYS_NONE);
	
	slide = cria_slide_show_get_slide(CRIA_SLIDE_SHOW(widget));

	/* TODO If we want to make it configurable we have to use a
	 * method instead of a switch statement */
	switch(event->keyval) {
	case GDK_space:
	case GDK_Right:
	case GDK_KP_Enter:
	case GDK_Return:
	case GDK_Page_Down:
		action = KEYS_FORWARD;
		break;
	case GDK_BackSpace:
	case GDK_Left:
	case GDK_Page_Up:
		action = KEYS_BACK;
		break;
	case GDK_Escape:
		action = KEYS_CLOSE;
		break;
	case 'p':
	case 'P':
	case 'b':
	case 'B':
		action = KEYS_PAUSE;
		break;
	default:
		cdebugo(widget, "keyPressEvent()", "this key is not handled");
		break;
	}

	return action;
}

static guint
cria_slide_show_next_slide(CriaSlideShow* self) {
	/* returns the index of the next slide */
	g_return_val_if_fail(CRIA_IS_SLIDE_SHOW(self), 0);
	g_return_val_if_fail(CRIA_IS_PRESENTATION(self->presentation), 0);
	
	if(cria_presentation_get_loop(self->presentation)) {
		g_return_val_if_fail(self->slide <= cria_slide_list_n_slides(CRIA_SLIDE_LIST(self->presentation)), 0);
		return (self->slide + 1) % cria_slide_list_n_slides(CRIA_SLIDE_LIST(self->presentation));
	} else {
		g_return_val_if_fail(self->slide <= cria_slide_list_n_slides(CRIA_SLIDE_LIST(self->presentation)), cria_slide_list_n_slides(CRIA_SLIDE_LIST(self->presentation)));
		return self->slide + 1;
	}
}

static guint
cria_slide_show_prev_slide(CriaSlideShow* self) {
	/* returns the index of the previous slide */
	g_return_val_if_fail(CRIA_IS_SLIDE_SHOW(self), 0);
	g_return_val_if_fail(CRIA_IS_PRESENTATION(self->presentation), 0);

	if(cria_presentation_get_loop(self->presentation)) {
		return (self->slide == 0) ? cria_slide_list_n_slides(CRIA_SLIDE_LIST(self->presentation)) - 1 : self->slide - 1;
	} else {
		return (self->slide == 0) ? 0 : self->slide - 1;
	}
}

static void
remove_slide_change_timer(CriaSlideShow* self) {
	g_source_remove(self->priv->slide_change_timer);
	self->priv->slide_change_timer = 0;
}

static gboolean
slide_needs_auto_switch(CriaSlideShow* self) {
	return  (cria_presentation_get_auto_switch(self->presentation) /* should we switch automatically on every slide? */
#warning "setSlide(): FIXME: check for the slide-specific auto-switch stuff"
	   /* || *//* should we switch automatically on this slide? */)
		&& self->slide != cria_slide_list_n_slides(CRIA_SLIDE_LIST(self->presentation));
}

static void
toggle_pause_mode(CriaSlideShow* self) {
	if(self->priv->slide_change_timer) {
		/* stop the timer */
		remove_slide_change_timer(self);
	} else if (slide_needs_auto_switch(self)) {
		/* step to the next slide */
		cria_slide_show_set_slide(self, cria_slide_show_next_slide(self));
	}
}

static gboolean
cria_key_press_event_cb(GtkWidget* widget, GdkEventKey* event, gpointer data) {
	gboolean               retval = FALSE;
	guint		       slide  = 0;
	CriaSlideEditorAction  action = KEYS_NONE;

	g_return_val_if_fail(CRIA_IS_SLIDE_SHOW(widget), FALSE);

	action = cria_key_press_event_cb_next_slide(widget, event, data);
	slide = cria_slide_show_get_slide(CRIA_SLIDE_SHOW(widget));
	
	if(slide >= cria_slide_list_n_slides(CRIA_SLIDE_LIST(cria_slide_show_get_presentation(CRIA_SLIDE_SHOW(widget)))) &&
	   action != KEYS_BACK) {
		cdebug("cbKeyPressEvent()", "after show, we're quitting now");
		gtk_object_destroy(GTK_OBJECT(widget));
		return TRUE;
	} else {
		cdebug("cbKeyPressEvent()", "not last slide");
	}
	
	switch(action) {
	case KEYS_FORWARD:
		cdebug("cbKeyPressEvent()", "next slide");
		cria_slide_show_set_slide(CRIA_SLIDE_SHOW(widget), cria_slide_show_next_slide(CRIA_SLIDE_SHOW(widget)));
		retval = TRUE;
		break;
	case KEYS_BACK:
		cdebugo(widget, "cbKeyPressEvent()", "previous slide");
		cria_slide_show_set_slide(CRIA_SLIDE_SHOW(widget), cria_slide_show_prev_slide(CRIA_SLIDE_SHOW(widget)));
		retval = TRUE;
		break;
	case KEYS_CLOSE:
		cdebugo(widget, "cbKeyPressEvent()", "close slide show");
		gtk_object_destroy(GTK_OBJECT(widget));
		retval = TRUE;
		break;
	case KEYS_PAUSE:
		toggle_pause_mode(CRIA_SLIDE_SHOW(widget));
		retval = TRUE;
		break;
	case KEYS_NONE:
		cdebugo(widget, "cbKeyPressEvent()", "nothing to do");
		retval = FALSE;
	}

	return retval;
}

static gboolean
cria_slide_show_button_press_event(GtkWidget* self, GdkEventButton* ev) {
	if(cria_slide_show_get_slide(CRIA_SLIDE_SHOW(self)) >= cria_slide_list_n_slides(CRIA_SLIDE_LIST(cria_slide_show_get_presentation(CRIA_SLIDE_SHOW(self))))) {
		gtk_object_destroy(GTK_OBJECT(self));
		return TRUE;
	}
	
	return FALSE;
}

static void
exec_command(GError** error) {
	static gchar* child_argv[] = {
		"xscreensaver-command",
		"-deactivate",
		NULL
	};
	
	g_return_if_fail(error == NULL || *error == NULL);

	g_spawn_async(NULL, child_argv, 
		      NULL, G_SPAWN_SEARCH_PATH | G_SPAWN_STDOUT_TO_DEV_NULL | G_SPAWN_STDERR_TO_DEV_NULL,
		      NULL, NULL,
		      NULL, error);
}

static gboolean
disable_screensaver(gpointer user_data) {
	GError* error = NULL;
	CriaSlideShow* self = (CriaSlideShow*)user_data;
	
	g_return_val_if_fail(CRIA_IS_SLIDE_SHOW(self), FALSE);
	g_return_val_if_fail(self->priv, FALSE);

	exec_command(&error);
	
	if(error) {
		g_critical(_("Error disabling xscreensaver: %s"), error->message);
		g_error_free(error);
		self->priv->screensaver_notify_timer = 0;
		return FALSE;
	}

	return TRUE;
}

static void
cria_slide_show_show(GtkWidget* self) {
	g_return_if_fail(CRIA_IS_SLIDE_SHOW(self));
	if(cria_application_get_disable_screensaver() && !CRIA_SLIDE_SHOW(self)->priv->screensaver_notify_timer) {
		CRIA_SLIDE_SHOW(self)->priv->screensaver_notify_timer = g_timeout_add(59 * MILLIS_PER_SECOND,
										      disable_screensaver,
										      self);
		exec_command(NULL);
#warning "show(): connect a signal handler to listen to disable-screensaver changes at the application"
	}
	GTK_WIDGET_CLASS(parent_class)->show(self);
}

static void
cria_slide_show_hide(GtkWidget* self) {
	g_return_if_fail(CRIA_IS_SLIDE_SHOW(self));
	g_return_if_fail(CRIA_SLIDE_SHOW(self)->priv);
	if(CRIA_SLIDE_SHOW(self)->priv->screensaver_notify_timer) {
		g_source_remove(CRIA_SLIDE_SHOW(self)->priv->screensaver_notify_timer);
	}
	GTK_WIDGET_CLASS(parent_class)->hide(self);
}

static void
css_finalize(GObject* object) {
	CriaSlideShow* self = CRIA_SLIDE_SHOW(object);

	if(self->priv->screensaver_notify_timer) {
		g_source_remove(CRIA_SLIDE_SHOW(self)->priv->screensaver_notify_timer);
	}

	if(self->priv->slide_change_timer) {
		remove_slide_change_timer(self);
	}

	if(self->presentation) {
		g_object_unref(self->presentation);
		self->presentation = NULL;
	}
	
	parent_class->finalize(object);
}

static void
cria_slide_show_class_init(CriaSlideShowClass* cria_slide_show_class) {
	GObjectClass	* g_object_class;
	GtkWidgetClass	* widget_class;
	CriaBlock	* block;
	GOColor           gray = RGBA_GREY(0xaa);

	parent_class = g_type_class_peek_parent(cria_slide_show_class);

	cdebug("classInit()", "start");

	/* create the after show slide */
	cria_slide_show_class->after_show = cria_slide_new(NULL);
	cria_slide_set_title(cria_slide_show_class->after_show, _("End of Presentation"));
	block = cria_block_new("message");
	cria_block_set_model(block, cria_text_model_new());
	/* TRANSLATORS: there is a UTF-8 Character for the ellipsis (...), please use it and not three dots */
	cria_text_model_set_text(cria_block_get_model(block), _("Press a Key or click to close this Window..."));
	cria_block_set_color(block, &gray);
	cria_block_set_alignment(block, CRIA_ALIGNMENT_CENTER);
	cria_block_set_valignment(block, CRIA_ALIGNMENT_MIDDLE);
	{
		GOColor  color = RGBA_BLACK;
		GOPoint* size = cria_slide_get_size(cria_slide_show_class->after_show);

		cdebug("classInit()", "Slide size %llix%lli", size->x, size->y);
		cria_background_set_color(cria_slide_get_background(cria_slide_show_class->after_show, TRUE), &color);
		cria_slide_element_set_position(CRIA_SLIDE_ELEMENT(block), 0.0, 0.0, 1.0*size->x, 1.0*size->y);
	}
	cria_slide_add_element(cria_slide_show_class->after_show, CRIA_SLIDE_ELEMENT(block));

	/* setting up the object class */
	g_object_class = G_OBJECT_CLASS(cria_slide_show_class);
	g_object_class->finalize = css_finalize;

	/* setting up the GtkWidgetClass */
	GTK_WIDGET_CLASS(cria_slide_show_class)->show = cria_slide_show_show;
	GTK_WIDGET_CLASS(cria_slide_show_class)->hide = cria_slide_show_hide;

	/* setting up signal system */
#if 0
	cria_slide_show_class->signal = cria_slide_show_signal;

	cria_slide_show_signals[SIGNAL] = g_signal_new (
			"signal",
			CRIA_TYPE_SLIDE_SHOW,
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (
				CriaSlideShowClass,
				signal),
			NULL,
			NULL,
			g_cclosure_marshal_VOID__STRING,
			G_TYPE_NONE,
			0);
#endif
	/* setting up property system */
	g_object_class->set_property = cria_slide_show_set_property;
	g_object_class->get_property = cria_slide_show_get_property;

	g_object_class_install_property (
			g_object_class,
			PROP_PRESENTATION,
			g_param_spec_object (
				"presentation",
				"Presentation",
				"The presentation currently shown by this CriaSlideShow",
				CRIA_TYPE_PRESENTATION,
				G_PARAM_READWRITE | G_PARAM_CONSTRUCT)
			);
	g_object_class_install_property (
			g_object_class,
			PROP_SLIDE,
			g_param_spec_uint (
				"slide",
				"Slide",
				"The slide that's presented in this moment or that we're "
				"currently switching to",
				0,
				G_MAXUINT,
				0,
				G_PARAM_READWRITE | G_PARAM_CONSTRUCT)
			);

	/* setting up the widget class */
	widget_class = GTK_WIDGET_CLASS(cria_slide_show_class);
	widget_class->button_press_event = cria_slide_show_button_press_event;

	cdebugc(cria_slide_show_class, "init()", "end");
}

guint
cria_slide_show_get_slide (CriaSlideShow*self) {
	g_return_val_if_fail (CRIA_IS_SLIDE_SHOW (self), 0);

	return self->slide;
}

CriaPresentation *
cria_slide_show_get_presentation(CriaSlideShow* self) {
	g_return_val_if_fail (CRIA_IS_SLIDE_SHOW(self), NULL);
	
	return self->presentation;
}

static void
cria_slide_show_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaSlideShow* self;

	self = CRIA_SLIDE_SHOW(object);

	switch(prop_id) {
	case PROP_PRESENTATION:
		g_value_set_object(value, self->presentation);
		break;
	case PROP_SLIDE:
		g_value_set_uint(value, self->slide);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

GType
cria_slide_show_get_type (void)
{
	static GType	type = 0;

	if (!type)
	{
		const GTypeInfo info = {
			sizeof (CriaSlideShowClass),
			NULL,	/* base initializer */
			NULL,	/* base finalizer */
			(GClassInitFunc)cria_slide_show_class_init,
			NULL,	/* class finalizer */
			NULL,	/* class data */
			sizeof (CriaSlideShow),
			0,
			(GInstanceInitFunc)cria_slide_show_init,
			0
		};

		type = g_type_register_static (
				GTK_TYPE_WINDOW,
				"CriaSlideShow",
				&info,
				0);
	}

	return type;
}

static void
css_pause_event(GtkGestureHandler* gh, CriaSlideShow* self, gpointer user_data_b) {
	if(self->priv->slide_change_timer) {
		remove_slide_change_timer(self);
	}
}

static void
cria_slide_show_init(CriaSlideShow *self) {
	GdkColor color;

	g_return_if_fail(CRIA_IS_SLIDE_SHOW(self));

	self->slide = -1;
	gdk_color_parse("black", &color);

	cdebugo(self, "init()", "start");

	/* setting up our private data */
	self->priv = g_new0 (CriaSlideShowPrivate, 1);

	/* setting up the canvas */
	self->priv->display = cria_slide_display_new(cria_slide_renderer_new(FALSE));
	cria_slide_display_set_slide_zoom(CRIA_SLIDE_DISPLAY(self->priv->display), 1.0);

#warning "SlideShow::init(): FIXME: make this a presentation renderer that can blend from one slide to the other"
	gdk_colormap_alloc_color(gtk_widget_get_colormap(GTK_WIDGET(self->priv->display)),
				 &color,
				 FALSE,
				 TRUE);
	gtk_widget_modify_bg(GTK_WIDGET(self->priv->display), GTK_STATE_NORMAL, &color);

	/* setting up gestures */
	self->priv->gesture_handler = gtk_gesture_handler_new (GTK_WIDGET (self->priv->display));
	/* adding gestures */
	gtk_gesture_add_callback(self->priv->gesture_handler, "012",
			GTK_GESTURE_FUNC(cria_slide_show_next_page_event),
			self, NULL);
	gtk_gesture_add_callback(self->priv->gesture_handler, "210",
			GTK_GESTURE_FUNC(cria_slide_show_prev_page_event),
			self, NULL);
	gtk_gesture_add_callback(self->priv->gesture_handler, "036",
			GTK_GESTURE_FUNC(css_pause_event),
			self, NULL);

	gtk_window_fullscreen (GTK_WINDOW (self));
	gtk_container_add(GTK_CONTAINER(self),
			  GTK_WIDGET(self->priv->display));

	g_signal_connect_swapped(G_OBJECT(self->priv->display), "key-release-event",
				 G_CALLBACK(cria_key_press_event_cb), self);

	gtk_widget_show_all(GTK_WIDGET(self));
	cdebugo(self, "init()", "end");
}

static void
cria_slide_show_next_page_event(GtkGestureHandler* gh, gpointer user_data_a, gpointer user_data_b) {
	CriaSlideShow	* self;

	g_return_if_fail(CRIA_IS_SLIDE_SHOW(user_data_a));
	g_return_if_fail(CRIA_SLIDE_SHOW(user_data_a)->slide <= cria_slide_list_n_slides(CRIA_SLIDE_LIST(cria_slide_show_get_presentation(CRIA_SLIDE_SHOW(user_data_a)))));

	self = CRIA_SLIDE_SHOW(user_data_a);

	if(self->slide < cria_slide_list_n_slides(CRIA_SLIDE_LIST(cria_slide_show_get_presentation(self)))) {
		guint next = cria_slide_show_next_slide(self);
		cria_slide_show_set_slide(self, next);
	} else {
		gtk_object_destroy(GTK_OBJECT(self));
	}
}

GtkWidget*
cria_slide_show_new(CriaPresentation* presentation) {
	g_return_val_if_fail(CRIA_IS_PRESENTATION(presentation), NULL);

	return g_object_new(CRIA_TYPE_SLIDE_SHOW, "presentation", presentation, NULL);
}

static void
cria_slide_show_prev_page_event(GtkGestureHandler* gh, gpointer user_data_a, gpointer user_data_b) {
	CriaSlideShow   * self;

	g_return_if_fail(CRIA_IS_SLIDE_SHOW(user_data_a));

	self = CRIA_SLIDE_SHOW(user_data_a);

	if(self->slide > 0) {
		cria_slide_show_set_slide(self, self->slide - 1);
	}
}

static void
cria_slide_show_render(CriaSlideShow* self) {
	CriaSlide	* slide;
	GOPoint		* slide_s;
	gchar		* title;
	CcRectangle	  rect;

	g_return_if_fail(CRIA_IS_SLIDE_SHOW(self));
	g_return_if_fail(CRIA_IS_PRESENTATION(self->presentation));

	cdebugo(self, "render()", "start");

	if(self->slide < cria_slide_list_n_slides(CRIA_SLIDE_LIST(cria_slide_show_get_presentation(self)))) {
		slide = cria_slide_list_get(CRIA_SLIDE_LIST(cria_slide_show_get_presentation(self)), self->slide);
	} else {
		slide = CRIA_SLIDE_SHOW_GET_CLASS(self)->after_show;
	}

	slide_s = cria_slide_get_size(slide);
	rect.x = 0.0;
	rect.y = 0.0;
	rect.w = 0.0 + slide_s->x;
	rect.h = 0.0 + slide_s->y;
	cria_canvas_set_extents(CRIA_CANVAS(self->priv->display), &rect);
	cria_slide_display_set_slide(CRIA_SLIDE_DISPLAY(self->priv->display), slide);

	/* update window's title */
	title = g_strdup_printf(_("%s"), cria_slide_get_title(slide));
	cdebugo(self, "render()", "title: %s", title);
	gtk_window_set_title(GTK_WINDOW(self), title);

	g_free(title);
	g_free(slide_s);

	cdebugo(self, "render()", "end");
}

void
cria_slide_show_set_presentation(CriaSlideShow* self, CriaPresentation* presentation) {
	g_return_if_fail(CRIA_IS_SLIDE_SHOW(self));

	cdebug("setPresentation()", "start");

	if(self->presentation != NULL) {
		g_object_unref(self->presentation);
	}

	self->presentation = presentation;
	if(self->presentation) {
		g_object_ref(presentation);
	}

	/* ensure we don't pass invalid slide numbers */
	cria_slide_show_set_slide(self, self->slide);

	g_object_notify(G_OBJECT(self), "presentation");

	cdebug("setPresentation()", "set presentation 0x%p", (uintptr_t)self->presentation);
	cdebug("setPresentation()", "end");
}

static void
cria_slide_show_set_property(
		GObject		* object,
		guint		  prop_id,
		const	GValue	* value,
		GParamSpec	* param_spec)
{
	CriaSlideShow	* self;
	
	self = CRIA_SLIDE_SHOW(object);
	
	switch(prop_id) {
	case PROP_PRESENTATION:
		cria_slide_show_set_presentation(self, g_value_get_object(value));
		break;
	case PROP_SLIDE:
		cria_slide_show_set_slide(self, g_value_get_uint(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

static gboolean
slide_change_timeout_cb(CriaSlideShow* self) {
	g_return_val_if_fail(CRIA_IS_SLIDE_SHOW(self), FALSE);

	/* calling this automatically removes this timer */
	cria_slide_show_set_slide(self, cria_slide_show_next_slide(self));

	return FALSE;
}

void
cria_slide_show_set_slide(CriaSlideShow* self, guint slide) {
	g_return_if_fail(CRIA_IS_SLIDE_SHOW(self));
	g_return_if_fail(CRIA_IS_PRESENTATION(self->presentation));
	g_return_if_fail(slide == (guint)-1 || slide <= cria_slide_list_n_slides(CRIA_SLIDE_LIST(self->presentation)));

	if(slide == (guint)-1) {
		slide = 0;
	}

	if(self->slide == slide) {
		cdebugo(self, "setSlide()", "slide hasn't changed");
		return;
	}

	if(self->priv->slide_change_timer) {
		/* disconnect a timer if there is one */
		remove_slide_change_timer(self);
	}

	self->slide = slide;

	if(slide == cria_slide_list_n_slides(CRIA_SLIDE_LIST(self->presentation))) {
		cdebugo(self, "setSlide()", "setting to the after show slide");
	} else {
		cdebugo(self, "setSlide()", "setting to %d/%d", slide + 1, cria_slide_list_n_slides(CRIA_SLIDE_LIST(self->presentation)));
	}
	
	cria_slide_show_render(self);

#warning "setSlide(): FIXME: adjust timer conditions"
	if(slide_needs_auto_switch(self)) {
		/* connect a timer if neccessary */
		self->priv->slide_change_timer = g_timeout_add(MILLIS_PER_SECOND * cria_presentation_get_slide_delay(self->presentation),
							       G_SOURCE_FUNC(slide_change_timeout_cb),
							       self);
	}

	g_object_notify(G_OBJECT(self), "slide");
}

