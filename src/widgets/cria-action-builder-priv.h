/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIAWIPS_ACTION_BUILDER_PRIVATE_H
#define CRIAWIPS_ACTION_BUILDER_PRIVATE_H

#include <widgets/cria-action-builder.h>
#include <gtk/gtkradioaction.h>

G_BEGIN_DECLS

enum _CriaActionBuilderType {
	CRIA_ACTION_BUILDER_MENU   = 1,
	CRIA_ACTION_BUILDER_TOGGLE,
	CRIA_ACTION_BUILDER_RADIO
};

struct _CriaActionBuilder {
	GObject         base_object;
	guint           merge_id;
	GtkUIManager  * ui_manager;
	gchar         * placeholder;
	GtkActionGroup* actions;
	guint           overflow;
	gchar         * label;
	GtkAction     * submenu_action;
	gpointer        user_data; // for action callbacks
};

struct _CriaActionBuilderClass {
	GObjectClass           object_class;
	CriaActionBuilderType  type;

	/* signal handler in the radio case */
	void (*changed) (GtkRadioAction   * action,
			 GtkRadioAction   * current,
			 CriaActionBuilder* self);

	/* vtable */
	GtkActionEntry const* (*get_actions)       (CriaActionBuilder* self,
					      guint *n_elem);
	GtkRadioActionEntry const* (*get_radio_actions) (CriaActionBuilder* self,
					      guint *n_elem,
					      gint  *value);
	GtkToggleActionEntry const* (*get_toggle_actions)(CriaActionBuilder* self,
					      guint *n_elem);
};

G_END_DECLS

#endif /* !CRIAWIPS_ACTION_BUILDER_PRIVATE_H */

