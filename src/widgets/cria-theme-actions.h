/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIAWIPS_THEME_ACTIONS_H
#define CRIAWIPS_THEME_ACTIONS_H

#include <dom/cria-theme.h>
#include <widgets/cria-action-builder.h>

G_BEGIN_DECLS

typedef struct _CriaThemeActions CriaThemeActions;
typedef struct _CriaThemeActionsClass CriaThemeActionsClass;

#define CRIA_TYPE_THEME_ACTIONS         (cria_theme_actions_get_type())
#define CRIA_THEME_ACTIONS(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), CRIA_TYPE_THEME_ACTIONS, CriaThemeActions))
#define CRIA_THEME_ACTIONS_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), CRIA_TYPE_THEME_ACTIONS, CriaThemeActionClass))
#define CRIA_IS_THEME_ACTIONS(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), CRIA_TYPE_THEME_ACTIONS))
#define CRIA_IS_THEME_ACTIONS_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), CRIA_TYPE_THEME_ACTIONS))
#define CRIA_THEME_ACTIONS_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS(i), CRIA_TYPE_THEME_ACTIONS, CriaTheme_actionsClass)

GType cria_theme_actions_get_type(void);

CriaThemeActions* cria_theme_actions_new      (GtkUIManager* ui_manager,
					       gchar const * placeholder_path);
void              cria_theme_actions_select   (CriaThemeActions* self,
					       CriaSlide       * master_slide);
void              cria_theme_actions_set_theme(CriaThemeActions* self,
					       CriaTheme       * theme);

G_END_DECLS

#endif /* CRIAWIPS_THEME_ACTIONS_H */

