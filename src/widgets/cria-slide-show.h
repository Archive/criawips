/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef SLIDE_SHOW_H
#define SLIDE_SHOW_H

#include <gtk/gtkwindow.h>

#include <dom/cria-presentation.h>

G_BEGIN_DECLS

typedef struct _CriaSlideShow		CriaSlideShow;
typedef struct _CriaSlideShowClass	CriaSlideShowClass;
typedef struct _CriaSlideShowPrivate	CriaSlideShowPrivate;

GType		  cria_slide_show_get_type	       (void);
CriaPresentation* cria_slide_show_get_presentation     (CriaSlideShow		* self);
guint		  cria_slide_show_get_slide	       (CriaSlideShow		* self);
GtkWidget	* cria_slide_show_new		       (CriaPresentation	* presentation);
void		  cria_slide_show_set_presentation     (CriaSlideShow		* self,
							CriaPresentation	* presentation);
void		  cria_slide_show_set_slide	       (CriaSlideShow		* self,
							guint			  slide);

#define CRIA_TYPE_SLIDE_SHOW			(cria_slide_show_get_type ())
#define CRIA_SLIDE_SHOW(object)			(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_SLIDE_SHOW, CriaSlideShow))
#define CRIA_SLIDE_SHOW_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_SLIDE_SHOW, CriaSlideShowClass))
#define CRIA_IS_SLIDE_SHOW(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_SLIDE_SHOW))
#define CRIA_IS_SLIDE_SHOW_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_SLIDE_SHOW))
#define CRIA_SLIDE_SHOW_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_SLIDE_SHOW, CriaSlideShowClass))

struct _CriaSlideShow {
	GtkWindow		  base_instance;
	CriaPresentation	* presentation;
	guint			  slide;
	CriaSlideShowPrivate	* priv;
};

struct _CriaSlideShowClass {
	GtkWindowClass		  base_class;
	CriaSlide		* after_show;

	/* signals */
	void (*signal)	       (CriaSlideShow	* template,
				const	gchar	* string);
};

G_END_DECLS

#endif /* SLIDE_SHOW_H */

