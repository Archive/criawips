/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <widgets/cria-template-pane.h>

#include <widgets/cria-template-model-priv.h>

G_DEFINE_TYPE(CriaTemplatePane, cria_template_pane, GTK_TYPE_ICON_VIEW)

GtkWidget*
cria_template_pane_new(void) {
	return g_object_new(CRIA_TYPE_TEMPLATE_PANE, NULL);
}

static void
cria_template_pane_init(CriaTemplatePane* self) {
	gtk_icon_view_set_text_column(GTK_ICON_VIEW(self), COLUMN_TEXT);
	gtk_icon_view_set_pixbuf_column(GTK_ICON_VIEW(self), COLUMN_PREVIEW);
}

static void
cria_template_pane_class_init(CriaTemplatePaneClass* self_class) {
}

void
cria_template_pane_set_theme(CriaTemplatePane* self, CriaTheme* theme) {
	GtkTreeModel* model = GTK_TREE_MODEL(cria_template_model_for_theme(theme));
	gtk_icon_view_set_model(GTK_ICON_VIEW(self), model);
	g_object_unref(model);
}

