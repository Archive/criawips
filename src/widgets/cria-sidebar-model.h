/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIAWIPS_SIDEBAR_MODEL_H
#define CRIAWIPS_SIDEBAR_MODEL_H

#include <glib-object.h>
#include <gtk/gtktreemodel.h>

#include <dom/cria-presentation-view.h>

G_BEGIN_DECLS

typedef struct _CriaSidebarModel CriaSidebarModel;
typedef struct _CriaSidebarModelClass CriaSidebarModelClass;
typedef struct _CriaSidebarModelPrivate CriaSidebarModelPrivate;

CriaSidebarModel* cria_sidebar_model_for_presentation(CriaPresentation	* self);
CriaSlide*          cria_sidebar_model_get_slide       (CriaSidebarModel 	* self,
							   GtkTreeIter		* iter);
gint		    cria_sidebar_model_get_title_column(void);
GType		    cria_sidebar_model_get_type        (void);
CriaPresentation*   cria_sidebar_model_get_presentation(CriaSidebarModel	* self);

#define CRIA_TYPE_SIDEBAR_MODEL		(cria_sidebar_model_get_type ())
#define CRIA_SIDEBAR_MODEL(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_SIDEBAR_MODEL, CriaSidebarModel))
#define CRIA_SIDEBAR_MODEL_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_SIDEBAR_MODEL, CriaSidebarModelClass))
#define CRIA_IS_SIDEBAR_MODEL(object)	(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_SIDEBAR_MODEL))
#define CRIA_IS_SIDEBAR_MODEL_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_SIDEBAR_MODEL))
#define CRIA_SIDEBAR_MODEL_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_SIDEBAR_MODEL, CriaSidebarModelClass))

struct _CriaSidebarModel {
	GObject			   base_instance;
	CriaSidebarModelPrivate* priv;
};

struct _CriaSidebarModelClass {
	GObjectClass	  base_class;

	/* signals */
	void (*signal)	       (CriaSidebarModel	* self,
				const	gchar	* string);
};

G_END_DECLS

#endif /* !CRIAWIPS_SIDEBAR_MODEL_H */

