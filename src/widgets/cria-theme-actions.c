/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <widgets/cria-theme-actions.h>

#include <string.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

#define CDEBUG_TYPE cria_theme_actions_get_type
#include <cdebug/cdebug.h>

#include <dom/cria-theme-view.h>
#include <widgets/cria-action-builder-priv.h>

struct _CriaThemeActions {
	CriaActionBuilder    builder;
	CriaTheme          * theme;
	GtkRadioActionEntry* entries;
	guint                n_entries;
};

struct _CriaThemeActionsClass {
	CriaActionBuilderClass builder_class;

	/* signals */
	void (*selected) (CriaThemeActions* self,
			  CriaSlide       * master_slide);
};

enum {
	PROP_0,
	PROP_THEME
};

static guint cta_selected_signal;

static void cta_added_master_slide(CriaThemeView* view, CriaSlide* master);

#define build_action_name(d) g_strdup_printf("CriaMasterSlide%d", (d + 1))

static void cta_init_theme_view(CriaThemeViewIface* iface);

G_DEFINE_TYPE_WITH_CODE(CriaThemeActions, cria_theme_actions, CRIA_TYPE_ACTION_BUILDER,
			G_IMPLEMENT_INTERFACE(CRIA_TYPE_THEME_VIEW, cta_init_theme_view));

CriaThemeActions*
cria_theme_actions_new(GtkUIManager* ui_manager, gchar const* placeholder_path) {
	return g_object_new(CRIA_TYPE_THEME_ACTIONS,
			    "label", _("_Apply"),
			    "placeholder", placeholder_path,
			    "ui-manager", ui_manager,
			    NULL);
}

void
cria_theme_actions_select(CriaThemeActions* self, CriaSlide* master_slide) {
	gint index = -1;
	gchar* action_name;
	GtkAction* action;

	if(master_slide) {
		GList* slides = cria_theme_get_master_slides(self->theme);
		index = g_list_index(slides, master_slide);
		g_list_free(slides);
	}

	action_name = build_action_name(index);
	action = gtk_action_group_get_action(CRIA_ACTION_BUILDER(self)->actions, action_name);
	if(action) {
		gtk_toggle_action_set_active(GTK_TOGGLE_ACTION(action), TRUE);
	}
	g_free(action_name);
}

/* GType stuff */
static void
cria_theme_actions_init(CriaThemeActions* self) {}

static void
cta_finalize(GObject* object) {
	CriaThemeActions* self = CRIA_THEME_ACTIONS(object);

	if(self->theme) {
		cria_theme_view_unregister(CRIA_THEME_VIEW(self), self->theme);
		cdebug("finalize()", "removing one of %d references on the theme", G_OBJECT(self->theme)->ref_count);
		g_object_unref(self->theme);
		self->theme = NULL;
	}
	
	G_OBJECT_CLASS(cria_theme_actions_parent_class)->finalize(object);
}

static void
cta_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
	switch(prop_id) {
	case PROP_THEME:
		g_value_set_object(value, CRIA_THEME_ACTIONS(object)->theme);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

void
cria_theme_actions_set_theme(CriaThemeActions* self, CriaTheme* theme) {
	g_return_if_fail(CRIA_IS_THEME_ACTIONS(self));
	g_return_if_fail(!theme || CRIA_IS_THEME(theme));

	if(theme == self->theme) {
		return;
	}
	
	if(self->theme) {
		cria_theme_view_unregister(CRIA_THEME_VIEW(self), self->theme);
		g_object_unref(self->theme);
		self->theme = NULL;
	}

	if(theme) {
		self->theme = g_object_ref(theme);
		cria_theme_view_register(CRIA_THEME_VIEW(self), theme);
	}

#warning "FIXME: next line into theme view"
	cta_added_master_slide(CRIA_THEME_VIEW(self), NULL);

	g_object_notify(G_OBJECT(self), "theme");
}

static void
cta_set_property(GObject* object, guint prop_id, GValue const* value, GParamSpec* pspec) {
	switch(prop_id) {
	case PROP_THEME:
		cria_theme_actions_set_theme(CRIA_THEME_ACTIONS(object), g_value_get_object(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static GtkRadioActionEntry const*
cta_get_radio_actions(CriaActionBuilder* builder, guint* n_elements, gint* value) {
	CriaThemeActions* self = CRIA_THEME_ACTIONS(builder);
	static GtkRadioActionEntry const base[] = {
		{"CriaMasterSlide0", NULL, "_No Master Slide",
		 NULL, NULL, 0},
		{"CriaActionSeparatorConditional", NULL, ""}
	};
	guint i;
	
	g_return_val_if_fail(n_elements, NULL);
	if(!self->theme) {
		*n_elements = 0;
		return NULL;
	}

	*n_elements = G_N_ELEMENTS(base) + cria_theme_get_n_slides(self->theme);
	*value = -1;

	for(i = G_N_ELEMENTS(base); i < self->n_entries; i++) {
		g_free((gchar*)self->entries[i].name);
		g_free((gchar*)self->entries[i].label);
		g_free((gchar*)self->entries[i].tooltip);
	}
	g_free(self->entries);
	self->entries = NULL;

	self->entries = g_new0(GtkRadioActionEntry, *n_elements);
	self->n_entries = *n_elements;
	memcpy(self->entries, base, sizeof(GtkRadioActionEntry) * G_N_ELEMENTS(base));

	for(i = G_N_ELEMENTS(base); i < *n_elements; i++) {
		guint d = i - G_N_ELEMENTS(base);
		CriaSlide* slide = cria_theme_get_master_slide_i(self->theme, d);
		gchar const* title = cria_slide_get_title(slide);
		
		self->entries[i].name = build_action_name(d);
		self->entries[i].stock_id = NULL;
		/* Translators: "_1. <title of master slide>" is the menu title */
		self->entries[i].label = g_strdup_printf(_("_%d. %s"), d + 1, title);
#warning "FIXME: this will lead to problems with more that 9 master slides"
		self->entries[i].accelerator = NULL;
		/* Translators: "Apply <title of master slide>" appears in the status bar */
		self->entries[i].tooltip = g_strdup_printf(_("Apply %s"), title);
		self->entries[i].value = d + 1;
	}

	return self->entries;
}

static void
cta_changed(GtkRadioAction* action, GtkRadioAction* current, CriaActionBuilder* builder) {
	CriaThemeActions* self = CRIA_THEME_ACTIONS(builder);
	gint value;
	CriaSlide* slide;
	
	g_object_get(current, "value", &value, NULL);
	if(value) {
		slide = cria_theme_get_master_slide_i(self->theme, value - 1);
	} else {
		slide = NULL;
	}
	g_signal_emit(self, cta_selected_signal, 0, slide);
}

static void
cria_theme_actions_class_init(CriaThemeActionsClass* self_class) {
	GObjectClass          * go_class;
	CriaActionBuilderClass* cab_class;

	/* GObjectClass */
	go_class = G_OBJECT_CLASS(self_class);
	go_class->finalize     = cta_finalize;
	go_class->get_property = cta_get_property;
	go_class->set_property = cta_set_property;
	
	/* CriaActionBuilderClass */
	cab_class = CRIA_ACTION_BUILDER_CLASS(self_class);
	cab_class->changed           = cta_changed;
	cab_class->get_radio_actions = cta_get_radio_actions;
	cab_class->type              = CRIA_ACTION_BUILDER_RADIO;

	/* CriaThemeViewIface */
	_cria_theme_view_install_properties(go_class, PROP_THEME);

	/* CriaThemeActionsClass */
	/**
	 * CriaThemeActions::selected:
	 * @self: a #CriaThemeActions instance
	 * @slide: the #CriaSlide which has been selected
	 *
	 * This signal get emitted when one of the menu items gets selected,
	 * use it to assign master slides.
	 */
	cta_selected_signal = g_signal_new("selected",
					   CRIA_TYPE_THEME_ACTIONS,
					   G_SIGNAL_RUN_LAST,
					   G_STRUCT_OFFSET(CriaThemeActionsClass, selected),
					   NULL, NULL,
					   g_cclosure_marshal_VOID__OBJECT,
					   G_TYPE_NONE,
					   1, CRIA_TYPE_SLIDE);
}

/* CriaThemeViewIface */
static void
cta_added_master_slide(CriaThemeView* view, CriaSlide* master_slide) {
	cria_action_builder_rebuild(CRIA_ACTION_BUILDER(view));
}

static void
cta_init_theme_view(CriaThemeViewIface* iface) {
	iface->added_master_slide = cta_added_master_slide;
}

