/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIAWIPS_SIDEBAR_H
#define CRIAWIPS_SIDEBAR_H

#include <gtk/gtktreeview.h>

#include <dom/cria-presentation.h>

G_BEGIN_DECLS

typedef struct _CriaSidebar CriaSidebar;
typedef struct _CriaSidebarClass CriaSidebarClass;
typedef struct _CriaSidebarPrivate CriaSidebarPrivate;

#define CRIA_TYPE_SIDEBAR		(cria_sidebar_get_type ())
#define CRIA_SIDEBAR(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_SIDEBAR, CriaSidebar))
#define CRIA_SIDEBAR_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_SIDEBAR, CriaSidebarClass))
#define CRIA_IS_SIDEBAR(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_SIDEBAR))
#define CRIA_IS_SIDEBAR_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_SIDEBAR))
#define CRIA_SIDEBAR_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_SIDEBAR, CriaSidebarClass))

GType		  cria_sidebar_get_type	            (void);

CriaPresentation* cria_sidebar_get_presentation     (CriaSidebar	* self);
CriaSlide*	  cria_sidebar_get_selected	    (CriaSidebar	* self);
GtkWidget*	  cria_sidebar_new		    (CriaPresentation   * presentation);
void		  cria_sidebar_select_first	    (CriaSidebar	* self);
void		  cria_sidebar_select_last	    (CriaSidebar	* self);
void		  cria_sidebar_select_next	    (CriaSidebar	* self);
void		  cria_sidebar_select_previous      (CriaSidebar	* self);
void		  cria_sidebar_set_presentation     (CriaSidebar	* self,
						     CriaPresentation   * presentation);
void              cria_sidebar_set_selected         (CriaSidebar        * self,
						     CriaSlide          * slide);

struct _CriaSidebar {
	GtkTreeView		  base_instance;
	CriaSidebarPrivate	* priv;
};

struct _CriaSidebarClass {
	GtkTreeViewClass  base_class;

	/* signals */
	void (*signal)	       (CriaSidebar	* template,
				const	gchar	* string);
};

G_END_DECLS

#endif /* CRIAWIPS_SIDEBAR_H */

