/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "cria-action-builder-priv.h"

#include <string.h>
#include <glib/gi18n.h>

#define CDEBUG_TYPE cria_action_builder_get_type
#include <cdebug/cdebug.h>

G_DEFINE_ABSTRACT_TYPE(CriaActionBuilder, cria_action_builder, G_TYPE_OBJECT);

enum {
	PROP_0,
	PROP_LABEL,
	PROP_OVERFLOW,
	PROP_PLACEHOLDER,
	PROP_UI_MANAGER
};

static void
cria_action_builder_clear(CriaActionBuilder* self) {
	if(self->merge_id) {
		cdebug("clear()", "removing the ui elements");
		gtk_ui_manager_remove_ui(self->ui_manager, self->merge_id);
		self->merge_id = 0;
	}

	if(self->submenu_action) {
		cdebug("clear()", "removing sub menu action");
		g_object_unref(self->submenu_action);
		self->submenu_action = NULL;
	}

	if(self->actions) {
		cdebug("clear()", "removing the action group");
		gtk_ui_manager_remove_action_group(self->ui_manager, self->actions);
		g_object_unref(self->actions);
		self->actions = NULL;
	}

	cdebug("clear()", "updating the ui manager");
	gtk_ui_manager_ensure_update(self->ui_manager);
	cdebug("clear()", "done updating the ui manager");
}

static GtkActionEntry const*
cria_action_builder_get_actions(CriaActionBuilder* self, guint* n_elements) {
	g_return_val_if_fail(n_elements, NULL);
	*n_elements = 0;

	g_return_val_if_fail(CRIA_ACTION_BUILDER_GET_CLASS(self)->get_actions, NULL);

	return CRIA_ACTION_BUILDER_GET_CLASS(self)->get_actions(self, n_elements);
}

static GtkRadioActionEntry const*
cria_action_builder_get_radio_actions(CriaActionBuilder* self, guint* n_elements, gint* value) {
	g_return_val_if_fail(n_elements, NULL);
	*n_elements = 0;
	g_return_val_if_fail(value, NULL);
	*value = -1;

	g_return_val_if_fail(CRIA_ACTION_BUILDER_GET_CLASS(self)->get_radio_actions, NULL);

	return CRIA_ACTION_BUILDER_GET_CLASS(self)->get_radio_actions(self, n_elements, value);
}

static GtkToggleActionEntry const*
cria_action_builder_get_toggle_actions(CriaActionBuilder* self, guint* n_elements) {
	g_return_val_if_fail(n_elements, NULL);
	*n_elements = 0;

	g_return_val_if_fail(CRIA_ACTION_BUILDER_GET_CLASS(self)->get_toggle_actions, NULL);

	return CRIA_ACTION_BUILDER_GET_CLASS(self)->get_toggle_actions(self, n_elements);
}

static gchar*
cab_add_parent_menu(CriaActionBuilder* self, guint n_real_elements) {
	gchar* merge_path = NULL;
	
	if(n_real_elements > self->overflow) {
		gchar const * const name = "ActionParentMenu";
		self->submenu_action = g_object_new(GTK_TYPE_ACTION,
						 "name",  name,
						 "label", self->label,
						 NULL);
		gtk_action_group_add_action(self->actions, self->submenu_action);
		gtk_ui_manager_add_ui(self->ui_manager,
				      self->merge_id,
				      self->placeholder,
				      name,
				      name,
				      GTK_UI_MANAGER_MENU,
				      FALSE);
		merge_path = g_strdup_printf("%s/%s", self->placeholder, name);
	} else {
		merge_path = g_strdup(self->placeholder);
	}

	return merge_path;
}

#define SEPARATOR_PREFIX "CriaActionSeparator"
#define SEPARATOR_COND   SEPARATOR_PREFIX "Conditional"

static gboolean
cab_is_separator(gchar const* name) {
	static gchar const * const separator_prefix = SEPARATOR_PREFIX;
	static size_t const separator_len =  19; //strlen(separator_prefix);
	
	return !strncmp(name, separator_prefix, separator_len);
}

static gboolean
cab_is_conditional_separator(gchar const* name) {
	static gchar const * const separator_cond = SEPARATOR_COND;
	static size_t const separator_len  = 19; //strlen(SEPARATOR_PREFIX);
	static size_t const separator_diff = 11; //strlen(SEPARATOR_COND) - strlen(SEPARATOR_PREFIX);

	return name && strlen(name) >= 19 &&
	       !strncmp(name + separator_len,
			separator_cond + separator_len,
			separator_diff);
}

static void
cab_add_ui(CriaActionBuilder* self, gchar const* merge_path, gchar const* name, guint n_real_elements) {
	GtkUIManagerItemType type = GTK_UI_MANAGER_AUTO;
	gboolean is_separator = cab_is_separator(name);

	if(is_separator) {
		// Check whether this separator should be built.
		if(cab_is_conditional_separator(name) && // conditional and
		   (n_real_elements <= self->overflow)) {               // not necessary
			// don't write the separators when we're building the
			// items inline (and not in a sub menu)
			return;
		}
		type = GTK_UI_MANAGER_SEPARATOR;
	}

	gtk_ui_manager_add_ui(self->ui_manager,
			      self->merge_id,
			      merge_path,
			      name, is_separator ? NULL : name,
			      type, FALSE);
}

static void
cab_build_menus(CriaActionBuilder* self) {
	guint n_elements, n_separators = 0, i;
	GtkActionEntry const* actions = cria_action_builder_get_actions(self, &n_elements);
	gchar* merge_path;
	
	gtk_action_group_add_actions(self->actions,
				     actions,
				     n_elements,
				     self->user_data);

	for(i = 0; i < n_elements; i++) {
		if(cab_is_conditional_separator(actions[i].name)) {
			n_separators++;
		}
	}
	
	merge_path = cab_add_parent_menu(self, n_elements - n_separators);

	for(i = 0; i < n_elements; i++) {
		cab_add_ui(self, merge_path, actions[i].name, n_elements - n_separators);
	}
}

static void
cab_build_radio_menus(CriaActionBuilder* self) {
	guint n_elements, n_separators = 0, i;
	gint  selected;
	GtkRadioActionEntry const* actions = cria_action_builder_get_radio_actions(self, &n_elements, &selected);
	gchar* merge_path;

	gtk_action_group_add_radio_actions(self->actions,
					   actions,
					   n_elements,
					   selected,
					   G_CALLBACK(CRIA_ACTION_BUILDER_GET_CLASS(self)->changed), self);

	for(i = 0; i < n_elements; i++) {
		if(cab_is_conditional_separator(actions[i].name)) {
			n_separators++;
		}
	}
	
	merge_path = cab_add_parent_menu(self, n_elements - n_separators);

	for(i = 0; i < n_elements; i++) {
		cab_add_ui(self, merge_path, actions[i].name, n_elements - n_separators);
	}
}

static void
cab_build_toggle_menus(CriaActionBuilder* self) {
	guint n_elements, n_separators = 0, i;
	GtkToggleActionEntry const* actions = cria_action_builder_get_toggle_actions(self, &n_elements);
	gchar* merge_path;

	gtk_action_group_add_toggle_actions(self->actions,
					    actions,
					    n_elements,
					    self->user_data);

	for(i = 0; i < n_elements; i++) {
		if(cab_is_conditional_separator(actions[i].name)) {
			n_separators++;
		}
	}
	
	merge_path = cab_add_parent_menu(self, n_elements - n_separators);

	for(i = 0; i < n_elements; i++) {
		cab_add_ui(self, merge_path, actions[i].name, n_elements - n_separators);
	}
}

void
cria_action_builder_rebuild(CriaActionBuilder* self) {
	cria_action_builder_clear(self);
	
	if(!self->merge_id) {
		self->merge_id = gtk_ui_manager_new_merge_id(self->ui_manager);
	}

	if(!self->actions) {
		gchar* group = g_strdup_printf("ActionBuilder%u", self->merge_id);
		self->actions = gtk_action_group_new(group);
		// no need to connect to the connect-proxy as users can connect
		// to the connect-proxy of the ui manager, which should be enough
		gtk_ui_manager_insert_action_group(self->ui_manager, self->actions, -1);
		g_free(group);
	}

	switch(CRIA_ACTION_BUILDER_GET_CLASS(self)->type) {
	case CRIA_ACTION_BUILDER_MENU:
		cab_build_menus(self);
		break;
	case CRIA_ACTION_BUILDER_RADIO:
		cab_build_radio_menus(self);
		break;
	case CRIA_ACTION_BUILDER_TOGGLE:
		cab_build_toggle_menus(self);
		break;
	default:
		g_warning("Got invalid value for the menu type: %d", CRIA_ACTION_BUILDER_GET_CLASS(self)->type);
		break;
	}
}

// GType stuff

static void
cria_action_builder_init(CriaActionBuilder* self) {
	self->label = g_strdup(_("_Actions"));
}

static void
cab_finalize(GObject* object) {
	CriaActionBuilder* self = CRIA_ACTION_BUILDER(object);
	
	g_free(self->label);
	self->label = NULL;

	g_free(self->placeholder);
	self->placeholder = NULL;

	if(self->submenu_action) {
		g_object_unref(self->submenu_action);
		self->submenu_action = NULL;
	}

	g_object_unref(self->ui_manager);
	self->ui_manager = NULL;
	
	G_OBJECT_CLASS(cria_action_builder_parent_class)->finalize(object);
}

static void
cab_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
	CriaActionBuilder* self = CRIA_ACTION_BUILDER(object);

	switch(prop_id) {
	case PROP_LABEL:
		g_value_set_string(value, self->label);
		break;
	case PROP_OVERFLOW:
		g_value_set_uint(value, self->overflow);
		break;
	case PROP_PLACEHOLDER:
		g_value_set_string(value, self->placeholder);
		break;
	case PROP_UI_MANAGER:
		g_value_set_object(value, self->ui_manager);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void
cab_set_property(GObject* object, guint prop_id, GValue const* value, GParamSpec* pspec) {
	CriaActionBuilder* self = CRIA_ACTION_BUILDER(object);
	
	switch(prop_id) {
	case PROP_LABEL:
		g_return_if_fail(g_value_get_string(value));
		g_free(self->label);
		self->label = g_strdup(g_value_get_string(value));
		if(self->submenu_action) {
			g_object_set(self->submenu_action,
				     "label", self->label,
				     NULL);
		}
		g_object_notify(object, "label");
		break;
	case PROP_OVERFLOW:
		self->overflow = g_value_get_uint(value);
		g_object_notify(object, "overflow");
		if(self->ui_manager && self->placeholder) {
			cria_action_builder_rebuild(self);
		}
		break;
	case PROP_PLACEHOLDER:
		if(self->placeholder) {
			g_free(self->placeholder);
		}
		self->placeholder = g_strdup(g_value_get_string(value));
		g_object_notify(object, "placeholder");
		if(self->ui_manager) {
			cria_action_builder_rebuild(self);
		}
		break;
	case PROP_UI_MANAGER:
		self->ui_manager = g_object_ref(g_value_get_object(value));
		g_object_notify(object, "ui-manager");
		if(self->placeholder) {
			cria_action_builder_rebuild(self);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void
cria_action_builder_class_init(CriaActionBuilderClass* self_class) {
	/* GObjectClass */
	GObjectClass* go_class = G_OBJECT_CLASS(self_class);
	go_class->finalize     = cab_finalize;
	go_class->get_property = cab_get_property;
	go_class->set_property = cab_set_property;

	g_object_class_install_property(go_class,
					PROP_LABEL,
					g_param_spec_string("label",
							    "Sub menu label",
							    "The label that's used for the sub menu if we have one",
							    _("_Actions"),
							    G_PARAM_READWRITE));
	g_object_class_install_property(go_class,
					PROP_OVERFLOW,
					g_param_spec_uint("overflow",
							  "Overflow",
							  "The maximum number of elements that's allowed to "
							  "be nested. If there are more items to be displayed, "
							  "they'll be displayed in a separate submenu",
							  0, G_MAXUINT,
							  4,
							  G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property(go_class,
					PROP_PLACEHOLDER,
					g_param_spec_string("placeholder",
							    "Placeholder",
							    "The path to the placeholder for the menu entries",
							    NULL,
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property(go_class,
					PROP_UI_MANAGER,
					g_param_spec_object("ui-manager",
							    "UI Manager",
							    "The ui manager to connect to",
							    GTK_TYPE_UI_MANAGER,
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));

	/* CriaActionBuilderClass */
	self_class->type = CRIA_ACTION_BUILDER_MENU;
}

