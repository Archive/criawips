/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef PRESENTATION_PROPERTIES_DIALOG_H
#define PRESENTATION_PROPERTIES_DIALOG_H

#include <gtk/gtkdialog.h>

#include <dom/cria-presentation.h>

G_BEGIN_DECLS

typedef struct _CriaPresentationPropertiesDialog CriaPresentationPropertiesDialog;
typedef struct _CriaPresentationPropertiesDialogClass CriaPresentationPropertiesDialogClass;
typedef struct _CriaPresentationPropertiesDialogPrivate CriaPresentationPropertiesDialogPrivate;

CriaPresentation*
		cria_presentation_properties_dialog_get_presentation   (CriaPresentationPropertiesDialog*
												self);
GType		cria_presentation_properties_dialog_get_type	       (void);
GtkWidget*	cria_presentation_properties_dialog_new		       (CriaPresentation*	presentation);
void		cria_presentation_properties_dialog_set_presentation   (CriaPresentationPropertiesDialog*
												self,
									CriaPresentation*       presentation);

#define CRIA_TYPE_PRESENTATION_PROPERTIES_DIALOG		(cria_presentation_properties_dialog_get_type ())
#define CRIA_PRESENTATION_PROPERTIES_DIALOG(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_PRESENTATION_PROPERTIES_DIALOG, CriaPresentationPropertiesDialog))
#define CRIA_PRESENTATION_PROPERTIES_DIALOG_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_PRESENTATION_PROPERTIES_DIALOG, CriaPresentationPropertiesDialogClass))
#define CRIA_IS_PRESENTATION_PROPERTIES_DIALOG(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_PRESENTATION_PROPERTIES_DIALOG))
#define CRIA_IS_PRESENTATION_PROPERTIES_DIALOG_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_PRESENTATION_PROPERTIES_DIALOG))
#define CRIA_PRESENTATION_PROPERTIES_DIALOG_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_PRESENTATION_PROPERTIES_DIALOG, CriaPresentationPropertiesDialogClass))

struct _CriaPresentationPropertiesDialog {
	GtkDialog	  base_instance;
	CriaPresentationPropertiesDialogPrivate
			* priv;
};

struct _CriaPresentationPropertiesDialogClass {
	GtkDialogClass	  base_class;

	/* signals */
	/*void (*signal)	       (CriaPresentationPropertiesDialog	* self,
				const	gchar	* string);*/
};

G_END_DECLS

#endif /* !PRESENTATION_PROPERTIES_DIALOG_H */

