/* This file is part of Criawips
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2007 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <widgets/cria-sidebar.h>

#include <inttypes.h>

#include <glib.h>
#include <glib-object.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <glade/glade.h>

#define CDEBUG_TYPE cria_sidebar_get_type
#include <cdebug/cdebug.h>

#include <widgets/cria-sidebar-model.h>
#include <widgets/cria-slide-properties-dialog.h>

enum {
	PROP_0,
	PROP_PRESENTATION
};

enum {
	SIGNAL,
	N_SIGNALS
};

struct _CriaSidebarPrivate {
	CriaPresentation	* presentation;
	GtkTreeModel		* model;
	GladeXML		* xml;
};

static	void	cria_sidebar_get_property   (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static void  cria_sidebar_init	       (CriaSidebar	* self);
static void  cria_sidebar_new_slide_after   (CriaSidebar	* self,
						GtkMenuItem	* item);
static void  cria_sidebar_new_slide_before  (CriaSidebar	* self,
						GtkMenuItem	* item);
static	void	cria_sidebar_set_property   (GObject		* object,
						guint		  prop_id,
						const	GValue	* value,
						GParamSpec	* param_spec);
static	void	cria_sidebar_show_properties(CriaSidebar	* self,
						GtkMenuItem	* item);
#if 0
static	guint	cria_sidebar_signals[N_SIGNALS] = { 0 };

static	void	cria_sidebar_signal	       (CriaSidebar	* template,
						const	gchar	* string);
#endif

G_DEFINE_TYPE_WITH_CODE(CriaSidebar, cria_sidebar, GTK_TYPE_TREE_VIEW,
			G_IMPLEMENT_INTERFACE(CRIA_TYPE_PRESENTATION_VIEW, NULL));

static void
cria_sidebar_do_popup(CriaSidebar* self, GdkEventButton* event) {
	gint	button;
	guint	time;

	g_return_if_fail(CRIA_IS_SIDEBAR(self));
	g_return_if_fail(CRIA_SIDEBAR(self)->priv != NULL);
	g_return_if_fail(GLADE_IS_XML(CRIA_SIDEBAR(self)->priv->xml));

	if(event) {
		button = event->button;
		time = event->time;
	} else {
		button = 0;
		time = gtk_get_current_event_time();
	}

	gtk_menu_popup(GTK_MENU(glade_xml_get_widget(CRIA_SIDEBAR(self)->priv->xml, "popup_slide")),
		       NULL, NULL, NULL, NULL,
		       button, time);
}

static gboolean
cria_sidebar_popup_menu(GtkWidget* self) {
	g_return_val_if_fail(CRIA_IS_SIDEBAR(self), FALSE);

	cria_sidebar_do_popup(CRIA_SIDEBAR(self), NULL);

	return TRUE;
}

static gboolean
cria_sidebar_button_press (GtkWidget     * widget,
			   GdkEventButton* ev)
{
	gboolean     forwarded = FALSE;
	gboolean     retval = FALSE;
	CriaSidebar* self   = CRIA_SIDEBAR(widget);

	if(ev->button != 3 || gtk_tree_selection_count_selected_rows(gtk_tree_view_get_selection(GTK_TREE_VIEW(widget))) == 0) {
		retval |= GTK_WIDGET_CLASS(cria_sidebar_parent_class)->button_press_event(widget, ev);
		forwarded = TRUE;
	}

	if(ev->button == 3 && ev->type == GDK_BUTTON_PRESS) {
		cria_sidebar_do_popup(self, ev);
		retval = TRUE;
	}

	if (!forwarded) {
		retval |= GTK_WIDGET_CLASS(cria_sidebar_parent_class)->button_press_event(widget, ev);
	}

	return retval;
}

static void
csl_finalize(GObject* object) {
	CriaSidebar* self = CRIA_SIDEBAR(object);

	if(self->priv->presentation) {
		g_object_unref(self->priv->presentation);
		self->priv->presentation = NULL;
	}
}

static void
cria_sidebar_class_init(CriaSidebarClass* cria_sidebar_class) {
	GObjectClass	* g_object_class;
	GtkWidgetClass	* widget_class;

	/* setting up the GObject class */
	g_object_class = G_OBJECT_CLASS(cria_sidebar_class);
	g_object_class->finalize = csl_finalize;
#if 0
	/* setting up signal system */
	cria_sidebar_class->signal = cria_sidebar_signal;

	cria_sidebar_signals[SIGNAL] = g_signal_new (
			"signal",
			CRIA_TYPE_SIDEBAR,
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (
				CriaSidebarClass,
				signal),
			NULL,
			NULL,
			g_cclosure_marshal_VOID__STRING,
			G_TYPE_NONE,
			0);
#endif
	/* setting up property system */
	g_object_class->set_property = cria_sidebar_set_property;
	g_object_class->get_property = cria_sidebar_get_property;

	g_object_class_install_property(g_object_class,
					PROP_PRESENTATION,
					g_param_spec_object("presentation",
							    "Presentation",
							    "The presentation that's currently shown in the slide list",
							    CRIA_TYPE_PRESENTATION,
							    G_PARAM_READWRITE));

	/* setting up the GtkWidget class */
	widget_class = GTK_WIDGET_CLASS(cria_sidebar_class);
	widget_class->button_press_event = cria_sidebar_button_press;
	widget_class->popup_menu = cria_sidebar_popup_menu;
}

/**
 * cria_sidebar_get_presentation:
 * @self: The #CriaSidebar to query
 *
 * Get the presentation that's being displayed by a #CriaSidebar
 *
 * Returns the presentation displayed by a #CriaSidebar
 */
CriaPresentation*
cria_sidebar_get_presentation(CriaSidebar* self) {
	g_return_val_if_fail (CRIA_IS_SIDEBAR(self), NULL);
	
	return self->priv->presentation;
}

static void
cria_sidebar_get_property (GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaSidebar	* self;

	self = CRIA_SIDEBAR (object);

	switch (prop_id)
	{
	case PROP_PRESENTATION:
		g_value_set_object (
				value,
				self->priv->presentation);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (
				object,
				prop_id,
				param_spec);
		break;
	}
}

/**
 * cria_sidebar_get_selected:
 * @self: a #CriaSidebar
 *
 * Get the slide that's currently selected on a #CriaSidebar (or the first
 * one if no slide is selected.
 *
 * Returns a #CriaSlide (either the selected or the first one), NULL on error
 */
CriaSlide*
cria_sidebar_get_selected(CriaSidebar* self) {
	GtkTreeIter	  iter;
	GtkTreeModel	* model;
	CriaSlide	* slide;
	
	g_return_val_if_fail(CRIA_IS_SIDEBAR(self), NULL);
	g_return_val_if_fail(GTK_IS_TREE_VIEW(self), NULL);
	
	if(gtk_tree_selection_get_selected(gtk_tree_view_get_selection(GTK_TREE_VIEW(self)), &model, &iter)) {
		slide = cria_sidebar_model_get_slide(CRIA_SIDEBAR_MODEL(model), &iter);
	} else {
		slide = NULL;
	}
	
	return slide;
}

static void
cb_renderer_text_edited(CriaSidebar* self, gchar* tree_path, gchar* new_value, GtkCellRendererText* renderer) {
	GtkTreeIter	  iter;
	CriaSlide	* slide;

	g_return_if_fail(tree_path);

	g_return_if_fail(gtk_tree_model_get_iter_from_string(self->priv->model, &iter, tree_path));
	slide = cria_sidebar_model_get_slide(CRIA_SIDEBAR_MODEL(self->priv->model), &iter);
	g_return_if_fail(CRIA_IS_SLIDE(slide));

#warning "SlideList::cbRendererTextEdited(): FIXME: move this functionality to the SlideListProxy"
	cria_slide_set_title(slide, new_value);
}

enum {
	DRAG_TARGET_SLIDE,
};

static void
sidebar_remove_slide (CriaSidebar* self,
		      GtkMenuItem* item)
{
	GtkTreeModel	* model;
	GtkTreeIter       iter;

	if (gtk_tree_selection_get_selected(gtk_tree_view_get_selection(GTK_TREE_VIEW(self)),
					    &model,
					    &iter))
	{
		CriaSlide* slide    = cria_sidebar_model_get_slide (CRIA_SIDEBAR_MODEL (model), &iter);
		gint       position = cria_slide_list_index (CRIA_SLIDE_LIST (cria_sidebar_get_presentation (self)), slide);
		cria_slide_list_remove (CRIA_SLIDE_LIST (cria_sidebar_get_presentation (self)),
				        position);
	}
}

static void
cria_sidebar_init(CriaSidebar *self) {
	GtkTreeViewColumn	* column;
	GtkCellRenderer		* renderer;
#warning "SlideList::init(): FIXME: add drag and drop support once the SlideListProxy works"
/*	const GtkTargetEntry target_table [] = {
		{ "GTK_TREE_MODEL_ROW", GTK_TARGET_SAME_WIDGET, 0 }
	};*/
	
	/* setting up private data */
	self->priv = g_new0(CriaSidebarPrivate,1);
#warning "SlideList::init(): FIXME: add SlideSelectionClass"
	
	/* setting up the tree view */
#warning "SlideList::init(): FIXME: add drag and drop support once the SlideListProxy works"
/*	gtk_tree_view_enable_model_drag_source(GTK_TREE_VIEW(self),
					       0,
					       target_table,
					       G_N_ELEMENTS(target_table),
					       GDK_ACTION_MOVE);
	gtk_tree_view_enable_model_drag_dest(GTK_TREE_VIEW(self),
					     target_table,
					     G_N_ELEMENTS(target_table),
					     GDK_ACTION_MOVE);*/
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(self), FALSE);
	renderer = gtk_cell_renderer_text_new();
	g_object_set(G_OBJECT(renderer), "editable", TRUE, "editable-set", TRUE, NULL);
	g_signal_connect_swapped(renderer, "edited",
				 G_CALLBACK(cb_renderer_text_edited), self);
	column = gtk_tree_view_column_new();
	gtk_tree_view_column_pack_start(column, renderer, FALSE);
	gtk_tree_view_column_add_attribute(column, renderer,
					   "text", cria_sidebar_model_get_title_column());
	gtk_tree_view_column_set_title(column, _("Slides"));
	gtk_tree_view_append_column(GTK_TREE_VIEW(self), column);

	/* setting up the context menu */
	self->priv->xml = glade_xml_new(PACKAGE_DATA_DIR "/" PACKAGE "/data/criawips.glade", "popup_slide", NULL);
	g_signal_connect_swapped(glade_xml_get_widget(self->priv->xml, "popup_slide_menu_new_before"), "activate",
				 G_CALLBACK(cria_sidebar_new_slide_before), self);
	g_signal_connect_swapped(glade_xml_get_widget(self->priv->xml, "popup_slide_menu_new_after"), "activate",
				 G_CALLBACK(cria_sidebar_new_slide_after), self);
	g_signal_connect_swapped (glade_xml_get_widget (self->priv->xml, "popup_slide_menu_remove"),
				  "activate",
				  G_CALLBACK (sidebar_remove_slide),
				  self);
	g_signal_connect_swapped(glade_xml_get_widget(self->priv->xml, "popup_slide_menu_properties"), "activate",
				 G_CALLBACK(cria_sidebar_show_properties), self);

	return;
}

GtkWidget*
cria_sidebar_new(CriaPresentation* presentation) {
	CriaSidebar	* self;

	g_return_val_if_fail(!presentation || CRIA_IS_PRESENTATION(presentation), NULL);
	
	self = g_object_new(CRIA_TYPE_SIDEBAR, NULL);

	if(presentation) {
		cria_sidebar_set_presentation(self, presentation);
	}

	return GTK_WIDGET(self);
}

static void
cria_sidebar_new_slide (CriaSidebar* self,
			gboolean     after)
{
	CriaTheme	* theme;
	CriaSlide	* slide;
	CriaSlide	* master_slide = NULL;
	gint		  new_pos;
	GtkTreeIter	  iter;
	GtkTreeModel	* model;

	gtk_tree_selection_get_selected(gtk_tree_view_get_selection(GTK_TREE_VIEW(self)),
					&model,
					&iter);
	slide = cria_sidebar_model_get_slide(CRIA_SIDEBAR_MODEL(model), &iter);

	new_pos = cria_slide_list_index(CRIA_SLIDE_LIST(cria_sidebar_get_presentation(self)), slide);

	if(after) {
		new_pos += 1;
	}

	cdebugo(self, "newSlide()", "new index is %i", new_pos);

	slide = cria_slide_new_pos(CRIA_SLIDE_LIST(cria_sidebar_get_presentation(self)), new_pos);

	theme = cria_presentation_get_theme (cria_sidebar_get_presentation (self));
	if (theme) {
		master_slide = cria_theme_get_default_slide (theme);
	}

	if (master_slide) {
		cria_slide_set_master_slide (slide, master_slide);
	}
}

static void
cria_sidebar_new_slide_after(CriaSidebar* self, GtkMenuItem* item) {
	cria_sidebar_new_slide(self, TRUE);
}

static void
cria_sidebar_new_slide_before(CriaSidebar* self, GtkMenuItem* item) {
	cria_sidebar_new_slide(self, FALSE);
}

void
cria_sidebar_select_first(CriaSidebar* self) {
	GtkTreeIter	  iter;
	
	g_return_if_fail(CRIA_IS_SIDEBAR(self));
	g_return_if_fail(GTK_IS_TREE_VIEW(self));

	gtk_tree_model_get_iter_first(self->priv->model, &iter);
	gtk_tree_selection_select_iter(gtk_tree_view_get_selection(GTK_TREE_VIEW(self)),
				       &iter);
}

void
cria_sidebar_select_last(CriaSidebar* self) {
	GtkTreePath	* path;
	
	g_return_if_fail(CRIA_IS_SIDEBAR(self));
	g_return_if_fail(GTK_IS_TREE_VIEW(self));
	
	path = gtk_tree_path_new_from_indices(cria_slide_list_n_slides(CRIA_SLIDE_LIST(self->priv->presentation)) - 1,
					      -1);
	gtk_tree_selection_select_path(gtk_tree_view_get_selection(GTK_TREE_VIEW(self)),
				       path);
	gtk_tree_path_free(path);
}

void
cria_sidebar_select_next(CriaSidebar* self) {
	GtkTreeIter	  iter;
	GtkTreeModel	* store;
	
	g_return_if_fail(CRIA_IS_SIDEBAR(self));
	g_return_if_fail(GTK_IS_TREE_VIEW(self));
	
	gtk_tree_selection_get_selected(gtk_tree_view_get_selection(GTK_TREE_VIEW(self)),
					&store,
					&iter);
	if(gtk_tree_model_iter_next(GTK_TREE_MODEL(store), &iter)) {
		gtk_tree_selection_select_iter(gtk_tree_view_get_selection(GTK_TREE_VIEW(self)),
					       &iter);
	}
}

void
cria_sidebar_select_previous(CriaSidebar* self) {
	gint	  	* indices;
	GtkTreeIter	  iter;
	GtkTreeModel	* model;
	GtkTreePath	* path,
			* newpath;

	gtk_tree_selection_get_selected(gtk_tree_view_get_selection(GTK_TREE_VIEW(self)),
					&model,
					&iter);
	
	path = gtk_tree_model_get_path(model, &iter);
	indices = gtk_tree_path_get_indices(path);
	
	if(*indices > 0) {
		*indices -= 1;
	}

	newpath = gtk_tree_path_new_from_indices(*indices, -1);
	gtk_tree_selection_select_path(gtk_tree_view_get_selection(GTK_TREE_VIEW(self)),
				       newpath);
	gtk_tree_path_free(path);
	gtk_tree_path_free(newpath);
}

/**
 * cria_sidebar_set_presentation:
 * @self: a #CriaSidebar
 * @presentation: a #CriaPresentation
 *
 * Tell @self to display @presentation.
 */
void
cria_sidebar_set_presentation(CriaSidebar* self, CriaPresentation* presentation) {
	cdebugo(self, "setPresentation()", "start");
	
	g_return_if_fail(CRIA_IS_SIDEBAR(self));
	g_return_if_fail(CRIA_IS_PRESENTATION(presentation));

	g_return_if_fail(cria_slide_list_n_slides(CRIA_SLIDE_LIST(presentation)) > 0);
	g_return_if_fail(CRIA_IS_SLIDE(cria_slide_list_get(CRIA_SLIDE_LIST(presentation), 0)));

	if(presentation == self->priv->presentation) {
		return;
	}

	if(self->priv->presentation != NULL) {
		cria_presentation_view_register(CRIA_PRESENTATION_VIEW(self), self->priv->presentation);
		g_object_unref(self->priv->presentation);
		self->priv->presentation = NULL;
	}

	if(presentation != NULL) {
		cdebugo(self, "setPresentation()", "got a real presentation");
		self->priv->presentation = g_object_ref(presentation);
		self->priv->model = GTK_TREE_MODEL(cria_sidebar_model_for_presentation(presentation));
		gtk_tree_view_set_model(GTK_TREE_VIEW(self),
					self->priv->model);
		g_object_unref(self->priv->model);
	}

	g_object_notify(G_OBJECT(self), "presentation");

	cdebugo(self, "setPresentation()", "end");
}

void
cria_sidebar_set_selected(CriaSidebar* self, CriaSlide* slide) {
	GtkTreeSelection* sel;
	guint index;
	
	g_return_if_fail(CRIA_IS_SIDEBAR(self));
	g_return_if_fail(CRIA_IS_SLIDE(slide));

	sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(self));

	if(slide && slide == cria_sidebar_get_selected(self)) {
		// nothing to do
	} else if(!slide || (guint)-1 == (index = cria_slide_list_index(CRIA_SLIDE_LIST(self->priv->presentation), slide))) {
		gtk_tree_selection_unselect_all(sel);
	} else {
		GtkTreePath* path = gtk_tree_path_new();
		gtk_tree_path_append_index(path, index);
		gtk_tree_selection_select_path(sel, path);
		gtk_tree_path_free(path);
	}
}

static void
cria_sidebar_set_property (GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaSidebar	* self;
	
	self = CRIA_SIDEBAR (object);
	
	switch (prop_id) {
	case PROP_PRESENTATION:
		cria_sidebar_set_presentation(self, g_value_get_object (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

static void
cria_sidebar_show_properties(CriaSidebar* self, GtkMenuItem* item) {
	CriaSlide	* slide;
	GtkTreeModel	* model;
	GtkTreeIter	  iter;

	g_return_if_fail(CRIA_IS_SIDEBAR(self));

	gtk_tree_selection_get_selected(gtk_tree_view_get_selection(GTK_TREE_VIEW(self)),
					&model,
					&iter);
	slide = cria_sidebar_model_get_slide(CRIA_SIDEBAR_MODEL(model), &iter);
	g_return_if_fail(CRIA_IS_SLIDE(slide));
	gtk_widget_show(cria_slide_properties_dialog_new(slide));
}

