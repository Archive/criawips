/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <widgets/cria-sidebar-model.h>

#include <inttypes.h>
#include <string.h>

#include <gtk/gtktreednd.h>

#define CDEBUG_TYPE cria_sidebar_model_get_type
#include <cdebug/cdebug.h>

#include <dom/cria-slide-view.h>

enum {
	PROP_0,
};

enum {
	N_SIGNALS
};

enum {
	COLUMN_PREVIEW,
	COLUMN_TITLE,
	COLUMN_SLIDE_PTR,
	N_COLUMNS
};

static GType *column_types = NULL;

#warning "FIXME: extract a private header file"
struct _CriaSidebarModelPrivate {
	CriaPresentation	* presentation;
	gboolean                  disposed;
};

static void     cria_sidebar_model_get_property     (GObject			* object,
							guint			  prop_id,
							GValue			* value,
							GParamSpec		* param_spec);
static void     cria_sidebar_model_init	       (CriaSidebarModel	* self);
static gboolean cria_sidebar_model_iter_children    (GtkTreeModel		* tree_model,
							GtkTreeIter		* iter,
							GtkTreeIter		* parent);
static gboolean cria_sidebar_model_iter_has_child   (GtkTreeModel		* tree_model,
							GtkTreeIter		* iter);
static gboolean cria_sidebar_model_iter_next	       (GtkTreeModel		* tree_model,
							GtkTreeIter		* iter);
static gint     cria_sidebar_model_iter_n_children  (GtkTreeModel		* tree_model,
							GtkTreeIter		* iter);
static gboolean cria_sidebar_model_iter_nth_child   (GtkTreeModel		* tree_model,
							GtkTreeIter		* iter,
							GtkTreeIter		* parent,
							gint			  n);
static gboolean cria_sidebar_model_iter_parent      (GtkTreeModel		* tree_model,
							GtkTreeIter		* iter,
							GtkTreeIter		* child);
static void     cria_sidebar_model_ref_node         (GtkTreeModel		* tree_model,
							GtkTreeIter		* iter);
static void     cria_sidebar_model_set_property     (GObject			* object,
							guint			  prop_id,
							const GValue		* value,
							GParamSpec		* param_spec);
static void     cria_sidebar_model_unref_node       (GtkTreeModel		* tree_model,
							GtkTreeIter		* iter);
#if 0
/* enable these to add support for signals */
static	guint	cria_sidebar_model_signals[N_SIGNALS] = { 0 };

static	void	cria_sidebar_model_signal	       (CriaSidebarModel	* self,
						const	gchar	* string);
#endif

static void csm_init_presentation_view(CriaPresentationViewIface* iface);
static void csm_init_tree_model(GtkTreeModelIface* iface);
static void csm_init_tree_drag_source(GtkTreeDragSourceIface* iface);
static void csm_init_tree_drag_dest(GtkTreeDragDestIface* iface);
static void csm_init_slide_view(CriaSlideViewIface* iface);

G_DEFINE_TYPE_WITH_CODE(CriaSidebarModel, cria_sidebar_model, G_TYPE_OBJECT,
			G_IMPLEMENT_INTERFACE(CRIA_TYPE_PRESENTATION_VIEW, csm_init_presentation_view);
			G_IMPLEMENT_INTERFACE(GTK_TYPE_TREE_MODEL, csm_init_tree_model);
			G_IMPLEMENT_INTERFACE(GTK_TYPE_TREE_DRAG_SOURCE, csm_init_tree_drag_source);
			G_IMPLEMENT_INTERFACE(GTK_TYPE_TREE_DRAG_DEST, csm_init_tree_drag_dest);
			G_IMPLEMENT_INTERFACE(CRIA_TYPE_SLIDE_VIEW, csm_init_slide_view));

static void
cslp_dispose(GObject* object) {
	CriaSidebarModel* self = CRIA_SIDEBAR_MODEL(object);

	if(self->priv->disposed) {
		return;
	}

	self->priv->disposed = TRUE;

	if(self->priv->presentation) {
		cdebug("dispose()", "unregistering presentation");
		cria_presentation_view_unregister(CRIA_PRESENTATION_VIEW(self), self->priv->presentation);
		cdebug("dispose()", "unregistered presentation");
		g_object_unref(self->priv->presentation);
		self->priv->presentation = NULL;
	}

	G_OBJECT_CLASS(cria_sidebar_model_parent_class)->dispose(object);
}

static void
cria_sidebar_model_class_init(CriaSidebarModelClass* cria_sidebar_model_class) {
	GObjectClass	* g_object_class;

#if 0
	/* setting up signal system */
	cria_sidebar_model_class->signal = cria_sidebar_model_signal;

	cria_sidebar_model_signals[SIGNAL] = g_signal_new (
			"signal",
			CRIA_TYPE_SIDEBAR_MODEL,
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (
				CriaSidebarModelClass,
				signal),
			NULL,
			NULL,
			g_cclosure_marshal_VOID__STRING,
			G_TYPE_NONE,
			0);
#endif
	/* setting up property system */
	g_object_class = G_OBJECT_CLASS(cria_sidebar_model_class);
	g_object_class->dispose      = cslp_dispose;
	g_object_class->get_property = cria_sidebar_model_get_property;
	g_object_class->set_property = cria_sidebar_model_set_property;

	/* setting up the PresentationView interface */
	_cria_presentation_view_install_properties(g_object_class);

	/* setting up the slide list proxy class */
#warning "SlideListProxy::classInit(): FIXME: initialize this a bit more dynamically"
	column_types = g_new0(GType, N_COLUMNS);
	column_types[0] = GDK_TYPE_PIXBUF;
	column_types[1] = G_TYPE_STRING;
	column_types[2] = CRIA_TYPE_SLIDE;
}

/**
 * cria_sidebar_model_for_presentation:
 * @presentation: a #CriaPresentation
 *
 * Get a #CriaSidebarModel for @presentation.
 *
 * Returns a #CriaSidebarModel that provides a #GtkTreeModel for a #CriaPresentation.
 */
CriaSidebarModel*
cria_sidebar_model_for_presentation(CriaPresentation* presentation) {
#warning "SlideBarModel::forPresentation(): FIXME: build a hash tree and do not always create a new proxy"
	return g_object_new(CRIA_TYPE_SIDEBAR_MODEL, "presentation", presentation, NULL);
}

static GType
cria_sidebar_model_get_column_type(GtkTreeModel* tree_model, gint index) {
	g_return_val_if_fail(0 <= index && index < N_COLUMNS, 0);

	return column_types[index];
}

static GtkTreeModelFlags
cria_sidebar_model_get_flags(GtkTreeModel* tree_model) {
	g_return_val_if_fail(CRIA_IS_SIDEBAR_MODEL(tree_model), 0);

#warning "getFlags(): FIXME: check whether we want GTK_TREE_MODEL_ITERS_PERSIST too"
	return GTK_TREE_MODEL_LIST_ONLY;
}

static gboolean
cria_sidebar_model_get_iter(GtkTreeModel* tree_model, GtkTreeIter* iter, GtkTreePath* path) {
	guint		  i;
	CriaSidebarModel* self;
	
	g_return_val_if_fail(CRIA_IS_SIDEBAR_MODEL(tree_model), FALSE);
	g_return_val_if_fail(gtk_tree_path_get_depth(path) > 0, FALSE);

	self = CRIA_SIDEBAR_MODEL(tree_model);

	i = gtk_tree_path_get_indices(path)[0];

	if(i >= cria_slide_list_n_slides(CRIA_SLIDE_LIST(self->priv->presentation))) {
		return FALSE;
	}
	
	iter->user_data = cria_slide_list_get(CRIA_SLIDE_LIST(self->priv->presentation), i);

	if(iter->user_data) {
		return TRUE;
	}

	return FALSE;
}

static gint
cria_sidebar_model_get_n_columns(GtkTreeModel *tree_model) {
	g_return_val_if_fail(CRIA_IS_SIDEBAR_MODEL(tree_model), 0);

	return N_COLUMNS;
}

static GtkTreePath*
cria_sidebar_model_get_path(GtkTreeModel* tree_model, GtkTreeIter* iter) {
	GtkTreePath     * path = NULL;
	CriaSidebarModel* self;
	gint		  index;

	g_return_val_if_fail(CRIA_IS_SIDEBAR_MODEL(tree_model), NULL);
	self = CRIA_SIDEBAR_MODEL(tree_model);

	index = cria_slide_list_index(CRIA_SLIDE_LIST(self->priv->presentation), CRIA_SLIDE(iter->user_data));
	g_return_val_if_fail(index >= 0, path);

	path = gtk_tree_path_new();
	gtk_tree_path_append_index(path, index);
	return path;
}

CriaPresentation*
cria_sidebar_model_get_presentation(CriaSidebarModel* self) {
	g_return_val_if_fail(CRIA_IS_SIDEBAR_MODEL(self), NULL);
	
	return self->priv->presentation;
}

static void
cria_sidebar_model_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaSidebarModel	* self;

	self = CRIA_SIDEBAR_MODEL(object);

	switch(prop_id) {
	case CRIA_PRESENTATION_VIEW_PROP_PRESENTATION:
		g_value_set_object(value, self->priv->presentation);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object,
						  prop_id,
						  param_spec);
		break;
	}
}

/**
 * cria_sidebar_model_get_slide:
 * @self: a #CriaSidebarModel
 * @iter: a #GtkTreeIter
 *
 * Get the slide represented by an Iterator.
 *
 * Returns the #CriaSlide that's being referred to by the iterator.
 */
CriaSlide*
cria_sidebar_model_get_slide(CriaSidebarModel* self, GtkTreeIter* iter) {
	return iter->user_data;
}

/**
 * cria_sidebar_model_get_title_column:
 * 
 * Get the column that should be used by a #GtkTreeView to get the title of a
 * slide.
 *
 * Returns the title column index.
 */
gint
cria_sidebar_model_get_title_column(void) {
	return COLUMN_TITLE;
}

static gboolean
cslp_row_draggable(GtkTreeDragSource* source, GtkTreePath* path) {
	/* as long as we only display slides, there's no problem here */
	return TRUE;
}

static CriaSlideList*
cslp_container_for_path_depth(CriaSidebarModel* self, GtkTreePath* path, gint depth) {
	CriaSlideList* c = CRIA_SLIDE_LIST(self->priv->presentation);
	gint i = 0;

	g_return_val_if_fail(gtk_tree_path_get_depth(path)  >= depth, NULL);

	for(i = 0; i < depth; i++) {
		guint num = gtk_tree_path_get_indices(path)[i];
		if(num >= cria_slide_list_n_slides(c)) {
			return NULL;
		}
		c = CRIA_SLIDE_LIST(cria_slide_list_get(c, num));
	}

	g_return_val_if_fail(CRIA_IS_SLIDE_LIST(c), NULL);
	return c;
}

static CriaSlideList*
cslp_container_for_path(CriaSidebarModel* self, GtkTreePath* path) {
	return cslp_container_for_path_depth(self, path, gtk_tree_path_get_depth(path));
}

static gboolean
cslp_drag_data_get(GtkTreeDragSource* source, GtkTreePath* path, GtkSelectionData* data) {
	CriaSlideList* c;

	c = cslp_container_for_path(CRIA_SIDEBAR_MODEL(source), path);
	g_return_val_if_fail(CRIA_IS_SLIDE(c), FALSE);
	data->data = (guchar*)g_new0(CriaSlide*,1);
	*((CriaSlide**)data->data) = g_object_ref(c);
	data->length = sizeof(CriaSlide*);

	return TRUE;
}

static gboolean
cslp_drag_data_delete(GtkTreeDragSource* source, GtkTreePath* path) {
	CriaSlideList* c;

	c = cslp_container_for_path_depth(CRIA_SIDEBAR_MODEL(source),
					  path,
					  gtk_tree_path_get_depth(path) - 1);
	cria_slide_list_remove(c, gtk_tree_path_get_indices(path)[gtk_tree_path_get_depth(path) - 1]);
	return TRUE;
}

static gboolean
cslp_drag_data_received(GtkTreeDragDest* dest, GtkTreePath* path, GtkSelectionData* data) {
	CriaSlide* slide, * copy;

	slide = *((CriaSlide**)(data->data));
	copy = cria_slide_copy(slide,
			       cslp_container_for_path_depth(CRIA_SIDEBAR_MODEL(dest),
							     path,
							     gtk_tree_path_get_depth(path) - 1),
			       gtk_tree_path_get_indices(path)[gtk_tree_path_get_depth(path) - 1]);
	g_object_unref(slide);

	return TRUE;
}

static gboolean
cslp_row_drop_possible(GtkTreeDragDest* dest, GtkTreePath* path, GtkSelectionData* data) {
#warning "dragDataReceived(): FIXME: enable this for slides too"
	return gtk_tree_path_get_depth(path) == 1;
}

static void
cria_sidebar_model_get_value(GtkTreeModel* tree_model, GtkTreeIter* iter, gint column, GValue* value) {
	g_return_if_fail(0 <= column && column < N_COLUMNS);

	g_value_init(value, cria_sidebar_model_get_column_type (tree_model, column));

	switch(column) {
	case COLUMN_PREVIEW:
#warning "SlideListProxy::getValue(): add support for preview pixmaps"
		g_value_set_object(value, NULL);
		break;
	case COLUMN_TITLE:
		g_value_set_string(value, cria_slide_get_title(CRIA_SLIDE(iter->user_data)));
		break;
	case COLUMN_SLIDE_PTR:
		g_value_set_object(value, CRIA_SLIDE(iter->user_data));
		break;
	default:
		cdebug("getValue()", "unknown column '%i'", column);
		g_warning("unknown column: %i", column);
	}
}

static void
cria_sidebar_model_init(CriaSidebarModel* self) {
	g_return_if_fail(CRIA_IS_SIDEBAR_MODEL(self));

	self->priv = g_new0(CriaSidebarModelPrivate,1);
}

static gboolean
cria_sidebar_model_iter_children(GtkTreeModel* tree_model, GtkTreeIter *iter, GtkTreeIter* parent) {
	return FALSE;
}

static gboolean
cria_sidebar_model_iter_has_child(GtkTreeModel* tree_model, GtkTreeIter* iter) {
	return FALSE;
}

static gboolean
cria_sidebar_model_iter_next(GtkTreeModel* tree_model, GtkTreeIter* iter) {
	guint             n;
	gboolean         retval = FALSE;
	CriaPresentation*presentation;
	CriaSlide       *slide;

	g_return_val_if_fail(CRIA_IS_SIDEBAR_MODEL(tree_model), retval);
	
	presentation = CRIA_SIDEBAR_MODEL(tree_model)->priv->presentation;
	slide        = CRIA_SLIDE(iter->user_data);
	
	if(slide) {
		n = cria_slide_list_index(CRIA_SLIDE_LIST(presentation), slide);
	} else {
		n = 0;
	}

	if((n + 1) < cria_slide_list_n_slides(CRIA_SLIDE_LIST(presentation))) {
		iter->user_data = cria_slide_list_get(CRIA_SLIDE_LIST(presentation), n + 1);
		retval = TRUE;
	}

	return retval;
}

static gint
cria_sidebar_model_iter_n_children(GtkTreeModel* tree_model, GtkTreeIter* iter) {
	g_return_val_if_fail(CRIA_IS_SIDEBAR_MODEL(tree_model), 0);
	
	if(iter == NULL) {
		return cria_slide_list_n_slides(CRIA_SLIDE_LIST(CRIA_SIDEBAR_MODEL(tree_model)->priv->presentation));
	} else {
		return 0;
	}
}

static gboolean
cria_sidebar_model_iter_nth_child(GtkTreeModel* tree_model, GtkTreeIter* iter, GtkTreeIter* parent, gint n) {
	gboolean retval = FALSE;
	
	g_return_val_if_fail(CRIA_IS_SIDEBAR_MODEL(tree_model), retval);
	cdebugo(tree_model, "iterNthChild()", "iter(0x%x), parent(0x%x), n(%i)", (uintptr_t)iter, (uintptr_t)parent, n);

	if(parent == NULL) {
		GtkTreePath* path = gtk_tree_path_new();
		gtk_tree_path_append_index(path, n);
		retval = cria_sidebar_model_get_iter(tree_model, iter, path);
		gtk_tree_path_free(path);
	} else {
		g_warning("cannot get subslides of slides");
	}
	
	return retval;
}

static gboolean
cria_sidebar_model_iter_parent(GtkTreeModel* tree_model, GtkTreeIter* iter, GtkTreeIter* child) {
	return FALSE;
}

static void
cria_sidebar_model_ref_node(GtkTreeModel* tree_model, GtkTreeIter* iter) {
	cdebug("refNode()", "Adding %d. reference on %s %p", G_OBJECT(iter->user_data)->ref_count + 1, G_OBJECT_TYPE_NAME(iter->user_data), iter->user_data);
	g_object_ref(CRIA_SLIDE(iter->user_data));
}

static void
cria_sidebar_model_set_presentation(CriaSidebarModel* self, CriaPresentation* presentation) {
	g_return_if_fail(CRIA_IS_SIDEBAR_MODEL(self));
	g_return_if_fail(CRIA_IS_PRESENTATION(presentation));

	if(self->priv->presentation != NULL) {
		cria_presentation_view_unregister(CRIA_PRESENTATION_VIEW(self), self->priv->presentation);
		g_object_unref(self->priv->presentation);
		self->priv->presentation = NULL;
	}

	self->priv->presentation = g_object_ref(presentation);
	cria_presentation_view_register(CRIA_PRESENTATION_VIEW(self), self->priv->presentation);

	g_object_notify(G_OBJECT(self), "presentation");
}

static void
cria_sidebar_model_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaSidebarModel	* self;
	
	self = CRIA_SIDEBAR_MODEL(object);
	
	switch(prop_id) {
	case CRIA_PRESENTATION_VIEW_PROP_PRESENTATION:
		cria_sidebar_model_set_presentation(self, g_value_get_object(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

static void
cria_sidebar_model_unref_node(GtkTreeModel* tree_model, GtkTreeIter* iter) {
	cdebug("unrefNode()", "Deleting %d. reference on %s %p", G_OBJECT(iter->user_data)->ref_count, G_OBJECT_TYPE_NAME(iter->user_data), iter->user_data);
	g_object_unref(CRIA_SLIDE(iter->user_data));
}

/* CriaPresentationViewIface */
static void
csm_slide_added(CriaPresentationView* view, gint new_position, CriaSlide* slide) {
	GtkTreeIter	  iter;
	GtkTreePath	* path;
	CriaSidebarModel* self = CRIA_SIDEBAR_MODEL(view);
	
	path = gtk_tree_path_new();
	gtk_tree_path_append_index(path, new_position);
	gtk_tree_model_get_iter(GTK_TREE_MODEL(self), &iter, path);
	gtk_tree_model_row_inserted(GTK_TREE_MODEL(self), path, &iter);
	gtk_tree_path_free(path);

	g_object_ref(slide);
	cdebug("slideAdded()", "%p: Adding %d. reference on %p (%s)", self, G_OBJECT(slide)->ref_count, slide, G_OBJECT_TYPE_NAME(slide));
	cria_slide_view_register(CRIA_SLIDE_VIEW(self), slide);
}

static void
csm_slide_removed(CriaPresentationView* view, gint old_index, CriaSlide* slide) {
	GtkTreePath     * path;
	CriaSidebarModel* self = CRIA_SIDEBAR_MODEL(view);

	cdebug("slideRemoved()", "%p: Removing %d. reference on %p (%s)", self, G_OBJECT(slide)->ref_count, slide, G_OBJECT_TYPE_NAME(slide));
	path = gtk_tree_path_new();
	gtk_tree_path_append_index(path, old_index);
	gtk_tree_model_row_deleted(GTK_TREE_MODEL(self), path);
	gtk_tree_path_free(path);

	cria_slide_view_unregister(CRIA_SLIDE_VIEW(view), slide);
	g_object_unref(slide);
}

static void
csm_init_presentation_view(CriaPresentationViewIface* iface) {
	iface->slide_added   = csm_slide_added;
	iface->slide_removed = csm_slide_removed;
}

/* CriaSlideViewIface */
static void
csm_notify_title(CriaSlideView* view, CriaSlide* slide, gchar const* title) {
	GtkTreeIter  iter;
	GtkTreePath* path = gtk_tree_path_new();
	gtk_tree_path_append_index(path, cria_slide_list_index(CRIA_SLIDE_LIST(CRIA_SIDEBAR_MODEL(view)->priv->presentation), slide));
	g_signal_emit_by_name(GTK_TREE_MODEL(view), "row-changed", path, &iter);
}

static void
csm_init_slide_view(CriaSlideViewIface* iface) {
	iface->notify_title = csm_notify_title;
}

/* GtkTreeModelIface */
static void
csm_init_tree_model(GtkTreeModelIface* iface) {
	iface->get_column_type = cria_sidebar_model_get_column_type;
	iface->get_flags       = cria_sidebar_model_get_flags;
	iface->get_iter        = cria_sidebar_model_get_iter;
	iface->get_n_columns   = cria_sidebar_model_get_n_columns;
	iface->get_path        = cria_sidebar_model_get_path;
	iface->get_value       = cria_sidebar_model_get_value;
	iface->iter_children   = cria_sidebar_model_iter_children;
	iface->iter_has_child  = cria_sidebar_model_iter_has_child;
	iface->iter_n_children = cria_sidebar_model_iter_n_children;
	iface->iter_next       = cria_sidebar_model_iter_next;
	iface->iter_nth_child  = cria_sidebar_model_iter_nth_child;
	iface->iter_parent     = cria_sidebar_model_iter_parent;
	iface->ref_node        = cria_sidebar_model_ref_node;
	iface->unref_node      = cria_sidebar_model_unref_node;
}

/* GtkTreeDragSourceIface */
static void
csm_init_tree_drag_source(GtkTreeDragSourceIface* iface) {
	iface->row_draggable    = cslp_row_draggable;
	iface->drag_data_get    = cslp_drag_data_get;
	iface->drag_data_delete = cslp_drag_data_delete;
}

/* GtkTreeDragDestIface */
static void
csm_init_tree_drag_dest(GtkTreeDragDestIface* iface) {
	iface->drag_data_received = cslp_drag_data_received;
	iface->row_drop_possible  = cslp_row_drop_possible;
}

