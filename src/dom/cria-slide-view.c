/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <dom/cria-slide-view.h>

#include <dom/cria-slide.h>

#define CDEBUG_TYPE cria_slide_view_get_type
#include <cdebug/cdebug.h>
#include <helpers/gobject-helpers.h>

G_DEFINE_IFACE(CriaSlideView, cria_slide_view, G_TYPE_INTERFACE);

static void
csv_emit_notify_background(CriaSlideView* self, GParamSpec* ignored, CriaSlide* slide) {
	g_object_ref(self);
	CRIA_SLIDE_VIEW_GET_CLASS(self)->notify_background(self, slide, cria_slide_get_background(slide, FALSE));
	g_object_unref(self);
}

static void
csv_emit_notify_master_slide(CriaSlideView* self, GParamSpec* ignored, CriaSlide* slide) {
	g_object_ref(self);
	CRIA_SLIDE_VIEW_GET_CLASS(self)->notify_master_slide(self, cria_slide_get_master_slide(slide));
	g_object_unref(self);
}

static void
csv_emit_notify_title(CriaSlideView* self, GParamSpec* ignored, CriaSlide* slide) {
	g_object_ref(self);
	CRIA_SLIDE_VIEW_GET_CLASS(self)->notify_title(self, slide, cria_slide_get_title(slide));
	g_object_unref(self);
}

/**
 * cria_slide_view_register:
 * @self: a #CriaSlideView
 * @slide: a #CriaSlide
 *
 * Register the view at a slide. The view listens to signals from the slide
 * after this call. So callbacks can be more easily implemented as interface
 * functions.
 */
void
cria_slide_view_register(CriaSlideView* self, CriaSlide* slide) {
	GCallback callback;

	g_return_if_fail(CRIA_IS_SLIDE_VIEW(self));
	g_return_if_fail(CRIA_IS_SLIDE(slide));

	g_object_ref(slide);
	cdebug("register()", "%p: Added %d. references on %p (%s)", self, G_OBJECT(slide)->ref_count, slide, G_OBJECT_TYPE_NAME(slide));
	
	callback = G_CALLBACK(CRIA_SLIDE_VIEW_GET_CLASS(self)->element_added);
	if(callback) {
		GList* elements = cria_slide_get_elements(slide),
		     * element;
		g_signal_connect_swapped(slide, "element-added",
					 callback, self);
		for(element = elements; element; element = element->next) {
			CRIA_SLIDE_VIEW_GET_CLASS(self)->element_added(self, CRIA_SLIDE_ELEMENT(element->data));
		}

		g_list_free(elements);
	}

	callback = G_CALLBACK(CRIA_SLIDE_VIEW_GET_CLASS(self)->element_removed);
	if(callback) {
		g_signal_connect_swapped(slide, "element-removed",
					 callback, self);
	}

	callback = G_CALLBACK(CRIA_SLIDE_VIEW_GET_CLASS(self)->notify_background);
	if(callback) {
		g_signal_connect_swapped(slide, "notify::background",
					 G_CALLBACK(csv_emit_notify_background), self);
		csv_emit_notify_background(self, NULL, slide);
	}

	callback = G_CALLBACK(CRIA_SLIDE_VIEW_GET_CLASS(self)->notify_master_slide);
	if(callback) {
		g_signal_connect_swapped(slide, "notify::master-slide",
					 G_CALLBACK(csv_emit_notify_master_slide), self);
		csv_emit_notify_master_slide(self, NULL, slide);
	}

	callback = G_CALLBACK(CRIA_SLIDE_VIEW_GET_CLASS(self)->notify_title);
	if(callback) {
		g_signal_connect_swapped(slide, "notify::title",
					 G_CALLBACK(csv_emit_notify_title), self);
		csv_emit_notify_title(self, NULL, slide);
	}
}

/**
 * cria_slide_view_unregister:
 * @self: a #CriaSlideView
 * @slide: a #CriaSlide
 *
 * Unregister the view from the slide. The view doesn't listen for signals of
 * the slide after this call.
 */
void
cria_slide_view_unregister(CriaSlideView* self, CriaSlide* slide) {
	GList* elements,
	     * element;
	void (*element_removed) (CriaSlideView* self, CriaSlideElement* element);
	
	g_return_if_fail(CRIA_IS_SLIDE_VIEW(self));
	g_return_if_fail(CRIA_IS_SLIDE(slide));

	// disconnect 'element-added'
	g_signal_handlers_disconnect_by_func(slide, CRIA_SLIDE_VIEW_GET_CLASS(self)->element_added, self);
	
	// disconnect 'element-removed'
	elements = cria_slide_get_elements(slide);
	element_removed = CRIA_SLIDE_VIEW_GET_CLASS(self)->element_removed;
	for(element = elements; element && element_removed; element = element->next) {
		element_removed(self, CRIA_SLIDE_ELEMENT(element->data));
	}
	g_list_free(elements);
	element = elements = NULL;
	g_signal_handlers_disconnect_by_func(slide, CRIA_SLIDE_VIEW_GET_CLASS(self)->element_removed, self);

	// disconnect notifications
	g_signal_handlers_disconnect_by_func(slide, csv_emit_notify_background, self);
	g_signal_handlers_disconnect_by_func(slide, csv_emit_notify_master_slide, self);
	g_signal_handlers_disconnect_by_func(slide, csv_emit_notify_title, self);

	cdebug("unregister()", "%p: Removing %d. reference on %p (%s)", self, G_OBJECT(slide)->ref_count, slide, G_OBJECT_TYPE_NAME(slide));
	g_object_unref(slide);
}

