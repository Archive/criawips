/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIAWIPS_THEME_VIEW_H
#define CRIAWIPS_THEME_VIEW_H

#include <dom/cria-theme.h>

G_BEGIN_DECLS

typedef struct _CriaThemeView CriaThemeView;
typedef struct _CriaThemeViewIface CriaThemeViewIface;

#define CRIA_TYPE_THEME_VIEW         (cria_theme_view_get_type())
#define CRIA_THEME_VIEW(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), CRIA_TYPE_THEME_VIEW, CriaThemeView))
#define CRIA_IS_THEME_VIEW(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), CRIA_TYPE_THEME_VIEW))
#define CRIA_THEME_VIEW_GET_CLASS(i) (G_TYPE_INSTANCE_GET_INTERFACE((i), CRIA_TYPE_THEME_VIEW, CriaThemeViewIface))

GType cria_theme_view_get_type(void);

void cria_theme_view_register  (CriaThemeView* self,
				CriaTheme    * theme);
void cria_theme_view_unregister(CriaThemeView* self,
				CriaTheme    * theme);

void _cria_theme_view_install_properties(GObjectClass* go_class,
					 gint          prop_id);

struct _CriaThemeViewIface {
	GTypeInterface interface;

	void(*added_master_slide) (CriaThemeView* view,
				   CriaSlide    * master_slide);
};

G_END_DECLS

#endif /* !CRIAWIPS_THEME_VIEW_H */

