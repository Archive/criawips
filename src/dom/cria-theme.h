/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef THEME_H
#define THEME_H

#include <glib-object.h>

G_BEGIN_DECLS

typedef struct _CriaTheme		CriaTheme;
typedef struct _CriaThemeClass		CriaThemeClass;

#include <dom/cria-slide.h>

#define CRIA_TYPE_THEME			(cria_theme_get_type ())
#define CRIA_THEME(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_THEME, CriaTheme))
#define CRIA_THEME_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_THEME, CriaThemeClass))
#define CRIA_IS_THEME(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_THEME))
#define CRIA_IS_THEME_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_THEME))
#define CRIA_THEME_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_THEME, CriaThemeClass))

GType		  cria_theme_get_type	       (void);

void		  cria_theme_add_master_slide  (CriaTheme	* self,
						CriaSlide	* master_slide);
CriaSlide*        cria_theme_get_default_slide (CriaTheme const * self);
CriaSlide*	  cria_theme_get_master_slide  (CriaTheme	* self,
						const char	* master_slide);
CriaSlide*	  cria_theme_get_master_slide_i(CriaTheme	* self,
						guint             i);
GList*            cria_theme_get_master_slides (CriaTheme	* self);
guint             cria_theme_get_n_slides      (CriaTheme       * self);
const gchar*	  cria_theme_get_name	       (CriaTheme	* self);
CriaTheme*	  cria_theme_new	       (const char	* name);
void              cria_theme_set_default_slide (CriaTheme       * self,
						CriaSlide       * default_slide);
void		  cria_theme_set_name	       (CriaTheme	* self,
						const char	* attribute);

G_END_DECLS

#endif /* THEME_H */
