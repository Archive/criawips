/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <dom/cria-theme-priv.h>

#include <stdint.h>
#include <string.h>

#include <glib/gi18n.h>

#define CDEBUG_TYPE cria_theme_get_type
#include <cdebug/cdebug.h>
#include <helpers/gtk-helpers.h>

enum {
	PROP_0,
	PROP_DEFAULT,
	PROP_NAME
};

enum {
	ADDED_SLIDE_SIGNAL,
	N_SIGNALS
};

G_DEFINE_TYPE(CriaTheme, cria_theme, G_TYPE_OBJECT);

static guint cria_theme_signals[N_SIGNALS] = { 0 };

guint
cria_theme_get_n_slides(CriaTheme* self) {
	g_return_val_if_fail(CRIA_IS_THEME(self), 0);
	
	return g_list_length(self->master_slides);
}

void
cria_theme_add_master_slide(CriaTheme* self, CriaSlide* master_slide) {
	g_return_if_fail(CRIA_IS_THEME(self));
	g_return_if_fail(CRIA_IS_SLIDE(master_slide));
	g_return_if_fail(cria_theme_get_master_slide(self, cria_slide_get_title(master_slide)) == NULL);

	self->master_slides = g_list_append (self->master_slides, master_slide);

	g_signal_emit(self, cria_theme_signals[ADDED_SLIDE_SIGNAL], 0, master_slide);

	if(!self->default_slide) {
		cria_theme_set_default_slide(self, master_slide);
	}
}

static void
ct_remove_slide_reverse(CriaSlide* slide, CriaTheme* self) {
	cdebug("finalize()", "%p: removing %d. reference in %p (%s)", self, G_OBJECT(slide)->ref_count, slide, G_OBJECT_TYPE_NAME(slide));
	g_object_unref(slide);
}

static void
ct_finalize(GObject* object) {
	CriaTheme* self = CRIA_THEME(object);

	cdebug("finalize()", "start");

#warning "FIXME: call remove_slide for each one"
	g_list_foreach(self->master_slides, G_FUNC(ct_remove_slide_reverse), self);
	g_list_free(self->master_slides);

	cdebug("finalize()", "leave");

	G_OBJECT_CLASS(cria_theme_parent_class)->finalize(object);
}

static void
ct_foreach_find(CriaSlide* slide, gpointer param[2]) {
	g_return_if_fail(param);
	
	if(param[0]) {
		return;
	}

	if(!strcmp(cria_slide_get_title(slide), (const gchar*)param[1])) {
		param[0] = slide;
	}
}

CriaSlide*
cria_theme_get_default_slide(CriaTheme const* self) {
	g_return_val_if_fail(self->default_slide, NULL);
	
	return self->default_slide;
}

CriaSlide*
cria_theme_get_master_slide(CriaTheme* self, const gchar* name) {
	gpointer param[2] = {NULL, (gchar*)name};
	
	g_return_val_if_fail(CRIA_IS_THEME(self), NULL);

	cdebugo(self, "getMasterSlide()", "looking for \"%s\"", name);
#warning "FIXME: this can be done more cleverly"
	g_list_foreach(self->master_slides, (GFunc)ct_foreach_find, param);

	return CRIA_SLIDE(param[0]);
}

CriaSlide*
cria_theme_get_master_slide_i(CriaTheme* self, guint i) {
	g_return_val_if_fail(CRIA_IS_THEME(self), NULL);
	g_return_val_if_fail(i < (guint)cria_theme_get_n_slides(self), NULL);

	return g_list_nth_data(self->master_slides, i);
}

void
cria_theme_set_default_slide(CriaTheme* self, CriaSlide* default_slide) {
	g_return_if_fail(CRIA_IS_THEME(self));
	g_return_if_fail(CRIA_IS_SLIDE(default_slide));
	g_return_if_fail(g_list_index(self->master_slides, default_slide) >= 0);

	// no ref-counting in here as we already own a ref-count on the default
	// slide
	
	self->default_slide = default_slide;

	g_object_notify(G_OBJECT(self), "default-slide");
}

const gchar*
cria_theme_get_name(CriaTheme* self) {
	g_return_val_if_fail(CRIA_IS_THEME(self), NULL);
	
	return self->name;
}

static void
cria_theme_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaTheme* self = CRIA_THEME(object);

	switch(prop_id) {
	case PROP_DEFAULT:
		g_value_set_object(value, self->default_slide);
		break;
	case PROP_NAME:
		g_value_set_string(value, self->name);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

CriaTheme*
cria_theme_new(const gchar* name) {
	g_return_val_if_fail(name != NULL && strlen(name), NULL);
	
	CriaTheme* self = g_object_new(CRIA_TYPE_THEME, "name", name, NULL);
	return self;
}

void
cria_theme_set_name(CriaTheme* self, const gchar* name) {
	g_return_if_fail(CRIA_IS_THEME(self));

	g_free(self->name);

	self->name = g_strdup(name);

	g_object_notify(G_OBJECT(self), "name");
}

static void
cria_theme_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaTheme	* self;
	
	self = CRIA_THEME(object);
	
	switch(prop_id) {
	case PROP_DEFAULT:
		cria_theme_set_default_slide(self, g_value_get_object(value));
		break;
	case PROP_NAME:
		cria_theme_set_name(self, g_value_get_string(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

/**
 * cria_theme_get_master_slides:
 * @self: a #CriaTheme
 *
 * Get a list of the master slides. The slides are NOT reffed extra, so you can
 * do what you like with the list (even leak that memory if you want to). It
 * cause any harm to your application.
 *
 * So please don't forget to ref the slides that you're interested in.
 *
 * Returns a list of the master slides. Free with #g_list_free.
 */
GList*
cria_theme_get_master_slides(CriaTheme *self) {
	return g_list_copy(self->master_slides);
}

static void
cria_theme_class_init(CriaThemeClass* self_class) {
	GObjectClass	* go_class;

	/* setting up the GObjectClass */
	go_class = G_OBJECT_CLASS(self_class);
	go_class->get_property = cria_theme_get_property;
	go_class->set_property = cria_theme_set_property;
	go_class->finalize     = ct_finalize;

	g_object_class_install_property(go_class,
					PROP_DEFAULT,
					g_param_spec_object("default-slide",
							    "Default Slide",
							    "The slide that gets applied to newly created slides by default",
							    CRIA_TYPE_SLIDE,
							    G_PARAM_READWRITE));
	g_object_class_install_property(go_class,
					PROP_NAME,
					g_param_spec_string("name",
							    "Name",
							    "The name of this theme",
							    _("untitled"),
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

	/**
	 * CriaTheme::added-master-slide:
	 * @self: the #CriaTheme that emitted the signal
	 * @master_slide: the #CriaSlide that was added
	 *
	 * This signal gets emitted when a new master slide gets added to a
	 * theme.
	 */
	cria_theme_signals[ADDED_SLIDE_SIGNAL] =
		g_signal_new("added-master-slide", CRIA_TYPE_THEME,
			     G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(CriaThemeClass, added_master_slide),
			     NULL, NULL,
			     g_cclosure_marshal_VOID__OBJECT,
			     G_TYPE_NONE, 1,
			     CRIA_TYPE_SLIDE);
}

static void
cria_theme_init(CriaTheme *self) {
}

