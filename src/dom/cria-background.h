/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <glib-object.h>
#include <goffice/utils/go-color.h>

#include <dom/cria-image.h>

G_BEGIN_DECLS

typedef struct _CriaBackground CriaBackground;
typedef struct _CriaBackgroundClass CriaBackgroundClass;
typedef struct _CriaBackgroundPrivate CriaBackgroundPrivate;

GOColor*	cria_background_get_color(CriaBackground* self);
CriaImage*	cria_background_get_image(CriaBackground* self);
GType		cria_background_get_type	       (void);
CriaBackground*	cria_background_new(void);
void		cria_background_set_color(CriaBackground* self, GOColor* color);
void		cria_background_set_image(CriaBackground* self, CriaImage* image);

#define CRIA_TYPE_BACKGROUND			(cria_background_get_type())
#define CRIA_BACKGROUND(object)			(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_BACKGROUND, CriaBackground))
#define CRIA_BACKGROUND_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_BACKGROUND, CriaBackgroundClass))
#define CRIA_IS_BACKGROUND(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_BACKGROUND))
#define CRIA_IS_BACKGROUND_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_BACKGROUND))
#define CRIA_BACKGROUND_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_BACKGROUND, CriaBackgroundClass))

struct _CriaBackground {
	GObject		  base_instance;
	GOColor		* color;
#warning "CriaBackground: FIXME: add background gradient"
	CriaImage	* image;
#warning "CriaBackground: FIXME: add image scaling types"
};

struct _CriaBackgroundClass {
	GObjectClass	  base_class;

	/* signals */
	void (*signal)	       (CriaBackground	* self,
				const	gchar	* string);
};

G_END_DECLS

#endif /* !BACKGROUND_H */

