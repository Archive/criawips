/* This file is part of criawips, a GNOME presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <dom/cria-presentation-view.h>

#define CDEBUG_TYPE cria_presentation_view_get_type
#include <cdebug/cdebug.h>
#include <helpers/gobject-helpers.h>

G_DEFINE_IFACE_FULL(CriaPresentationView, cria_presentation_view, G_TYPE_INTERFACE);

static void
cria_presentation_view_class_init(gpointer iface) {
	g_object_interface_install_property(iface,
					    g_param_spec_object("presentation",
						    		"Presentation",
								"The presentation connected to this view",
								CRIA_TYPE_PRESENTATION,
								(G_PARAM_CONSTRUCT | G_PARAM_READWRITE)));
}

void
_cria_presentation_view_install_properties(GObjectClass *klass) {
        g_object_class_override_property(klass, CRIA_PRESENTATION_VIEW_PROP_PRESENTATION, "presentation");
}

static void
cpv_notify_theme(CriaPresentationView* self, GParamSpec* pspec, CriaPresentation* presentation) {
	CRIA_PRESENTATION_VIEW_GET_CLASS(self)->notify_theme(self, cria_presentation_get_theme(presentation));
}

static void
cpv_notify_title(CriaPresentationView* self, GParamSpec* pspec, CriaPresentation* presentation) {
	CRIA_PRESENTATION_VIEW_GET_CLASS(self)->notify_title(self, cria_presentation_get_title(presentation));
}

void
cria_presentation_view_register(CriaPresentationView* self, CriaPresentation* presentation) {
	g_object_ref(presentation);

	if(CRIA_PRESENTATION_VIEW_GET_CLASS(self)->notify_theme) {
		g_signal_connect_swapped(presentation, "notify::theme",
					 G_CALLBACK(cpv_notify_theme), self);
		cpv_notify_theme(self, NULL, presentation);
	}

	if(CRIA_PRESENTATION_VIEW_GET_CLASS(self)->notify_title) {
		g_signal_connect_swapped(presentation, "notify::title",
					 G_CALLBACK(cpv_notify_title), self);
		cpv_notify_title(self, NULL, presentation);
	}

	if(CRIA_PRESENTATION_VIEW_GET_CLASS(self)->slide_added) {
		guint i;
		g_signal_connect_swapped(presentation, "inserted-slide",
					 G_CALLBACK(CRIA_PRESENTATION_VIEW_GET_CLASS(self)->slide_added), self);

		for(i = 0; i < cria_slide_list_n_slides(CRIA_SLIDE_LIST(presentation)); i++) {
			CRIA_PRESENTATION_VIEW_GET_CLASS(self)->slide_added(self, i,
						cria_slide_list_get(CRIA_SLIDE_LIST(presentation), i));
		}
	}

	if(CRIA_PRESENTATION_VIEW_GET_CLASS(self)->slide_removed) {
		g_signal_connect_swapped(presentation, "slide-deleted",
					 G_CALLBACK(CRIA_PRESENTATION_VIEW_GET_CLASS(self)->slide_removed), self);
	}
}

void
cria_presentation_view_unregister(CriaPresentationView* self, CriaPresentation* presentation) {
	cdebug("unregister()", "%p: start", self);
	if(CRIA_PRESENTATION_VIEW_GET_CLASS(self)->slide_added) {
		g_signal_handlers_disconnect_by_func(presentation, CRIA_PRESENTATION_VIEW_GET_CLASS(self)->slide_added, self);
	}
	
	if(CRIA_PRESENTATION_VIEW_GET_CLASS(self)->slide_removed) {
		gint i = 0, j = cria_slide_list_n_slides(CRIA_SLIDE_LIST(presentation));
		cdebug("unregister()", "%p: emitting the 'slide-removed' signal for %d slides", self, i);
		for(i = 1; i <= j; i++) {
			cdebug("unregister()", "%p: %d. slide", self, i);
			CRIA_PRESENTATION_VIEW_GET_CLASS(self)->slide_removed(self, j - i,
						cria_slide_list_get(CRIA_SLIDE_LIST(presentation), j - i));
		}
		cdebug("unregister()", "%p: finished with the 'slide-removed' emission", self, i);
		g_signal_handlers_disconnect_by_func(presentation, CRIA_PRESENTATION_VIEW_GET_CLASS(self)->slide_removed, self);
	}
	
	g_signal_handlers_disconnect_by_func(presentation, cpv_notify_theme, self);
	g_signal_handlers_disconnect_by_func(presentation, cpv_notify_title, self);
	
	g_object_unref(presentation);
	cdebug("unregister()", "%p: end", self);
}

