/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <dom/cria-background.h>

#include <inttypes.h>

#define CDEBUG_TYPE cria_background_get_type
#include <cdebug/cdebug.h>

enum {
	PROP_0,
	PROP_COLOR,
	PROP_IMAGE,
};

enum {
	SIGNAL,
	N_SIGNALS
};

static	void	cria_background_get_property	       (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static	void	cria_background_set_property        (GObject		* object,
						guint		  prop_id,
						const	GValue	* value,
						GParamSpec	* param_spec);
#if 0
/* enable these to add support for signals */
static	guint	cria_background_signals[N_SIGNALS] = { 0 };

static	void	cria_background_signal	       (CriaBackground	* self,
						const	gchar	* string);
#endif

static void
cria_background_class_init(CriaBackgroundClass* cria_background_class) {
	GObjectClass	* g_object_class;

#if 0
	/* setting up signal system */
	cria_background_class->signal = cria_background_signal;

	cria_background_signals[SIGNAL] = g_signal_new("signal",
						  CRIA_TYPE_BACKGROUND,
						  G_SIGNAL_RUN_LAST,
						  G_STRUCT_OFFSET(CriaBackgroundClass,
							  	  signal),
						  NULL,
						  NULL,
						  g_cclosure_marshal_VOID__STRING,
						  G_TYPE_NONE,
						  0);
#endif
	/* setting up the object class */
	g_object_class = G_OBJECT_CLASS(cria_background_class);
	g_object_class->set_property = cria_background_set_property;
	g_object_class->get_property = cria_background_get_property;

	g_object_class_install_property(g_object_class,
					PROP_COLOR,
					g_param_spec_uint("color",
							  "Color",
							  "The background color",
							  0,
							  G_MAXUINT,
							  0,
							  G_PARAM_READWRITE));
	g_object_class_install_property(g_object_class,
					PROP_IMAGE,
					g_param_spec_object("image",
							    "Image",
							    "The background image",
							    CRIA_TYPE_IMAGE,
							    G_PARAM_READWRITE));
}

/**
 * cria_background_get_color:
 * @self: a #CriaBackground
 *
 * Get the color of a background. Free it after use.
 *
 * Returns the color of a background.
 */
GOColor*
cria_background_get_color(CriaBackground* self) {
	g_return_val_if_fail(CRIA_IS_BACKGROUND(self), NULL);

	return self->color;
}

/**
 * cria_background_get_image:
 * @self: a #CriaBackground
 *
 * Get the image from a background.
 *
 * Returns the image from this background.
 */
CriaImage*
cria_background_get_image(CriaBackground* self) {
	g_return_val_if_fail(CRIA_IS_BACKGROUND(self), NULL);

	cdebugo(self, "getImage()", "returning image 0x%x", (uintptr_t)self->image);
	return self->image;
}

static void
cria_background_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaBackground	* self;

	self = CRIA_BACKGROUND(object);

	switch(prop_id) {
	case PROP_COLOR:
		g_value_set_uint(value, *(cria_background_get_color(self)));
		break;
	case PROP_IMAGE:
		g_value_set_object(value, cria_background_get_image(self));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object,
						  prop_id,
						  param_spec);
		break;
	}
}

GType
cria_background_get_type(void) {
	static GType	type = 0;

	if(!type) {
		static const GTypeInfo info = {
			sizeof(CriaBackgroundClass),
			NULL,	/* base initializer */
			NULL,	/* base finalizer */
			(GClassInitFunc)cria_background_class_init,
			NULL,	/* class finalizer */
			NULL,	/* class data */
			sizeof(CriaBackground),
			0,
			NULL,
			0
		};

		type = g_type_register_static(G_TYPE_OBJECT,
					      "CriaBackground",
					      &info,
					      0);
	}

	return type;
}

CriaBackground*
cria_background_new(void) {
	return g_object_new(CRIA_TYPE_BACKGROUND, NULL);
}

/**
 * cria_background_set_color:
 * @self: a #CriaBackground
 * @color: a #GOColor, NULL to unset
 *
 * Set the color for this background.
 */
void
cria_background_set_color(CriaBackground* self, GOColor* color) {
	g_return_if_fail(CRIA_IS_BACKGROUND(self));

	if(self->color == color) {
		return;
	}
	
	if(self->color) {
		g_free(self->color);
		self->color = NULL;
	}
	
	if(color) {
		self->color = g_new0(GOColor,1);
		*(self->color) = *color;
	}

	g_object_notify(G_OBJECT(self), "color");
}

/**
 * cria_background_set_image:
 * @self: a #CriaBackground
 * @image: a #CriaImage, NULL to unset
 *
 * Set the image for this background.
 */
void
cria_background_set_image(CriaBackground* self, CriaImage* image) {
	g_return_if_fail(CRIA_IS_BACKGROUND(self));
	g_return_if_fail(image == NULL || CRIA_IS_IMAGE(image));

	cdebugo(self, "setImage()", "setting image 0x%x", (uintptr_t)image);

	if(self->image == image) {
		return;
	}
	
	if(self->image) {
		g_object_unref(self->image);
		self->image = NULL;
	}
	
	if(image) {
		self->image = g_object_ref(image);
	}
	
	g_object_notify(G_OBJECT(self), "image");
}

static void
cria_background_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaBackground	* self;
	GOColor		  color;
	
	self = CRIA_BACKGROUND(object);
	
	switch(prop_id) {
	case PROP_COLOR:
		color = g_value_get_uint(value);
		cria_background_set_color(self, &color);
		break;
	case PROP_IMAGE:
		cria_background_set_image(self, g_value_get_object(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object,
						  prop_id,
						  param_spec);
		break;
	}
}

