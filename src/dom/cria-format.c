/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <dom/cria-format.h>

#include <string.h>
#include <glib/gmem.h>
#include <glib/gmessages.h>
#include <glib/gstring.h>

gchar*
cria_format_get(CriaLepton lepton, const gchar* attribute) {
	const gchar* iterator;
	const gchar* format;
	gchar* retval = NULL;

	if(!lepton) {
		return NULL;
	}

	format = cria_lepton_get(lepton);
	g_return_val_if_fail(attribute, NULL);

	for(iterator = format; iterator && *iterator; ) {
		const gchar* rest = strstr(iterator, ";");
		if(!strncmp(iterator, attribute, strlen(attribute))) {
			iterator += strlen(attribute);
			iterator += 2; /* "font-family-->: <--Sans" */
			
			if G_LIKELY(rest) {
				retval = g_strndup(iterator, (gsize)(rest - iterator));
			} else {
				retval = g_strdup(iterator);
			}
			break;
		}
		iterator = rest;
		while(iterator && (*iterator == ';' || *iterator == ' ')) {
			iterator++;
		}
	}

	return retval;
}

gchar*
cria_format_set(CriaLepton lepton, const gchar* attribute, const gchar* value) {
	GString    * new_val;
	const gchar* format,
	      	   * iterator;
	gchar      * retval;

	/* try to find the attribute */
	if(!lepton) {
		/* just set the attribute */
		return g_strdup_printf("%s: %s", attribute, value);
	}

	format = cria_lepton_get(lepton);
	g_return_val_if_fail(attribute, g_strdup(format));

	iterator = strstr(format, attribute);

	if(iterator) {
		/* replace the attribute */
		gchar* pair = g_strdup_printf("%s: %s", attribute, value);
		new_val = g_string_new("");

		/* add the part before the element */
		g_string_append_len(new_val, format, (gssize)(iterator - format));
		g_string_append(new_val, pair);
		g_free(pair);

		iterator = strstr(iterator, ";");
		while (iterator && (*iterator == ';' || *iterator == ' ')) {
			iterator++;
		}
		if(iterator) {
			g_string_append_printf(new_val, "; %s", iterator);
		}
	} else {
		/* add the attribute */
		new_val = g_string_new(format);
		
		for(iterator = format; iterator && strncmp(iterator, attribute, strlen(attribute)) < 0;) {
			iterator = strstr(iterator, ";");
			while (iterator && (*iterator == ';' || *iterator == ' ')) {
				iterator++;
			}
		}
		
		if(iterator && *iterator) {
			gchar* new_format = g_strdup_printf("%s: %s; ", attribute, value);
			/* iterator points to the NEXT element, which is actually the
			 * position we want to insert at */

			g_string_insert(new_val, (gssize)(iterator - format), new_format);
			g_free(new_format);
		} else {
			g_string_append_printf(new_val, "; %s: %s", attribute, value);
		}
	}
	
	retval = new_val->str;
	g_string_free(new_val, FALSE);

	return retval;
}

