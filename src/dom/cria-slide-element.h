/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIA_SLIDE_ELEMENT_H
#define CRIA_SLIDE_ELEMENT_H

#include <glib-object.h>
#include <goffice/utils/go-units.h>

G_BEGIN_DECLS

typedef struct _CriaSlideElement CriaSlideElement;
typedef struct _CriaSlideElementClass CriaSlideElementClass;

#define CRIA_TYPE_SLIDE_ELEMENT         (cria_slide_element_get_type())
#define CRIA_SLIDE_ELEMENT(o)           (G_TYPE_CHECK_INSTANCE_CAST((o), CRIA_TYPE_SLIDE_ELEMENT, CriaSlideElement))
#define CRIA_SLIDE_ELEMENT_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), CRIA_TYPE_SLIDE_ELEMENT, CriaSlideElementClass))
#define CRIA_IS_SLIDE_ELEMENT(o)        (G_TYPE_CHECK_INSTANCE_TYPE((o), CRIA_TYPE_SLIDE_ELEMENT))
#define CRIA_IS_SLIDE_ELEMENT_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE((k), CRIA_TYPE_SLIDE_ELEMENT))
#define CRIA_SLIDE_ELEMENT_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS((o), CRIA_TYPE_SLIDE_ELEMENT, CriaSlideElementClass))

GType cria_slide_element_get_type(void);

CriaSlideElement*
             cria_slide_element_get_master  (CriaSlideElement const* self);
gchar const* cria_slide_element_get_name    (CriaSlideElement const* self);
GORect const*cria_slide_element_get_position(CriaSlideElement const * self);
void         cria_slide_element_set_position(CriaSlideElement       * self,
					     gdouble           x1,
					     gdouble           y1,
					     gdouble           x2,
					     gdouble           y2);
gboolean     cria_slide_element_is_empty    (CriaSlideElement const* self);
void         cria_slide_element_set_master  (CriaSlideElement      * self,
					     CriaSlideElement      * master);

G_END_DECLS

#endif /* !CRIA_SLIDE_ELEMENT_H */
