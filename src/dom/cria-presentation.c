/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <dom/cria-presentation-priv.h>

#include <inttypes.h>
#include <string.h>

#include <glib.h>
#include <glib-object.h>
#include <libgnome/gnome-i18n.h>
#include <gsf/gsf.h>
#include <gsf-gnome/gsf-input-gnomevfs.h>
#include <gsf/gsf-input-stdio.h>

#define CDEBUG_TYPE cria_presentation_get_type
#include <cdebug/cdebug.h>
#include <helpers/glib-helpers.h>
#include <dom/cria-marshallers.h>
#include <io/cria-presentation-parser.h>

#include "application.h"

#define STR_UNSAVED_PRESENTATION _("Unsaved Presentation")

enum {
	PROP_0,
	PROP_LOOP,
	PROP_SLIDE_AUTO,
	PROP_SLIDE_DELAY,
	PROP_THEME,
	PROP_TITLE,
	PROP_URI
};

enum {
	INSERTED_SLIDE_SIGNAL,
	SLIDE_DELETED_SIGNAL,
	N_SIGNALS
};

static void		  cria_presentation_get_property       (GObject		* object,
								guint		  prop_id,
								GValue		* value,
								GParamSpec	* param_spec);
static CriaPresentation*  cria_presentation_new_from_uri       (GnomeVFSURI	* uri,
								GError		**error);
static void		  cria_presentation_init	       (CriaPresentation* self);
static void		  cria_presentation_set_property       (GObject		* object,
								guint		  prop_id,
								const	GValue	* value,
								GParamSpec	* param_spec);

static guint		  cria_presentation_signals[N_SIGNALS] = { 0 };
static void		  cria_presentation_inserted_slide     (CriaPresentation* self,
								gint		  new_position,
								CriaSlide	* new_slide);

static GObjectClass* parent_class = NULL;
static gint unsaved_presentations = 0;

/**
 * cria_presentation_set_theme:
 * @self: a CriaPresentation
 * @theme: a CriaTheme
 *
 * Set the @theme to presentation references by @self.
 */
void
cria_presentation_set_theme(CriaPresentation* self, CriaTheme* theme) {
	g_return_if_fail(CRIA_IS_PRESENTATION(self));
	g_return_if_fail(CRIA_IS_THEME(theme));

	if(self->theme == theme) {
		return;
	}
	
	if(self->theme) {
		g_object_unref(self->theme);
		self->theme = NULL;
	}
	
	self->theme = g_object_ref(theme);

	g_object_notify(G_OBJECT(self), "theme");
}

gboolean
cria_presentation_get_auto_switch(CriaPresentation* self) {
	g_return_val_if_fail(CRIA_IS_PRESENTATION(self), FALSE);

	return self->auto_switch;
}

/**
 * cria_presentation_get_default_name:
 * @self: a #CriaPresentation
 *
 * Get a default filename for @self. This can be used from file choosers to
 * suggest a filename for a presentation.
 *
 * Returns a suggested filename. Don't forget to g_free() it.
 */
gchar*
cria_presentation_get_filename(CriaPresentation* self) {
	gchar* retval;
	const gchar* title = cria_presentation_get_title(self);

	if(cria_presentation_get_uri(self)) {
		retval = gnome_vfs_uri_extract_short_name(cria_presentation_get_uri(self));
	} else if(strcmp(STR_UNSAVED_PRESENTATION, title)) {
		GString* string = g_string_new(title);
		g_string_append(string, ".criawips");
		retval = string->str;
		g_string_free(string, FALSE);
	} else {
#warning "getDefaultName(): FIXME: try to get some information from the first slide with text or similar"
		retval = g_strdup_printf("%s.criawips", _("New Presentation"));
	}

	return retval;
}

gboolean
cria_presentation_get_loop(CriaPresentation* self) {
	g_return_val_if_fail(CRIA_IS_PRESENTATION(self), FALSE);

	return self->loop;
}

gdouble
cria_presentation_get_slide_delay(CriaPresentation* self) {
	g_return_val_if_fail(CRIA_IS_PRESENTATION(self), 0.0);

	return self->slide_delay;
}

CriaTheme*
cria_presentation_get_theme(CriaPresentation *self) {
	g_return_val_if_fail(CRIA_IS_PRESENTATION(self), NULL);
	
	return self->theme;
}

static void
cria_presentation_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaPresentation	* self;

	self = CRIA_PRESENTATION(object);

	switch(prop_id) {
	case PROP_LOOP:
		g_value_set_boolean(value, cria_presentation_get_loop(self));
		break;
	case PROP_SLIDE_AUTO:
		g_value_set_boolean(value, cria_presentation_get_auto_switch(self));
		break;
	case PROP_SLIDE_DELAY:
		g_value_set_double(value, cria_presentation_get_slide_delay(self));
		break;
	case PROP_TITLE:
		g_value_set_string(value, self->title);
		break;
	case PROP_URI:
		g_value_set_pointer(value, self->uri);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

/**
 * cria_presentation_get_slide_size:
 * @self: The presentation to query
 *
 * Get the slide size for this presentation (in Master Coordinates, 576dpi).
 * Don't free it, it's internal data from the presentation.
 *
 * Returns the size of a slide
 */
const GOPoint*
cria_presentation_get_slide_size(CriaPresentation* self) {
	g_return_val_if_fail(CRIA_IS_PRESENTATION(self), NULL);
	
	cdebugo(self, "getSlideSlize()", "%llix%lli", self->size.x, self->size.y);

	return &(self->size);
}

/**
 * cria_presentation_get_title:
 * @self: The presentation to get the title from
 *
 * Get the title of the presentation.
 *
 * Returns the title of the presentation, of one is set, an empty string otherwise.
 */
const gchar*
cria_presentation_get_title(CriaPresentation* self) {
	g_return_val_if_fail(CRIA_IS_PRESENTATION(self), "");
	g_return_val_if_fail(self->title, "");

	return self->title;
}

/**
 * cria_presentation_get_uri:
 * @self: the presentation to get the uri from
 *
 * Get information about the path of a presentation.
 *
 * Returns a GnomeVFSURI defining the path and filename of a presentation,
 * NULL if none is set. If you want to use it, don't forget to gnome_vfs_uri_ref()
 */
GnomeVFSURI*
cria_presentation_get_uri(CriaPresentation* self) {
	g_return_val_if_fail(CRIA_IS_PRESENTATION(self), NULL);
	
	return self->uri;
}

static void
cria_presentation_inserted_slide(CriaPresentation* self, gint new_position, CriaSlide* slide) {
	g_return_if_fail(CRIA_IS_PRESENTATION(self));

	if(self->saved) {
		self->saved = FALSE;
		g_object_notify(G_OBJECT(self), "saved");
	}
}

/**
 * cria_presentation_new_default:
 *
 * Creates a simple untitled presentation containing one empty slide.
 *
 * Returns a new presentation
 */
CriaPresentation*
cria_presentation_new_default(void) {
	CriaPresentation* self;
	GError		* error = NULL;
	gchar           * uri;

	uri = g_strdup_printf("file://%s/%s/%s", PACKAGE_DATA_DIR, PACKAGE, "default-presentation.criawips");
	self = cria_presentation_new_from_text_uri(uri, &error);
	g_free(uri);
	uri = NULL;

	if(!self) {
		CriaTheme* theme  = cria_theme_new(_("Default Theme"));
		CriaSlide* master = cria_slide_new(NULL);

		self = CRIA_PRESENTATION(g_object_new(CRIA_TYPE_PRESENTATION, "title", STR_UNSAVED_PRESENTATION, NULL));
		cria_slide_set_title(master, _("Default Master Slide"));
		
		cria_theme_add_master_slide(theme, master);
		cria_presentation_set_theme(self, theme);
		cria_slide_new(CRIA_SLIDE_LIST(self));

		if (error) {
			g_warning ("Unable to open default presentation: %s\n", error->message);
			g_error_free (error);
		}
	} else {
		cria_presentation_set_uri(self, NULL);
	}

	return self;
}

/**
 * cria_presentation_new_from_file:
 * @filename: the name of the file to open
 * @error: a location to return a GError, if this is 
 *
 * Creates a new presentation by parsing a file. @filename needs to be given
 * relative to the working directory. This function is used only for parsing
 * command line arguments.
 *
 * Returns the new presentation or NULL if an error occured
 */
CriaPresentation*
cria_presentation_new_from_file(const gchar* filename, GError** error) {
	CriaPresentation* self;
	gchar		* uri	= NULL;

	g_return_val_if_fail(filename != NULL && strlen(filename), NULL);
	g_return_val_if_fail(error == NULL || *error == NULL, NULL);
	
	cdebugt(CRIA_TYPE_PRESENTATION, "newFromFile()", "start");

	uri = gnome_vfs_make_uri_from_shell_arg(filename);
	self = cria_presentation_new_from_text_uri(uri, error);
	
	if(error && *error) {
		if(self) {
			g_object_unref(self);
		}
		
		self = NULL;
	}

	cdebugo(self, "newFromFile()", "end");
	
	g_free(uri);
	return self;
}

/**
 * cria_presentation_new_from_text_uri:
 * @text_uri: the text representation of a URI, e.g. ftp://user@host/path/to/file.ext
 * @error: a pointer to return a GError
 *
 * Create a new presentation by parsing the file given by the text_uri.
 *
 * Returns a new presentation
 */
CriaPresentation*
cria_presentation_new_from_text_uri(const gchar* text_uri, GError**errloc) {
	CriaPresentation* presentation;
	GError		* error	= NULL;
	GnomeVFSURI	* uri	= NULL;
	GnomeVFSFileInfo* info  = NULL;

	g_return_val_if_fail(text_uri != NULL && strlen(text_uri), NULL);
	g_return_val_if_fail(errloc == NULL || *errloc == NULL, NULL);

	uri = gnome_vfs_uri_new(text_uri);
	info = gnome_vfs_file_info_new();
	gnome_vfs_get_file_info_uri(uri, info, GNOME_VFS_FILE_INFO_DEFAULT | GNOME_VFS_FILE_INFO_GET_MIME_TYPE);

	presentation = cria_presentation_new_from_uri(uri, &error);
	gnome_vfs_uri_unref(uri);

	if(error && errloc) {
		*errloc = error;
		return NULL;
	}
	
	return presentation;
}

/*
 * cria_presentation_new_from_uri:
 * @uri: ...
 * errloc: ...
 *
 * ...
 *
 * Returns ...
 */
static CriaPresentation*
cria_presentation_new_from_uri(GnomeVFSURI* uri, GError** errloc) {
	GsfInput	* input = NULL;
	GError		* error = NULL;
	CriaPresentation* self = NULL;

	g_return_val_if_fail(uri != NULL, NULL);
	g_return_val_if_fail(errloc == NULL || *errloc == NULL, NULL);

	input = gsf_input_gnomevfs_new_uri(uri, &error);

	if(!error) {
#warning "newFromUri(): FIXME: add file type detection"
		/* TODO add mime type detection:
		 * this one works basically by getting a GnomeVFSFileInfo from the URI
		 * and let GnomeVFS do the detection */
#warning "newFromUri(): FIXME: extract file parsing into a plugin"
		/* TODO extract the file reading into a plugin (refer to gnumeric's code
		 * for examples) */
		
		self = cria_presentation_populate_from_xml_input(input, uri, &error);
	}

	if(!error) {
		return self;
	} else if(self) {
		g_object_unref(self);
	}

	*errloc = error;
	return NULL;
}

void
cria_presentation_set_auto_switch(CriaPresentation* self, gboolean auto_switch) {
	g_return_if_fail(CRIA_IS_PRESENTATION(self));

	if(self->auto_switch == auto_switch) {
		return;
	}

	self->auto_switch = auto_switch;

	g_object_notify(G_OBJECT(self), "auto-switch");
}

void
cria_presentation_set_loop(CriaPresentation* self, gboolean loop) {
	g_return_if_fail(CRIA_IS_PRESENTATION(self));

	if(self->loop == loop) {
		return;
	}

	self->loop = loop;

	g_object_notify(G_OBJECT(self), "loop");
}

void
cria_presentation_set_slide_delay(CriaPresentation* self, gdouble delay) {
	g_return_if_fail(CRIA_IS_PRESENTATION(self));

	if(self->slide_delay == delay) {
		return;
	}

	self->slide_delay = delay;
	cdebugo(self, "setSlideDelay()", "new delay is %f", self->slide_delay);

	g_object_notify(G_OBJECT(self), "slide-delay");
}

static void
cria_presentation_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaPresentation	* self;
	
	self = CRIA_PRESENTATION (object);
	
	switch (prop_id) {
	case PROP_LOOP:
		cria_presentation_set_loop(self, g_value_get_boolean(value));
		break;
	case PROP_SLIDE_AUTO:
		cria_presentation_set_auto_switch(self, g_value_get_boolean(value));
		break;
	case PROP_SLIDE_DELAY:
		cria_presentation_set_slide_delay(self, g_value_get_double(value));
		break;
	case PROP_TITLE:
		cria_presentation_set_title(self, g_value_get_string(value));
		break;
	case PROP_URI:
		cria_presentation_set_uri(self, g_value_get_pointer(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object,
						  prop_id,
						  param_spec);
		break;
	}
}

/**
 * cria_presentation_set_title:
 * @self: the presentation to set the title for
 * @title: the new title for the presentation
 *
 * Sets a new title for the presentation. The title is only changed if
 * both titles are different, so we prevent recursive loops with title
 * entry elements.
 */
void
cria_presentation_set_title(CriaPresentation* self, const gchar* title) {
	g_return_if_fail(CRIA_IS_PRESENTATION(self));
	g_return_if_fail(title != NULL);

	cdebugo(self, "setTitle()", "setting to %s", title);

	if(title != NULL && self->title != NULL && !strcmp(title, self->title)) {
		/* the text's are equal */
		return;
	}

	if(self->title != NULL) {
		g_free(self->title);
	}

	if(self->title == NULL && !strcmp(title, STR_UNSAVED_PRESENTATION)) {
		self->title = g_strdup_printf("%s %d", STR_UNSAVED_PRESENTATION, ++unsaved_presentations);
	} else {
		self->title = g_strdup(title);
	}

	g_object_notify(G_OBJECT(self), "title");
}

void
cria_presentation_set_uri(CriaPresentation* self, GnomeVFSURI* uri) {
	g_return_if_fail(CRIA_IS_PRESENTATION(self));
	/* unfortunately we don't have GType information for GnomeVFSURI */
	/* g_return_if_fail(self->uri == NULL || GNOME_IS_VFS_URI(self->uri)); */
	/* g_return_if_fail(uri == NULL || GNOME_IS_VFS_URI(uri)); */

	if(self->uri) {
		gnome_vfs_uri_unref(self->uri);
		self->uri = NULL;
	}

	if(uri) {
		self->uri = gnome_vfs_uri_ref(uri);
	}

	g_object_notify(G_OBJECT(self), "uri");
}

/* GObject stuff */
static void
cria_presentation_init(CriaPresentation *self) {
	self->size.x = 5760;
	self->size.y = 4320;
}

static void
cp_slide_unref(CriaSlide* slide, CriaPresentation* self) {
	cdebug("finalize()", "%p: removing %d. reference of %p (%s)", self, G_OBJECT(slide)->ref_count, slide, G_OBJECT_TYPE_NAME(slide));
	cria_slide_list_remove(CRIA_SLIDE_LIST(self), cria_slide_list_index(CRIA_SLIDE_LIST(self), slide));
}

static void
cp_finalize(GObject* obj) {
	CriaPresentation* self = CRIA_PRESENTATION(obj);
	
	cdebug("finalize()", "%p: removing %d slides", self, cria_slide_list_n_slides(CRIA_SLIDE_LIST(self)));
	g_list_foreach(self->slides, G_FUNC(cp_slide_unref), self);
	g_list_free(self->slides);
	self->slides = NULL;
	
	cdebug("finalize()", "%p: removing %d. reference of %p (%s)", self, G_OBJECT(self->theme)->ref_count, self->theme, G_OBJECT_TYPE_NAME(self->theme));
	g_object_unref(self->theme);
	self->theme = NULL;

	parent_class->finalize(obj);
}

static void
cria_presentation_class_init(CriaPresentationClass* cria_presentation_class) {
	GObjectClass	* g_object_class;

	parent_class = g_type_class_peek_parent(cria_presentation_class);

	/* setting up the gobject class */
	g_object_class = G_OBJECT_CLASS(cria_presentation_class);

	g_object_class->finalize     = cp_finalize;
	g_object_class->set_property = cria_presentation_set_property;
	g_object_class->get_property = cria_presentation_get_property;

	g_object_class_install_property(g_object_class,
					PROP_SLIDE_AUTO,
					g_param_spec_boolean("auto-switch",
							     "Switch between slides automatically",
							     "Specifies whether this presentation should automatically switch between slides",
							     FALSE,
							     G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property(g_object_class,
					PROP_LOOP,
					g_param_spec_boolean("loop",
							     "Loop",
							     "Restart presentation after the last slide",
							     FALSE,
							     G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property(g_object_class,
					PROP_SLIDE_DELAY,
					g_param_spec_double("slide-delay",
							    "Slide Delay",
							    "The time that a slide is shown in auto-switch mode by default (in seconds)",
							    0.0,
							    G_MAXDOUBLE,
							    5.0,
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property(g_object_class,
					PROP_THEME,
					g_param_spec_object("theme",
							    "theme",
							    "Theme",
							    CRIA_TYPE_THEME,
							    G_PARAM_READWRITE));
	g_object_class_install_property(g_object_class,
					PROP_TITLE,
					g_param_spec_string("title",
							    "Title",
							    "The title of the presentation",
							    _("untitled"),
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property(g_object_class,
					PROP_URI,
					g_param_spec_pointer("uri",
							     "GnomeVFSURI for this file",
							     "The URI this file can be located at, NULL if newly created",
							     G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

	/* setting up the presentation class */
/*	cria_presentation_class->slide_deleted  = cp_slide_deleted;*/
	cria_presentation_class->inserted_slide = cria_presentation_inserted_slide;

	cria_presentation_signals[SLIDE_DELETED_SIGNAL] =
				g_signal_new("slide-deleted",
					     CRIA_TYPE_PRESENTATION,
					     G_SIGNAL_RUN_LAST,
					     G_STRUCT_OFFSET(CriaPresentationClass, slide_deleted),
					     NULL, NULL,
					     cria_marshal_VOID__INT_OBJECT,
					     G_TYPE_NONE, 2,
					     G_TYPE_INT,
					     CRIA_TYPE_SLIDE);
	cria_presentation_signals[INSERTED_SLIDE_SIGNAL] = g_signal_new("inserted-slide",
									CRIA_TYPE_PRESENTATION,
									G_SIGNAL_RUN_LAST,
									G_STRUCT_OFFSET(CriaPresentationClass,
											inserted_slide),
									NULL,
									NULL,
									cria_marshal_VOID__INT_OBJECT,
									G_TYPE_NONE,
									2,
									G_TYPE_INT,
									CRIA_TYPE_SLIDE);
}

static guint
cp_n_slides(CriaSlideList* self) {
	g_return_val_if_fail(CRIA_IS_PRESENTATION(self), 0);
	
	return g_list_length(CRIA_PRESENTATION(self)->slides);
}

static CriaSlide*
cp_get(CriaSlideList* self, guint slide) {
	CriaSlide* retval;
	
	g_return_val_if_fail(CRIA_IS_PRESENTATION(self), NULL);
	g_return_val_if_fail(slide < cp_n_slides(self), NULL);
	
	retval = g_list_nth(CRIA_PRESENTATION(self)->slides, slide)->data;

	g_return_val_if_fail(CRIA_IS_SLIDE(retval), NULL);

	return retval;
}

static gint
cp_index(CriaSlideList* self, const CriaSlide* slide) {
	g_return_val_if_fail(CRIA_IS_PRESENTATION(self), -1);
	g_return_val_if_fail(CRIA_IS_SLIDE(slide), -1);
	
	return g_list_index(CRIA_PRESENTATION(self)->slides, slide);
}

static void
cp_insert(CriaSlideList* self, CriaSlide* slide, guint pos) {
	g_return_if_fail(CRIA_IS_PRESENTATION(self));
	g_return_if_fail(CRIA_IS_SLIDE(slide));
	g_return_if_fail(cp_index(self, slide) == -1);
	
	CRIA_PRESENTATION(self)->slides = g_list_insert(CRIA_PRESENTATION(self)->slides, g_object_ref(slide), pos);

	pos = cp_index(self, slide);
	cdebugo(self, "insertSlide()", "new index = %i", pos);
	
#warning "inserSlide(): FIXME: the signal should come from the container"
	g_signal_emit(self, cria_presentation_signals[INSERTED_SLIDE_SIGNAL], 0, pos, slide);
}

static void
cp_remove(CriaSlideList* self, guint pos) {
	CriaSlide* slide;
	
	g_return_if_fail(CRIA_IS_PRESENTATION(self));
	g_return_if_fail(pos < cp_n_slides(self));

	slide = cp_get(self, pos);
	CRIA_PRESENTATION(self)->slides = g_list_remove(CRIA_PRESENTATION(self)->slides, slide);

	g_signal_emit(self, cria_presentation_signals[SLIDE_DELETED_SIGNAL], 0, pos, slide);
	
	cdebug("remove()", "%p: removing %d. reference on %p (%s)", self, G_OBJECT(slide)->ref_count, slide, G_OBJECT_TYPE_NAME(slide));
	g_object_unref(slide);
}

static void
cp_container_init(gpointer interface) {
	CriaSlideListIface* iface = interface;

	iface->get      = cp_get;
	iface->index    = cp_index;
	iface->insert   = cp_insert;
	iface->n_slides = cp_n_slides;
	iface->remove   = cp_remove;
}

G_DEFINE_TYPE_WITH_CODE(CriaPresentation, cria_presentation, G_TYPE_OBJECT,
			G_IMPLEMENT_INTERFACE(CRIA_TYPE_SLIDE_LIST, cp_container_init))
