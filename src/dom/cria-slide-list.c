/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <dom/cria-slide-list.h>

void
cria_slide_list_append(CriaSlideList* self, CriaSlide* slide) {
	if(CRIA_SLIDE_LIST_GET_CLASS(self)->append) {
		CRIA_SLIDE_LIST_GET_CLASS(self)->append(self, slide);
	} else {
		/* try to fall back to insert */
		cria_slide_list_insert(self, slide, -1);
	}
}

CriaSlide*
cria_slide_list_get(CriaSlideList* self, guint num) {
	g_return_val_if_fail(CRIA_SLIDE_LIST_GET_CLASS(self)->get, NULL);

	return CRIA_SLIDE_LIST_GET_CLASS(self)->get(self, num);
}

/**
 * cria_slide_list_index:
 * @self: a #CriaSlideList
 * @slide: The slide to be found
 *
 * Get the index of a slide in a slide container.
 *
 * Returns the index of the slide if found, -1 otherwise
 */
gint
cria_slide_list_index(CriaSlideList* self, const CriaSlide* slide) {
	g_return_val_if_fail(CRIA_SLIDE_LIST_GET_CLASS(self)->index, -1);

	return CRIA_SLIDE_LIST_GET_CLASS(self)->index(self, slide);
}

/**
 * cria_slide_list_insert:
 * @self: a #CriaSlideList
 * @slide: a #CriaSlide
 * @pos: a position
 *
 * Insert @slide at @pos in @self. If @pos is smaller than 0 or bigger than the
 * list's length, the slide will be appended.
 */
void
cria_slide_list_insert(CriaSlideList* self, CriaSlide* slide, guint position) {
	g_return_if_fail(CRIA_SLIDE_LIST_GET_CLASS(self)->insert);

	return CRIA_SLIDE_LIST_GET_CLASS(self)->insert(self, slide, position);
}

guint
cria_slide_list_n_slides(CriaSlideList* self) {
	g_return_val_if_fail(CRIA_SLIDE_LIST_GET_CLASS(self)->n_slides, 0);

	return CRIA_SLIDE_LIST_GET_CLASS(self)->n_slides(self);
}

void
cria_slide_list_remove(CriaSlideList* self, guint position) {
	g_return_if_fail(CRIA_SLIDE_LIST_GET_CLASS(self)->remove);

	CRIA_SLIDE_LIST_GET_CLASS(self)->remove(self, position);
}

GType
cria_slide_list_get_type(void) {
	static GType type = 0;

	if(!type) {
		static const GTypeInfo info = {
			sizeof(CriaSlideListIface),
			NULL,
			NULL,
			NULL
		};

		type = g_type_register_static(G_TYPE_INTERFACE,
					      "CriaSlideList",
					      &info,
					      0);
	}

	return type;
}

