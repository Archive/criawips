/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef PRESENTATION_H
#define PRESENTATION_H

#include <libgnomevfs/gnome-vfs-uri.h>
#include <goffice/utils/go-units.h>

#include <dom/cria-theme.h>

G_BEGIN_DECLS

typedef struct _CriaPresentation	CriaPresentation;
typedef struct _CriaPresentationClass	CriaPresentationClass;

G_END_DECLS

#include <dom/cria-slide.h>

G_BEGIN_DECLS

gboolean	  cria_presentation_get_auto_switch    (CriaPresentation* self);
gchar*		  cria_presentation_get_filename       (CriaPresentation* self);
gboolean	  cria_presentation_get_loop           (CriaPresentation* self);
CriaTheme*        cria_presentation_get_theme 	       (CriaPresentation *self);
GType		  cria_presentation_get_type	       (void);
GnomeVFSURI*      cria_presentation_get_uri	       (CriaPresentation* self);
gdouble		  cria_presentation_get_slide_delay    (CriaPresentation* self);
const GOPoint	* cria_presentation_get_slide_size     (CriaPresentation* self);
const gchar     * cria_presentation_get_title	       (CriaPresentation* self);
CriaPresentation* cria_presentation_new_default	       (void);
CriaPresentation* cria_presentation_new_from_file      (const	gchar	* filename,
							GError		**error);
CriaPresentation* cria_presentation_new_from_text_uri  (const gchar	* text_uri,
							GError		**error);
void		  cria_presentation_set_auto_switch    (CriaPresentation* self,
							gboolean	  auto_switch);
void		  cria_presentation_set_loop           (CriaPresentation* self,
							gboolean	  loop);
void		  cria_presentation_set_slide_delay    (CriaPresentation* self,
							gdouble		  delay);
void		  cria_presentation_set_theme	       (CriaPresentation* self,
							CriaTheme	* theme);
void		  cria_presentation_set_title	       (CriaPresentation* self,
							const char	* title);
void		  cria_presentation_set_uri	       (CriaPresentation* self,
							GnomeVFSURI	* uri);

#define CRIA_TYPE_PRESENTATION			(cria_presentation_get_type())
#define CRIA_PRESENTATION(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_PRESENTATION, CriaPresentation))
#define CRIA_PRESENTATION_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_PRESENTATION, CriaPresentationClass))
#define CRIA_IS_PRESENTATION(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_PRESENTATION))
#define CRIA_IS_PRESENTATION_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_PRESENTATION))
#define CRIA_PRESENTATION_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_PRESENTATION, CriaPresentationClass))

G_END_DECLS

#endif /* PRESENTATION_H */
