/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef BLOCK_VIEW_H
#define BLOCK_VIEW_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

typedef struct _CriaBlockView CriaBlockView;
typedef struct _CriaBlockViewClass CriaBlockViewClass;

#define CRIA_TYPE_BLOCK_VIEW           (cria_block_view_get_type())
#define CRIA_BLOCK_VIEW(obj)           (G_TYPE_CHECK_INSTANCE_CAST((obj), CRIA_TYPE_BLOCK_VIEW, CriaBlockView))
#define CRIA_IS_BLOCK_VIEW(obj)        (G_TYPE_CHECK_INSTANCE_TYPE((obj), CRIA_TYPE_BLOCK_VIEW))
#define CRIA_BLOCK_VIEW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_INTERFACE((obj), CRIA_TYPE_BLOCK_VIEW, CriaBlockView))

GType	cria_block_view_get_type		(void);
void	_cria_block_view_install_properties     (GObjectClass*	klass);

struct _CriaBlockViewClass {
	GTypeInterface base_interface;

	/* add signals here */
};

enum {
	CRIA_BLOCK_VIEW_PROP_BLOCK = 0xf000
};

G_END_DECLS

#endif /* BLOCK_VIEW_H */

