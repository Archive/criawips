/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIAWIPS_SLIDE_CONTAINER_H
#define CRIAWIPS_SLIDE_CONTAINER_H

#include <glib-object.h>

G_BEGIN_DECLS

typedef struct _CriaSlideList CriaSlideList;
typedef struct _CriaSlideListIface CriaSlideListIface;

G_END_DECLS

#include <dom/cria-slide.h>

G_BEGIN_DECLS

#define CRIA_TYPE_SLIDE_LIST	    (cria_slide_list_get_type())
#define CRIA_SLIDE_LIST(obj)	    (G_TYPE_CHECK_INSTANCE_CAST((obj), CRIA_TYPE_SLIDE_LIST, CriaSlideList))
#define CRIA_IS_SLIDE_LIST(obj)	    (G_TYPE_CHECK_INSTANCE_TYPE((obj), CRIA_TYPE_SLIDE_LIST))
#define CRIA_SLIDE_LIST_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_INTERFACE((obj), CRIA_TYPE_SLIDE_LIST, CriaSlideListIface))

GType cria_slide_list_get_type(void);

void cria_slide_list_append (CriaSlideList* self,
				  CriaSlide	    * slide);
CriaSlide* cria_slide_list_get (CriaSlideList* self,
				     guint		 num);
gint cria_slide_list_index (CriaSlideList* self,
				 const CriaSlide   * slide);
void cria_slide_list_insert (CriaSlideList* self,
				  CriaSlide	    * slide,
				  guint		      position);
guint cria_slide_list_n_slides (CriaSlideList* self);
void cria_slide_list_remove(CriaSlideList* self,
				 guint position);

struct _CriaSlideListIface {
	GTypeInterface interface;

	guint slide_insert_id;
	guint slide_remove_id;

	/* vtable */
	void      (*append)  (CriaSlideList* self,
			      CriaSlide	  * slide);
	CriaSlide*(*get)     (CriaSlideList* self,
			      guint		  num);
	gint      (*index)   (CriaSlideList* self,
			      const CriaSlide   * slide);
	void      (*insert)  (CriaSlideList* self,
			      CriaSlide		* slide,
			      guint		  position);
	guint     (*n_slides)(CriaSlideList* self);
	void      (*remove)  (CriaSlideList* self,
			      guint               position);
};

G_END_DECLS

#endif /* !CRIAWIPS_SLIDE_CONTAINER_H */

