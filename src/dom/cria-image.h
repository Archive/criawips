/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <glib-object.h>

#ifndef IMAGE_H
#define IMAGE_H

G_BEGIN_DECLS

typedef struct _CriaImage CriaImage;
typedef struct _CriaImageClass CriaImageClass;
typedef struct _CriaImagePrivate CriaImagePrivate;
typedef struct _CriaImageClassPrivate CriaImageClassPrivate;

GType		cria_image_get_type	       (void);
const gchar*	cria_image_get_uri             (CriaImage	* self);
CriaImage*	cria_image_new		       (const gchar	* uri);

#define CRIA_TYPE_IMAGE			(cria_image_get_type ())
#define CRIA_IMAGE(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_IMAGE, CriaImage))
#define CRIA_IMAGE_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_IMAGE, CriaImageClass))
#define CRIA_IS_IMAGE(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_IMAGE))
#define CRIA_IS_IMAGE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_IMAGE))
#define CRIA_IMAGE_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_IMAGE, CriaImageClass))

struct _CriaImage {
	GObject		  base_instance;
	CriaImagePrivate* priv;
};

struct _CriaImageClass {
	GObjectClass		  base_class;
	CriaImageClassPrivate	* priv;
};

G_END_DECLS

#endif /* !IMAGE_H */

