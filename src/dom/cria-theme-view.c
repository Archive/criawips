/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <dom/cria-theme-view.h>
#include <helpers/gobject-helpers.h>

G_DEFINE_IFACE_FULL(CriaThemeView, cria_theme_view, G_TYPE_INTERFACE);

void
cria_theme_view_register(CriaThemeView* self, CriaTheme* theme) {
	g_return_if_fail(CRIA_IS_THEME_VIEW(self));
	
	g_object_ref(theme);

	if(CRIA_THEME_VIEW_GET_CLASS(self)->added_master_slide) {
		GList* slides = cria_theme_get_master_slides(theme),
		     * slide;
		g_signal_connect_swapped(theme, "added-master-slide",
					 G_CALLBACK(CRIA_THEME_VIEW_GET_CLASS(self)->added_master_slide), self);
		for(slide = slides; slide; slide = slide->next) {
			CRIA_THEME_VIEW_GET_CLASS(self)->added_master_slide(self, slide->data);
		}

		g_list_free(slides);
	}
}

void
cria_theme_view_unregister(CriaThemeView* self, CriaTheme* theme) {
	g_return_if_fail(CRIA_IS_THEME_VIEW(self));
	
	if(CRIA_THEME_VIEW_GET_CLASS(self)->added_master_slide) {
		g_signal_handlers_disconnect_by_func(theme, CRIA_THEME_VIEW_GET_CLASS(self)->added_master_slide, self);
	}
	g_object_unref(theme);
}

/* GInterface stuff */
void
_cria_theme_view_install_properties(GObjectClass* go_class, gint prop_id) {
	g_object_class_override_property(go_class, prop_id, "theme");
}

static void
cria_theme_view_class_init(gpointer iface) {
	g_object_interface_install_property(iface,
					    g_param_spec_object("theme",
						    		"Theme",
								"The theme that this view is connected to",
								CRIA_TYPE_THEME,
								G_PARAM_READWRITE));
}

