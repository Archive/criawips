/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <dom/cria-alignment.h>

#include <string.h>

#define CDEBUG_TYPE -1
#include <cdebug/cdebug.h>

/**
 * cria_alignment_from_string:
 * @string: the string to read the alignment from
 *
 * Get an alignment from a string value. This method translates the strings
 * "left", "center" and "right" into the equivalent CriaAlignment values.
 * If an unknown string or NULL gets passed into this method, it returns
 * CRIA_ALIGNMENT_UNSET.
 *
 * Returns the alignment specified by the string
 */
CriaAlignment
cria_alignment_from_string(const gchar* string) {
	CriaAlignment	  retval = CRIA_ALIGNMENT_UNSET;
	
	if(string == NULL) {
		cdebug("CriaAlignment::fromString()", "unknown alignment tag \"%s\"", string);
	} else if(!strcmp(string, "left")) {
		cdebug("CriaAlignmentn::fromString()", "\"%s\" means \"left\"", string);
		retval = CRIA_ALIGNMENT_LEFT;
	} else if(!strcmp(string, "right")) {
		cdebug("CriaAlignment::fromString()", "\"%s\" means \"right\"", string);
		retval = CRIA_ALIGNMENT_RIGHT;
	} else if(!strcmp(string, "center")) {
		cdebug("CriaAlignment::fromString()", "\"%s\" means \"center\"", string);
		retval = CRIA_ALIGNMENT_CENTER;
	}
	
	return retval;
}

/**
 * cria_valignment_from_string:
 * @string: the string to read the alignment from
 *
 * Get a vertical alignment from a string value. This method translates the
 * strings "top", "middle" and "bottom" into the equivalent CriaVAlignment
 * values. If an unknown string or NULL gets passed into this method, it
 * returns CRIA_VALIGNMENT_UNSET.
 *
 * Returns the vertical alignment specified by the string
 */
CriaVAlignment
cria_valignment_from_string(const gchar* string) {
	CriaVAlignment	  retval = CRIA_VALIGNMENT_UNSET;

	if(string == NULL) {
	} else if(!strcmp(string, "top")) {
		retval = CRIA_ALIGNMENT_TOP;
	} else if(!strcmp(string, "middle")) {
		retval = CRIA_ALIGNMENT_MIDDLE;
	} else if(!strcmp(string, "bottom")) {
		retval = CRIA_ALIGNMENT_BOTTOM;
	}

	return retval;
}

