/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef ALIGNMENT_H
#define ALIGNMENT_H

#include <glib.h>

typedef enum {
	CRIA_ALIGNMENT_UNSET,
	CRIA_ALIGNMENT_LEFT,
	CRIA_ALIGNMENT_CENTER,
	CRIA_ALIGNMENT_RIGHT,
	CRIA_ALIGNMENT_JUSTIFY
} CriaAlignment;

typedef enum {
	CRIA_VALIGNMENT_UNSET,
	CRIA_ALIGNMENT_TOP,
	CRIA_ALIGNMENT_MIDDLE,
	CRIA_ALIGNMENT_BOTTOM
} CriaVAlignment;

CriaAlignment	cria_alignment_from_string	       (char const	* string);
CriaVAlignment	cria_valignment_from_string	       (char const	* string);

#endif /* !ALIGNMENT_H */

