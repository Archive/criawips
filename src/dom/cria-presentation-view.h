/* This file is part of criawips, a GNOME presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef PRESENTATION_VIEW_H
#define PRESENTATION_VIEW_H

#include <dom/cria-presentation.h>

G_BEGIN_DECLS

typedef struct _CriaPresentationView CriaPresentationView;
typedef struct _CriaPresentationViewIface CriaPresentationViewIface;

#define CRIA_TYPE_PRESENTATION_VIEW	    (cria_presentation_view_get_type())
#define CRIA_PRESENTATION_VIEW(obj)	    (G_TYPE_CHECK_INSTANCE_CAST ((obj), CRIA_TYPE_PRESENTATION_VIEW, CriaPresentationView))
#define CRIA_IS_PRESENTATION_VIEW(obj)	    (G_TYPE_CHECK_INSTANCE_TYPE((obj), CRIA_TYPE_PRESENTATION_VIEW))
#define CRIA_PRESENTATION_VIEW_GET_CLASS(o) (G_TYPE_INSTANCE_GET_INTERFACE((o), CRIA_TYPE_PRESENTATION_VIEW, CriaPresentationViewIface))


GType	cria_presentation_view_get_type		       (void);
void    _cria_presentation_view_install_properties     (GObjectClass*  klass);

void	cria_presentation_view_register	 (CriaPresentationView	* self,
					  CriaPresentation	* presentation);
void	cria_presentation_view_unregister(CriaPresentationView	* self,
					  CriaPresentation      * presentation);

struct _CriaPresentationViewIface {
	GTypeInterface	  parent;

	/* vtable */
	void (*notify_theme) (CriaPresentationView* view,
			      CriaTheme           * theme);
	void (*notify_title) (CriaPresentationView* view,
			      gchar const         * title);
	void (*slide_added)  (CriaPresentationView* self,
			      gint                  index,
			      CriaSlide           * slide);
	void (*slide_removed)(CriaPresentationView* self,
			      gint                  old_index,
			      CriaSlide           * slide);
};

enum {
	CRIA_PRESENTATION_VIEW_PROP_PRESENTATION = 0xe00
};

G_END_DECLS

#endif /* !PRESENTATION_VIEW_H */
