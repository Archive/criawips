/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <dom/cria-text-model-priv.h>

#define CDEBUG_TYPE cria_text_model_get_type
#include <cdebug/cdebug.h>

G_DEFINE_TYPE(CriaTextModel, cria_text_model, G_TYPE_OBJECT);

CriaTextModel*
cria_text_model_new(void) {
	return g_object_new(CRIA_TYPE_TEXT_MODEL, NULL);
}

CriaTextModel*
cria_text_model_clone(CriaTextModel* original) {
	CriaTextModel* clone;
	gchar const  * text;

	g_return_val_if_fail(CRIA_IS_TEXT_MODEL(original), NULL);
	
	cdebug("clone()", "Cloning '%s'", cria_text_model_get_text(original));
	clone = g_object_new(CRIA_TYPE_TEXT_MODEL, NULL);
	text = cria_text_model_get_text(original);
	
	if(text) {
		cria_text_model_append(clone, text);
	}

	return clone;
}

void
cria_text_model_append(CriaTextModel* self, const gchar* text) {
	g_return_if_fail(CRIA_IS_TEXT_MODEL(self));
	g_return_if_fail(text);

	g_string_append(self->string, text);

	g_object_notify(G_OBJECT(self), "text");
}

void
cria_text_model_delete(CriaTextModel* self, gsize offset, gsize length) {
	g_string_erase(self->string, offset, length);
	g_object_notify(G_OBJECT(self), "text");
}

gchar const*
cria_text_model_get_text(CriaTextModel const* self) {
	g_return_val_if_fail(CRIA_IS_TEXT_MODEL(self), NULL);
	
	return self->string->str;
}

void
cria_text_model_insert(CriaTextModel* self, gchar const* text, gsize position) {
	g_return_if_fail(CRIA_IS_TEXT_MODEL(self));

	g_string_insert(self->string, position, text);
	g_object_notify(G_OBJECT(self), "text");
}

void
cria_text_model_set_text(CriaTextModel* self, gchar const* text) {
	g_return_if_fail(CRIA_IS_TEXT_MODEL(self));
	g_return_if_fail(text);

	g_string_truncate(self->string, 0);
	cria_text_model_append(self, text);

	g_object_notify(G_OBJECT(self), "text");
}

/* GType stuff */

enum {
	PROP_0,
	PROP_TEXT
};

static void
cria_text_model_init(CriaTextModel* self) {
	self->string = g_string_new("");
}

static void
ctm_finalize(GObject* object) {
	CriaTextModel* self = CRIA_TEXT_MODEL(object);
	
	g_string_free(self->string, TRUE);
	
	G_OBJECT_CLASS(cria_text_model_parent_class)->finalize(object);
}

static void
ctm_get_property() {}

static void
ctm_set_property() {}

static void
cria_text_model_class_init(CriaTextModelClass* self_class) {
	GObjectClass* go_class;

	/* setting up GObjectClass */
	go_class = G_OBJECT_CLASS(self_class);
	go_class->finalize     = ctm_finalize;
	go_class->get_property = ctm_get_property;
	go_class->set_property = ctm_set_property;

	g_object_class_install_property(go_class,
					PROP_TEXT,
					g_param_spec_string("text",
							    "Text",
							    "The text that's contained by this model",
							    NULL,
							    G_PARAM_READWRITE));
}

