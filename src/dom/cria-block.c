/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <dom/cria-block-priv.h>

#include <inttypes.h>
#include <string.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <glib-object.h>
#include <goffice/utils/go-color.h>

#define CDEBUG_TYPE cria_block_get_type
#include <cdebug/cdebug.h>

#include <dom/cria-format.h>
#include <dom/cria-enums.h>
#include <utils/cria-units.h>

#include "application.h"

enum {
	PROP_0,
	PROP_ALIGNMENT_HORIZONTAL,
	PROP_ALIGNMENT_VERTICAL,
	PROP_COLOR,
	PROP_FONT_FAMILY,
	PROP_FONT_SIZE,
	PROP_MODEL
};

enum {
	FORMAT_CHANGED,
	N_SIGNALS
};

G_DEFINE_TYPE(CriaBlock, cria_block, CRIA_TYPE_SLIDE_ELEMENT);

static	void	cria_block_get_property	       (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static	void	cria_block_set_property        (GObject		* object,
						guint		  prop_id,
						const	GValue	* value,
						GParamSpec	* param_spec);

static	guint	cria_block_signals[N_SIGNALS] = { 0 };

static void
cb_finalize(GObject* object) {
	CriaBlock* self = CRIA_BLOCK(object);

	if(self->format) {
		cdebug("finalize()", "removing %d. reference on CriaLepton %d (\"%s\")", cria_lepton_refs(self->format), self->format, cria_lepton_get(self->format));
		cria_lepton_unref(self->format);
		self->format = 0;
	}

	if(self->model) {
		g_object_unref(self->model);
		self->model = NULL;
	}

	G_OBJECT_CLASS(cria_block_parent_class)->finalize(object);
}

static gboolean
cb_is_empty (CriaSlideElement const* element)
{
	CriaBlock* self = CRIA_BLOCK(element),
		 * master = CRIA_BLOCK(element->master);
	gchar const* text1 = self->model ? cria_text_model_get_text(self->model) : NULL,
	           * text2 = master && master->model ? cria_text_model_get_text(master->model) : NULL;
	gboolean retval = FALSE;
	
	if (master) {
#if 0
		retval = !self->format || (self->format == master->format &&
			  self->model && self->model == master->model) ||
			  (!text1 || !text2 || strcmp(text1, text2));
#else
		retval = !text1 || // no text is empty
			 (text2 && !strcmp (text1, text2)); // text equals master is empty
#endif
	} else {
		retval = !self->format && !self->model && !text1;
	}

	cdebug("isEmpty()", "returning %s", retval ? "TRUE" : "FALSE");
	return retval;
}

static void
cria_block_class_init(CriaBlockClass* self_class) {
	GObjectClass         * go_class;
	CriaSlideElementClass* cse_class;

	/* GObjectClass */
	go_class = G_OBJECT_CLASS(self_class);
	go_class->set_property = cria_block_set_property;
	go_class->get_property = cria_block_get_property;
	go_class->finalize     = cb_finalize;

	g_object_class_install_property(go_class,
					PROP_ALIGNMENT_HORIZONTAL,
					g_param_spec_enum("alignment",
							  "Horizontal Alignment",
							  "The horizontal alignment of embedded elements",
							  CRIA_TYPE_ALIGNMENT,
							  CRIA_ALIGNMENT_UNSET,
							  G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property(go_class,
					PROP_COLOR,
					g_param_spec_pointer("color",
							     "Color",
							     "The text's foreground color",
							     G_PARAM_READWRITE));
	g_object_class_install_property(go_class,
					PROP_MODEL,
					g_param_spec_object("model",
							    "text model",
							    "The text contained in this block",
							    CRIA_TYPE_TEXT_MODEL,
							    G_PARAM_READWRITE));
	g_object_class_install_property(go_class,
					PROP_ALIGNMENT_VERTICAL,
					g_param_spec_enum("valignment",
							  "Vertical Alignment",
							  "The vertical alignment of embedded elements",
							  CRIA_TYPE_VALIGNMENT,
							  CRIA_VALIGNMENT_UNSET,
							  G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

	/* CriaSlideElementClass */
	cse_class = CRIA_SLIDE_ELEMENT_CLASS(self_class);
	cse_class->is_empty = cb_is_empty;

	/* CriaBlockClass */
	self_class->format_changed = NULL;
	
	cria_block_signals[FORMAT_CHANGED] = g_signal_new("format-changed",
					CRIA_TYPE_BLOCK,
					G_SIGNAL_RUN_LAST,
					G_STRUCT_OFFSET(CriaBlockClass, format_changed),
					NULL, NULL,
					g_cclosure_marshal_VOID__FLAGS,
					G_TYPE_NONE,
					1,
					CRIA_TYPE_FORMAT_DOMAIN);
}

/**
 * cria_block_get_alignment:
 * @self: the block to get the alignment from
 *
 * Get the horizontal alignment of content in this block
 *
 * Returns the horizontal alignment of content
 */
CriaAlignment
cria_block_get_alignment(CriaBlock const* self) {
	g_return_val_if_fail(CRIA_IS_BLOCK(self), CRIA_ALIGNMENT_UNSET);

	return self->alignment;
}

/**
 * cria_block_get_color:
 * @self: a #CriaBlock
 * 
 * Get the foreground color of a text block.
 *
 * Returns the color of this text block; if it's not set it returns a default
 * value.
 */
GOColor const*
cria_block_get_color(CriaBlock const* self) {
	g_return_val_if_fail(CRIA_IS_BLOCK(self), NULL);

	return self->color;
}

/**
 * cria_block_get_format:
 * @block: a #CriaBlock
 *
 * Get the format information for a block.
 *
 * Returns the format information for the block (as a #CriaLepton).
 */
CriaLepton
cria_block_get_format(CriaBlock const* self) {
	return self->format;
}

/**
 * cria_block_get_model:
 * @self: a #CriaBlock
 *
 * Get the text that should be rendered into a block.
 *
 * Returns the text that should be rendered into a block
 */
CriaTextModel*
cria_block_get_model(CriaBlock const* self) {
	g_return_val_if_fail(CRIA_IS_BLOCK(self), NULL);
	
	return self->model;
}

static void
cria_block_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaBlock* self;

	self = CRIA_BLOCK(object);

	switch(prop_id) {
	case PROP_ALIGNMENT_HORIZONTAL:
		g_value_set_enum(value, cria_block_get_alignment(self));
		break;
	case PROP_ALIGNMENT_VERTICAL:
		g_value_set_enum(value, cria_block_get_valignment(self));
		break;
	case PROP_COLOR:
#warning: "FIXME: this is ugly"
		g_value_set_pointer(value, (gpointer)cria_block_get_color(self));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

/**
 * cria_block_get_valignment:
 * @self: the block to get the alignment from
 *
 * Get the vertical alignment of content in this block
 *
 * Returns the vertical alignment of content
 */
CriaVAlignment
cria_block_get_valignment(CriaBlock const* self) {
	g_return_val_if_fail(CRIA_IS_BLOCK(self), CRIA_VALIGNMENT_UNSET);

	return self->valignment;
}

static void
cria_block_init(CriaBlock* self) {
}

CriaBlock*
cria_block_new(const gchar* name) {
	return g_object_new(CRIA_TYPE_BLOCK, "name", name, NULL);
}

void
cria_block_set_alignment(CriaBlock* self, CriaAlignment align) {
	g_return_if_fail(CRIA_IS_BLOCK(self));

	if(align == self->alignment) {
		return;
	}

	self->alignment = align;

	g_object_notify(G_OBJECT(self), "alignment");
}

/**
 * cria_block_set_color:
 * @self: a #CriaBlock
 * @color: a #GOColor
 *
 * Set the foreground color of a text block.
 */
void
cria_block_set_color(CriaBlock* self, GOColor const* color) {
	g_return_if_fail(CRIA_IS_BLOCK(self));

	if(!self->color && color) {
		self->color = g_new0(GOColor, 1);
	} else if(self->color && !color) {
		g_free(self->color);
		self->color = NULL;
	}

	if(color) {
		*self->color = *color;
	}

	g_object_notify(G_OBJECT(self), "color");
}

void
cria_block_set_font_family(CriaBlock* self, const gchar* font) {
	gchar     * new_format;
	CriaLepton  other;
	
	g_return_if_fail(CRIA_IS_BLOCK(self));

	new_format = cria_format_set(self->format, "font-family", font);
	other = cria_lepton_ref(new_format);
	g_free(new_format);
	
	if(other != self->format) {
		CriaLepton three = self->format;
		self->format = other;
		other = three;

		g_signal_emit(self, cria_block_signals[FORMAT_CHANGED], 0, (CriaFormatFont | CriaFormatSize));
	}
	
	cria_lepton_unref(other);
}

/**
 * cria_block_set_font_size:
 * @self: a #CriaBlock
 * @size: a string specifying a font size
 *
 * Sets the font size.
 */
void
cria_block_set_font_size(CriaBlock* self, const gchar* size) {
	gchar     * new_format;
	CriaLepton  other;
	
	g_return_if_fail(CRIA_IS_BLOCK(self));

	new_format = cria_format_set(self->format, "font-size", size);
	other = cria_lepton_ref(new_format);
	g_free(new_format);

	if(other != self->format) {
		CriaLepton three = self->format;
		self->format = other;
		other = three;

		g_signal_emit(self, cria_block_signals[FORMAT_CHANGED], 0, (CriaFormatFont | CriaFormatSize));
	}

	cria_lepton_unref(other);
}

/**
 * cria_block_set_font_size_int:
 * @self: a #CriaBlock
 * @size: the font size (in MCS)
 *
 * Sets the font size in master coordinates.
 */
void
cria_block_set_font_size_int(CriaBlock* self, gint size) {
	gchar* new_size;
	g_return_if_fail(CRIA_IS_BLOCK(self));

	new_size = g_strdup_printf("%dpx", size / MASTER_COORDINATES_PER_POINT);
	cria_block_set_font_size(self, new_size);
	g_free(new_size);
}

void
cria_block_set_font_weight(CriaBlock* self, const gchar* weight) {
	static const gchar* const valid_values[] = {
		"100", "200", "300",
		"400", "500", "600",
		"700", "800", "900",
		"bold", "bolder",
		"inherit",
		"lighter", "normal"
	}; /* according to http://www.w3.org/TR/REC-CSS2/fonts.html#font-styling */
	guint      i;
	
	g_return_if_fail(CRIA_IS_BLOCK(self));
	g_return_if_fail(weight);

	for(i = 0; i < sizeof(valid_values) && strcmp(valid_values[i], weight) < 1; i++) {
		if(!strcmp(weight, valid_values[i])) {
			gchar* new_format = cria_format_set(self->format, "font-weight", valid_values[i]);
			CriaLepton new = cria_lepton_ref(new_format);

			if(new != self->format) {
				CriaLepton three = self->format;
				self->format = new;
				new = three;

				g_signal_emit(self, cria_block_signals[FORMAT_CHANGED], 0, (CriaFormatFont | CriaFormatSize));
			}
			
			cria_lepton_unref(new);
			g_free(new_format);

			return;
		}
	}

	g_critical("Unknown font-weight value: %s", weight);
}

void
cria_block_set_font_style(CriaBlock* self, const gchar* style) {
	static const gchar* const valid_values[] = {
		"inherit", "italic",
		"normal", "oblique"
	}; /* according to http://www.w3.org/TR/REC-CSS2/fonts.html#font-styling */
	guint i;

	g_return_if_fail(CRIA_IS_BLOCK(self));
	g_return_if_fail(style);

	for(i = 0; i < sizeof(valid_values) && strcmp(valid_values[i], style) < 1; i++) {
		if(!strcmp(style, valid_values[i])) {
			gchar* new_format = cria_format_set(self->format, "font-style", valid_values[i]);
			CriaLepton new = cria_lepton_ref(new_format);

			if(new != self->format) {
				CriaLepton three = self->format;
				self->format = new;
				new = three;

				g_signal_emit(self, cria_block_signals[FORMAT_CHANGED], 0, (CriaFormatFont | CriaFormatSize));
			}

			cria_lepton_unref(new);
			g_free(new_format);
			return;
		}
	}

	g_critical("Unknown font-style value: %s", style);
}

static void
cria_block_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaBlock* self;
	
	self = CRIA_BLOCK(object);
	
	switch(prop_id) {
	case PROP_ALIGNMENT_HORIZONTAL:
		cria_block_set_alignment(self, g_value_get_enum(value));
		break;
	case PROP_ALIGNMENT_VERTICAL:
		cria_block_set_valignment(self, g_value_get_enum(value));
		break;
	case PROP_COLOR:
		cria_block_set_color(self, g_value_get_pointer(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

void
cria_block_set_model(CriaBlock* self, CriaTextModel* model) {
	g_return_if_fail(CRIA_IS_BLOCK(self));
	g_return_if_fail(CRIA_IS_TEXT_MODEL(model));

	if(self->model == model) {
		return;
	}

	if(self->model) {
		g_object_unref(self->model);
	}

	self->model = g_object_ref(model);

	g_object_notify(G_OBJECT(self), "model");
}

void
cria_block_set_valignment(CriaBlock* self, CriaVAlignment valign) {
	g_return_if_fail(CRIA_IS_BLOCK(self));
	
	self->valignment = valign;

	g_object_notify(G_OBJECT(self), "valignment");
}

