/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <dom/cria-block-view.h>

#include <dom/cria-block.h>

static void
cria_block_view_class_init(gpointer iface) {
/*	GType	type = G_TYPE_FROM_INTERFACE(iface); */

	g_object_interface_install_property(iface,
					    g_param_spec_object("block",
						    		"Block",
								"The block that's being displayed in the BlockView",
								CRIA_TYPE_BLOCK,
								G_PARAM_READWRITE));
}

GType
cria_block_view_get_type(void) {
	static GType type = 0;

	if(!type) {
		static const GTypeInfo info = {
			sizeof(CriaBlockViewClass),
			NULL, /* GBaseInitFunc */
			NULL, /* GBaseFinalizeFunc */
			(GClassInitFunc)cria_block_view_class_init
		};

		type = g_type_register_static(G_TYPE_INTERFACE,
					      "CriaBlockView",
					      &info,
					      0);
	}

	return type;
}

void
_cria_block_view_install_properties(GObjectClass *klass) {
	g_object_class_override_property(klass, CRIA_BLOCK_VIEW_PROP_BLOCK, "block");
}

