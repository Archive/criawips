/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef SLIDE_VIEW_H
#define SLIDE_VIEW_H

#include <dom/cria-slide.h>
#include <dom/cria-slide-element.h>

G_BEGIN_DECLS

typedef struct _CriaSlideView CriaSlideView;
typedef struct _CriaSlideViewIface CriaSlideViewIface;

#define CRIA_TYPE_SLIDE_VIEW           (cria_slide_view_get_type())
#define CRIA_SLIDE_VIEW(obj)           (G_TYPE_CHECK_INSTANCE_CAST((obj), CRIA_TYPE_SLIDE_VIEW, CriaSlideView))
#define CRIA_IS_SLIDE_VIEW(obj)        (G_TYPE_CHECK_INSTANCE_TYPE((obj), CRIA_TYPE_SLIDE_VIEW))
#define CRIA_SLIDE_VIEW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_INTERFACE((obj), CRIA_TYPE_SLIDE_VIEW, CriaSlideViewIface))

GType	cria_slide_view_get_type		(void);

void    cria_slide_view_register                (CriaSlideView* self,
						 CriaSlide    * slide);
void    cria_slide_view_unregister              (CriaSlideView* self,
						 CriaSlide    * slide);

struct _CriaSlideViewIface {
	GTypeInterface base_interface;

	/* vtable */
	void (*element_added)      (CriaSlideView   * self,
				    CriaSlideElement* element);
	void (*element_removed)    (CriaSlideView   * self,
				    CriaSlideElement* element);
	void (*notify_background)  (CriaSlideView   * self,
				    CriaSlide       * slide,
				    CriaBackground  * background);
	void (*notify_master_slide)(CriaSlideView   * self,
				    CriaSlide       * master_slide);
	void (*notify_title)       (CriaSlideView   * self,
				    CriaSlide       * slide,
				    gchar const     * title);
};

G_END_DECLS

#endif /* SLIDE_VIEW_H */

