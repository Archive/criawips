/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <dom/cria-slide-priv.h>

#include <inttypes.h>
#include <string.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <glib-object.h>
#include <gdk/gdk.h>

#define CDEBUG_TYPE cria_slide_get_type
#include <cdebug/cdebug.h>
#include <helpers/glib-helpers.h>
#include <dom/cria-background.h>
#include <dom/cria-presentation.h>
#include <dom/cria-slide-view.h>

enum {
	PROP_0,
	PROP_BACKGROUND,
	PROP_COMMENT,
	PROP_TITLE,
	PROP_MASTER_SLIDE
};

enum {
	ELEMENT_ADDED,
	ELEMENT_REMOVED,
	N_SIGNALS
};

static void cs_init_slide_list(CriaSlideListIface* iface);
static void cs_init_slide_view(CriaSlideViewIface* iface);

G_DEFINE_TYPE_WITH_CODE(CriaSlide, cria_slide, G_TYPE_OBJECT,
			G_IMPLEMENT_INTERFACE(CRIA_TYPE_SLIDE_LIST, cs_init_slide_list);
			G_IMPLEMENT_INTERFACE(CRIA_TYPE_SLIDE_VIEW, cs_init_slide_view));

static void	  cria_slide_get_property      (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static void cria_slide_must_have_background    (CriaSlide	* self);
static void	  cria_slide_set_property      (GObject		* object,
						guint		  prop_id,
						const	GValue	* value,
						GParamSpec	* param_spec);

static guint cria_slide_signals[N_SIGNALS] = { 0 };

struct is_element{
	CriaSlideElement* element;
	gboolean          was_found;
};

static void
cs_is_element(CriaSlideElement* element, struct is_element* run) {
	run->was_found |= element == run->element;
}

static gboolean
cs_contains_element(CriaSlide* self, CriaSlideElement* element) {
	struct is_element run;
	run.element   = element;
	run.was_found = FALSE;
	g_list_foreach(self->elements, G_FUNC(cs_is_element), &run);
	return run.was_found;
}

/**
 * cria_slide_add_element:
 * @self: a #CriaSlide
 * @block: a #CriaSlideElement
 *
 * Adds an element to this slide (elements can be text blocks)
 */
void
cria_slide_add_element(CriaSlide* self, CriaSlideElement* element) {
	g_return_if_fail(CRIA_IS_SLIDE(self));
	g_return_if_fail(CRIA_IS_SLIDE_ELEMENT(element));
	g_return_if_fail(!cs_contains_element(self, element));

	cdebug("addElement()", "adding element '%s' to a list of %d elements",
				cria_slide_element_get_name(element),
				g_list_length(self->elements));
	self->elements = g_list_append(self->elements, g_object_ref(element));
	cdebug("addElement()", "new list length is %d", g_list_length(self->elements));

	g_signal_emit(self, cria_slide_signals[ELEMENT_ADDED], 0, element);
}

static void
cs_element_unref(CriaSlideElement* element, CriaSlide* self) {
	cria_slide_remove_element(self, element);
}

static void
cs_finalize(GObject* object) {
	CriaSlide* self = CRIA_SLIDE(object);

	cdebug("finalize()", "start");

	if(self->master_slide) {
		cdebug("finalize()", "master slide has %d references, removing one", G_OBJECT(self->master_slide)->ref_count);
		g_object_unref(self->master_slide);
		cria_slide_view_unregister(CRIA_SLIDE_VIEW(self), self->master_slide);
		self->master_slide = NULL;
	}

	g_list_foreach(self->elements, (GFunc)cs_element_unref, self);
	// self->elements is NULL

	g_free(self->comment);

	if(self->background) {
		cdebug("finalize()", "background has %d references, removing one", G_OBJECT(self->background)->ref_count);
		g_object_unref(self->background);
		self->background = NULL;
	}

	if(self->presentation) {
		g_object_remove_weak_pointer(G_OBJECT(self->presentation), (gpointer*)&(self->presentation));
	}

	cdebug("finalize()", "leave");
	G_OBJECT_CLASS(cria_slide_parent_class)->finalize(object);
}

static void
cria_slide_class_init(CriaSlideClass* self_class) {
	GObjectClass	* g_object_class,
			* go_class;

	/* initialize the object class */
	go_class = g_object_class = G_OBJECT_CLASS(self_class);
	g_object_class->finalize = cs_finalize;
	g_object_class->get_property = cria_slide_get_property;
	g_object_class->set_property = cria_slide_set_property;

	g_object_class_install_property(g_object_class,
					PROP_BACKGROUND,
					g_param_spec_object("background",
							    "Slide Background",
							    "The background for this slide",
							    CRIA_TYPE_BACKGROUND,
							    G_PARAM_READWRITE));
	g_object_class_install_property(g_object_class,
					PROP_COMMENT,
					g_param_spec_string("comment",
							    "Comment",
							    "Some descriptive comment for the slide, typically used for "
							    "master slides",
							    "",
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property(g_object_class,
					PROP_MASTER_SLIDE,
					g_param_spec_object("master-slide",
							    "Master Slide",
							    "A Slide that specifies drawing details",
							    CRIA_TYPE_SLIDE,
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property(g_object_class,
					PROP_TITLE,
					g_param_spec_string("title",
							    "Title",
							    "The title of that slide",
							    _("untitled"),
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

	/* initialize the CriaSlide class */
	self_class->element_added = NULL;
	cria_slide_signals[ELEMENT_ADDED] = g_signal_new("element-added", CRIA_TYPE_SLIDE,
		     G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(CriaSlideClass, element_added),
		     NULL, NULL,
		     g_cclosure_marshal_VOID__OBJECT,
		     G_TYPE_NONE,
		     1,
		     CRIA_TYPE_SLIDE_ELEMENT);
	cria_slide_signals[ELEMENT_REMOVED] = g_signal_new("element-removed", CRIA_TYPE_SLIDE,
			G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(CriaSlideClass, element_removed),
			NULL, NULL,
			g_cclosure_marshal_VOID__OBJECT,
			G_TYPE_NONE,
			1,
			CRIA_TYPE_SLIDE_ELEMENT);
}

/**
 * cria_slide_get_background:
 * @self: a #CriaSlide
 * @no_recurse: tell the function whether to recuse through master slides or not
 *
 * Get the background of a slide.
 *
 * Returns the #CriaBackground of a slide.
 */
CriaBackground*
cria_slide_get_background(CriaSlide* self, gboolean no_recurse) {
	g_return_val_if_fail(CRIA_IS_SLIDE(self), NULL);

	if(!no_recurse && !self->background && self->master_slide && cria_slide_get_background(self->master_slide, FALSE)) {
		return cria_slide_get_background(self->master_slide, FALSE);
	}

	if(no_recurse && !self->background) {
		cria_slide_must_have_background(self);
	}

	cdebugo(self, "getBackground()", "returning background 0x%p", (uintptr_t)self->background);
	return self->background;
}

CriaSlideElement*
cria_slide_get_element(CriaSlide* self, guint index) {
	g_return_val_if_fail(CRIA_IS_SLIDE(self), NULL);

	/* search for the block in here */
	return CRIA_SLIDE_ELEMENT(g_list_nth_data(self->elements, index));
}

/**
 * cria_slide_get_elements:
 * @self: the slide to get the elements from
 *
 * Get the elements of a slide.
 *
 * Returns a list of elements of the slide. Free with g_list_free()
 */
GList*
cria_slide_get_elements(CriaSlide* self) {
	g_return_val_if_fail(CRIA_IS_SLIDE(self), NULL);
	
	return g_list_copy(self->elements);
}

/**
 * cria_slide_get_master_slide:
 * @self: The slide to the get master slide from
 *
 * The master slide defines several rendering details that the slide doesn't
 * need to contain (because it's shared across all similar looking slides).
 * This method returns the master slide of a slide or NULL if the given slide
 * is a master slide.
 *
 * Returns the master slide of a slide
 */
CriaSlide*
cria_slide_get_master_slide(CriaSlide const* self) {
	g_return_val_if_fail(CRIA_IS_SLIDE(self), NULL);
	
	return self->master_slide;
}

static void
cria_slide_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaSlide	* self;

	self = CRIA_SLIDE(object);

	switch(prop_id) {
	case PROP_BACKGROUND:
		g_value_set_object(value, self->background);
		break;
	case PROP_COMMENT:
		g_value_set_string(value, self->comment);
		break;
	case PROP_MASTER_SLIDE:
		g_value_set_object(value, self->master_slide);
		break;
	case PROP_TITLE:
		g_value_set_string(value, self->title);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

/**
 * cria_slide_get_size:
 * @self: a #CriaSlide
 *
 * ...
 *
 * Returns a #GOPoint defining the size of the slide in master coordinates
 * (576dpi). Don't forget to g_free it.
 */
GOPoint*
cria_slide_get_size(CriaSlide* self) {
#warning "Slide::getSize(): FIXME return value from presentation"
	GOPoint* size = g_new0(GOPoint,1);
	size->x = 5760;
	size->y = 4320;
	return size;
}

const char*
cria_slide_get_title(CriaSlide* self) {
	g_return_val_if_fail (CRIA_IS_SLIDE(self), NULL);
	
	return self->title;
}

static void
cria_slide_init(CriaSlide* self) {
	cdebugo(self, "init()", "initializing Slide at 0x%p", (uintptr_t)self);
}

/**
 * cria_slide_new:
 * @presentation: a #CriaSlideList
 *
 * Creates a new empty slide in @presentation at the end.
 *
 * Returns the new #CriaSlide.
 */
CriaSlide*
cria_slide_new(CriaSlideList* container) {
	CriaSlide* self = cria_slide_new_pos(container, -1);
	return self;
}

/**
 * cria_slide_new_pos:
 * @presentation: a #CriaPresentation
 * @pos: a position
 *
 * Creates a new empty slide in @presentation at @pos.
 *
 * Returns the new #CriaSlide.
 */
CriaSlide*
cria_slide_new_pos(CriaSlideList* container, gint pos) {
	CriaSlide* self;

	g_return_val_if_fail(!container || CRIA_IS_SLIDE_LIST(container), NULL);
	
	self = g_object_new(CRIA_TYPE_SLIDE, NULL);

	if(container) {
		self->presentation = container;
		g_object_add_weak_pointer(G_OBJECT(container), (gpointer*)&(self->presentation));
		cria_slide_list_insert(container, self, pos);
		/* the presentation keeps a reference on the slide */
		g_object_unref(self);
	}

	return self;
}

/**
 * cria_slide_copy:
 */
CriaSlide*
cria_slide_copy(CriaSlide* slide, CriaSlideList* container, gint pos) {
	CriaSlide* self = cria_slide_new_pos(container, pos);

	/* copy properties from one slide to the other */
	return self;
}

/*
 * cria_slide_must_have_background:
 * @self: a #CriaSlide
 *
 * Ensures that @self has a valid background structure
 */
static void
cria_slide_must_have_background(CriaSlide* self) {
	g_return_if_fail(CRIA_IS_SLIDE(self));
	
	if(!self->background) {
		CriaBackground* bg = cria_background_new();
		cria_slide_set_background(self, bg);
		g_object_unref(bg);
	}
}

/**
 * cria_slide_remove_element:
 * @self: a #CriaSlide
 * @element: a #CriaSlideElement
 *
 * Removes a #CriaSlideElement from a #CriaSlide
 */
void
cria_slide_remove_element(CriaSlide* self, CriaSlideElement* element) {
	g_return_if_fail(cs_contains_element(self, element));

	self->elements = g_list_remove(self->elements, element);

	g_signal_emit(self, cria_slide_signals[ELEMENT_REMOVED], 0, element);

	cdebug("removeElement()", "%p: removing %d. reference on %p (%s)", self, G_OBJECT(element)->ref_count, element, G_OBJECT_TYPE_NAME(element));
	g_object_unref(element);
}

/**
 * cria_slide_set_background:
 * @self: the slide to set the background for
 * @background: the background to be set
 *
 * Specify a background for a slide
 */
void
cria_slide_set_background(CriaSlide* self, CriaBackground* background) {
	g_return_if_fail(CRIA_IS_SLIDE(self));
	g_return_if_fail(!background || CRIA_IS_BACKGROUND(background));

	if(background == self->background) {
		return;
	}

	if(self->background) {
		g_object_unref(self->background);
		self->background = NULL;
	}

	if(background) {
		self->background = g_object_ref(background);
	}
	
	g_object_notify(G_OBJECT(self), "background");
}

/**
 * cria_slide_set_comment:
 * @self: The slide to set a comment for
 * @comment: The comment, a plain NUL terminated UTF8 string
 *
 * Set a comment for this slide. This is usually used for master slides
 * to set some descriptive text for a layout.
 */
void
cria_slide_set_comment(CriaSlide* self, const gchar* comment) {
	g_return_if_fail(CRIA_IS_SLIDE(self));
	g_return_if_fail(comment);

	cdebugo(self, "setComment()", "start");

	if(self->comment && !strcmp(comment, self->comment)) {
		return;
	}

	if(self->comment != NULL) {
		g_free(self->comment);
	}

	self->comment = g_strdup(comment);

	g_object_notify(G_OBJECT(self), "comment");

	cdebugo(self, "setComment()", "end");
}

static void
cs_create_matching_element(CriaSlide* self, CriaSlideElement* master) {
	CriaSlideElement* element = NULL;
	// add an empty element of the same name and the same type
#warning "FIXME: make sure the name is okay"
	element =  g_object_new(G_TYPE_FROM_INSTANCE(master),
				"name", cria_slide_element_get_name(master),
				"master", master,
				NULL);
	cria_slide_add_element(self, element);
	g_object_unref(element);
}

struct matching_element {
	CriaSlideElement* searched;
	CriaSlideElement* found;
};

static void
cs_find_matching_element(CriaSlideElement* element, struct matching_element* match) {
	if(!match->found && match->searched == cria_slide_element_get_master(element)) {
		match->found = element;
	}
}

static void
cs_delete_matching_element(CriaSlide* self, CriaSlideElement* master) {
	struct matching_element match;
	match.searched = master;
	match.found    = NULL;
	g_list_foreach(self->elements, G_FUNC(cs_find_matching_element), &match);

	g_return_if_fail(match.found);
	
	if(cria_slide_element_is_empty(match.found)) {
		cria_slide_remove_element(self, match.found);
	} else {
#warning "FIXME: copy template properties into this element"
		cria_slide_element_set_master(match.found, NULL);
	}
}

/**
 * cria_slide_set_master_slide:
 * @self: the slide to set the master slide for
 * @master_slide: the slide to be set as the master slide
 *
 * Defines a slide containing common rendering details shared across several
 * slides.
 */
void
cria_slide_set_master_slide(CriaSlide* self, CriaSlide* master_slide) {
	g_return_if_fail(CRIA_IS_SLIDE(self));
	g_return_if_fail(!master_slide || CRIA_IS_SLIDE(master_slide));

	if(self->master_slide == master_slide) {
		return;
	}

	cdebug("setMasterSlide()", "setting '%s' as the master slide of '%s'",
				   cria_slide_get_title(master_slide),
				   cria_slide_get_title(self));

	if(self->master_slide) {
		cria_slide_view_unregister(CRIA_SLIDE_VIEW(self), self->master_slide);
		g_object_unref(self->master_slide);
		self->master_slide = NULL;
	}

	if(master_slide) {
		self->master_slide = g_object_ref(master_slide);
		cria_slide_view_register(CRIA_SLIDE_VIEW(self), self->master_slide);
	}

	g_object_notify(G_OBJECT(self), "master_slide");

	cdebug("setMasterSlide()", "master slide got set");
}

static void
cria_slide_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaSlide	* self = CRIA_SLIDE(object);
	
	switch(prop_id) {
	case PROP_BACKGROUND:
		cria_slide_set_background(self, g_value_get_object(value));
		break;
	case PROP_COMMENT:
		cria_slide_set_comment(self, g_value_get_string(value));
		break;
	case PROP_MASTER_SLIDE:
		cria_slide_set_master_slide(self, g_value_get_object(value));
		break;
	case PROP_TITLE:
		cria_slide_set_title(self, g_value_get_string(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

void
cria_slide_set_title(CriaSlide* self, const gchar* title) {
	g_return_if_fail (CRIA_IS_SLIDE(self));

	if (self->title != NULL) {
		g_free (self->title);
	}

	cdebugo(self, "setTitle()", "\"%s\"", title);
	self->title = g_strdup(title);

	g_object_notify(G_OBJECT(self), "title");
}

/* CriaSlideListIface */
static CriaSlide*
cs_get(CriaSlideList* container, guint num) {
	return g_list_nth_data(CRIA_SLIDE(container)->sub_slides, num);
}

static gint
cs_index(CriaSlideList* container, const CriaSlide* slide) {
	return g_list_index(CRIA_SLIDE(container)->sub_slides, slide);
}

static void
cs_insert(CriaSlideList* container, CriaSlide* slide, guint position) {
	g_list_insert(CRIA_SLIDE(container)->sub_slides, slide, position);
}

static guint
cs_n_slides(CriaSlideList* container) {
	return g_list_length(CRIA_SLIDE(container)->sub_slides);
}

static void
cs_init_slide_list(CriaSlideListIface* iface) {
	iface->get      = cs_get;
	iface->index    = cs_index;
	iface->insert   = cs_insert;
	iface->n_slides = cs_n_slides;
}

/* CriaSlideViewIface */
static void
cs_element_added(CriaSlideView* view, CriaSlideElement* element) {
	cs_create_matching_element(CRIA_SLIDE(view), element);
}

static void
cs_element_removed(CriaSlideView* view, CriaSlideElement* element) {
	cs_delete_matching_element(CRIA_SLIDE(view), element);
}

static void
cs_init_slide_view(CriaSlideViewIface* iface) {
	iface->element_added   = cs_element_added;
	iface->element_removed = cs_element_removed;
}

