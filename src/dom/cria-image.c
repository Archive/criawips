/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <dom/cria-image.h>

#include <string.h>

enum {
	PROP_0,
	PROP_URI
};

enum {
	SIGNAL,
	N_SIGNALS
};

struct _CriaImagePrivate {
	gchar	* uri;
};

struct _CriaImageClassPrivate {
};

static	void	cria_image_get_property	       (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static  void	cria_image_init		       (CriaImage	* self);
static	void	cria_image_set_property        (GObject		* object,
						guint		  prop_id,
						const	GValue	* value,
						GParamSpec	* param_spec);
static	void	cria_image_set_uri	       (CriaImage	* self,
						const gchar	* uri);

#if 0
/* enable these to add support for signals */
static	guint	cria_image_signals[N_SIGNALS] = { 0 };

static	void	cria_image_signal	       (CriaImage	* self,
						const	gchar	* string);
#endif

static void
cria_image_class_init (CriaImageClass	* cria_image_class) {
	GObjectClass	* g_object_class;

	g_object_class = G_OBJECT_CLASS(cria_image_class);
#if 0
	/* setting up signal system */
	cria_image_class->signal = cria_image_signal;

	cria_image_signals[SIGNAL] = g_signal_new (
			"signal",
			CRIA_TYPE_IMAGE,
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (
				CriaImageClass,
				signal),
			NULL,
			NULL,
			g_cclosure_marshal_VOID__STRING,
			G_TYPE_NONE,
			0);
#endif
	/* setting up property system */
	g_object_class->set_property = cria_image_set_property;
	g_object_class->get_property = cria_image_get_property;

	g_object_class_install_property(g_object_class,
					PROP_URI,
					g_param_spec_string("uri",
							    "URI",
							    "The URI of the image file",
							    NULL,
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
}

/**
 * cria_image_new:
 * @uri: a text uri
 *
 * Create a new #CriaImage poiting to the image file specified by @uri.
 *
 * Returns a new #CriaImage
 */
CriaImage*
cria_image_new(const gchar* uri) {
	CriaImage* self = NULL;

	g_return_val_if_fail(uri && strlen(uri), self);
	self = g_object_new(CRIA_TYPE_IMAGE, "uri", uri, NULL);

	return self;
}

const char*
cria_image_get_uri(CriaImage* self) {
	g_return_val_if_fail(CRIA_IS_IMAGE(self), NULL);
	g_return_val_if_fail(self->priv, NULL);
	
	return self->priv->uri;
}

static void
cria_image_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaImage	* self;

	self = CRIA_IMAGE(object);

	switch (prop_id) {
	case PROP_URI:
		g_value_set_string(value, cria_image_get_uri(self));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object,
						  prop_id,
						  param_spec);
		break;
	}
}

GType
cria_image_get_type(void) {
	static GType	type = 0;

	if(!type) {
		static const GTypeInfo info = {
			sizeof(CriaImageClass),
			NULL,	/* base initializer */
			NULL,	/* base finalizer */
			(GClassInitFunc)cria_image_class_init,
			NULL,	/* class finalizer */
			NULL,	/* class data */
			sizeof(CriaImage),
			0,
			(GInstanceInitFunc)cria_image_init,
			0
		};

		type = g_type_register_static(G_TYPE_OBJECT,
					      "CriaImage",
					      &info,
					      0);
	}

	return type;
}

static void
cria_image_init(CriaImage* self) {
	g_return_if_fail(CRIA_IS_IMAGE(self));

	self->priv = g_new0(CriaImagePrivate, 1);
}

static void
cria_image_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaImage	* self;
	
	self = CRIA_IMAGE (object);
	
	switch (prop_id) {
	case PROP_URI:
		cria_image_set_uri(self, g_value_get_string(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object,
						  prop_id,
						  param_spec);
		break;
	}
}

static void
cria_image_set_uri(CriaImage* self, const gchar* uri) {
	g_return_if_fail(CRIA_IS_IMAGE(self));
	g_return_if_fail(uri);

	self->priv->uri = g_strdup(uri);

	g_object_notify(G_OBJECT(self), "uri");
}

