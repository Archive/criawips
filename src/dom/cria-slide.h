/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIAWIPS_SLIDE_H
#define CRIAWIPS_SLIDE_H

#include <glib-object.h>
#include <goffice/utils/go-units.h>
#include <gdk/gdk.h>

#include <dom/cria-background.h>
#include <dom/cria-image.h>

G_BEGIN_DECLS

typedef struct _CriaSlide		CriaSlide;
typedef struct _CriaSlideClass		CriaSlideClass;

G_END_DECLS

#include <dom/cria-slide-element.h>
#include <dom/cria-slide-list.h>

G_BEGIN_DECLS

#define CRIA_TYPE_SLIDE			(cria_slide_get_type ())
#define CRIA_SLIDE(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_SLIDE, CriaSlide))
#define CRIA_SLIDE_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_SLIDE, CriaSlideClass))
#define CRIA_IS_SLIDE(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_SLIDE))
#define CRIA_IS_SLIDE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_SLIDE))
#define CRIA_SLIDE_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_SLIDE, CriaSlideClass))

GType		cria_slide_get_type	       (void);

void		cria_slide_add_element	       (CriaSlide	* self,
						CriaSlideElement* element);
#warning "call cria_slide_new_copy"
CriaSlide*	cria_slide_copy		       (CriaSlide	* slide,
						CriaSlideList* container,
						gint		  pos);
CriaBackground*	cria_slide_get_background      (CriaSlide	* self,
						gboolean	  no_recurse);
CriaSlideElement*cria_slide_get_element        (CriaSlide	* self,
						guint             index);
GList*          cria_slide_get_elements        (CriaSlide       * self);
GOPoint*	cria_slide_get_display_size    (CriaSlide	* self,
						GdkScreen	* screen);
CriaSlide*	cria_slide_get_master_slide    (CriaSlide const	* self);
GOPoint*	cria_slide_get_size	       (CriaSlide	* self);
const gchar*	cria_slide_get_title	       (CriaSlide	* self);
CriaSlide*	cria_slide_new		       (CriaSlideList* container);
CriaSlide*	cria_slide_new_pos	       (CriaSlideList* container,
						gint		  pos);
void            cria_slide_remove_element      (CriaSlide       * self,
						CriaSlideElement* element);
void		cria_slide_set_background      (CriaSlide	* self,
						CriaBackground	* background);
void		cria_slide_set_comment	       (CriaSlide	* self,
						const char	* comment);
void		cria_slide_set_master_slide    (CriaSlide	* self,
						CriaSlide	* master_slide);
void		cria_slide_set_title	       (CriaSlide	* self,
						const char	* title);

G_END_DECLS

#endif /* !CRIAWIPS_SLIDE_H */
