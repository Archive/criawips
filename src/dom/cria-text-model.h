/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIAWIPS_TEXT_MODEL_H
#define CRIAWIPS_TEXT_MODEL_H

#include <glib-object.h>

G_BEGIN_DECLS

typedef struct _CriaTextModel CriaTextModel;
typedef struct _GObjectClass CriaTextModelClass;

#define CRIA_TYPE_TEXT_MODEL         (cria_text_model_get_type())
#define CRIA_TEXT_MODEL(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), CRIA_TYPE_TEXT_MODEL, CriaTextModel))
#define CRIA_TEXT_MODEL_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), CRIA_TYPE_TEXT_MODEL, CriaTextModelClass))
#define CRIA_IS_TEXT_MODEL(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), CRIA_TYPE_TEXT_MODEL))
#define CRIA_IS_TEXT_MODEL_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), CRIA_TYPE_TEXT_MODEL))
#define CRIA_TEXT_MODEL_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), CRIA_TYPE_TEXT_MODEL, CriaTextModelClass))

GType cria_text_model_get_type(void);

void         cria_text_model_append  (CriaTextModel* self,
				      gchar const  * text);
void         cria_text_model_delete  (CriaTextModel      * self,
				      gsize                cursor,
				      gsize                length);
gchar const* cria_text_model_get_text(CriaTextModel const* self);
void         cria_text_model_insert  (CriaTextModel      * self,
				      gchar const        * text,
				      gsize                position);
void         cria_text_model_set_text(CriaTextModel      * self,
				      gchar const        * text);

CriaTextModel* cria_text_model_new   (void);
CriaTextModel* cria_text_model_clone (CriaTextModel* self);

G_END_DECLS

#endif /* CRIAWIPS_TEXT_MODEL_H */

