/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <dom/cria-slide-element-priv.h>

#include <string.h>

#define CDEBUG_TYPE cria_slide_element_get_type
#include <cdebug/cdebug.h>

struct CriaSlideElementPrivate {
	GORect* position;
};
#define P(i) G_TYPE_INSTANCE_GET_PRIVATE((i), CRIA_TYPE_SLIDE_ELEMENT, struct CriaSlideElementPrivate)

/**
 * cria_slide_element_get_master:
 * @self: a #CriaSlideElement
 *
 * Get the master element for @self. The master element is the element which is
 * part of the master slide and thus some kind of a template for this element.
 *
 * Returns the master element for a #CriaSlideElement.
 */
CriaSlideElement*
cria_slide_element_get_master(CriaSlideElement const* self) {
	g_return_val_if_fail(CRIA_IS_SLIDE_ELEMENT(self), NULL);

	return self->master;
}

/**
 * cria_slide_element_get_name:
 * @self: a #CriaSlideElement
 *
 * Gets the name of a #CriaSlideElement. The name is especially important for
 * the serialization of objects. A slide cannot contain more than one element
 * with the same name.
 */
gchar const*
cria_slide_element_get_name(CriaSlideElement const* self) {
	g_return_val_if_fail(CRIA_IS_SLIDE_ELEMENT(self), NULL);
	
	return self->name;
}

/**
 * cria_slide_element_get_position:
 * @self: a #CriaSlideElement
 *
 * Get the position of a #CriaSlideElement.
 *
 * Returns the block's position. May return NULL if unset.
 */
GORect const*
cria_slide_element_get_position(CriaSlideElement const* self) {
	g_return_val_if_fail(CRIA_IS_SLIDE_ELEMENT(self), NULL);

	return P(self)->position;
}

static void
cria_slide_element_set_pos_bottom(CriaSlideElement* self, GODistance bottom) {
	g_return_if_fail(CRIA_IS_SLIDE_ELEMENT(self));

	cdebugo(self, "setPosBottom()", "%lli", bottom);
	
	if(P(self)->position == NULL) {
		P(self)->position = g_new0(GORect,1);
	}

	if(bottom != P(self)->position->bottom) {
		P(self)->position->bottom = bottom;

		g_object_notify(G_OBJECT(self), "bottom");
	}
}

static void
cria_slide_element_set_pos_left(CriaSlideElement* self, GODistance left) {
	g_return_if_fail(CRIA_IS_SLIDE_ELEMENT(self));
	
	cdebugo(self, "setPosLeft()", "%lli", left);
	
	if(P(self)->position == NULL) {
		P(self)->position = g_new0(GORect,1);
	}

	if(left != P(self)->position->left) {
		P(self)->position->left = left;

		g_object_notify(G_OBJECT(self), "left");
	}
}

static void
cria_slide_element_set_pos_right(CriaSlideElement* self, GODistance right) {
	g_return_if_fail(CRIA_IS_SLIDE_ELEMENT(self));
	
	cdebugo(self, "setPosRight()", "%lli", right);
	
	if(P(self)->position == NULL) {
		P(self)->position = g_new0(GORect,1);
	}

	if(right != P(self)->position->right) {
		P(self)->position->right = right;

		g_object_notify(G_OBJECT(self), "right");
	}
}

static void
cria_slide_element_set_pos_top(CriaSlideElement* self, GODistance top) {
	g_return_if_fail(CRIA_IS_SLIDE_ELEMENT(self));
	
	cdebugo(self, "setPosTop()", "%lli", top);
	
	if(P(self)->position == NULL) {
		P(self)->position = g_new0(GORect,1);
	}

	if(top != P(self)->position->top) {
		P(self)->position->top = top;

		g_object_notify(G_OBJECT(self), "top");
	}
}

/**
 * cria_slide_element_set_position:
 * @self: a #CriaSlideElement
 * @x1: a horizontal position
 * @y1: a vertical position
 * @x2: a horizontal position
 * @y2: a vertical position
 *
 * Set the position of a block.
 */
void
cria_slide_element_set_position(CriaSlideElement* self, gdouble x1, gdouble y1, gdouble x2, gdouble y2) {
	cria_slide_element_set_pos_left(self, MIN(x1, x2));
	cria_slide_element_set_pos_right(self, MAX(x1, x2));
	
	cria_slide_element_set_pos_top(self, MIN(y1, y2));
	cria_slide_element_set_pos_bottom(self, MAX(y1, y2));
}

/**
 * cria_slide_element_is_empty:
 * @self: a #CriaSlideElement
 *
 * Check whether an element is empty or not.
 *
 * Returns TRUE if the element is empty, FALSE if not.
 */
gboolean
cria_slide_element_is_empty(CriaSlideElement const* self) {
	g_return_val_if_fail(CRIA_SLIDE_ELEMENT_GET_CLASS(self)->is_empty, FALSE);
	return CRIA_SLIDE_ELEMENT_GET_CLASS(self)->is_empty(self);
}

/**
 * cria_slide_element_set_master:
 * @self: a #CriaSlideElement
 * @master: another #CriaSlideElement
 *
 * Sets the master element for a slide element. The master ist used as a
 * template for #CriaSlideElements.
 */
void
cria_slide_element_set_master(CriaSlideElement* self, CriaSlideElement* master) {
	g_return_if_fail(CRIA_IS_SLIDE_ELEMENT(self));
	g_return_if_fail(self != master);
	g_return_if_fail(!master || CRIA_IS_SLIDE_ELEMENT(master));
	g_return_if_fail(!master || G_OBJECT_TYPE(self) == G_OBJECT_TYPE(master));

	if(self->master == master) {
		return;
	}

	if(self->master) {
		/* disconnect from master; must be done via vfunc */
		g_object_unref(self->master);
		self->master = NULL;
	}

	if(master) {
		self->master = g_object_ref(master);
		/* connect to master; must be done via vfunc */
	}

	g_object_notify(G_OBJECT(self), "master");
}

/*
 * cria_slide_element_set_name:
 * @self: a #CriaSlideElement
 * @name: a name for this element
 *
 * Specify the name of a #CriaSlideElement.
 */
static void
cria_slide_element_set_name(CriaSlideElement* self, gchar const* name) {
	g_return_if_fail(CRIA_IS_SLIDE_ELEMENT(self));
	g_return_if_fail(name && *name);

	if(self->name && !strcmp(name, self->name)) {
		return;
	}

	g_free(self->name);
	self->name = g_strdup(name);

	g_object_notify(G_OBJECT(self), "name");
}

/* GType */
G_DEFINE_TYPE(CriaSlideElement, cria_slide_element, G_TYPE_OBJECT);

enum {
	PROP_0,
	PROP_MASTER,
	PROP_NAME,
	PROP_POS_LEFT,
	PROP_POS_RIGHT,
	PROP_POS_TOP,
	PROP_POS_BOTTOM
};

static void
cria_slide_element_init(CriaSlideElement* self){}

static void
cse_dispose(GObject* object) {
	CriaSlideElement* self = CRIA_SLIDE_ELEMENT(object);
	
	if(self->disposed) {
		return;
	}
	self->disposed = TRUE;

	if(self->master) {
		cdebug("dispose()", "%p: removing %d. reference on %p (%s)", self, G_OBJECT(self->master)->ref_count, self->master, G_OBJECT_TYPE_NAME(self->master));
		g_object_unref(self->master);
		self->master = NULL;
	}

	G_OBJECT_CLASS(cria_slide_element_parent_class)->dispose(object);
}

static void
cse_finalize(GObject* object) {
	CriaSlideElement* self = CRIA_SLIDE_ELEMENT(object);

	g_free(self->name);
	self->name = NULL;

	G_OBJECT_CLASS(cria_slide_element_parent_class)->finalize(object);
}

static void
cse_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
	CriaSlideElement* self = CRIA_SLIDE_ELEMENT(object);

	switch(prop_id) {
	case PROP_MASTER:
		g_value_set_object(value, cria_slide_element_get_master(self));
		break;
	case PROP_NAME:
		g_value_set_string(value, cria_slide_element_get_name(self));
		break;
	case PROP_POS_LEFT:
		g_value_set_uint64(value, P(self)->position->left);
		break;
	case PROP_POS_RIGHT:
		g_value_set_uint64(value, P(self)->position->right);
		break;
	case PROP_POS_TOP:
		g_value_set_uint64(value, P(self)->position->top);
		break;
	case PROP_POS_BOTTOM:
		g_value_set_uint64(value, P(self)->position->bottom);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void
cse_set_property(GObject* object, guint prop_id, GValue const* value, GParamSpec* pspec) {
	CriaSlideElement* self = CRIA_SLIDE_ELEMENT(object);

	switch(prop_id) {
	case PROP_MASTER:
		cria_slide_element_set_master(self, g_value_get_object(value));
		break;
	case PROP_NAME:
		cria_slide_element_set_name(self, g_value_get_string(value));
		break;
	case PROP_POS_LEFT:
		cria_slide_element_set_pos_left(self, g_value_get_uint64(value));
		break;
	case PROP_POS_RIGHT:
		cria_slide_element_set_pos_right(self, g_value_get_uint64(value));
		break;
	case PROP_POS_TOP:
		cria_slide_element_set_pos_top(self, g_value_get_uint64(value));
		break;
	case PROP_POS_BOTTOM:
		cria_slide_element_set_pos_bottom(self, g_value_get_uint64(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void
cria_slide_element_class_init(CriaSlideElementClass* self_class) {
	GObjectClass* go_class;

	/* setting up GObjectClass */
	go_class = G_OBJECT_CLASS(self_class);
	go_class->dispose      = cse_dispose;
	go_class->finalize     = cse_finalize;
	go_class->get_property = cse_get_property;
	go_class->set_property = cse_set_property;
	
	g_object_class_install_property(go_class,
					PROP_MASTER,
					g_param_spec_object("master",
							    "master element",
							    "The master element for this one. It serves "
							    "as a template.",
							    CRIA_TYPE_SLIDE_ELEMENT,
							    G_PARAM_READWRITE));
	g_object_class_install_property(go_class,
					PROP_NAME,
					g_param_spec_string("name",
							    "Name",
							    "The name of the element",
							    NULL,
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property(go_class,
					PROP_POS_BOTTOM,
					g_param_spec_uint64("bottom",
							    "Bottom",
							    "The width of the block",
							    0,
							    G_MAXUINT64,
							    0,
							    G_PARAM_READWRITE));
	g_object_class_install_property(go_class,
					PROP_POS_LEFT,
					g_param_spec_uint64("left",
							    "Left",
							    "The Position of the topleft corner of the layout box",
							    0,
							    G_MAXUINT64,
							    0,
							    G_PARAM_READWRITE));
	g_object_class_install_property(go_class,
					PROP_POS_RIGHT,
					g_param_spec_uint64("right",
							    "Right",
							    "The height of the block",
							    0,
							    G_MAXUINT64,
							    0,
							    G_PARAM_READWRITE));
	g_object_class_install_property(go_class,
					PROP_POS_TOP,
					g_param_spec_uint64("top",
							    "Top",
							    "The vertical position of the block",
							    0,
							    G_MAXUINT64,
							    0,
							    G_PARAM_READWRITE));

	/* CriaSlideElementClass */
	g_type_class_add_private(self_class, sizeof(struct CriaSlideElementPrivate));
}

