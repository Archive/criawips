/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIAWIPS_BLOCK_H
#define CRIAWIPS_BLOCK_H

#include <goffice/utils/goffice-utils.h>
#include <utils/cria-lepton.h>

#include <dom/cria-alignment.h>
#include <dom/cria-text-model.h>

G_BEGIN_DECLS

typedef struct _CriaBlock CriaBlock;
typedef struct _CriaBlockClass CriaBlockClass;

G_END_DECLS

#include <dom/cria-slide.h>

G_BEGIN_DECLS

#define CRIA_TYPE_BLOCK			(cria_block_get_type ())
#define CRIA_BLOCK(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_BLOCK, CriaBlock))
#define CRIA_BLOCK_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_BLOCK, CriaBlockClass))
#define CRIA_IS_BLOCK(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_BLOCK))
#define CRIA_IS_BLOCK_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_BLOCK))
#define CRIA_BLOCK_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_BLOCK, CriaBlockClass))

GType		cria_block_get_type	       (void);

CriaAlignment	cria_block_get_alignment       (CriaBlock const	* self);
GOColor const*	cria_block_get_color	       (CriaBlock const	* self);
CriaLepton      cria_block_get_format          (CriaBlock const	* self);
CriaTextModel*	cria_block_get_model	       (CriaBlock const	* self);
CriaVAlignment	cria_block_get_valignment      (CriaBlock const	* self);
CriaBlock*	cria_block_new		       (const gchar	* name);
void		cria_block_set_alignment       (CriaBlock	* self,
						CriaAlignment	  alignment);
void		cria_block_set_color	       (CriaBlock	* self,
						GOColor const	* color);
void		cria_block_set_font_family     (CriaBlock	* self,
						const gchar	* family);
void		cria_block_set_font_size       (CriaBlock	* self,
						const gchar	* size);
void		cria_block_set_font_size_int   (CriaBlock	* self,
						gint		  size);
void	        cria_block_set_font_style      (CriaBlock	* self,
						const gchar	* style);
void            cria_block_set_font_weight     (CriaBlock       * self,
						const gchar     * weight);
void		cria_block_set_model	       (CriaBlock	* self,
						CriaTextModel   * model);
void		cria_block_set_valignment      (CriaBlock	* self,
						CriaVAlignment	  valignment);

G_END_DECLS

#endif /* !CRIAWIPS_BLOCK_H */

