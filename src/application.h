/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005,2006 Sven Herzberg
 * Copyright (C) 2004 Keith Sharp
 * Copyright (C) 2004 Adrien Beaucreux
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef APPLICATION_H
#define APPLICATION_H

#include <gconf/gconf-client.h>

#include <goffice/utils/go-font.h>

#include <widgets/cria-main-window.h>

G_BEGIN_DECLS

typedef struct _CriaApplication CriaApplication;
typedef struct _CriaApplicationClass CriaApplicationClass;
typedef struct _CriaApplicationPriv CriaApplicationPriv;

void		  criawips_init			       (int		* argc,
							char	       ***argv);
void              criawips_shutdown                    (void);
void		  criawips_quit			       (void);

gchar*		  cria_application_get_default_geometry(void);
gboolean	  cria_application_get_detach_menubars (void);
gboolean	  cria_application_get_detach_toolbars (void);
gboolean	  cria_application_get_disable_screensaver(void);
CriaApplication	* cria_application_get_instance	       (void);
gboolean	  cria_application_get_page_down_creates_slide(void);
gboolean	  cria_application_get_show_sidebar    (void);
gboolean	  cria_application_get_show_tearoff    (void);
gint		  cria_application_get_sidebar_width   (void);
gboolean          cria_application_get_templates_visible(void);
gint              cria_application_get_templates_width (void);
const gchar	* cria_application_get_zoom	       (void);
GType		  cria_application_get_type	       (void);
void		  cria_application_register_window     (CriaMainWindow	* window);
void		  cria_application_unregister_window   (CriaMainWindow	* window);
void		  cria_application_set_default_geometry(const gchar	* geometry);
void		  cria_application_set_disable_screensaver(gboolean       disable_screensaver);
void		  cria_application_set_page_down_creates_slide(gboolean   pdcs);
void		  cria_application_set_show_sidebar    (gboolean	  ssb);
void		  cria_application_set_sidebar_width   (gint		  sidebar_width);
void              cria_application_set_templates_visible(gboolean         templates_width);
void              cria_application_set_templates_width (gint              templates_width);
void		  cria_application_set_zoom	       (const gchar	* zoom);
void		  cria_application_show_error_dialog   (GtkWindow	* parent,
							gchar const	* primary,
							gchar const	* secondary);
PangoFontDescription * cria_application_get_default_font(void);
void                   cria_application_set_default_font_name(const gchar* font);

#define CRIA_TYPE_APPLICATION			(cria_application_get_type ())
#define CRIA_APPLICATION(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_APPLICATION, CriaApplication))
#define CRIA_APPLICATION_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_APPLICATION, CriaApplicationClass))
#define CRIA_IS_APPLICATION(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_APPLICATION))
#define CRIA_IS_APPLICATION_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_APPLICATION))
#define CRIA_APPLICATION_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_APPLICATION, CriaApplicationClass))

struct _CriaApplication {
	GObject			  base_instance;
	CriaApplicationPriv	* priv;
};

struct _CriaApplicationClass {
	GObjectClass		  base_class;
	void (*changed_default_font) (void);
};

G_END_DECLS

#endif /* APPLICATION_H */

