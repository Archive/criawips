/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gsf-pixbuf.h"

#include <gsf/gsf-input.h>
#include <gsf-gnome/gsf-input-gnomevfs.h>

static GdkPixbuf*
new_from_input(GsfInput* input, GError** error) {
	GdkPixbufLoader	* loader;
	const guint8	* data;
	gsf_off_t	  length;
	GdkPixbuf	* retval;

	length = gsf_input_remaining(input);
	data = gsf_input_read(input, length, NULL);

	loader = gdk_pixbuf_loader_new();
	gdk_pixbuf_loader_write(loader, data, length, error);
	g_return_val_if_fail(*error == NULL, NULL);

	retval = gdk_pixbuf_loader_get_pixbuf(loader);
	g_object_ref(retval);
	g_return_val_if_fail(retval != NULL, NULL);

	gdk_pixbuf_loader_close(loader, error);
	g_object_unref(loader);

	return retval;
}

GdkPixbuf*
gdk_pixbuf_new_from_uri(GnomeVFSURI* uri, GError** error) {
	GsfInput	* input;
	GdkPixbuf	* retval;

	g_return_val_if_fail(*error == NULL, NULL);
	g_return_val_if_fail(uri != NULL/* && GNOME_VFS_IS_URI(uri)*/, NULL);
	
	input = gsf_input_gnomevfs_new_uri(uri, error);
	g_return_val_if_fail(*error == NULL, NULL);
	
	retval = new_from_input(input, error);
	if(*error) {
		if(retval) {
			g_object_unref(retval);
			retval = NULL;
		}
	}

	g_object_unref(input);
	return retval;
}

