/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005,2006 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <rendering/cria-block-renderer-priv.h>

#include <inttypes.h>
#include <string.h>
#include <glib/gi18n.h>
#include <gtk/gtkstock.h>
#include <canvas/rd-line.h>
#include <canvas/rd-rectangle.h>
#include <libgnomecanvas/libgnomecanvas.h>

#include <helpers/gtk-helpers.h>

#define CDEBUG_TYPE cria_block_renderer_get_type
#include <cdebug/cdebug.h>

#include <utils/cria-units.h>
#include <dom/cria-format.h>
#include <dom/cria-block-view.h>
#include <rendering/cria-canvas-text-view.h>
#include <rendering/cria-format.h>
#include <rendering/cria-slide-renderer.h>

#include "application.h"

enum {
	PROP_0,
};

enum {
	SIGNAL,
	N_SIGNALS
};

#define MSG_CLICK_HERE _("Click here to add text...")

G_DEFINE_TYPE_WITH_CODE(CriaBlockRenderer, cria_block_renderer, RD_TYPE_ITEM,
			G_IMPLEMENT_INTERFACE(CRIA_TYPE_BLOCK_VIEW, NULL))

static	void	cria_block_renderer_get_property(GObject		* object,
						 guint		  prop_id,
						 GValue		* value,
						 GParamSpec	* param_spec);
static	void	cria_block_renderer_set_property(GObject		* object,
						 guint		  prop_id,
						 const GValue	* value,
						 GParamSpec	* param_spec);
static void cbr_update_position(CriaBlockRenderer* self);

static void cbr_update_cursor_position(CriaBlockRenderer* self);

#if 0
/* enable these to add support for signals */
static	guint	cria_block_renderer_signals[N_SIGNALS] = { 0 };

static	void	cria_block_renderer_signal	       (CriaBlockRenderer	* self,
						const	gchar	* string);
#endif

static gboolean
cbr_button_press_event (RdItem        * self,
			GdkEventButton* ev)
{
	cria_item_grab_focus (self);
	return TRUE;
}

static void
cbr_ensure_caret(CriaBlockRenderer* self) {
	g_return_if_fail(CRIA_IS_BLOCK_RENDERER(self));
	
	if(!self->caret && RD_ITEM_IS_INTERACTIVE(self)) {
		self->caret = rd_line_new (RD_ITEM (self));
		g_object_set (self->caret,
			      "fill-color", "black",
			      NULL);
		self->caret_visible = TRUE;
		rd_item_raise_to_top (self->caret);
		cbr_update_cursor_position(self);
	}
}

static gboolean
toggle_caret(CriaBlockRenderer* self) {
	if(self->caret_visible) {
		rd_item_hide (self->caret);
	} else {
		cbr_ensure_caret(self);
		rd_item_show (self->caret);
	}

	self->caret_visible = !self->caret_visible;
	gtk_main_flush();

	return TRUE;
}

static void
cbr_disable_caret_timeout (CriaBlockRenderer* self)
{
	if(self->caret_timeout) {
		g_source_remove(self->caret_timeout);
		self->caret_timeout = 0;
	}

	if(self->caret) {
		rd_item_hide (self->caret);
		self->caret_visible = FALSE;
	}
}

static void
cbr_reset_caret(CriaBlockRenderer* self) {
	if(G_UNLIKELY(!self->block)) {
		return;
	}

	if(!RD_ITEM_IS_INTERACTIVE(self)) {
		cdebug("resetCaret()", "Not resetting the caret.");
		return;
	}
	
	cbr_ensure_caret(self);
	cbr_disable_caret_timeout(self);

	if(self->blink) {
		self->caret_timeout = g_timeout_add(self->blink_time,
					            G_SOURCE_FUNC(toggle_caret),
					            self);
	}

	rd_item_show (self->caret);
	self->caret_visible = TRUE;
	
	gtk_main_flush();
}

static void
cbr_ensure_text_item(CriaBlockRenderer* self) {
	if(!self->text_item) {
		self->text_item = cria_canvas_text_view_new (RD_ITEM (self));
		g_object_set (self->text_item,
			      "clip", TRUE,
			      "size-set", TRUE,
			      NULL);
		g_signal_connect_swapped(self->text_item, "notify::text",
					 G_CALLBACK(cbr_update_position), self);
		g_signal_connect_swapped(self->text_item, "notify::cursor",
					 G_CALLBACK(cbr_update_cursor_position), self);
	}
}

#define GET_RECURSIVE_FROM_BLOCK_FUNC(type, property, _RETURN_DEFAULT_) \
static type \
cbr_get_##property(CriaBlockRenderer const* self) {\
	type  property = _RETURN_DEFAULT_;\
	CriaBlock const* block = NULL;\
	\
	g_return_val_if_fail(CRIA_IS_BLOCK_RENDERER(self), property);\
	\
	block = cria_block_renderer_get_block(self);\
	g_return_val_if_fail(CRIA_IS_BLOCK(block), property);\
	property = cria_block_get_##property(block);\
	if(property == _RETURN_DEFAULT_) {\
		/* return from one of the templates */\
		CriaBlock const* template    = CRIA_BLOCK(cria_slide_element_get_master(CRIA_SLIDE_ELEMENT(block)));\
		\
		cdebug("cbr_get_" G_STRINGIFY(property) "()", "getting value from template");\
		\
		for(; CRIA_IS_BLOCK(template); template = CRIA_BLOCK(cria_slide_element_get_master(CRIA_SLIDE_ELEMENT(template)))) {\
			property = cria_block_get_##property(template);\
			if(property != _RETURN_DEFAULT_) {\
				cdebug("cbr_get_" G_STRINGIFY(property) "()", "got %d from template", property);\
				break;\
			}\
		}\
	}\
	\
	return property;\
}

#define GET_RECURSIVE_FROM_BLOCK_FUNC_PTR(type, property, _RETURN_DEFAULT_) \
static type \
cbr_get_##property(CriaBlockRenderer const* self) {\
	type  property = _RETURN_DEFAULT_;\
	type const* property##_ptr = NULL;\
	CriaBlock const* block = NULL;\
	\
	g_return_val_if_fail(CRIA_IS_BLOCK_RENDERER(self), property);\
	\
	block = cria_block_renderer_get_block(self);\
	g_return_val_if_fail(CRIA_IS_BLOCK(block), property);\
	property##_ptr = cria_block_get_##property(block);\
	if(property##_ptr) {\
		property = *property##_ptr;\
	} else {\
		/* return from one of the templates */\
		CriaBlock const* template    = CRIA_BLOCK(cria_slide_element_get_master(CRIA_SLIDE_ELEMENT(block)));\
		\
		for(; CRIA_IS_BLOCK(template); template = CRIA_BLOCK(cria_slide_element_get_master(CRIA_SLIDE_ELEMENT(template)))) {\
			property##_ptr = cria_block_get_##property(template);\
			if(property##_ptr) {\
				property = *property##_ptr;\
				break;\
			}\
		}\
	}\
	return property;\
}

/*
 * cbr_get_alignment:
 * @self: a #CriaBlockRenderer
 *
 * Get the alignment value that should be used for display. As this calculation
 * needs some inheritance magic, it's been split out to an own function.
 *
 * Returns the alignment of the block or the first template that sets it;
 * #CRIA_ALIGNMENT_UNSET if none was set.
 */
GET_RECURSIVE_FROM_BLOCK_FUNC(CriaAlignment, alignment, CRIA_ALIGNMENT_UNSET);

/*
 * cbr_get_valignment:
 * @self: a #CriaBlockRenderer
 *
 * Get the vertical alignment value that should be used for display. As this
 * calculation needs some inheritance magic, it's been split out to an own
 * function.
 *
 * Returns the vertical alignment of the block or the first template that sets
 * it; #CRIA_VALIGNMENT_UNSET if none was set.
 */
GET_RECURSIVE_FROM_BLOCK_FUNC(CriaVAlignment, valignment, CRIA_VALIGNMENT_UNSET);

/*
 * cbr_get_color:
 * @self: a #CriaBlockRenderer
 *
 * Get the foreground color of the rendered block.
 *
 * Returns the foreground color of the rendered block (even if specified in a
 * template).
 */
GET_RECURSIVE_FROM_BLOCK_FUNC_PTR(GOColor, color, RGBA_BLACK);

/*
 * cbr_get_format:
 * @self: a #CriaBlockRenderer
 *
 * Get the format of the rendered block.
 *
 * Returns the format string of the rendered block (even if specified in a
 * template).
 */
GET_RECURSIVE_FROM_BLOCK_FUNC(CriaLepton, format, 0);

/*
 * cbr_get_model:
 * @self: a #CriaBlockRenderer
 *
 * Get the model of the rendered block.
 *
 * Returns the model of the rendered block (even if specified in a template).
 */
GET_RECURSIVE_FROM_BLOCK_FUNC(CriaTextModel*, model, NULL);

/*
 * cbr_get_position:
 * @self: a #CriaBlockRenderer
 *
 * Get the position of the rendered block.
 *
 * Returns the position of the rendered block (even if it's specified in one of
 * its templates).
 */
static GORect const default_rect = {0l, 0l, 0l, 0l};
static GORect 
cbr_get_position(CriaBlockRenderer const* self) {
	GORect position = default_rect;
	GORect const* position_ptr = NULL;
	CriaBlock const* block = NULL;
	
	g_return_val_if_fail(CRIA_IS_BLOCK_RENDERER(self), position);
	
	block = cria_block_renderer_get_block(self);
	g_return_val_if_fail(CRIA_IS_SLIDE_ELEMENT(block), position);
	position_ptr = cria_slide_element_get_position(block);
	if(position_ptr) {
		position = *position_ptr;
	} else {
		/* return from one of the templates */
		CriaBlock const* template    = CRIA_BLOCK(cria_slide_element_get_master(CRIA_SLIDE_ELEMENT(block)));
		
		for(; CRIA_IS_BLOCK(template); template = CRIA_BLOCK(cria_slide_element_get_master(CRIA_SLIDE_ELEMENT(template)))) {
			position_ptr = cria_slide_element_get_position(template);
			if(position_ptr) {
				position = *position_ptr;
				break;
			}
		}
	}
	return position;
}

static void
cbr_update_cursor_position(CriaBlockRenderer* self) {
	gint               cursor, trail;
	PangoRectangle     r;
	GnomeCanvasPoints* p = gnome_canvas_points_new(2);
	gdouble            coords[2];
	gdouble            zoom_times_pango_scale;

	if(!RD_ITEM_IS_INTERACTIVE(self)) {
		return;
	}

	cbr_ensure_caret(self);
	cbr_ensure_text_item(self);
	
	g_object_get(self->text_item, "cursor", &cursor, "cursor-trail", &trail, NULL);
	cdebug("updateCursorPosition()", "current cursor position is {%d+%d}", cursor, trail);
	pango_layout_get_cursor_pos (rd_text_get_layout (RD_TEXT (self->text_item)),
				     cursor + trail,
				     &r, NULL);
	g_object_get(self->text_item,
		     "x", &(p->coords[0]),
		     "y", &(p->coords[1]),
		     "text-width", &(coords[0]),
		     "text-height", &(coords[1]),
		     NULL);

	zoom_times_pango_scale =
		rd_canvas_get_zoom (rd_item_get_canvas (RD_ITEM (self))) * PANGO_SCALE;

	p->coords[0] = p->coords[2] =
		r.x / zoom_times_pango_scale + p->coords[0] /*- 0.5 * coords[2]*/;
	p->coords[1] = r.y / zoom_times_pango_scale + p->coords[1];
	p->coords[3] = p->coords[1] + (r.height / zoom_times_pango_scale);

	switch(cbr_get_alignment(self)) {
	case CRIA_ALIGNMENT_RIGHT:
		p->coords[0] -= coords[0];
		p->coords[2] -= coords[0];
		break;
	case CRIA_ALIGNMENT_CENTER:
		p->coords[0] -= 0.5 * coords[0];
		p->coords[2] -= 0.5 * coords[0];
		break;
	case CRIA_ALIGNMENT_LEFT:
	case CRIA_ALIGNMENT_JUSTIFY:
	case CRIA_ALIGNMENT_UNSET:
		/* unset, left and justify need no modifications */
#warning "FIXME: check whether we should follow the gtk text direction in the unset case"
		break;
	default:
		g_warning("got a value for CriaAlignment that cannot be understood: %d", cbr_get_alignment(self));
		break;
	}

	switch(cbr_get_valignment(self)) {
	case CRIA_ALIGNMENT_BOTTOM:
		p->coords[1] -= coords[1];
		p->coords[3] -= coords[1];
		break;
	case CRIA_ALIGNMENT_MIDDLE:
		p->coords[1] -= 0.5 * coords[1];
		p->coords[3] -= 0.5 * coords[1];
		break;
	case CRIA_ALIGNMENT_TOP:
	case CRIA_VALIGNMENT_UNSET:
		/* unset and top don't need modification */
		break;
	default:
		g_warning("got a value for CriaVAlignment that cannot be understood: %d", cbr_get_valignment(self));
		break;
	}

	g_object_set(self->caret, "points", p, NULL);
	cbr_reset_caret(self);
}

static void
cbr_border_unfocused (CriaBlockRenderer* self)
{
	g_return_if_fail(CRIA_IS_BLOCK_RENDERER(self));
	g_return_if_fail(RD_IS_ITEM(self));
	g_return_if_fail(rd_item_is_constructed (RD_ITEM (self)));
	g_return_if_fail(RD_IS_RECTANGLE (self->border));
	g_return_if_fail(GTK_WIDGET(rd_item_get_canvas (RD_ITEM (self)))->style);
	
	g_object_set(self->border,
		     "outline-color-gdk", &(GTK_WIDGET(rd_item_get_canvas (RD_ITEM (self)))->style->base[GTK_STATE_INSENSITIVE]),
		     "width-pixels", 1,
		     NULL);
}

static gboolean
cbr_focus_out_event (RdItem       * item,
		     GdkEventFocus* ev)
{
	CriaBlockRenderer* self = CRIA_BLOCK_RENDERER(item);
	
	if(self->border) {
		cbr_border_unfocused(self);
	}

	cbr_disable_caret_timeout(self);
	
	return RD_ITEM_CLASS(cria_block_renderer_parent_class)->focus_out_event(item, ev);
}

CriaBlock*
cria_block_renderer_get_block(CriaBlockRenderer const* self) {
	g_return_val_if_fail(CRIA_IS_BLOCK_RENDERER(self), NULL);

	return self->block;
}

static void
cria_block_renderer_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaBlockRenderer	* self;

	self = CRIA_BLOCK_RENDERER(object);

	switch(prop_id) {
	case CRIA_BLOCK_VIEW_PROP_BLOCK:
		g_value_set_object(value, cria_block_renderer_get_block(self));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

static void
hide_block_border (CriaBlockRenderer* self)
{
	if (!self->border) {
		return;
	}

	rd_item_hide (self->border);
}

void
cria_block_renderer_hide(CriaBlockRenderer* self) {
	g_return_if_fail(CRIA_IS_BLOCK_RENDERER(self));

	if(self->text_item) {
		cdebugo(self, "rendererHide()", "hiding item 0x%x", (uintptr_t)self->text_item);
		rd_item_hide (self->text_item);
	}

	if(self->border) {
		hide_block_border(self);
	}
}

static void
cbr_notify_blink(CriaBlockRenderer* self, GParamSpec* pspec, GtkSettings* settings) {
	g_object_get(settings,
		     "gtk-cursor-blink", &(self->blink),
		     "gtk-cursor-blink-time", &(self->blink_time),
		     NULL);
	cbr_reset_caret(self);
}

static void
cbr_settings_set(CriaBlockRenderer* self, GdkScreen* old_screen) {
	GtkSettings* new = gtk_widget_get_settings(GTK_WIDGET(rd_item_get_canvas (RD_ITEM (self))));
	if(old_screen) {
		GtkSettings* old = gtk_settings_get_for_screen(old_screen);
		g_signal_handlers_disconnect_by_func(old, cbr_notify_blink, self);
	}

	g_signal_connect(new, "notify::gtk-cursor-blink",
			 G_CALLBACK(cbr_notify_blink), self);
	g_signal_connect(new, "notify::gtk-cursor-blink-time",
			 G_CALLBACK(cbr_notify_blink), self);
	cbr_notify_blink(self, NULL, new);
}

static void
disconnect_canvas(CriaBlockRenderer* self)
{
	g_signal_handlers_disconnect_by_func(self->old_canvas, cbr_settings_set, self);
	g_object_unref(self->old_canvas);
	self->old_canvas = NULL;
}

static void
cbr_notify_parent(CriaBlockRenderer* self) {
#warning "enable this"
	/* if(self->old_canvas) {
		disconnect_canvas(self);
	}

	self->old_canvas = g_object_ref (rd_item_get_canvas (RD_ITEM (self)));
	g_signal_connect_swapped(self->old_canvas, "screen-changed",
				 G_CALLBACK(cbr_settings_set), self);
	cbr_settings_set(self, NULL);*/
}

static void
cria_block_renderer_init(CriaBlockRenderer* self) {
	RD_ITEM_SET_FLAG(self, RD_CAN_FOCUS);
	g_signal_connect(self, "notify::parent",
			 G_CALLBACK(cbr_notify_parent), NULL);
}

/**
 * cria_block_renderer_new:
 * @display: a #CriaCanvas
 * @editable: specify whether the block should be editable by this renderer
 *
 * Create a new block renderer.
 *
 * Returns the new block renderer.
 */
CriaBlockRenderer*
cria_block_renderer_new (RdItem* parent,
			 gboolean interactive)
{
	CriaBlockRenderer* result;

	g_return_val_if_fail(RD_IS_ITEM (parent), NULL);

	result = g_object_new (CRIA_TYPE_BLOCK_RENDERER,
			       "interactive", interactive,
			       NULL);
#warning "FIXME: change the return type of this function"
	rd_item_construct (RD_ITEM (result), parent);

	return CRIA_BLOCK_RENDERER (result);
}

/* these are type_map[horiz][vert] */
static GtkAnchorType type_map[][4] = {
	/* unset */
	{ GTK_ANCHOR_NW, GTK_ANCHOR_NW, GTK_ANCHOR_W,      GTK_ANCHOR_SW },
	/* left */
	{ GTK_ANCHOR_NW, GTK_ANCHOR_NW, GTK_ANCHOR_W,      GTK_ANCHOR_SW },
	/* center */
	{ GTK_ANCHOR_N,  GTK_ANCHOR_N,  GTK_ANCHOR_CENTER, GTK_ANCHOR_S  },
	/* right */
	{ GTK_ANCHOR_NE, GTK_ANCHOR_NE, GTK_ANCHOR_E,      GTK_ANCHOR_SE },
	/* block */
	{ GTK_ANCHOR_NW, GTK_ANCHOR_NW, GTK_ANCHOR_W,      GTK_ANCHOR_SW }
};

static GtkAnchorType
get_anchor_from_alignments(CriaAlignment align, CriaVAlignment valign) {
	return type_map[align][valign];
}

static GtkJustification
get_justification_from_alignment(CriaAlignment align) {
	switch(align) {
	case CRIA_ALIGNMENT_RIGHT:
		return GTK_JUSTIFY_RIGHT;
	case CRIA_ALIGNMENT_CENTER:
		return GTK_JUSTIFY_CENTER;
	case CRIA_ALIGNMENT_LEFT:
	default:
		return GTK_JUSTIFY_LEFT;
	}
}

static gdouble
get_x_from_alignment(CriaAlignment align, GODistance left, GODistance right) {
	switch(align) {
	case CRIA_ALIGNMENT_RIGHT:
		return 1.0 * right;
	case CRIA_ALIGNMENT_CENTER:
		return 0.5 * (left + right);
	case CRIA_ALIGNMENT_UNSET:
	case CRIA_ALIGNMENT_LEFT:
	case CRIA_ALIGNMENT_JUSTIFY:
		return 1.0 * left;
	default:
		g_warning("This should not be reached");
		return 0.0;
	}
}

static gdouble
get_y_from_valignment(CriaVAlignment align, GODistance top, GODistance bottom) {
	switch(align) {
	case CRIA_ALIGNMENT_BOTTOM:
		return 1.0 * bottom;
	case CRIA_ALIGNMENT_MIDDLE:
		return 0.5 * (top + bottom);
	case CRIA_VALIGNMENT_UNSET:
	case CRIA_ALIGNMENT_TOP:
		return 1.0 * top;
	default:
		g_warning("This should not be reached");
		return 0.0;
	}
}

static gboolean
cbr_displays_text (CriaBlockRenderer const* self)
{
	CriaTextModel* model = cria_block_get_model (cria_block_renderer_get_block (self));
	gchar const* text = model ? cria_text_model_get_text (model) : NULL;

	gboolean retval = /* non-interactive items display what they're set to: */
			  !RD_ITEM_IS_INTERACTIVE(self) ||
			  /* focused items display their real text: */
			  rd_item_is_focused (RD_ITEM (self)) ||
			  /* non-empty text should be displayed */
	                  (text && *text);
#ifndef CDEBUG_DISABLED
	{
	gchar *msg = text ? g_strescape(text, "") : NULL;
	cdebug("displaysText()", "interactive? %d; focused? %d; text '%s' => %d",
	       RD_ITEM_IS_INTERACTIVE(self),
	       rd_item_is_focused (RD_ITEM (self)),
	       msg,
	       retval);
	g_free(msg);
	}
#endif
	return retval;
}

static void
cbr_update_position(CriaBlockRenderer* self) {
#warning "updatePosition(): FIXME: make this a callback for the 'notify::position' signal of the block"
	GORect block_position = cbr_get_position(self);
	gdouble x, y;
	CriaAlignment  align  = CRIA_ALIGNMENT_CENTER;
	CriaVAlignment valign = CRIA_ALIGNMENT_MIDDLE;

	/* extend the block size as far as necessary to contain the whole text */
	g_object_get(self->text_item,
		     "text-width", &x,
		     "text-height", &y,
		     NULL);
	if(x > (block_position.right - block_position.left)) {
		/* extend by alignment */
#warning "updatePosition(): FIXME: extend by alignment first"
		block_position.right = block_position.left + x;
#warning "updatePosition(): FIXME: get the slide size dynamically"
		if(block_position.right > 5760) {
			block_position.right = 5760;

			block_position.left = MAX(block_position.right - x, 0);
		}
	}

	if(y > (block_position.bottom - block_position.top)) {
		/* extend to the bottom until we hit the slide border */
		block_position.bottom = block_position.top + y;

		if(block_position.bottom > 4320) {
#warning "updatePosition(): FIXME: get the slide size dynamically"
			block_position.bottom = 4320;
			block_position.top = MAX(block_position.bottom - y, 0);
		}
	}

	if(cbr_displays_text(self)) {
		align  = cbr_get_alignment(self);
		valign = cbr_get_valignment(self);
	}

	if(self->text_item) {
		g_object_set(self->text_item,
			     "x", get_x_from_alignment(align, block_position.left, block_position.right),
			     "y", get_y_from_valignment(valign, block_position.top, block_position.bottom),
			     "clip-height", 0.0 + block_position.bottom - block_position.top,
			     "clip-width", 0.0 + block_position.right - block_position.left,
			     NULL);
	}

	if(self->border) {
		g_object_set(self->border,
			     "x1", 0.0 + block_position.left,
			     "x2", 0.0 + block_position.right,
			     "y1", 0.0 + block_position.top,
			     "y2", 0.0 + block_position.bottom,
			     NULL);
	}
}

static void
cbr_update_font(CriaBlockRenderer* self) {
	PangoFontDescription* font = pango_font_description_copy(cria_application_get_default_font());
	CriaLepton format = cbr_get_format(self);
	gdouble  font_size;
	gchar  * font_string;

	g_return_if_fail(CRIA_IS_BLOCK_RENDERER(self));

	cdebug("updateFont()", "setting to format string '%s'", cria_lepton_get(format));
	cria_format_set_font(font, format);

	cbr_ensure_text_item(self);
	font_size   = 0.0 + pango_font_description_get_size(font) * MASTER_COORDINATES_PER_POINT / PANGO_SCALE;
	font_string = pango_font_description_to_string(font);

	cdebugo(self, "updateFont()", "Font is now 0x%x", (uintptr_t)font);
	cdebugo(self, "updateFont()", "Font: %s", font_string);
	g_object_set(self->text_item,
		     "font-desc", font,
		     NULL);

	cdebugo(self, "updateFont()", "showing canvas item at size %f", font_size);
#warning "FIXME: make it unnecessary to set the font size explicitly"
	rd_text_set_zoom_size (RD_TEXT (self->text_item),
			       font_size);
	pango_font_description_free(font);

	g_free(font_string);
}

static void
render_block_content (CriaBlockRenderer* self)
{
	GtkAnchorType	  anchor;
	GtkJustification  justification;
	CriaBlock	* block = cria_block_renderer_get_block(self);
	CriaTextModel   * model;
	CriaAlignment     align;
	CriaVAlignment    valign;

	g_return_if_fail(CRIA_IS_BLOCK(block));
	align = cbr_get_alignment(self);
	valign = cbr_get_valignment(self);

	cbr_ensure_text_item(self);
	
	if(!cbr_displays_text(self)) {
		GOColor color = 0x666666FF;
		g_object_set(self->text_item,
			     "anchor", GTK_ANCHOR_CENTER,
			     "justification", GTK_JUSTIFY_CENTER,
			     "text", MSG_CLICK_HERE,
			     "fill-color-rgba", color,
			     NULL);
		goto finish;
	}

	cdebug("renderBlock()", "the block is called '%s'", cria_slide_element_get_name(CRIA_SLIDE_ELEMENT(block)));

	/* disconnect the default font changed signal */
	if(self->default_font_changed_handler) {
		g_signal_handler_disconnect(cria_application_get_instance(),
					    self->default_font_changed_handler);
		self->default_font_changed_handler = 0;
	}

	if(TRUE) {
#warning "renderBlockContent(): FIXME: automatically connect the the default font"
		self->default_font_changed_handler = g_signal_connect_swapped(cria_application_get_instance(),
										    "default-font-changed",
										    G_CALLBACK(cbr_update_font),
										    self);
	}

	cdebugo(self, "renderBlock()", "alignment %i", align);

	anchor = get_anchor_from_alignments(align, valign);
	justification = get_justification_from_alignment(align);

	model = cria_block_get_model (block);

	g_object_set(self->text_item,
		     "anchor", anchor,
		     "fill-color-rgba", cbr_get_color(self),
		     "justification", justification,
		     "text", model ? cria_text_model_get_text (model) : "",
		     NULL);

finish:
	cbr_update_font(self);
	cbr_update_position(self);

	cdebugo(self, "renderBlock()", "done");
}

static gboolean
cbr_focus_in_event (RdItem       * item,
		    GdkEventFocus* ev)
{
	CriaBlockRenderer* self = CRIA_BLOCK_RENDERER(item);
	gboolean retval = FALSE;

	cbr_disable_caret_timeout(self);

	if(self->border) {
		g_object_set(self->border,
			     "outline-color-gdk", &(GTK_WIDGET(rd_item_get_canvas (RD_ITEM (item)))->style->base[GTK_STATE_SELECTED]),
			     NULL);
	}
	
	retval |= RD_ITEM_CLASS(cria_block_renderer_parent_class)->focus_in_event(item, ev);
	
	render_block_content(self);
	cbr_reset_caret(self);
	
	return retval;
}

static void
render_block_border(CriaBlockRenderer* self) {
	if(!self->border) {
		self->border = rd_rectangle_new (RD_ITEM (self));

		if(self->caret) {
			rd_item_raise_to_top (self->caret);
		}
		
		cbr_border_unfocused(self);
#warning "renderBlockBorder(): FIXME: think of the smart may to update the position"
		cbr_update_position(self);
	}
}

static void
render_block(CriaBlockRenderer* self) {
	render_block_content(self);
	if(cria_item_is_interactive(RD_ITEM(self))) {
		render_block_border(self);
	} else {
		hide_block_border(self);
	}
}

static void
cbr_format_changed(CriaBlockRenderer* self, CriaFormatDomain changed, CriaBlock* block) {
	if(changed & CriaFormatFont) {
		cbr_update_font(self);
	}

	if(changed & CriaFormatSize) {
		cbr_update_position(self);
	}
}

void
cria_block_renderer_set_block(CriaBlockRenderer* self, CriaBlock* block) {
	CriaTextModel* model;
	g_return_if_fail(RD_IS_ITEM (self));
	g_return_if_fail(CRIA_IS_BLOCK_RENDERER(self));
	g_return_if_fail(CRIA_IS_BLOCK(block));

	if(self->block == block) {
		return;
	}

	if(self->block) {
		/* the text view is completely disconnected from the block when
		 * the new model gets set */
		g_signal_handlers_disconnect_by_func(self->block, cbr_update_font, self);
		g_object_unref(self->block);
		self->block = NULL;
	}

	self->block = g_object_ref(block);

	g_signal_connect_swapped(self->block, "format-changed",
			         G_CALLBACK(cbr_format_changed), self);

	cbr_ensure_text_item(self);
	if(cria_item_is_interactive(RD_ITEM(self))) {
		// we're in editing mode, clone the standard model if necessary
		model = cria_block_get_model(block);

		if(!model) {
			cdebug("setBlock()", "cloning model for text element %s",
				cria_slide_element_get_name(CRIA_SLIDE_ELEMENT(block)));
			model = cbr_get_model(self);

			if(model) {
				cdebug("setBlock()", "cloned text ist '%s'", cria_text_model_get_text(model));
				model = cria_text_model_clone(model);
			} else {
				model = cria_text_model_new();
			}

			cria_block_set_model(block, model);
			g_object_unref(model);
		}
	} else {
		// we're in view-only model, display the master block model
		model = cbr_get_model(self);
	}
	g_object_set(self->text_item, "model", model, NULL);
	render_block(self);

	if(cria_item_is_interactive(RD_ITEM(self)) &&
	   rd_item_is_focused (RD_ITEM (self)))
	{
		cbr_update_cursor_position(self);
	}

	g_object_notify(G_OBJECT(self), "block");
}

static void
cria_block_renderer_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaBlockRenderer	* self;

	self = CRIA_BLOCK_RENDERER(object);

	switch(prop_id) {
	case CRIA_BLOCK_VIEW_PROP_BLOCK:
		cria_block_renderer_set_block(self, g_value_get_object(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

void
cria_block_renderer_show(CriaBlockRenderer* self) {
	g_return_if_fail(CRIA_IS_BLOCK_RENDERER(self));

	if(self->text_item) {
		cdebugo (self, "rendererShow()", "showing item 0x%x", (uintptr_t)self->text_item);
		rd_item_show (self->text_item);
	}

	if(self->border && cria_item_is_interactive(RD_ITEM(self))) {
		rd_item_show (self->border);
	}
}

static void
block_renderer_dispose(GObject* object)
{
	if(CRIA_BLOCK_RENDERER(object)->old_canvas) {
		disconnect_canvas(CRIA_BLOCK_RENDERER(object));
	}

	G_OBJECT_CLASS(cria_block_renderer_parent_class)->dispose(object);
}

static void
cbr_finalize(GObject* object) {
	CriaBlockRenderer* self = CRIA_BLOCK_RENDERER(object);

	cdebug("finalize()", "Start");
	g_signal_handlers_disconnect_by_func(self->block, cbr_format_changed, self);

	cdebug("finalize()", "%p: removing %d. reference on %p (%s)", self, G_OBJECT(self->block)->ref_count, self->block, G_OBJECT_TYPE_NAME(self->block));
	g_object_unref(self->block);
	self->block = NULL;

	cdebug("finalize()", "Finished");
}

static void
cbr_remove_cb(GtkAction* action, CriaBlockRenderer* self) {
	CriaSlideRenderer* sr = CRIA_SLIDE_RENDERER(GNOME_CANVAS_ITEM(self)->parent);
	CriaSlide        * slide = cria_slide_renderer_get_slide(sr);
	CriaBlock        * block = cria_block_renderer_get_block(self);

	cria_slide_remove_element(slide, CRIA_SLIDE_ELEMENT(block));
}

static void
cbr_build_actions (RdItem      * item,
		   GtkUIManager* ui_manager,
		   guint         merge_id,
		   gchar const * path)
{
	CriaBlockRenderer* self = CRIA_BLOCK_RENDERER(item);
	GtkActionEntry action_entries[] = {
		{"BlockRemove", GTK_STOCK_DELETE, N_("_Remove Block"),
		 NULL,		N_("_Deletes this block from the slide"),
		 G_CALLBACK(cbr_remove_cb)}
	};
	guint i;

	GtkActionGroup* actions = gtk_action_group_new("cria-block-actions");
	gtk_action_group_add_actions(actions, action_entries, G_N_ELEMENTS(action_entries), self);
	gtk_ui_manager_insert_action_group(ui_manager, actions, 0);

	for(i = 0; i < G_N_ELEMENTS(action_entries); i++) {
		gtk_ui_manager_add_ui(ui_manager, merge_id,
				      path,
				      action_entries[i].name,
				      action_entries[i].name,
				      GTK_UI_MANAGER_AUTO,
				      FALSE);

		if(cria_slide_element_get_master(CRIA_SLIDE_ELEMENT(cria_block_renderer_get_block(self)))) {
			gtk_action_set_sensitive(gtk_action_group_get_action(actions, action_entries[i].name),
						 FALSE);
		}
	}
	g_object_unref(actions);
}

static gboolean
cbr_event (GnomeCanvasItem* item,
	   GdkEvent       * ev)
{
	CriaBlockRenderer* self = CRIA_BLOCK_RENDERER(item);
	gboolean retval = FALSE;

	switch(ev->type) {
	case GDK_ENTER_NOTIFY:
		if(RD_ITEM_IS_INTERACTIVE(self)) {
			gtk_widget_set_cursor(GTK_WIDGET(rd_item_get_canvas (RD_ITEM (self))), GDK_XTERM);
			retval = TRUE;
		}
		break;
	case GDK_LEAVE_NOTIFY:
		if(RD_ITEM_IS_INTERACTIVE(self)) {
			gtk_widget_set_cursor(GTK_WIDGET(rd_item_get_canvas (RD_ITEM (self))), GDK_LEFT_PTR);
			retval = TRUE;
		}
		break;
	case GDK_BUTTON_PRESS:
	case GDK_BUTTON_RELEASE:
		retval = GNOME_CANVAS_ITEM_CLASS (cria_block_renderer_parent_class)->event (item, ev);
		break;
	default:
		/* ignore */
		break;
	}

	/* forward events to the text item */
	if (ev->type != GDK_BUTTON_PRESS || ev->type != GDK_BUTTON_RELEASE) {
		retval |= GNOME_CANVAS_ITEM_GET_CLASS(self->text_item)->event
			(GNOME_CANVAS_ITEM (self->text_item), ev);
	}

	return retval;
}

static gdouble
block_renderer_point (GnomeCanvasItem* item,
		      gdouble          x,
		      gdouble          y,
		      gint             cx,
		      gint             cy,
		      GnomeCanvasItem**found)
{
	CriaBlockRenderer* self = CRIA_BLOCK_RENDERER (self);
#warning "FIXME: rewrite to not follow children"
	gdouble result = GNOME_CANVAS_ITEM_CLASS (cria_block_renderer_parent_class)->point
							(item, x, y, cx, cy, found);

	if (*found == GNOME_CANVAS_ITEM (self->text_item) ||
	    *found == GNOME_CANVAS_ITEM (self->border) ||
	    *found == GNOME_CANVAS_ITEM (self->caret))
	{
		*found = item;
	}

	return result;
}

static void
cria_block_renderer_class_init(CriaBlockRendererClass* self_class) {
	GObjectClass	    * g_object_class;
	GnomeCanvasItemClass* gci_class;
	RdItemClass	    * ci_class;

	/* setting up property system */
	g_object_class = G_OBJECT_CLASS(self_class);
	g_object_class->dispose      = block_renderer_dispose;
	g_object_class->finalize     = cbr_finalize;
	g_object_class->get_property = cria_block_renderer_get_property;
	g_object_class->set_property = cria_block_renderer_set_property;

	/* GnomeCanvasItemClass */
	gci_class = GNOME_CANVAS_ITEM_CLASS(self_class);
	gci_class->event = cbr_event;
	gci_class->point = block_renderer_point;

	/* setting up the cria item class */
	ci_class = RD_ITEM_CLASS(self_class);
	ci_class->build_actions      = cbr_build_actions;
	ci_class->button_press_event = cbr_button_press_event;
	ci_class->focus_in_event     = cbr_focus_in_event;
	ci_class->focus_out_event    = cbr_focus_out_event;

	/* setting up the block view interface */
	_cria_block_view_install_properties(g_object_class);
}

