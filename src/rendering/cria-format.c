/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <rendering/cria-format.h>

#include <stdlib.h>
#include <string.h>
#include <utils/cria-units.h>
#include <dom/cria-format.h>

void
cria_format_set_font(PangoFontDescription* desc, CriaLepton lepton) {
	gchar    * text;
	gssize     size;

	g_return_if_fail(desc);

	/* font family */
	text = cria_format_get(lepton, "font-family");
	if(text) {
		pango_font_description_set_family(desc, text);
		g_free(text);
	}

	/* font size */
	text = cria_format_get(lepton, "font-size");
	if(text) {
		size = cria_unit_to_pixels(text, pango_font_description_get_size(desc) / PANGO_SCALE) * PANGO_SCALE;
		pango_font_description_set_size(desc, size);
		g_free(text);
	}

	/* font style */
	text = cria_format_get(lepton, "font-style");
	if(text) {
		PangoStyle style;

		if(!strcmp(text, "oblique")) {
			style = PANGO_STYLE_OBLIQUE;
		} else if(!strcmp(text, "italic")) {
			style = PANGO_STYLE_ITALIC;
		} else {
			/* "normal" and other stuff */
			style = PANGO_STYLE_NORMAL;
		}
		
		pango_font_description_set_style(desc, style);
		g_free(text);
	}

	/* font weight */
	text = cria_format_get(lepton, "font-weight");
	if(text) {
		PangoWeight weight = PANGO_WEIGHT_NORMAL;
		if(pango_font_description_get_set_fields(desc) & PANGO_FONT_MASK_WEIGHT) {
			weight = pango_font_description_get_weight(desc);
		}
		if(*text >= 1 && *text <= 9) {
			weight = atoi(text);
		} else if(!strcmp(text, "lighter")) {
			if(weight >= 200) {
				weight -= 100;
			}
		} else if(!strcmp(text, "bolder")) {
			if(weight <= 800) {
				weight += 100;
			}
		} else if(!strcmp(text, "bold")) {
			weight = PANGO_WEIGHT_BOLD;
		} else if (!strcmp(text, "normal")) {
			weight = PANGO_WEIGHT_NORMAL;
		} else if (strcmp(text, "inherit")) {
			g_critical("Invalid font weight: %s", text);
		}

		pango_font_description_set_weight(desc, weight);
		g_free(text);
	}
#warning "setFont(): FIXME: add Italic attribute"
#warning "setFont(): FIXME: add Underline attribute"
}

