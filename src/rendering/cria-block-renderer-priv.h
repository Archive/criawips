/* This file is part of criawips
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005,2007 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIAWIPS_BLOCK_RENDERER_PRIVATE_H
#define CRIAWIPS_BLOCK_RENDERER_PRIVATE_H

#include <rendering/cria-block-renderer.h>

struct _CriaBlockRenderer {
	RdItem     base_instance;
	RdItem   * text_item;
	RdItem   * border;
	RdItem   * caret;
	CriaBlock* block;
	GtkWidget* old_canvas;
	gboolean   blink;
	gint       blink_time;
	gboolean   caret_visible;
	guint      caret_timeout;
	gulong     default_font_changed_handler;
};

struct _CriaBlockRendererClass {
	RdItemClass  base_class;
};

#endif /* !CRIAWIPS_BLOCK_RENDERER_PRIVATE_H */
