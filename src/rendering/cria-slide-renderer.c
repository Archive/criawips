/* this file is part of criawips
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005,2007 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <rendering/cria-slide-renderer-priv.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <inttypes.h>
#include <string.h>

#include <glib/gi18n.h>
#include <canvas/rd-rectangle.h>

#define CDEBUG_TYPE cria_slide_renderer_get_type
#include <cdebug/cdebug.h>

#include <dom/cria-enums.h>
#include <dom/cria-slide-view.h>
#include <widgets/cria-slide-editor.h>
#include <rendering/cria-block-renderer.h>
#include <rendering/cria-image-pool.h>

enum {
	PROP_0,
	PROP_SLIDE
};

enum {
	N_SIGNALS
};

static void csv_iface_init(CriaSlideViewIface* iface);
G_DEFINE_TYPE_WITH_CODE(CriaSlideRenderer, cria_slide_renderer, RD_TYPE_ITEM,
			G_IMPLEMENT_INTERFACE(CRIA_TYPE_SLIDE_VIEW, csv_iface_init));

static void cria_slide_renderer_get_property   (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static void cria_slide_renderer_set_property   (GObject		* object,
						guint		  prop_id,
						const	GValue	* value,
						GParamSpec	* param_spec);
#if 0
/* enable these to add support for signals */
static	guint	cria_slide_renderer_signals[N_SIGNALS] = { 0 };

static	void	cria_slide_renderer_signal	       (CriaSlideRenderer	* self,
						const	gchar	* string);
#endif

static void
csr_update_focus_layer_size (CriaSlideRenderer* self)
{
	GOPoint* size;
	g_return_if_fail (RD_IS_ITEM (self->focus_layer));
	g_return_if_fail (CRIA_IS_SLIDE (self->slide));

	size = cria_slide_get_size(self->slide);
	g_object_set (self->focus_layer,
			"x1", 0.0,
			"y1", 0.0,
			"x2", 0.0 + size->x,
			"y2", 0.0 + size->y,
			NULL);
	g_free(size);
}

static void
csr_ensure_stacking (CriaSlideRenderer* self)
{
	if (self->frame) {
		rd_item_raise_to_top(self->frame);
	}

	if (self->focus_layer) {
		rd_item_lower_to_bottom (self->focus_layer);
	}
	if(self->background_renderer) {
		rd_item_lower_to_bottom (RD_ITEM (self->background_renderer));
	}
	if(self->shadow) {
		rd_item_lower_to_bottom (self->shadow);
	}
}

static void
csr_ensure_focus_layer (CriaSlideRenderer* self)
{
	g_return_if_fail (rd_item_is_constructed (RD_ITEM (self)));

	if(!self->focus_layer) {
		self->focus_layer = rd_rectangle_new (RD_ITEM (self));
		csr_update_focus_layer_size(self);
		csr_ensure_stacking(self);
	}
}


static gdouble
csr_point (GnomeCanvasItem* self,
	   gdouble          x,
	   gdouble          y,
	   int              cx,
	   int              cy,
	   GnomeCanvasItem**actual_item)
{
	gdouble retval = FALSE;

	if(!retval && GNOME_CANVAS_ITEM_CLASS(cria_slide_renderer_parent_class)->point) {
		retval = GNOME_CANVAS_ITEM_CLASS(cria_slide_renderer_parent_class)->point(self, x, y, cx, cy, actual_item);
	}

	if(actual_item && *actual_item == GNOME_CANVAS_ITEM(CRIA_SLIDE_RENDERER(self)->background_renderer)) {
		/* don't focus the background renderer, focus ourselves */
		*actual_item = self;
	}

	return retval;
}

static gboolean
csr_button_press_event (RdItem        * item,
			GdkEventButton* ev)
{
	gboolean retval = FALSE;

	if(ev->type == GDK_BUTTON_PRESS && ev->button == 1) {
		cria_item_grab_focus(item);
		retval = TRUE;
	}

	return retval;
}

static void
csr_frame_unfocused (CriaSlideRenderer* self)
{
	g_object_set(self->frame,
		     /* MEMO text would get style->text[GTK_STATE_ACTIVE] */
		     "outline-color-gdk", &GTK_WIDGET(rd_item_get_canvas(RD_ITEM (self)))->style->black,
		     "width-pixels", 1,
		     NULL);
}

static void
csr_update_frame_size (CriaSlideRenderer* self)
{
	GOPoint* size;
	g_return_if_fail (RD_IS_ITEM (self->frame));
	g_return_if_fail (CRIA_IS_SLIDE (self->slide));

	size = cria_slide_get_size(self->slide);
	g_object_set(self->frame,
		     "x1", 0.0,
		     "y1", 0.0,
		     "x2", 0.0 + size->x,
		     "y2", 0.0 + size->y,
		     NULL);
	g_free(size);
}

static void
csr_ensure_frame(CriaSlideRenderer* self) {
	if(!self->frame) {
		self->frame = rd_rectangle_new (RD_ITEM (self));
		csr_frame_unfocused(self);

		if (self->slide) {
			csr_update_frame_size(self);
		}
		rd_item_raise_to_top (self->frame);
		rd_item_show (self->frame);
	}
}

static gboolean
csr_focus_in_event (RdItem       * item,
		    GdkEventFocus* ev)
{
	CriaSlideRenderer* self = CRIA_SLIDE_RENDERER(item);
	guint		   color;
	GdkColor           gdk_color;

	csr_ensure_frame(self);
	gdk_color = GTK_WIDGET(rd_item_get_canvas (RD_ITEM (self)))->style->base[GTK_STATE_SELECTED];
	g_object_set(self->frame,
		     /* MEMO text would get style->text[GTK_STATE_SELECTED] */
		     "outline-color-gdk", &gdk_color,
		     "width-pixels", 2,
		     NULL);

	csr_ensure_focus_layer(self);
	color = GNOME_CANVAS_COLOR_A(gdk_color.red >> 8, gdk_color.green >> 8, gdk_color.blue >> 8, 0x80);
	g_object_set(self->focus_layer,
		     "fill-color-rgba", color,
		     NULL);
	rd_item_show (self->focus_layer);
	return TRUE;
}

static gboolean
csr_focus_out_event (RdItem       * item,
		     GdkEventFocus* ev)
{
	CriaSlideRenderer* self = CRIA_SLIDE_RENDERER(item);

	csr_frame_unfocused(self);

	if (self->focus_layer) {
		rd_item_hide (self->focus_layer);
	}

	return TRUE;
}

static void
csr_finalize(GObject* object) {
	CriaSlideRenderer* self = CRIA_SLIDE_RENDERER(object);

	cdebug("finalize()", "Start");
	if(self->slide) {
		cria_slide_view_unregister(CRIA_SLIDE_VIEW(self), self->slide);
		cdebug("finalize()", "slide has %d references, removing one of them", G_OBJECT(self->slide)->ref_count);
		g_object_unref(self->slide);
		self->slide = NULL;
	}

	cdebug("finalize()", "parent_class->finalize()");
	G_OBJECT_CLASS(cria_slide_renderer_parent_class)->finalize(object);
	cdebug("finalize()", "End");
}

static void
cria_slide_renderer_class_init(CriaSlideRendererClass* self_class) {
	GObjectClass* go_class;
	GnomeCanvasItemClass* canvas_item_class;
	RdItemClass         * item_class;

	/* GObjectClass */
	go_class = G_OBJECT_CLASS(self_class);
	go_class->finalize = csr_finalize;
	go_class->set_property = cria_slide_renderer_set_property;
	go_class->get_property = cria_slide_renderer_get_property;

	g_object_class_install_property(go_class,
					PROP_SLIDE,
					g_param_spec_object("slide",
							    "Slide",
							    "The slide being rendered by this renderer",
							    CRIA_TYPE_SLIDE,
							    G_PARAM_READWRITE));

	/* setting up the canvas item class */
	canvas_item_class = GNOME_CANVAS_ITEM_CLASS(self_class);
	canvas_item_class->point = csr_point;

	/* setting up the cria item class */
	item_class = RD_ITEM_CLASS(self_class);
	item_class->button_press_event = csr_button_press_event;
	item_class->focus_in_event = csr_focus_in_event;
	item_class->focus_out_event = csr_focus_out_event;
}

static void
csr_update_shadow_size (CriaSlideRenderer* self)
{
	GOPoint* size;
	g_return_if_fail (RD_IS_ITEM (self->shadow));
	g_return_if_fail (CRIA_IS_SLIDE (self->slide));

	size = cria_slide_get_size(self->slide);
	g_object_set(self->shadow,
		     "x1", 0.0 + SLIDE_EDITOR_SHADOW_OFFSET,
		     "y1", 0.0 + SLIDE_EDITOR_SHADOW_OFFSET,
		     "x2", 0.0 + (size->x + SLIDE_EDITOR_SHADOW_OFFSET),
		     "y2", 0.0 + (size->y + SLIDE_EDITOR_SHADOW_OFFSET),
		     NULL);
	g_free(size);
}

static void
csr_ensure_shadow(CriaSlideRenderer* self) {
	if(!self->shadow) {
		/* add the shadow */
		self->shadow = rd_rectangle_new (RD_ITEM (self));
		g_object_set (self->shadow,
			      "fill-color-rgba", GNOME_CANVAS_COLOR_A(0, 0, 0, 127),
			      NULL);

		if(self->slide) {
			csr_update_shadow_size(self);
		}
		rd_item_lower_to_bottom(self->shadow);
		rd_item_show(self->shadow);
	}
}

static void
cria_slide_renderer_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaSlideRenderer	* self;

	self = CRIA_SLIDE_RENDERER(object);

	switch(prop_id) {
	case PROP_SLIDE:
		g_value_set_object(value, cria_slide_renderer_get_slide(self));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

CriaSlide*
cria_slide_renderer_get_slide(CriaSlideRenderer const* self) {
	g_return_val_if_fail(CRIA_IS_SLIDE_RENDERER(self), NULL);

	return self->slide;
}

static void
cb_interactive_changed (CriaSlideRenderer* self)
{
	if(cria_item_is_interactive(RD_ITEM(self)) && rd_item_is_constructed (RD_ITEM (self))) {
		csr_ensure_frame(self);
		csr_ensure_shadow(self);
		csr_ensure_stacking(self);
	} else {
		if(self->frame) {
			rd_item_hide (self->frame);
		}

		if(self->shadow) {
			rd_item_hide (self->shadow);
		}
	}
}

static void
cria_slide_renderer_init (CriaSlideRenderer* self)
{
	RD_ITEM_SET_FLAG(RD_ITEM(self), RD_CAN_FOCUS);

	g_signal_connect(self, "notify::interactive", G_CALLBACK(cb_interactive_changed), NULL);
}

/**
 * cria_slide_renderer_new:
 * @display: a #CriaCanvas
 * @interactive: specifies whether the renderer should allow editing
 *
 * Create a new renderer for a #CriaSlide.
 *
 * Returns the new slide renderer.
 */
CriaSlideRenderer*
cria_slide_renderer_new(gboolean interactive) {
	return g_object_new(CRIA_TYPE_SLIDE_RENDERER,
			    "interactive", interactive,
			    NULL);
}

static void
cria_slide_renderer_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaSlideRenderer	* self;

	self = CRIA_SLIDE_RENDERER(object);

	switch(prop_id) {
	case PROP_SLIDE:
		cria_slide_renderer_set_slide(self, g_value_get_object(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

static void
csr_ensure_background_renderer (CriaSlideRenderer* self)
{
	if (G_UNLIKELY (!self->background_renderer)) {
		self->background_renderer = cria_background_renderer_new (RD_ITEM (self));
	}

	csr_ensure_stacking(self);
}

static void
update_slide_size(CriaSlideRenderer* self) {
	GOPoint* size;
	g_return_if_fail(CRIA_IS_SLIDE_RENDERER(self));
	g_return_if_fail(CRIA_IS_SLIDE(self->slide));

	size = cria_slide_get_size(self->slide);
	if(self->frame) {
		csr_update_frame_size(self);
	}
	if(self->shadow) {
		csr_update_shadow_size(self);
	}
	if(self->focus_layer) {
		csr_update_focus_layer_size(self);
	}
	if(self->background_renderer) {
		GORect rect = {
			0, /* top */
			0, /* left */
			size->y, /* bottom */
			size->x  /* right */
		};

		cdebug("updateSlideSize()", "setting background position to: %llix%lli => %llix%lli", rect.left, rect.top, rect.right, rect.bottom);
		cria_background_renderer_set_position(self->background_renderer, &rect);
	}
	g_free(size);
}

void
cria_slide_renderer_set_slide (CriaSlideRenderer* self,
			       CriaSlide        * slide)
{
	g_return_if_fail(CRIA_IS_SLIDE_RENDERER(self));
	g_return_if_fail(CRIA_IS_SLIDE(slide));

	if(self->slide == slide) {
		return;
	}

	cdebug("setSlide()", "setting slide to '%s'", cria_slide_get_title(slide));

	if(self->slide != NULL) {
		cria_slide_view_unregister(CRIA_SLIDE_VIEW(self), self->slide);
		g_object_unref(self->slide);
		self->slide = NULL;
	}

	self->slide = g_object_ref(slide);
	cria_slide_view_register(CRIA_SLIDE_VIEW(self), slide);

	g_object_notify(G_OBJECT(self), "slide");

	if(rd_item_is_constructed (RD_ITEM (self)) && !self->background_renderer) {
		csr_ensure_background_renderer(self);
	}

	csr_ensure_frame (self);
	csr_ensure_shadow (self);

	cria_background_renderer_set_background (self->background_renderer,
						 cria_slide_get_background (self->slide, FALSE));
	update_slide_size(self);

	cdebug("setSlide()", "slide is set");
}

/* CriaSlideViewIface handling stuff */
static void
csr_element_added(CriaSlideView* view, CriaSlideElement* element) {
	CriaSlideRenderer* self;
	CriaBlockRenderer* renderer;

	g_return_if_fail(CRIA_IS_SLIDE_ELEMENT(element));

	cdebug("elementAdded()", "adding renderer for element '%s'", cria_slide_element_get_name(element));

	self     = CRIA_SLIDE_RENDERER(view);
	renderer = cria_block_renderer_new (RD_ITEM (self),
					    cria_item_is_interactive (RD_ITEM (self)));

	csr_ensure_stacking (self);

	cria_block_renderer_set_block (renderer, CRIA_BLOCK(element));
	self->block_renderers = g_list_prepend(self->block_renderers, g_object_ref(renderer));
}

static void
csr_element_removed(CriaSlideView* view, CriaSlideElement* element) {
	CriaSlideRenderer* self     = CRIA_SLIDE_RENDERER(view);
	CriaBlock* block = CRIA_BLOCK(element);
	GList    * list_renderer;

	for(list_renderer = self->block_renderers; list_renderer; list_renderer = list_renderer->next) {
		CriaBlockRenderer* renderer = CRIA_BLOCK_RENDERER(list_renderer->data);

		if(block == cria_block_renderer_get_block(renderer)) {
			gtk_object_destroy(list_renderer->data);
			cdebug("elementRemoved()", "%p: remove %d. reference of %p (%s)", self, G_OBJECT(list_renderer->data)->ref_count, list_renderer->data, G_OBJECT_TYPE_NAME(list_renderer->data));
			g_object_unref(list_renderer->data);
			self->block_renderers = g_list_delete_link(self->block_renderers, list_renderer);
			break;
		}
	}
}

static void
slide_renderer_notify_master_slide (CriaSlideView* view,
				    CriaSlide    * master_slide)
{
	CriaSlideRenderer* self = CRIA_SLIDE_RENDERER (view);

	cria_background_renderer_set_background (self->background_renderer,
						 cria_slide_get_background (self->slide, FALSE));
}

static void
csv_iface_init (CriaSlideViewIface* iface)
{
	iface->element_added       = csr_element_added;
	iface->element_removed     = csr_element_removed;
	iface->notify_master_slide = slide_renderer_notify_master_slide;
}

