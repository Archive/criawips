/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIAWIPS_CANVAS_TEXT_VIEW_PRIVATE_H
#define CRIAWIPS_CANVAS_TEXT_VIEW_PRIVATE_H

#include <rendering/cria-canvas-text-view.h>

#include <dom/cria-text-model.h>

G_BEGIN_DECLS

struct _CriaCanvasTextView {
	CriaCanvasText  text;
	CriaTextModel * model;
};

struct _CriaCanvasTextViewClass {
	CriaCanvasTextClass base_class;
};

G_END_DECLS

#endif /* CRIAWIPS_CANVAS_TEXT_VIEW_PRIVATE_H */

