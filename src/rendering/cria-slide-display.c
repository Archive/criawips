/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <rendering/cria-slide-display-priv.h>

#include <goffice/utils/go-units.h>
#include <gtk/gtkmenu.h>

#define CDEBUG_TYPE cria_slide_display_get_type
#include <cdebug/cdebug.h>

#include <helpers/gobject-helpers.h>
#include <rendering/cria-slide-renderer.h>
#include <utils/cria-units.h>

enum {
	PROP_0,
	PROP_SLIDE,
	PROP_SLIDE_RENDERER,
	PROP_SLIDE_ZOOM
};

enum {
	SIGNAL,
	N_SIGNALS
};

G_DEFINE_TYPE(CriaSlideDisplay, cria_slide_display, CRIA_TYPE_CANVAS)

static void cria_slide_display_get_property	       (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static void cria_slide_display_set_property	       (GObject		* object,
						guint		  prop_id,
						const GValue	* value,
						GParamSpec	* param_spec);
#if 0
/* enable these to add support for signals */
static	guint	cria_slide_display_signals[N_SIGNALS] = { 0 };

static	void	cria_slide_display_signal	       (CriaSlideDisplay	* self,
						const	gchar	* string);
#endif

/*
 * cria_slide_get_display_size:
 * @self: a #CriaSlideDisplay
 *
 * Get the size that the slide will take on its screen.
 *
 * Returns a #GOPoint defining the size of the screen in pixels. Don't forget
 * to g_free it.
 */
static GOPoint*
cria_slide_display_get_size(CriaSlideDisplay* self) {
	GOPoint  * retval;
	GdkScreen* screen = gtk_widget_get_screen(GTK_WIDGET(self));

	g_return_val_if_fail(CRIA_IS_SLIDE_DISPLAY(self), NULL);
	g_return_val_if_fail(GDK_IS_SCREEN(screen), NULL);
	
#warning "getSize(): FIXME: make this code slide-agnostic"

	retval = g_new0(GOPoint,1);
	retval->x = gdk_screen_get_width(screen);
	retval->y = gdk_screen_get_height(screen);

	cdebug("getDisplaySize()", "got resolution %llix%lli (%f)", retval->x, retval->y, 1.0 * retval->x / retval->y);

#warning "Slide::getDisplaySize(): FIXME: get the ratio from the presentation"
#define RATIO_X 4
#define RATIO_Y 3
	if ((1.0 * retval->x / retval->y) >= (1.0 * RATIO_X / RATIO_Y)) {
		/* screen is too wide */
		retval->y -= (retval->y % RATIO_Y);
		retval->x  = retval->y / RATIO_Y * RATIO_X;
		cdebug("getDisplaySize()", "The screen is too wide, display resolution is %llix%lli", retval->x, retval->y);
	} else {
		/* screen is too high */
		retval->x -= (retval->x % RATIO_X);
		retval->y  = retval->x / RATIO_X * RATIO_Y;
		cdebug("getDisplaySize()", "The screen is too high, display resolution is %llix%lli", retval->x, retval->y);
	}
	
	return retval;
#undef RATIO_X
#undef RATIO_Y
}

static void
csd_finalize(GObject* object) {
	CriaSlideDisplay* self = CRIA_SLIDE_DISPLAY(object);

	if(self->slide) {
		cdebug("finalize()", "removing one of %d references on the slide", G_OBJECT(self->slide)->ref_count);
		g_object_unref(self->slide);
		self->slide = NULL;
	}

	if(self->ui_manager) {
		g_object_unref(self->ui_manager);
		self->ui_manager = NULL;
	}

	if(G_OBJECT_CLASS(cria_slide_display_parent_class)->finalize) {
		G_OBJECT_CLASS(cria_slide_display_parent_class)->finalize(object);
	}
}

static gboolean
csd_popup_cleanup(CriaSlideDisplay* self) {
	g_object_unref(self->ui_manager);
	self->ui_manager = NULL;

	return FALSE;
}

static gboolean
csd_button_press_event(GtkWidget* widget, GdkEventButton* ev) {
	CriaSlideDisplay* self   = CRIA_SLIDE_DISPLAY(widget);
	gboolean          retval = FALSE;

	if(ev->button == 3) {
		GtkActionGroup * actions;
		gdouble          world_x, world_y;
		RdItem        * item;

#define POPUP_ACTION "SlideEditorPopup"
		static const GtkActionEntry action_entries[] = {
			{POPUP_ACTION, NULL, POPUP_ACTION}
		};

#warning "FIXME: check that we don't have a ui_manager"

		/* find the element below the click point */
		rd_canvas_window_to_world (RD_CANVAS (widget),
					   ev->x, ev->y,
					   &world_x, &world_y);
		item = rd_canvas_get_item_at (RD_CANVAS (widget),
					      world_x, world_y);

		if (item) {
			GtkWidget* popup;
			guint      merge_id;
			gchar    * merge_path;

			self->ui_manager = gtk_ui_manager_new();
			merge_id = gtk_ui_manager_new_merge_id(self->ui_manager);

			actions = gtk_action_group_new("slide-editor-popup");
			gtk_action_group_add_actions(actions, action_entries, G_N_ELEMENTS(action_entries), self);
			gtk_ui_manager_insert_action_group(self->ui_manager, actions, 0);
			g_object_unref(actions);

			/* Add the popup menu */
			gtk_ui_manager_add_ui(self->ui_manager,
					      merge_id, "/",
					      action_entries[0].name,
					      action_entries[0].name,
					      GTK_UI_MANAGER_POPUP,
					      FALSE);

			/* let's have fun... */
			merge_path = g_strdup_printf("/%s", POPUP_ACTION);
#warning "we should have a generic CriaItem with the build-actions stuff"
			g_signal_emit_by_name(RD_ITEM(item), "build-actions", self->ui_manager, merge_id, merge_path);

			/* display the popup */

			popup = gtk_ui_manager_get_widget(self->ui_manager, merge_path);

			GList* children = gtk_container_get_children(GTK_CONTAINER(popup));
			if(children) {
				g_signal_connect_swapped_after(popup, "selection-done",
							       G_CALLBACK(csd_popup_cleanup), self);
				gtk_menu_popup(GTK_MENU(popup),
					       NULL, NULL,
					       NULL,
					       self, ev->button,
					       ev->time);
				g_list_free(children);
			} else {
				csd_popup_cleanup(self);
			}

			g_free(merge_path);
		}
	}

	retval |= GTK_WIDGET_CLASS(cria_slide_display_parent_class)->button_press_event(widget, ev);

	return retval;
}

static void
cria_slide_display_class_init(CriaSlideDisplayClass* self_class) {
	GObjectClass  * g_object_class;
	GtkWidgetClass* gw_class;

	/* GObjectClass */
	g_object_class = G_OBJECT_CLASS(self_class);
	g_object_class->get_property = cria_slide_display_get_property;
	g_object_class->finalize     = csd_finalize;
	g_object_class->set_property = cria_slide_display_set_property;

	g_object_class_install_property(g_object_class,
					PROP_SLIDE,
					g_param_spec_object("slide",
							    "slide",
							    "slide",
							    CRIA_TYPE_SLIDE,
							    G_PARAM_READWRITE));
	g_object_class_install_property(g_object_class,
					PROP_SLIDE_RENDERER,
					g_param_spec_object("slide-renderer",
							    "Slide Renderer",
							    "BLURB",
							    CRIA_TYPE_SLIDE_RENDERER,
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property(g_object_class,
					PROP_SLIDE_ZOOM,
					g_param_spec_double("slide-zoom",
							    "Slide Zoom",
							    "This attribute specifies the zoom value in a way that "
							    "100% are the full screen zoom.",
							    G_MINDOUBLE,
							    G_MAXDOUBLE,
							    0.75,
							    G_PARAM_READWRITE));

	/* GtkWidgetClass */
	gw_class = GTK_WIDGET_CLASS(self_class);
	gw_class->button_press_event = csd_button_press_event;
}

CriaSlide*
cria_slide_display_get_slide(CriaSlideDisplay* self) {
	return self->slide;
}

static void
cria_slide_display_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaSlideDisplay	* self;

	self = CRIA_SLIDE_DISPLAY(object);

	switch(prop_id) {
	case PROP_SLIDE:
		g_value_set_object(value, cria_slide_display_get_slide(self));
		break;
	case PROP_SLIDE_RENDERER:
		g_value_set_object(value, self->slide_renderer);
		break;
	case PROP_SLIDE_ZOOM:
		g_value_set_double(value, cria_slide_display_get_slide_zoom(self));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

static void
notify_zoom_cb(CriaSlideDisplay* self) {
	g_object_notify(G_OBJECT(self), "slide-zoom");
}

static void
cria_slide_display_init(CriaSlideDisplay* self) {
	g_signal_connect(self, "notify::zoom", G_CALLBACK(notify_zoom_cb), NULL);
}

gdouble
cria_slide_display_get_slide_zoom(CriaSlideDisplay* self) {
	GOPoint* display = cria_slide_display_get_size(self),
	       * slide   = cria_slide_get_size(self->slide);
	gdouble retval = 1.0;

	retval = cria_canvas_get_zoom(CRIA_CANVAS(self)) * slide->x / display->x;
	g_free(slide);
	g_free(display);
	return retval;
}

/* we get a percentage here (0.0 - 1.0) and we want 100% to be fullscreen */
void
cria_slide_display_set_slide_zoom(CriaSlideDisplay* self, gdouble slide_zoom) {
	GOPoint* display;
	GOPoint* slide;

	g_return_if_fail(CRIA_IS_SLIDE_DISPLAY(self));

	display = cria_slide_display_get_size(self);
	slide   = cria_slide_get_size(self->slide);

	cria_canvas_set_zoom(CRIA_CANVAS(self), slide_zoom * display->x / slide->x);
	g_free(slide);
	g_free(display);
}

void
cria_slide_display_set_slide(CriaSlideDisplay* self, CriaSlide* slide) {
	g_return_if_fail(CRIA_IS_SLIDE_DISPLAY(self));
	g_return_if_fail(CRIA_IS_SLIDE(slide));

	if(self->slide) {
		g_object_unref(self->slide);
		self->slide = NULL;
	}

	self->slide = g_object_ref(slide);

	{
		GOPoint    * slide_s = cria_slide_get_size(self->slide);
		CcRectangle  rect;
		rect.x = 0.0;
		rect.y = 0.0;
		rect.w = 0.0 + slide_s->x;
		rect.h = 0.0 + slide_s->y;
		cria_canvas_set_extents(CRIA_CANVAS(self), &rect);
		g_free(slide_s);
	}
	cria_slide_renderer_set_slide(self->slide_renderer, slide);

	g_object_notify(G_OBJECT(self), "slide");
}

static void
cria_slide_display_set_property (GObject     * object,
				 guint         prop_id,
				 GValue const* value,
				 GParamSpec  * param_spec)
{
	CriaSlideDisplay* self = CRIA_SLIDE_DISPLAY (object);

	switch (prop_id) {
	case PROP_SLIDE:
		cria_slide_display_set_slide (self, g_value_get_object (value));
		break;
	case PROP_SLIDE_RENDERER:
		self->slide_renderer = g_value_get_object (value);
		rd_item_construct (RD_ITEM (self->slide_renderer),
				   rd_canvas_get_root (RD_CANVAS (self)));
		g_object_notify(object, "slide-renderer");
		break;
	case PROP_SLIDE_ZOOM:
		cria_slide_display_set_slide_zoom(self, g_value_get_double(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

CriaSlideDisplay*
cria_slide_display_new(CriaSlideRenderer* csr) {
	return g_object_new(CRIA_TYPE_SLIDE_DISPLAY,
			    "slide-renderer", csr,
			    "aa", TRUE,
			    NULL);
}

/**
 * cria_slide_display_get_preview:
 * @slide: a #CriaSlide
 *
 * Get a preview image of a #CriaSlide.
 *
 * Returns a #GdkPixbuf containing a preview of the slide
 */

GdkPixbuf*
cria_slide_display_get_preview(CriaSlide* slide) {
	GdkPixbuf* retval = NULL;
	/* we're not going to implement this for the old canvas */
#warning "implement with libccc"
	retval = gdk_pixbuf_new(GDK_COLORSPACE_RGB,
				TRUE, /* has alpha */
				8,
				64,
				48);
	return retval;
}

