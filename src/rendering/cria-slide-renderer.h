/* This file is part of criawips
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005,2007 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef SLIDE_RENDERER_H
#define SLIDE_RENDERER_H

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <canvas/rd-canvas.h>

#include <dom/cria-slide.h>

G_BEGIN_DECLS

typedef struct _CriaSlideRenderer CriaSlideRenderer;
typedef struct _CriaSlideRendererClass CriaSlideRendererClass;

GType	           cria_slide_renderer_get_type	       (void);
CriaSlide*         cria_slide_renderer_get_slide       (CriaSlideRenderer const* self);
CriaSlideRenderer* cria_slide_renderer_new             (gboolean	   editable);
void	           cria_slide_renderer_set_slide       (CriaSlideRenderer* self,
							CriaSlide	 * slide);

#define CRIA_TYPE_SLIDE_RENDERER		(cria_slide_renderer_get_type())
#define CRIA_SLIDE_RENDERER(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_SLIDE_RENDERER, CriaSlideRenderer))
#define CRIA_SLIDE_RENDERER_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_SLIDE_RENDERER, CriaSlideRendererClass))
#define CRIA_IS_SLIDE_RENDERER(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_SLIDE_RENDERER))
#define CRIA_IS_SLIDE_RENDERER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_SLIDE_RENDERER))
#define CRIA_SLIDE_RENDERER_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_SLIDE_RENDERER, CriaSlideRendererClass))

G_END_DECLS

#endif /* !SLIDE_RENDERER_H */

