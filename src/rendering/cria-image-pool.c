/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <rendering/cria-image-pool.h>

#include <libgnomevfs/gnome-vfs-uri.h>
#include <helpers/gsf-pixbuf.h>

enum {
	PROP_0,
};

enum {
	SIGNAL,
	N_SIGNALS
};

struct _CriaImagePool {
	GObject		  base_instance;
};

static	void	cria_image_pool_get_property	       (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static	void	cria_image_pool_set_property        (GObject		* object,
						guint		  prop_id,
						const	GValue	* value,
						GParamSpec	* param_spec);
#if 0
/* enable these to add support for signals */
static	guint	cria_image_pool_signals[N_SIGNALS] = { 0 };

static	void	cria_image_pool_signal	       (CriaImagePool	* self,
						const	gchar	* string);
#endif

static void
cria_image_pool_class_init (CriaImagePoolClass	* cria_image_pool_class) {
	GObjectClass	* g_object_class;

	g_object_class = G_OBJECT_CLASS(cria_image_pool_class);
#if 0
	/* setting up signal system */
	cria_image_pool_class->signal = cria_image_pool_signal;

	cria_image_pool_signals[SIGNAL] = g_signal_new (
			"signal",
			CRIA_TYPE_IMAGE_POOL,
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (
				CriaImagePoolClass,
				signal),
			NULL,
			NULL,
			g_cclosure_marshal_VOID__STRING,
			G_TYPE_NONE,
			0);
#endif
	/* setting up property system */
	g_object_class->set_property = cria_image_pool_set_property;
	g_object_class->get_property = cria_image_pool_get_property;
}

/**
 * cria_image_pool_get_pixbuf:
 * @image: a #CriaImage
 * @errloc: a return location for a #GError
 *
 * Get the #GdkPixbuf from a #CriaImage.
 *
 * Returns a #GdkPixbuf. ref it as long as you need it
 */
GdkPixbuf*
cria_image_pool_get_pixbuf(CriaImage* image, GError**errloc) {
	gchar		  * str_uri;
	GdkPixbuf         * retval = NULL;
	static GHashTable * cache = NULL;

	g_return_val_if_fail(CRIA_IS_IMAGE(image), NULL);

	if(!cache) {
		cache = g_hash_table_new_full(g_str_hash,
					      g_str_equal,
					      g_free,
					      g_object_unref);
	}
	
	str_uri = g_strdup(cria_image_get_uri(image));
	retval = g_hash_table_lookup(cache, str_uri);
	
	if(!retval) {
		GnomeVFSURI* uri = gnome_vfs_uri_new(cria_image_get_uri(image));
		
		retval = gdk_pixbuf_new_from_uri(uri, errloc);
		g_hash_table_insert(cache, str_uri, retval);

		gnome_vfs_uri_unref(uri);
	} else {
		/* free it only if we already have the image because the string will be
		 * used as the hash table's key */
		g_free(str_uri);
	}
	
	return retval;
}

static void
cria_image_pool_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaImagePool	* self;

	self = CRIA_IMAGE_POOL(object);

	switch(prop_id) {
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object,
						  prop_id,
						  param_spec);
		break;
	}
}

GType
cria_image_pool_get_type(void) {
	static GType	type = 0;

	if(!type) {
		static const GTypeInfo info = {
			sizeof(CriaImagePoolClass),
			NULL,	/* base initializer */
			NULL,	/* base finalizer */
			(GClassInitFunc)cria_image_pool_class_init,
			NULL,	/* class finalizer */
			NULL,	/* class data */
			sizeof(CriaImagePool),
			0,
			NULL,
			0
		};

		type = g_type_register_static(G_TYPE_OBJECT,
					      "CriaImagePool",
					      &info,
					      0);
	}

	return type;
}

static void
cria_image_pool_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaImagePool	* self;
	
	self = CRIA_IMAGE_POOL(object);
	
	switch(prop_id) {
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object,
						  prop_id,
						  param_spec);
		break;
	}
}

