/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005,2006 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <rendering/cria-background-renderer.h>

#define CDEBUG_TYPE cria_background_renderer_get_type
#include <cdebug/cdebug.h>
#include <canvas/rd-pixbuf.h>
#include <canvas/rd-rectangle.h>
#include <helpers/gnome-office-helpers.h>

#include <rendering/cria-image-pool.h>

enum {
	PROP_0,
	PROP_BACKGROUND,
	PROP_POSITION
};

enum {
	N_SIGNALS
};

struct _CriaBackgroundRendererPrivate {
	CriaBackground* background;
	GORect        * position;
	RdItem        * color_background;
	RdItem        * pixmap_background;
};

G_DEFINE_TYPE(CriaBackgroundRenderer, cria_background_renderer, RD_TYPE_ITEM);

static void cria_background_renderer_get_property      (GObject		* object,
							guint		  prop_id,
							GValue		* value,
							GParamSpec	* param_spec);
static void cria_background_renderer_init              (CriaBackgroundRenderer* self);
static void render_background_color                    (CriaBackgroundRenderer* self);
static void render_background_default                  (CriaBackgroundRenderer* self);
static void render_background_pixmap                   (CriaBackgroundRenderer* self);
static void cria_background_renderer_set_property      (GObject		* object,
							guint		  prop_id,
							const	GValue	* value,
							GParamSpec	* param_spec);
#if 0
/* enable these to add support for signals */
static	guint	cria_background_renderer_signals[N_SIGNALS] = { 0 };

static	void	cria_background_renderer_signal	       (CriaBackgroundRenderer	* self,
						const	gchar	* string);
#endif

static void
cbr_dispose(GObject* object) {
	CriaBackgroundRenderer* self = CRIA_BACKGROUND_RENDERER(object);

	if(self->disposed) {
		return;
	}
	self->disposed = TRUE;

	if(self->priv->background) {
		g_object_unref(self->priv->background);
		self->priv->background = NULL;
	}
}

static void
cria_background_renderer_class_init(CriaBackgroundRendererClass* cria_background_renderer_class) {
	GObjectClass	* go_class;

	go_class = G_OBJECT_CLASS(cria_background_renderer_class);

	/* setting up property system */
	go_class->dispose      = cbr_dispose;
	go_class->set_property = cria_background_renderer_set_property;
	go_class->get_property = cria_background_renderer_get_property;

	g_object_class_install_property(go_class,
					PROP_BACKGROUND,
					g_param_spec_object("background",
							    "Background",
							    "The background being displayed by a slide",
							    CRIA_TYPE_BACKGROUND,
							    G_PARAM_READWRITE));
	g_object_class_install_property(go_class,
					PROP_POSITION,
					g_param_spec_boxed("position",
							   "Position",
							   "The position of this background renderer",
							   GO_TYPE_RECT,
							   G_PARAM_READWRITE));
}

CriaBackground*
cria_background_renderer_get_background(CriaBackgroundRenderer* self) {
	g_return_val_if_fail(CRIA_IS_BACKGROUND_RENDERER(self), NULL);

	return self->priv->background;
}

static GORect*
cria_background_renderer_get_position(CriaBackgroundRenderer* self) {
	return self->priv->position;
}

static void
cria_background_renderer_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	CriaBackgroundRenderer	* self;

	self = CRIA_BACKGROUND_RENDERER(object);

	switch(prop_id) {
	case PROP_BACKGROUND:
		g_value_set_object(value, cria_background_renderer_get_background(self));
		break;
	case PROP_POSITION:
		g_value_set_boxed(value, cria_background_renderer_get_position(self));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

static void
cria_background_renderer_init (CriaBackgroundRenderer* self)
{
	cdebugo(self, "init()", "");
	self->priv = g_new0(CriaBackgroundRendererPrivate, 1);
	self->priv->position = g_new0(GORect, 1);
#warning "BackgroundRenderer::init(): FIXME: use setPosition()"
	self->priv->position->right = 5760;
	self->priv->position->bottom = 4320;
}

CriaBackgroundRenderer*
cria_background_renderer_new (RdItem* parent)
{
	RdItem* result;

	g_return_val_if_fail (RD_IS_ITEM (parent), NULL);

	result = g_object_new (CRIA_TYPE_BACKGROUND_RENDERER,
			       NULL);
	rd_item_construct (result, parent);

	return CRIA_BACKGROUND_RENDERER (result);
}

static void
cbr_update_color_bg_position(CriaBackgroundRenderer* self) {
	g_return_if_fail(CRIA_IS_BACKGROUND_RENDERER(self));
	g_return_if_fail(RD_IS_ITEM (self->priv->color_background));

	g_object_set(G_OBJECT(self->priv->color_background),
		     "x1", 0.0 + self->priv->position->left,
		     "y1", 0.0 + self->priv->position->top,
		     "x2", 0.0 + self->priv->position->right,
		     "y2", 0.0 + self->priv->position->bottom,
		     NULL);
}

static void
render_background_with_color (CriaBackgroundRenderer* self,
			      GOColor               * color)
{
	if(!rd_item_is_constructed (RD_ITEM (self))) {
		return;
	}

	g_return_if_fail(CRIA_IS_BACKGROUND_RENDERER(self));

	if(!self->priv->color_background) {
		self->priv->color_background = rd_rectangle_new (RD_ITEM (self));
	}

	if(!color) {
		rd_item_hide (self->priv->color_background);
	} else {
		g_object_set(G_OBJECT(self->priv->color_background),
			     "fill-color-rgba", *color,
			     NULL);
		cbr_update_color_bg_position(self);
		rd_item_show (self->priv->color_background);
	}
}

static void
cria_background_renderer_render(CriaBackgroundRenderer* self) {
#warning "BackgroundRenderer::render(): FIXME: shouldn't most of the code be in setBackground()?"
	CriaBackground* bg;
	gboolean has_background = FALSE;

	g_return_if_fail(CRIA_IS_BACKGROUND_RENDERER(self));
	cdebugo(self, "render()", "start");

	bg = cria_background_renderer_get_background(self);

	if(bg && FALSE) {
		/* create a layer with a gradient */
		render_background_default(self);
#warning "BackgroundRenderer::render(): FIXME: implement gradient background"
		has_background = TRUE;
	} else {
		/* unset possible gradients */
	}

	if(bg && cria_background_get_color(self->priv->background)) {
		/* create a layer with color */
		render_background_color(self);
		has_background = TRUE;
	} else {
		render_background_with_color(self, NULL);
	}

	if(!bg || !has_background) {
		/* render a default white background, this might   *
		 * be neccessary for transparent background images */
		render_background_default(self);
	}

	if(bg && cria_background_get_image(self->priv->background)) {
		/* render the image background */
		cdebugo(self, "render()", "trying to render the background image");
		render_background_pixmap(self);
		cdebugo(self, "render()", "did it");
	} else {
		/* hide image renderers */
		if(self->priv->pixmap_background) {
			rd_item_hide (self->priv->pixmap_background);
		}
	}

	cdebugo(self, "render()", "end");
}

static void
render_background_color(CriaBackgroundRenderer* self) {
	render_background_with_color(self, cria_background_get_color(cria_background_renderer_get_background(self)));
}

static void
render_background_default(CriaBackgroundRenderer* self) {
	GOColor	color = RGBA_WHITE;
	render_background_with_color(self, &color);
}

static void
cbr_update_pixmap_bg_position(CriaBackgroundRenderer* self) {
#define POS_X 0.0 + self->priv->position->left
#define POS_Y 0.0 + self->priv->position->top
#define POS_W 0.0 + (self->priv->position->right - self->priv->position->left)
#define POS_H 0.0 + (self->priv->position->bottom - self->priv->position->top)
	
	g_object_set(self->priv->pixmap_background,
		     "x", POS_X,
		     "y", POS_Y,
		     "height", POS_H,
		     "height-set", TRUE,
		     "width", POS_W,
		     "width-set", TRUE,
		     NULL);
	
	cdebug("updatePixmapBgPosition()", "x: %f", POS_X);
	cdebug("updatePixmapBgPosition()", "y: %f", POS_Y);
	cdebug("updatePixmapBgPosition()", "w: %f", POS_W);
	cdebug("updatePixmapBgPosition()", "h: %f", POS_H);
		
#undef POS_X
#undef POS_Y
#undef POS_W
#undef POS_H
}

static void
render_background_pixmap(CriaBackgroundRenderer* self) {
	g_return_if_fail(CRIA_IS_BACKGROUND_RENDERER(self));
	
	if(!self->priv->pixmap_background) {
		self->priv->pixmap_background = rd_pixbuf_new (RD_ITEM (self));
	}

	cbr_update_pixmap_bg_position(self);

	if(TRUE) { /* is_stretched */
		GError		* error = NULL;
		GdkPixbuf	* pixbuf = cria_image_pool_get_pixbuf(cria_background_get_image(cria_background_renderer_get_background(self)), &error);

		if(pixbuf == NULL) {
#warning "renderBackgroundPixmap(): FIXME: add GTK_STOCK_MISSING_IMAGE"
			cdebugo(self, "renderBackgroundPixmap()", "got no pixmap");
		}

		if(error) {
#warning "renderBackgroundPixmap(): FIXME: push out a warning here"
			cdebugo(self, "renderBackgroundPixmap()", "got the error %s", error->message);
			g_error_free(error);
			error = NULL;
		}

		g_object_set(self->priv->pixmap_background,
			     "pixbuf", pixbuf,
			     NULL);
#warning "renderBackground(): FIXME: move the show instruction to some saner place"
		rd_item_show (self->priv->pixmap_background);
	} else if(FALSE) { /* is scaled */
#warning "renderBackgroundPixmap(): FIXME: implement scaled background"
		render_background_default(self);
	} else if(FALSE) { /* is tiled */
#warning "renderBackgroundPixmap(): FIXME: implement tiled background"
		render_background_default(self);
	} else if(FALSE) { /* is aligned */
#warning "renderBackgroundPixmap(): FIXME: implement aligned background"
		render_background_default(self);
	} else {
		g_warning("This case should not be reached");
	}
}

void
cria_background_renderer_set_background(CriaBackgroundRenderer* self, CriaBackground* background) {
	g_return_if_fail(CRIA_IS_BACKGROUND_RENDERER(self));
	g_return_if_fail(background == NULL || CRIA_IS_BACKGROUND(background));

	cdebugo(self, "setBackground()", "start");

#warning "setBackground(): FIXME: Do we want to solve the not completed init problem like this or do we want to solve it in ::init()?"
	if(self->priv->background == background && background != NULL) {
		return;
	}

	if(self->priv->background != NULL) {
		g_object_unref(self->priv->background);
		self->priv->background = NULL;
	}

	if(background) {
		self->priv->background = g_object_ref(background);
	}

	cria_background_renderer_render(self);
	g_object_notify(G_OBJECT(self), "background");
	cdebugo(self, "setBackground()", "end");
}

void
cria_background_renderer_set_position(CriaBackgroundRenderer* self, const GORect* position) {
	g_return_if_fail(position);
	g_return_if_fail(self->priv->position);

	if(go_rect_equals(position, self->priv->position)) {
		return;
	}

	*(self->priv->position) = *position;

	cbr_update_color_bg_position(self);
	if(self->priv->pixmap_background) {
		cbr_update_pixmap_bg_position(self);
	}

	g_object_notify(G_OBJECT(self), "position");
}

static void
cria_background_renderer_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	CriaBackgroundRenderer	* self;

	self = CRIA_BACKGROUND_RENDERER(object);

	switch(prop_id) {
	case PROP_BACKGROUND:
		cria_background_renderer_set_background(self, g_value_get_object(value));
		break;
	case PROP_POSITION:
		cria_background_renderer_set_position(self, g_value_get_boxed(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

