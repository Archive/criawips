/* This file is part of criawips
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005,2007 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef BLOCK_RENDERER_H
#define BLOCK_RENDERER_H

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <canvas/rd-canvas-private.h>

#include <dom/cria-block.h>

G_BEGIN_DECLS

typedef struct _CriaBlockRenderer CriaBlockRenderer;
typedef struct _CriaBlockRendererClass CriaBlockRendererClass;
typedef struct _CriaBlockRendererPrivate CriaBlockRendererPrivate;

GType      cria_block_renderer_get_type	       (void);
CriaBlock* cria_block_renderer_get_block       (CriaBlockRenderer const	* self);
void       cria_block_renderer_hide            (CriaBlockRenderer	* self);
void       cria_block_renderer_show            (CriaBlockRenderer	* self);
CriaBlockRenderer* cria_block_renderer_new     (RdItem  		* parent,
						gboolean		  editable);
void       cria_block_renderer_set_block       (CriaBlockRenderer	* self,
						CriaBlock		* block);

#define CRIA_TYPE_BLOCK_RENDERER			(cria_block_renderer_get_type())
#define CRIA_BLOCK_RENDERER(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_BLOCK_RENDERER, CriaBlockRenderer))
#define CRIA_BLOCK_RENDERER_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_BLOCK_RENDERER, CriaBlockRendererClass))
#define CRIA_IS_BLOCK_RENDERER(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_BLOCK_RENDERER))
#define CRIA_IS_BLOCK_RENDERER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_BLOCK_RENDERER))
#define CRIA_BLOCK_RENDERER_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_BLOCK_RENDERER, CriaBlockRendererClass))

G_END_DECLS

#endif /* !BLOCK_RENDERER_H */

