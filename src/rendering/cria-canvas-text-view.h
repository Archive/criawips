/* This file is part of criawips
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005,2007 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CRIAWIPS_CANVAS_TEXT_VIEW_H
#define CRIAWIPS_CANVAS_TEXT_VIEW_H

#include <canvas/rd-text.h>

G_BEGIN_DECLS

typedef struct _CriaCanvasTextView      CriaCanvasTextView;
typedef struct _CriaCanvasTextViewClass CriaCanvasTextViewClass;

#define CRIA_TYPE_CANVAS_TEXT_VIEW         (cria_canvas_text_view_get_type())
#define CRIA_CANVAS_TEXT_VIEW(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), CRIA_TYPE_CANVAS_TEXT_VIEW, CriaCanvasTextView))
#define CRIA_CANVAS_TEXT_VIEW_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), CRIA_TYPE_CANVAS_TEXT_VIEW, CriaCanvasTextViewClass))
#define CRIA_IS_CANVAS_TEXT_VIEW(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), CRIA_TYPE_CANVAS_TEXT_VIEW))
#define CRIA_IS_CANVAS_TEXT_VIEW_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), CRIA_TYPE_CANVAS_TEXT_VIEW))
#define CRIA_CANVAS_TEXT_VIEW_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), CRIA_TYPE_CANVAS_TEXT_VIEW, CriaCanvasTextViewClass))

GType   cria_canvas_text_view_get_type (void);
RdItem* cria_canvas_text_view_new      (RdItem* parent);

G_END_DECLS

#endif /* CRIAWIPS_CANVAS_TEXT_VIEW_H */

