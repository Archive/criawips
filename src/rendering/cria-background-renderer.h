/* This file is part of criawips
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2006,2007 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef BACKGROUND_RENDERER_H
#define BACKGROUND_RENDERER_H

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <canvas/rd-canvas-private.h>

#include <dom/cria-slide.h>

G_BEGIN_DECLS

typedef struct _CriaBackgroundRenderer        CriaBackgroundRenderer;
typedef struct _CriaBackgroundRendererPrivate CriaBackgroundRendererPrivate;
typedef struct _CriaBackgroundRendererClass   CriaBackgroundRendererClass;

CriaBackground*         cria_background_renderer_get_background(CriaBackgroundRenderer* self);
GType                   cria_background_renderer_get_type      (void);
#warning "FIXME: new() should return an RdItem"
CriaBackgroundRenderer* cria_background_renderer_new	       (RdItem                * parent);
void                    cria_background_renderer_set_background(CriaBackgroundRenderer* self,
								CriaBackground        * background);
void			cria_background_renderer_set_position  (CriaBackgroundRenderer* self,
								const GORect	      * position);

#define CRIA_TYPE_BACKGROUND_RENDERER			(cria_background_renderer_get_type())
#define CRIA_BACKGROUND_RENDERER(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_BACKGROUND_RENDERER, CriaBackgroundRenderer))
#define CRIA_BACKGROUND_RENDERER_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_BACKGROUND_RENDERER, CriaBackgroundRendererClass))
#define CRIA_IS_BACKGROUND_RENDERER(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_BACKGROUND_RENDERER))
#define CRIA_IS_BACKGROUND_RENDERER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_BACKGROUND_RENDERER))
#define CRIA_BACKGROUND_RENDERER_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_BACKGROUND_RENDERER, CriaBackgroundRendererClass))

struct _CriaBackgroundRenderer {
	RdItem            base_instance;
	gboolean          disposed;
	CriaBackgroundRendererPrivate
			* priv;
};

struct _CriaBackgroundRendererClass {
	RdItemClass       base_class;
};

G_END_DECLS

#endif /* !BACKGROUND_RENDERER_H */

