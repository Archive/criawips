/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef IMAGE_POOL_H
#define IMAGE_POOL_H

#include <glib-object.h>
#include <gdk/gdkpixbuf.h>

#include <dom/cria-image.h>

G_BEGIN_DECLS

typedef struct _CriaImagePool CriaImagePool;
typedef struct _GObjectClass CriaImagePoolClass;

GType		cria_image_pool_get_type       (void);
GdkPixbuf*	cria_image_pool_get_pixbuf     (CriaImage	* image,
						GError		**errloc);

#define CRIA_TYPE_IMAGE_POOL			(cria_image_pool_get_type ())
#define CRIA_IMAGE_POOL(object)			(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_IMAGE_POOL, CriaImagePool))
#define CRIA_IMAGE_POOL_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_IMAGE_POOL, CriaImagePoolClass))
#define CRIA_IS_IMAGE_POOL(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_IMAGE_POOL))
#define CRIA_IS_IMAGE_POOL_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_IMAGE_POOL))
#define CRIA_IMAGE_POOL_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_IMAGE_POOL, CriaImagePoolClass))

G_END_DECLS

#endif /* !IMAGE_POOL_H */

