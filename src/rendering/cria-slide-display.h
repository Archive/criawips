/* This file is part of criawips
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005,2007 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef SLIDE_DISPLAY_H
#define SLIDE_DISPLAY_H

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <canvas/rd-canvas-private.h>

#include <dom/cria-slide.h>
#include <rendering/cria-slide-renderer.h>

G_BEGIN_DECLS

typedef struct _CriaSlideDisplay CriaSlideDisplay;
typedef struct _CriaSlideDisplayClass CriaSlideDisplayClass;

GType		  cria_slide_display_get_type	    (void);
CriaSlide*        cria_slide_display_get_slide      (CriaSlideDisplay* self);
gdouble		  cria_slide_display_get_slide_zoom (CriaSlideDisplay* self);
CriaSlideDisplay* cria_slide_display_new            (CriaSlideRenderer* csr);
void              cria_slide_display_set_slide      (CriaSlideDisplay* self,
						     CriaSlide       * slide);
void		  cria_slide_display_set_slide_zoom (CriaSlideDisplay* self,
						     gdouble	       slide_zoom);
GdkPixbuf*        cria_slide_display_get_preview    (CriaSlide       * slide);

#define CRIA_TYPE_SLIDE_DISPLAY			(cria_slide_display_get_type())
#define CRIA_SLIDE_DISPLAY(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), CRIA_TYPE_SLIDE_DISPLAY, CriaSlideDisplay))
#define CRIA_SLIDE_DISPLAY_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), CRIA_TYPE_SLIDE_DISPLAY, CriaSlideDisplayClass))
#define CRIA_IS_SLIDE_DISPLAY(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), CRIA_TYPE_SLIDE_DISPLAY))
#define CRIA_IS_SLIDE_DISPLAY_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), CRIA_TYPE_SLIDE_DISPLAY))
#define CRIA_SLIDE_DISPLAY_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), CRIA_TYPE_SLIDE_DISPLAY, CriaSlideDisplayClass))

G_END_DECLS

#endif /* !SLIDE_DISPLAY_H */

