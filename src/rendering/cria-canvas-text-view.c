/* this file is part of criawips
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005,2007 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <rendering/cria-canvas-text-view-priv.h>

#include <string.h>

/* gtype stuff */
G_DEFINE_TYPE(CriaCanvasTextView, cria_canvas_text_view, CRIA_TYPE_CANVAS_TEXT);

enum {
	PROP_0,
	PROP_MODEL
};

RdItem*
cria_canvas_text_view_new (RdItem* parent)
{
	RdItem* result;

	g_return_val_if_fail (RD_IS_ITEM (parent), NULL);

	result = g_object_new (CRIA_TYPE_CANVAS_TEXT_VIEW, NULL);
	rd_item_construct (result, parent);

	return result;
}

static void
cria_canvas_text_view_init(CriaCanvasTextView* self) {
}

static void
cctv_model_notify_text(CriaCanvasTextView* self) {
	gchar const* text_to_set = cria_text_model_get_text(self->model);
	gchar const* text_set    = pango_layout_get_text (rd_text_get_layout (CRIA_CANVAS_TEXT (self)));

	if(strcmp(text_to_set, text_set)) {
		g_object_set(self, "text", text_to_set, NULL);
	}
}

static void
cctv_unset_model(CriaCanvasTextView* self) {
	cria_canvas_text_reset_cursor(CRIA_CANVAS_TEXT(self));
	g_signal_handlers_disconnect_by_func(self->model, cctv_model_notify_text, self);
	g_object_unref(self->model);
	self->model = NULL;
}

static void
cctv_dispose(GObject* object)
{
	CriaCanvasTextView* self = CRIA_CANVAS_TEXT_VIEW(object);

	if(self->model) {
		cctv_unset_model(self);
	}

	G_OBJECT_CLASS(cria_canvas_text_view_parent_class)->dispose(object);
}

static void
cctv_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
	CriaCanvasTextView* self = CRIA_CANVAS_TEXT_VIEW(object);

	switch(prop_id) {
	case PROP_MODEL:
		g_value_set_object(value, self->model);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void
canvas_text_view_set_model(CriaCanvasTextView* self,
			   CriaTextModel     * model)
{
	g_return_if_fail(CRIA_IS_CANVAS_TEXT_VIEW(self));
	g_return_if_fail(CRIA_IS_TEXT_MODEL(model));

	if(self->model) {
		cctv_unset_model(self);
	}

	if(model) {
		self->model = g_object_ref(model);
		g_signal_connect_swapped(self->model, "notify::text",
					 G_CALLBACK(cctv_model_notify_text), self);
	}

	g_object_notify(G_OBJECT(self), "model");
}

static void
cctv_set_property(GObject* object, guint prop_id, GValue const* value, GParamSpec* pspec) {
	CriaCanvasTextView* self = CRIA_CANVAS_TEXT_VIEW(object);

	switch(prop_id) {
	case PROP_MODEL:
		canvas_text_view_set_model(self, g_value_get_object(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void
cctv_delete(CriaCanvasText* text, gsize offset, gsize length) {
	CriaCanvasTextView* self = CRIA_CANVAS_TEXT_VIEW(text);
	cria_text_model_delete(self->model, offset, length);
}

static void
cctv_insert(CriaCanvasText* text, gchar const* value, gsize offset) {
	CriaCanvasTextView* self = CRIA_CANVAS_TEXT_VIEW(text);
	cria_text_model_insert(self->model, value, offset);
}

static void
cria_canvas_text_view_class_init(CriaCanvasTextViewClass* self_class) {
	GObjectClass       * go_class;
	CriaCanvasTextClass* cct_class;

	/* setting up the GObjectClass */
	go_class = G_OBJECT_CLASS(self_class);
	go_class->dispose      = cctv_dispose;
	go_class->get_property = cctv_get_property;
	go_class->set_property = cctv_set_property;

	g_object_class_install_property(go_class,
					PROP_MODEL,
					g_param_spec_object("model",
							    "text model",
							    "the text model that's being displayed and edited by this view",
							    CRIA_TYPE_TEXT_MODEL,
							    G_PARAM_READWRITE));

	/* setting up the CriaCanvasTextClass */
	cct_class = CRIA_CANVAS_TEXT_CLASS(self_class);
	cct_class->delete = cctv_delete;
	cct_class->insert = cctv_insert;
}

