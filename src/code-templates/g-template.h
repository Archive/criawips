/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <glib-object.h>

#ifndef G_TEMPLATE_H
#define G_TEMPLATE_H

G_BEGIN_DECLS

typedef struct _GTemplate GTemplate;
typedef struct _GTemplateClass GTemplateClass;
typedef struct _GTemplatePrivate GTemplatePrivate;

GType		g_template_get_type	       (void);
const gchar*	g_template_get_attribute       (GTemplate	* self);
void		g_template_set_attribute       (GTemplate	* self,
						const char	* attribute);

#define G_TYPE_TEMPLATE			(g_template_get_type())
#define G_TEMPLATE(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), G_TYPE_TEMPLATE, GTemplate))
#define G_TEMPLATE_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), G_TYPE_TEMPLATE, GTemplateClass))
#define G_IS_TEMPLATE(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), G_TYPE_TEMPLATE))
#define G_IS_TEMPLATE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), G_TYPE_TEMPLATE))
#define G_TEMPLATE_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS((object), G_TYPE_TEMPLATE, GTemplateClass))

struct _GTemplate {
	GObject		  base_instance;
	gchar		* attribute;
};

struct _GTemplateClass {
	GObjectClass	  base_class;

	/* signals */
	void (*signal)	       (GTemplate	* self,
				const	gchar	* string);
};

G_END_DECLS

#endif /* !G_TEMPLATE_H */

