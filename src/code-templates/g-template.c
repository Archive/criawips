/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "g-template.h"

#include <glib.h>
#include <glib-object.h>

enum {
	PROP_0,
	PROP_ATTRIBUTE
};

enum {
	SIGNAL,
	N_SIGNALS
};

static GObjectClass* parent_class = NULL;

static void g_template_get_property	       (GObject		* object,
						guint		  prop_id,
						GValue		* value,
						GParamSpec	* param_spec);
static void g_template_set_property	       (GObject		* object,
						guint		  prop_id,
						const GValue	* value,
						GParamSpec	* param_spec);
#if 0
/* enable these to add support for signals */
static	guint	g_template_signals[N_SIGNALS] = { 0 };

static	void	g_template_signal	       (GTemplate	* self,
						const	gchar	* string);
#endif

static void
g_template_class_init(GTemplateClass* g_template_class) {
	GObjectClass	* g_object_class;

	parent_class = g_type_class_peek_parent(g_template_class);
	g_object_class = G_OBJECT_CLASS(g_template_class);
#if 0
	/* setting up signal system */
	g_template_class->signal = g_template_signal;

	g_template_signals[SIGNAL] = g_signal_new("signal",
						  G_TYPE_TEMPLATE,
						  G_SIGNAL_RUN_LAST,
						  G_STRUCT_OFFSET(GTemplateClass,
							  	  signal),
						  NULL,
						  NULL,
						  g_cclosure_marshal_VOID__STRING,
						  G_TYPE_NONE,
						  0);
#endif
	/* setting up property system */
	g_object_class->set_property = g_template_set_property;
	g_object_class->get_property = g_template_get_property;

	g_object_class_install_property(g_object_class,
					PROP_ATTRIBUTE,
					g_param_spec_string("attribute",
							    "Attribute",
							    "A simple unneccessary attribute that does nothing special except "
							    "being a demonstration for the correct implementation of a GObject "
							    "property",
							    "default_value",
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
}

const char*
g_template_get_attribute(GTemplate* self) {
	g_return_val_if_fail(G_IS_TEMPLATE(self), NULL);
	
	return self->attribute;
}

static void
g_template_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* param_spec) {
	GTemplate	* self;

	self = G_TEMPLATE(object);

	switch(prop_id) {
	case PROP_ATTRIBUTE:
		g_value_set_string(value, self->attribute);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

GType
g_template_get_type(void) {
	static GType	type = 0;

	if(!type) {
		static const GTypeInfo info = {
			sizeof(GTemplateClass),
			NULL,	/* base initializer */
			NULL,	/* base finalizer */
			(GClassInitFunc)g_template_class_init,
			NULL,	/* class finalizer */
			NULL,	/* class data */
			sizeof(GTemplate),
			0,
			NULL,
			0
		};

		type = g_type_register_static(G_TYPE_OBJECT,
					      "GTemplate",
					      &info,
					      0);
	}

	return type;
}

void
g_template_set_attribute(GTemplate* self, const gchar* attribute) {
	g_return_if_fail(G_IS_TEMPLATE(self));
	g_return_if_fail(attribute != NULL);

	if(self->attribute != NULL) {
		g_free(self->attribute);
	}

	self->attribute = g_strdup(attribute);

	g_object_notify(G_OBJECT(self), "attribute");
}

static void
g_template_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* param_spec) {
	GTemplate	* self;
	
	self = G_TEMPLATE(object);
	
	switch(prop_id) {
	case PROP_ATTRIBUTE:
		g_template_set_attribute(self, g_value_get_string(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, param_spec);
		break;
	}
}

