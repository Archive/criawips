#!/bin/sh

# This file is part of criawips
#
# AUTHORS
#       Sven Herzberg        <herzi@gnome-de.org>
#
# Copyright (C) 2004,2007 Sven Herzberg
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

# FIXME check for given argument
# FIXME make sure the class is specified as Prefix::Type
# FIXME make more flexible via optional rc-file
# FIXME improve copyright header of template files

CLASS_NAME=$1

echo
echo "Creating class $CLASS_NAME"
echo

CLASS_CAST=`echo "$CLASS_NAME" | sed 's/\([[:upper:]]\)/_\1/g' | sed 's/^_\([[:upper:]]\)/\1/' | sed 's/:://g' | awk '{print (toupper ($0))}'`
METHOD_PRE=`echo "$CLASS_NAME" | sed 's/\([[:upper:]]\)/_\1/g' | sed 's/^_\([[:upper:]]\)/\1/' | sed 's/:://g' | awk '{print (tolower ($0) "_")}'`
CLASS_TYPE=`echo "$CLASS_NAME" | sed 's/\([[:upper:]]\)/_\1/g' | sed 's/^_\([[:upper:]]\)/\1/' | sed 's/::/_TYPE/g' | awk '{print (toupper ($0))}'`
BASE_CHECK=`echo "$CLASS_NAME" | sed 's/\([[:upper:]]\)/_\1/g' | sed 's/^_\([[:upper:]]\)/\1/' | sed 's/::/_IS/g' | awk '{print (toupper ($0))}'`
FILENAME=`echo "$CLASS_CAST" | sed 's/_/-/g'  | awk '{print (tolower ($0))}'`
HEADER_DEF=`echo "$CLASS_NAME" | sed 's/\([[:upper:]]\)/_\1/g' | sed 's/^_[[:alpha:]]*::_//'  | awk '{print (toupper ($0 "_H"))}'`
C_TYPE=`echo "$CLASS_NAME" | sed 's/:://g'`

if [ -f $FILENAME.c ] ; then
	echo "File $FILENAME.c already exists"
	exit;
fi

if [ -f $FILENAME.h ] ; then
	echo "File $FILENAME.h already exists"
	exit;
fi

echo "Creating class $CLASS_NAME"
echo

# TODO: that's for the usern detection:
# /bin/grep herzi /etc/passwd | cut -d: -f5 | cut -d, -f1

echo "Fixing Filenames..."
sed "s/G_TEMPLATE_H/$HEADER_DEF/g" g-template.h > step1.h
sed "s/g-template.h/$FILENAME.h/g" g-template.c > step1.c

echo "Fixing macros..."
sed "s/G_TYPE_TEMPLATE/$CLASS_TYPE/g"	step1.h > step2.h
sed "s/G_TYPE_TEMPLATE/$CLASS_TYPE/g"	step1.c > step2.c
sed "s/G_TEMPLATE/$CLASS_CAST/g"	step2.h > step3.h
sed "s/G_TEMPLATE/$CLASS_CAST/g"	step2.c > step3.c
sed "s/G_IS_TEMPLATE/$BASE_CHECK/g"	step3.h > step4.h
sed "s/G_IS_TEMPLATE/$BASE_CHECK/g"     step3.c > step4.c

echo "fixing class names..."
sed "s/GTemplate/$C_TYPE/g"	step4.h > step5.h
sed "s/GTemplate/$C_TYPE/g"	step4.c > step5.c

echo "Fixing method names..."
sed "s/g_template_\([a-z]\)/$METHOD_PRE\1/g" step5.h > $FILENAME.h
sed "s/g_template_\([a-z]\)/$METHOD_PRE\1/g" step5.c > $FILENAME.c

echo "Cleaning up..."
rm -rf step[12345].[ch]

echo "New Files:"
echo "  $FILENAME.c"
echo "  $FILENAME.h"

