/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *       Ezequiel P�rez       <eazel7@yahoo.com.ar>
 *
 * Copyright (C) 2004 Sven Herzberg
 * Copyright (C) 2004 Ezequiel P�rez
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <io/cria-presentation-writer.h>

#include <libgnomevfs/gnome-vfs-uri.h>
#include <gsf-gnome/gsf-output-gnomevfs.h>
#include <gsf/gsf-libxml.h>
#include <goffice/utils/go-color.h>

#define CDEBUG_TYPE -1
#include <cdebug/cdebug.h>

#include <dom/cria-block.h>
#include <dom/cria-format.h>
#include <dom/cria-slide.h>
#include <dom/cria-theme.h>

static	void	write_slide_xml			(CriaSlide *self,
						 GsfXMLOut *xmlout,
						 gboolean theme);
static	void	write_presentation_xml		(CriaPresentation *self,
						 GsfXMLOut *xmlout);

static void
write_master_slide_func(CriaSlide* self, GsfXMLOut* xmlout) {
	cdebug("PresentationWriter::writeMasterSlideHFunc()", "start");
	write_slide_xml(self, xmlout, TRUE);
	cdebug("PresentationWriter::writeMasterSlideHFunc()", "master slide written");
	cdebug("PresentationWriter::writeMasterSlideHFunc()", "finish");
}

static void
write_theme_xml(CriaTheme *self, GsfXMLOut *xmlout) {
	CriaSlide* default_slide;
	GList* slides;
	
	cdebug("PresentationWriter::writeThemeXml()", "start");
	gsf_xml_out_start_element(xmlout, "theme");
	cdebug("PresentationWriter::writeThemeXml()", "theme element created");
	
	gsf_xml_out_add_cstr(xmlout, "name", cria_theme_get_name(self));
 	cdebug("PresentationWriter::writeThemeXml()", "name attribute set");

	default_slide = cria_theme_get_default_slide(self);
	if(default_slide) {
		gsf_xml_out_add_cstr(xmlout, "default", cria_slide_get_title(default_slide));
	}

	slides = cria_theme_get_master_slides(self);
	g_list_foreach(slides, (GFunc)write_master_slide_func, xmlout);
	g_list_free(slides);
	slides = NULL;
	
	cdebug("PresentationWriter::writeThemeXml()", "master slides written");
	gsf_xml_out_end_element(xmlout);
	cdebug("PresentationWriter::writeMasterSlideHFunc()", "finish");
}

static void
write_block_xml(CriaBlock* self,
		GOPoint  * slide_size,
		GsfXMLOut* xmlout)
{
	GORect const* rect;
	gchar * text;

	cdebug("PresentationWriter::writeBlockXML()", "started");
	gsf_xml_out_start_element(xmlout, "block");
	cdebug("PresentationWriter::writeBlockXML()", "block element created");
	gsf_xml_out_add_cstr(xmlout, "title", cria_slide_element_get_name(CRIA_SLIDE_ELEMENT(self)));
	cdebug("PresentationWriter::writeBlockXML()", "title attribute set");

	switch(cria_block_get_alignment(self)) {
		case CRIA_ALIGNMENT_LEFT:
			gsf_xml_out_add_cstr(xmlout, "align", "left");
			cdebug("PresentationWriter::writeBlockXML()", "align attribute set left");
			break;
		case CRIA_ALIGNMENT_CENTER:
			gsf_xml_out_add_cstr(xmlout, "align", "center");
			cdebug("PresentationWriter::writeBlockXML()", "align attribute set center");
			break;
		case CRIA_ALIGNMENT_RIGHT:
			gsf_xml_out_add_cstr(xmlout, "align", "right");
			cdebug("PresentationWriter::writeBlockXML()", "align attribute set right");
			break;
		case CRIA_ALIGNMENT_JUSTIFY:
			gsf_xml_out_add_cstr(xmlout, "align", "justify");
			cdebug("PresentationWriter::writeBlockXML()", "align attribute set justify");
			break;
		case CRIA_ALIGNMENT_UNSET:
			/* don't write if it's not set */
			break;
		default:
			g_warning("got an unknown value for CriaAlignment: %d", cria_block_get_alignment(self));
			break;
	}

	switch(cria_block_get_valignment(self)) {
		case CRIA_ALIGNMENT_TOP:
			gsf_xml_out_add_cstr(xmlout, "valign", "top");
			cdebug("PresentationWriter::writeBlockXML()", "valign attribute set top");
			break;
		case CRIA_ALIGNMENT_MIDDLE:
			gsf_xml_out_add_cstr(xmlout, "valign", "middle");
			cdebug("PresentationWriter::writeBlockXML()", "valign attribute set middle");
			break;
		case CRIA_ALIGNMENT_BOTTOM:
			gsf_xml_out_add_cstr(xmlout, "valign", "bottom");
			cdebug("PresentationWriter::writeBlockXML()", "valign attribute set bottom");
			break;
		case CRIA_VALIGNMENT_UNSET:
			break;
		default:
			g_warning("got an unknown value for CriaVAlignment: %d", cria_block_get_valignment(self));
			break;
	}

	rect = cria_slide_element_get_position (CRIA_SLIDE_ELEMENT(self));
	if(rect) {
		gdouble values[4] = {
			100.0 * rect->left   / slide_size->x,
			100.0 * rect->top    / slide_size->y,
			100.0 * (rect->right - rect->left) / slide_size->x,
			100.0 * (rect->bottom - rect->top) / slide_size->y
		};
		static gchar const* const names[4] = {
			"x", "y", "width", "height"
		};
		guint i = 0;
		for(; i < G_N_ELEMENTS(values); i++) {
			gchar  buffer[32];
			gchar* text;
			text = g_ascii_formatd(buffer, sizeof(buffer), "%.1f", values[i]);
			text = g_strdup_printf("%s%%", text);
			gsf_xml_out_add_cstr (xmlout, names[i], text);
			cdebug("PresentationWriter::writeBlockXML()", "%s attribute set", names[i]);
			g_free(text);
		}
	}

	text = cria_format_get(cria_block_get_format(self), "font-family");
	gsf_xml_out_add_cstr(xmlout, "family", text);
	g_free(text);
	cdebug("PresentationWriter::writeBlockXML()", "family attribute set");

	text = cria_format_get(cria_block_get_format(self), "font-size");
	gsf_xml_out_add_cstr(xmlout, "size", text);
	g_free(text);
	cdebug("PresentationWriter::writeBlockXML()", "size attribute set");

	{
		GOColor const* color = cria_block_get_color(self);
		if(color) {
			gchar* str = g_strdup_printf("#%.6x", (*color >> 8));
			gsf_xml_out_add_cstr(xmlout, "color", str);
			g_free(str);
		}

		if(color && (*color & 0xFF) != 0xFF) {
			g_warning("The writer doesn't support alpha for color yet");
		}
	}

	{
		CriaSlideElement* master = cria_slide_element_get_master(CRIA_SLIDE_ELEMENT(self));
		if(master) {
			gsf_xml_out_add_cstr(xmlout, "layout", cria_slide_element_get_name(master));
		}
	}
	
	{
		GString* output = g_string_new("\n");
		CriaTextModel* model = cria_block_get_model(self);
		gchar const* text = model ? cria_text_model_get_text(model) : NULL;
		gchar const* iterator;
		for(iterator = text; iterator && *iterator; iterator++) {
			if(*iterator == '\n') {
				gsf_xml_out_add_cstr(xmlout, NULL, output->str);
				gsf_xml_out_add_cstr_unchecked(xmlout, NULL, "<br/>\n");
				g_string_truncate(output, 0);
			} else {
				g_string_append_c(output, *iterator);
			}
		}

		if(output->len > 1) {
			gsf_xml_out_add_cstr(xmlout, NULL, output->str);
		}
		cdebug("PresentationWriter::writeBlockXML()", "block contents written");
		g_string_free(output, TRUE);
	}

	gsf_xml_out_end_element(xmlout);
	cdebug("PresentationWriter::writeBlockXML()", "block element closed");
	cdebug("PresentationWriter::writeBlockXML()", "finished");
}

static void
write_background_element (CriaBackground* self,
			  GsfXMLOut     * xmlout)
{
	CriaImage* image;
	GOColor  * color;

	if (!self) return;

	image = cria_background_get_image (self);
	color = cria_background_get_color (self);

	if (!image && !color) return;

	gsf_xml_out_start_element (xmlout, "background");
	if (image) {
		gchar* string = g_path_get_basename (cria_image_get_uri (image));
		gsf_xml_out_add_cstr (xmlout, "file", string);
		g_free (string);
	}

	if (color) {
		gchar* string = g_strdup_printf ("#%2.x%2.x%2.x",
						 UINT_RGBA_R (*color),
						 UINT_RGBA_G (*color),
						 UINT_RGBA_B (*color));
		gsf_xml_out_add_cstr (xmlout, "color", string);
		g_free (string);
	}
	gsf_xml_out_end_element (xmlout);
}

static void
write_slide_xml(CriaSlide *self, GsfXMLOut *xmlout, gboolean theme) {
	GList  *elements;
	GList  *element;
	GOPoint const* size;

	cdebug("PresentationWriter::writeSlideXML()", "start");
	if (!theme) {
		gsf_xml_out_start_element(xmlout, "slide");
		cdebug("PresentationWriter::writeSlideXML()", "slide element created");
		gsf_xml_out_add_cstr(xmlout, "title", cria_slide_get_title (self));
		cdebug("PresentationWriter::writeSlideXML()", "title attribute set");
		gsf_xml_out_add_cstr(xmlout, "layout", cria_slide_get_title (cria_slide_get_master_slide(self)));
		cdebug("PresentationWriter::writeSlideXML()", "layout attribute set");
	} else {
		gsf_xml_out_start_element(xmlout, "layout");
		cdebug("PresentationWriter::writeSlideXML()", "layout element created");
		gsf_xml_out_add_cstr(xmlout, "name", cria_slide_get_title (self));
		cdebug("PresentationWriter::writeSlideXML()", "name attribute set");
	}

	write_background_element (cria_slide_get_background (self, TRUE),
				  xmlout);

	elements = cria_slide_get_elements(self);
	cdebug("PresentationWriter::writeSlideXML()", "got elements");
	size = cria_slide_get_size(self);
	for(element = elements; element; element = element->next) {
		if(!cria_slide_element_is_empty(element->data)) {
			write_block_xml(CRIA_BLOCK(element->data), size, xmlout);
		}
		cdebug("PresentationWriter::writeSlideXML()", "got block written");
	}

	gsf_xml_out_end_element(xmlout);
	cdebug("PresentationWriter::writeSlideXML()", "block element closed");
	cdebug("PresentationWriter::writeSlideXML()", "finished");
}

static void
write_presentation_xml(CriaPresentation *self, GsfXMLOut *xmlout) {
	guint  slide_n;

	cdebug("PresentationWriter::writePresentationXML()", "start");
	g_hash_table_new_full(g_str_hash, g_str_equal, g_free, NULL);
	cdebug("PresentationWriter::writePresentationXML()", "themes hash table created");
	gsf_xml_out_start_element(xmlout, "presentation");
	cdebug("PresentationWriter::writePresentationXML()", "presentation element created");
	gsf_xml_out_add_cstr(xmlout, "title", cria_presentation_get_title(self));
	cdebug("PresentationWriter::writePresentationXML()", "title attribute set");
#warning "writePresentationXml(): FIXME: Implement the author element"

	write_theme_xml(cria_presentation_get_theme(self), xmlout);

	cdebug("PresentationWriter::writePresentationXML()", "theme elements written");

	for(slide_n = 0; slide_n < cria_slide_list_n_slides(CRIA_SLIDE_LIST(self)); slide_n++) {
		CriaSlide *slide = cria_slide_list_get(CRIA_SLIDE_LIST(self), slide_n);
		write_slide_xml(slide, xmlout, FALSE);
		cdebug("PresentationWriter::writePresentationXML()", "slide written");
	}
	gsf_xml_out_end_element(xmlout);
	cdebug("PresentationWriter::writePresentationXml()", "presentation element closed");
	cdebug("PresentationWriter::writePresentationXml()", "finished");
}

void
cria_presentation_save_to_file(CriaPresentation* self, GnomeVFSURI* uri, GError** error) {
	GsfXMLOut *xmlout;
	GsfOutput *gsfout;

	g_return_if_fail(!error || !*error);

	cdebug("PresentationWriter::saveToFile()", "start");

	g_return_if_fail(uri != NULL);
	/*g_return_if_fail(GNOME_IS_VFS_URI(uri));*/

	gnome_vfs_uri_ref(uri);

	{
		gchar* str = gnome_vfs_uri_to_string(uri, GNOME_VFS_URI_HIDE_USER_NAME | GNOME_VFS_URI_HIDE_PASSWORD);
		cdebug("PresentationWriter::saveToFile()", "Uri is %s", str);
		g_free(str);
	}
	gsfout = gsf_output_gnomevfs_new_uri(uri, error);

	if(error && *error) {
		return;
	}
	
	cdebug("PresentationWriter::saveToFile()", "gsf output opened");
	xmlout = gsf_xml_out_new(gsfout);
	cdebug("PresentationWriter::saveToFile()", "xml gsf output created");
#warning "saveToFile(): FIXME: Add a doctype"
/*	gsf_xml_out_set_doc_type () */
	write_presentation_xml(self, xmlout);
	cdebug("PresentationWriter::saveToFile()", "presentation xml is written");

	gsf_output_close(gsfout);
	cdebug ("PresentationWriter::saveToFile()", "gsf output closed");
	gnome_vfs_uri_unref(uri);
}

