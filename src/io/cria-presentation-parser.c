/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004,2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <io/cria-presentation-parser.h>

#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <glib/gstrfuncs.h>
#include <gsf/gsf-libxml.h>
#include <goffice/utils/go-color.h>

#define CDEBUG_TYPE -1
#include <cdebug/cdebug.h>

#include <dom/cria-block.h>
#include <utils/cria-units.h>

typedef struct {
	GList			* tags;
	gboolean                  block_in_slide;
	gchar                   * default_slide;
	GnomeVFSURI		* uri;
	guint			  unknown;
	GString			* cdata;
	GError			* error;
} Sax001Data;

typedef void (*cria_sax_start_element) (GHashTable	* attributes,
					Sax001Data	* sax_data);
typedef void (*cria_sax_end_element)   (Sax001Data	* sax_data);

typedef struct {
	const	gchar		* tag_name;
	cria_sax_start_element	  tag_start;
	cria_sax_end_element	  tag_end;
} cria_tag_data;

static xmlSAXHandler cria_0_0_1_handler;

static guint hex_to_int(gchar ch);

/**
 * cria_presentation_populate_from_xml_input:
 * @input: the #GsfInput used to fill the presentation
 * @uri: the #GnomeVFSURI for this file
 * @error: a location to return a #GError
 *
 * Takes an empty presentation and fills it with the data from the input
 *
 * Returns a new presentation from the #GsfInput
 */
CriaPresentation*
cria_presentation_populate_from_xml_input(GsfInput* input, GnomeVFSURI* uri, GError**error) {
	xmlParserCtxt	* context;
	Sax001Data	* data;
	CriaPresentation* self;
	
#warning "Presentation::populateFromXML(): check for empty presentation"
	g_return_val_if_fail(error == NULL || *error == NULL, NULL);
	cdebugt(CRIA_TYPE_PRESENTATION, "populateFromXML()", "start");

	context = gsf_xml_parser_context(input);
	context->sax = &cria_0_0_1_handler;
	data = g_new0(Sax001Data, 1);
	data->uri = gnome_vfs_uri_ref(uri);
	cdebugt(CRIA_TYPE_PRESENTATION, "populateFromXML()", "%d elements on the stack", g_list_length(data->tags));
	context->userData = data;

#warning "Presentation::populateFromXML(): FIXME: validate against DTD or Schema"
	xmlParseDocument(context);

	self = CRIA_PRESENTATION(data->tags->data);

	if(data->error) {
		*error = data->error;
	} else if(context->wellFormed) {
		cdebugo(self, "populateFromXML()", "really good file");
	} else {
		cdebugo(self, "populateFromXML()", "really bad file, we haven't gotten an error");
		*error = g_error_new(g_quark_from_static_string("criawips"), 1, "Unknown error: please file a bug report");
	}

	gnome_vfs_uri_unref(data->uri);
	g_free(data);
	cdebugo(self, "populateFromXML()", "end");

	return self;
}

static void
cria_sax_0_0_1_characters (
		gpointer	* user_data,
		const	xmlChar	* string,
		int		  length) {
	gchar		* str;
	Sax001Data	* data = (Sax001Data*) user_data;
	
	/* return if empty and if we're in some unknown space */
	if (data->unknown > 0 || !string) {
		return;
	}

	str = g_strndup((const gchar*)string, length);
	g_strstrip(str);
	
	if(strlen(str)) {
		/* just do this if we really have some data to care about */
		char	**strv,
			**iterator;

		/* TODO this code totally misbehaved (see the if-statement above)
		 * we need some new cdata parsing code. something like
		 * 1. replace any whitespace character with a space
		 * 2. replace any two spaces with one until there are no duplicated
		 *    spaces
		 * 3. cut trailing space
		 * 4. cut leading space if cdata was empty
		 */
		/* split the string into single lines to be processed */
		strv = g_strsplit(str, "\n", 0);

		if(!data->cdata) {
			data->cdata = g_string_new("");
		}

		for(iterator = strv; *iterator; iterator++) {
			g_strstrip(*iterator);

			/* append one whitespace between lines */
			/* but don't do if we already have '\n' there */
/*			if (strlen (data->cdata->str) && (data->cdata->str[strlen (data->cdata->str) - 1] != '\n'))
				g_string_append (data->cdata, " ");
*/
			g_string_append(data->cdata, *iterator);
		}

		g_strfreev(strv);
	}

	g_free(str);
}

static void
cria_sax_0_0_1_element_background_start(GHashTable* hash_table, Sax001Data* data) {
	GError		* error = NULL;
	GnomeVFSURI	* uri;
	gchar		* text_uri;
	
	g_return_if_fail(CRIA_IS_PRESENTATION(data->tags->data));
	g_return_if_fail(CRIA_IS_SLIDE(g_list_last(data->tags)->data));

	uri = gnome_vfs_uri_resolve_relative(cria_presentation_get_uri(data->tags->data),
					     g_hash_table_lookup(hash_table, "file"));
#warning "PresentationParser::elementBackgroundStart(): FIXME we have username's and password's in memory here"
	text_uri = gnome_vfs_uri_to_string(uri, GNOME_VFS_URI_HIDE_NONE);
	data->tags = g_list_append(data->tags, cria_image_new(text_uri));
	g_free(text_uri);
	gnome_vfs_uri_unref(uri);

	if(error) {
		data->error = error;
	}
}

static void
cria_sax_0_0_1_element_background_end(Sax001Data* data) {
	g_return_if_fail(CRIA_IS_IMAGE(g_list_last(data->tags)->data));
	g_return_if_fail(CRIA_IS_SLIDE(g_list_nth(data->tags, g_list_length(data->tags) - 2)->data));
	
	cria_background_set_image(cria_slide_get_background(g_list_nth(data->tags, g_list_length(data->tags) - 2)->data, TRUE),
				  CRIA_IMAGE(g_list_last(data->tags)->data));

	data->tags = g_list_remove(data->tags,
				   g_list_last(data->tags)->data);
}

static guint
hex_to_int(gchar ch) {
	if('0' <= ch && ch <= '9') {
		return (guint)(ch - '0');
	} else if ('a' <= ch && ch <= 'f') {
		return (guint)(10 + ch - 'a');
	} else if ('A' <= ch && ch <= 'F') {
		return (guint)(10 + ch - 'A');
	} else {
		g_warning("'%c' doesn't look like a valid hex character", ch);
		return 0;
	}
}

static void
cria_sax_0_0_1_element_block_start(GHashTable* hash_table,
				   Sax001Data* data)
{
	CriaBlock       * block = NULL;
	const gchar	* attr1,
	      		* attr2;
	const GOPoint 	* slide_size;
	GList           * elements,
			* element;
	CriaSlide       * slide = g_list_last(data->tags)->data;
	
	g_return_if_fail(CRIA_IS_SLIDE(slide));
	g_return_if_fail(CRIA_IS_PRESENTATION(data->tags->data));

	element = elements = cria_slide_get_elements(slide);

	for(; element; element = element->next) {
		CriaSlideElement* elem = CRIA_SLIDE_ELEMENT(element->data);
		attr1 = g_hash_table_lookup(hash_table, "title");
		if(attr1 && !strcmp(attr1, cria_slide_element_get_name(elem))) {
			block = CRIA_BLOCK(elem);
			data->block_in_slide = TRUE;
			break;
		}
	}

	if(!block) {
		cdebug("CriaPresentationParser::elementBlockStart()", "creating block '%s'", g_hash_table_lookup(hash_table, "title"));
		attr1 = g_hash_table_lookup(hash_table, "title");
		block = cria_block_new(attr1);
		data->block_in_slide = FALSE;
	}
	slide_size = cria_presentation_get_slide_size(CRIA_PRESENTATION(data->tags->data));

	/* set the position if we have information about it */
	attr1 = g_hash_table_lookup(hash_table, "x");
	attr2 = g_hash_table_lookup(hash_table, "width");
	if((attr1 != NULL && strlen(attr1)) || (attr2 != NULL && strlen(attr2))) {
		GODistance	x = 0.0 + cria_unit_to_pixels(attr1, slide_size->x / MASTER_COORDINATES_PER_POINT) * MASTER_COORDINATES_PER_POINT,
				w = 0.0 + cria_unit_to_pixels(attr2, slide_size->x / MASTER_COORDINATES_PER_POINT) * MASTER_COORDINATES_PER_POINT;
		
		g_object_set(G_OBJECT(block),
			     "left", x,
			     "right", x+w,
			     NULL);
	}

	attr1 = g_hash_table_lookup(hash_table, "y");
	attr2 = g_hash_table_lookup(hash_table, "height");
	if((attr1 != NULL && strlen(attr1)) || (attr2 != NULL && strlen(attr2))) {
		GODistance	y = 0.0 + cria_unit_to_pixels(attr1, slide_size->y / MASTER_COORDINATES_PER_POINT) * MASTER_COORDINATES_PER_POINT,
				h = 0.0 + cria_unit_to_pixels(attr2, slide_size->y / MASTER_COORDINATES_PER_POINT) * MASTER_COORDINATES_PER_POINT;
		
		g_object_set(G_OBJECT(block),
			     "top", y,
			     "bottom", y + h,
			     NULL);
	}

	/* the font */
	attr1 = g_hash_table_lookup(hash_table, "family");
	attr2 = g_hash_table_lookup(hash_table, "size");
	if(attr1 != NULL && strlen(attr1)) {
		cdebug("PresentationParser::elementBlockStart()", "setting font family \"%s\"", attr1);
		cria_block_set_font_family(block, attr1);
	}
	
	if(attr2 != NULL && strlen(attr2)) {
		cdebug("PresentationParser::elementBlockStart()",
		       "setting font size %lli (units); %i (int)",
		       cria_unit_to_pixels(attr2, slide_size->y / MASTER_COORDINATES_PER_POINT) * MASTER_COORDINATES_PER_POINT,
		       (int)cria_unit_to_pixels(attr2, slide_size->y / MASTER_COORDINATES_PER_POINT));
		cria_block_set_font_size_int(block, cria_unit_to_pixels(attr2, slide_size->y / MASTER_COORDINATES_PER_POINT) * MASTER_COORDINATES_PER_POINT);
	}

	/* colors */
	attr1 = g_hash_table_lookup(hash_table, "color");
	if(attr1 && *attr1) {
		GdkColor gdk_color;
		GOColor  color;

		gdk_color_parse(attr1, &gdk_color);

		color = GDK_TO_UINT(gdk_color);
		cdebug("PresentationParser::elementBlockStart()", "got \"%s\" as color, interpreted as 0x%x", attr1, color);
		cria_block_set_color(block, &color);
	}

	/* the alignment */
	attr1 = g_hash_table_lookup(hash_table, "align");
	attr2 = g_hash_table_lookup(hash_table, "valign");

	if(attr1 != NULL && strlen(attr1)) {
		cria_block_set_alignment(block, cria_alignment_from_string(attr1));
	}

	if(attr2 != NULL && strlen(attr2)) {
		cria_block_set_valignment(block, cria_valignment_from_string(attr2));
	}

	data->tags = g_list_append(data->tags, block);
}

static void
cria_sax_0_0_1_element_block_end(Sax001Data* data) {
	CriaBlock* block = NULL;

	g_return_if_fail(CRIA_IS_BLOCK(g_list_last(data->tags)->data) &&
			 CRIA_IS_SLIDE(g_list_nth(data->tags, g_list_length(data->tags) - 2)->data));

	block = CRIA_BLOCK(g_list_last(data->tags)->data);

	if(data->cdata && data->cdata->len) {
		CriaTextModel* model = cria_block_get_model(block);
		if(!model) {
			model = cria_text_model_new();
			cria_block_set_model(CRIA_BLOCK(g_list_last(data->tags)->data), model);
			g_object_unref(model);
		} else {
			g_message("had model");
		}
		cria_text_model_append(model, data->cdata->str);
		g_string_free(data->cdata, TRUE);
		data->cdata = NULL;
	}
	
	if(!data->block_in_slide) {
		cria_slide_add_element(CRIA_SLIDE(g_list_nth(data->tags, g_list_length(data->tags) - 2)->data),
				       CRIA_SLIDE_ELEMENT(block));
		g_object_unref(block);
	}

	cdebug("CriaPresentationParser::elementBlockEnd()", "%p (%s) has %d references", block, G_OBJECT_TYPE_NAME(block), G_OBJECT(block)->ref_count);

	data->tags = g_list_remove(data->tags,
				   g_list_last(data->tags)->data);
}

static void
cria_sax_0_0_1_element_br_end (Sax001Data* data) {
	if (data->cdata) {
		/*g_message("cria_sax_0_0_1_element_br_end(): data->cdata = 0x%x (%s)", (uintptr_t)data->cdata, (data->cdata==NULL)?NULL:data->cdata->str);*/
		g_string_append(data->cdata, "\n");
	}
}

static void
cria_sax_0_0_1_element_description_start(GHashTable* hash_table, Sax001Data* data) {
	cdebug("cria_sax_0_0_1_element_description_start()", "start");
	data->tags = g_list_append(data->tags,
				   g_strdup("description"));
	cdebug("cria_sax_0_0_1_element_description_start()", "end");
}

static void
cria_sax_0_0_1_element_description_end(Sax001Data* data) {
	cdebug("cria_sax_0_0_1_element_description_end()", "start");

	g_free(g_list_last(data->tags)->data);
	data->tags = g_list_remove(data->tags,
				   g_list_last(data->tags)->data);

	cria_slide_set_comment(CRIA_SLIDE(g_list_last(data->tags)->data),
			       data->cdata->str);

	g_string_free(data->cdata, TRUE);
	data->cdata = NULL;
	cdebug("cria_sax_0_0_1_element_description_end()", "end");
}

static void
cria_sax_0_0_1_element_layout_start(GHashTable* hash_table, Sax001Data* data) {
	CriaSlide * master_slide;
	
	cdebug("cria_sax_0_0_1_element_layout_start()", "start");

	master_slide = cria_slide_new(NULL);
	cria_slide_set_title(master_slide, g_hash_table_lookup(hash_table, "name"));
	data->tags = g_list_append(data->tags,
				   master_slide);
	
	cdebug("cria_sax_0_0_1_element_layout_start()", "end");
}

static void
cria_sax_0_0_1_element_layout_end(Sax001Data* data) {
	CriaTheme	* theme = g_list_nth(data->tags, g_list_length(data->tags) - 2)->data;
	CriaSlide	* slide = g_list_last(data->tags)->data;
	
	cdebug("cria_sax_0_0_1_element_layout_end()", "begin");
	
	g_return_if_fail(CRIA_IS_THEME(theme));
	g_return_if_fail(CRIA_IS_SLIDE(slide));

	cria_theme_add_master_slide(theme, slide);
	cdebug("CriaPresentationParser::elementLayoutEnd()", "the master slide has got %d references", G_OBJECT(slide)->ref_count);

	data->tags = g_list_remove(data->tags, slide);

	cdebug("cria_sax_0_0_1_element_layout_end()", "end");
}

static gboolean
is_true(const gchar* string) {
	g_return_val_if_fail(string, FALSE);
	if (!strcmp(string, "true") || !strcmp(string, "yes")) {
		return TRUE;
	} else {
		return FALSE;
	}
}

static void
cria_sax_0_0_1_element_presentation_start(GHashTable* hash_table, Sax001Data* data) {
	gchar* attribute;
	
	cdebug("cria_sax_element_presentation_start()", "start");
	g_return_if_fail(data != NULL);
	g_return_if_fail(0 == g_list_length(data->tags));

	attribute = g_hash_table_lookup(hash_table, "title");
	if(attribute) {
		data->tags = g_list_append(data->tags, g_object_new(CRIA_TYPE_PRESENTATION, "title", attribute, NULL));
	} else {
		data->tags = g_list_append(data->tags, g_object_new(CRIA_TYPE_PRESENTATION, NULL));
	}
	cria_presentation_set_uri(CRIA_PRESENTATION(data->tags->data), data->uri);

	attribute = g_hash_table_lookup(hash_table, "auto-switch");
	if(attribute) {
		cria_presentation_set_auto_switch(CRIA_PRESENTATION(data->tags->data), is_true(attribute));
	}

	attribute = g_hash_table_lookup(hash_table, "delay");
	if(attribute) {
		gdouble delay = g_ascii_strtod(attribute, NULL);
		cdebug("CriaPresentationParser::elementPresentationStart()", "parsed '%s' to %f", attribute, delay);
		cria_presentation_set_slide_delay(CRIA_PRESENTATION(data->tags->data), delay);
	}

	attribute = g_hash_table_lookup(hash_table, "loop");
	if(attribute) {
		cria_presentation_set_loop(CRIA_PRESENTATION(data->tags->data), is_true(attribute));
	}
	
	cdebug("cria_sax_element_presentation_start()", "end");
}

static void
cria_sax_0_0_1_element_presentation_end(Sax001Data* data) {
	/* just ensure we left the parser in a clean state */
	g_return_if_fail(data);
	g_return_if_fail(1 == g_list_length(data->tags));
	g_return_if_fail(CRIA_IS_PRESENTATION(data->tags->data));
}

static void
cria_sax_0_0_1_element_slide_start(GHashTable* hash_table, Sax001Data* data) {
	CriaPresentation* presentation;
	CriaSlide	* slide;
	CriaTheme	* theme;
	
#warning "elementSlideStart(): FIXME: add a more verbose error message"
	g_return_if_fail(g_hash_table_lookup(hash_table, "title") != NULL);

	slide = cria_slide_new(CRIA_SLIDE_LIST(data->tags->data));
	cdebug("CriaPresentationParser::elementSlideStart()", "slide has got %d references after creation", G_OBJECT(slide)->ref_count);
	cria_slide_set_title(slide, g_hash_table_lookup(hash_table, "title"));
	presentation = CRIA_PRESENTATION(data->tags->data);
	theme = cria_presentation_get_theme(presentation);

	if (g_hash_table_lookup (hash_table, "layout")) {
		cria_slide_set_master_slide (slide,
					     cria_theme_get_master_slide(theme,
									 g_hash_table_lookup(hash_table,
											     "layout")));
	}

	data->tags = g_list_append(data->tags, slide);
}

static void
cria_sax_0_0_1_element_slide_end(Sax001Data* data) {
	CriaSlide* slide = (CriaSlide*)g_list_last(data->tags)->data;
	g_return_if_fail(CRIA_IS_SLIDE(slide));

	cdebug("%s %p has got %d references", G_OBJECT_TYPE_NAME(slide), slide, G_OBJECT(slide)->ref_count);
	data->tags = g_list_remove(data->tags, slide);
}

static void
cria_sax_0_0_1_element_theme_start(GHashTable* hash_table, Sax001Data* data) {
	gchar const* attribute;
	cdebug("cria_sax_0_0_1_element_theme_start()", "start");
	data->tags = g_list_append(data->tags,
				   cria_theme_new(g_hash_table_lookup(hash_table, "name")));

	attribute = g_hash_table_lookup(hash_table, "default");
	if(attribute) {
		data->default_slide = g_strdup(attribute);
	}
	
	cdebug("cria_sax_0_0_1_element_theme_start()", "end");
}

static void
cria_sax_0_0_1_element_theme_end (Sax001Data* data) {
	CriaTheme* theme;
	
	cdebug("cria_sax_0_0_1_element_theme_end()", "start");
	g_return_if_fail(CRIA_IS_PRESENTATION(data->tags->data));
	g_return_if_fail(CRIA_IS_THEME(g_list_last(data->tags)->data));

	theme = CRIA_THEME(g_list_last(data->tags)->data);

	if(data->default_slide) {
		CriaSlide* slide = cria_theme_get_master_slide(theme, data->default_slide);
		cria_theme_set_default_slide(theme, slide);
		g_free(data->default_slide);
		data->default_slide = NULL;
	}
	cria_presentation_set_theme(data->tags->data, theme);
	
	g_object_unref(theme);
	cdebug("CriaPresentationParser::elementThemeEnd()",
		"theme had got %d references",
		G_OBJECT(theme)->ref_count);
			
	data->tags = g_list_remove(data->tags, g_list_last(data->tags)->data);
	cdebug("cria_sax_0_0_1_element_theme_end()", "end");
}

#warning "FIXME: do we have a cooler way to do reflections in C?"
static cria_tag_data cria_0_0_1_tag_data[] = {
	{"background",		&cria_sax_0_0_1_element_background_start,	&cria_sax_0_0_1_element_background_end		},
	{"block",		&cria_sax_0_0_1_element_block_start,		&cria_sax_0_0_1_element_block_end		},
	{"br",			NULL,						&cria_sax_0_0_1_element_br_end			},
	{"description",		&cria_sax_0_0_1_element_description_start,	&cria_sax_0_0_1_element_description_end		},
	{"layout",		&cria_sax_0_0_1_element_layout_start,		&cria_sax_0_0_1_element_layout_end		},
	{"presentation",	&cria_sax_0_0_1_element_presentation_start,	&cria_sax_0_0_1_element_presentation_end	},
	{"slide",		&cria_sax_0_0_1_element_slide_start,		&cria_sax_0_0_1_element_slide_end		},
	{"theme",		&cria_sax_0_0_1_element_theme_start,		&cria_sax_0_0_1_element_theme_end		},
	{NULL, NULL, NULL}
};

static GHashTable*
hash_table_from_strarray(const gchar **strarray) {
	GHashTable	* hash_table;
	const gchar	**iterator;

	hash_table = g_hash_table_new_full(g_str_hash,
					   g_str_equal,
					   g_free,
					   g_free);

	for(iterator = strarray; iterator && *iterator && *(iterator+1); iterator += 2) {
		gchar* key = g_strdup(*iterator);
		gchar* val = g_strdup(*(iterator + 1));
		
		cdebug("hash_table_from_strarray()", "0x%x: 0x%x=\"%s\" => 0x%x\"%s\"", (uintptr_t)hash_table, (uintptr_t)key, key, (uintptr_t)val, val);
		g_hash_table_insert(hash_table, key, val);
	}

	return hash_table;
}

static void
cria_0_0_1_start_element(gpointer userdata, gchar const* tagname, const gchar**attributes) {
	guint		  index = 0;
	Sax001Data	* sax_data = (Sax001Data*) userdata;

	cdebug("cria_0_0_1_start_element()", "%d elements on the stack", g_list_length(sax_data->tags));

	if(sax_data->error != NULL) {
		/* if we already have entered some invalid state the just do not try to make
		 * more damage happen
		 */
		return;
	}

	if(sax_data->unknown == 0) {
		/* We know where we are */
		for(index = 0; cria_0_0_1_tag_data[index].tag_name; index++) {
			if(strcmp(tagname, cria_0_0_1_tag_data[index].tag_name) == 0) {
				break;
			}
		}
	}

	if(sax_data->unknown > 0 || cria_0_0_1_tag_data[index].tag_name == NULL) {
		cdebug("cria_0_0_1_start_element()", "unknown tag \"%s\" (Unknown depth: %d)", tagname, sax_data->unknown);
		sax_data->unknown++;
	} else {
		if(cria_0_0_1_tag_data[index].tag_start) {
			GHashTable	* attrs = NULL;

			if(attributes != NULL) {
				attrs = hash_table_from_strarray(attributes);
			}
			
			cria_0_0_1_tag_data[index].tag_start(attrs, sax_data);

			if(attrs) {
				g_hash_table_destroy(attrs);
			}
		} else {
			cdebug("cria_0_0_1_start_element(%i)", "skipping known tag \"%s\"", tagname);
		}
	}
}

static void
cria_0_0_1_end_element(gpointer	userdata, gchar const* tagname) {
	guint		  index;
	Sax001Data      * sax_data;
	
	sax_data = (Sax001Data*) userdata;

	if(sax_data->error != NULL) {
		/* ignore this (see explanation in cria_0_0_1_start_element) */
		return;
	}

	if(sax_data->unknown > 0) {
		cdebug("cria_0_0_1_end_element()", "unknown tag \"%s\"", tagname);
		sax_data->unknown--;
	} else {
		for(index = 0; cria_0_0_1_tag_data[index].tag_name; index++) {
			if(strcmp(tagname, cria_0_0_1_tag_data[index].tag_name) == 0)
				break;
		}

		if (cria_0_0_1_tag_data[index].tag_end) {
			cria_0_0_1_tag_data[index].tag_end (sax_data);
		} else {
			cdebug("cria_0_0_1_end_element()", "skipping known tag \"%s\"", tagname);
		}
	}
}

static void
cria_0_0_1_start_document(gpointer userdata) {
	/* anything to do here? */
}

static void
cria_0_0_1_end_document(gpointer userdata) {
	/* anything to do here? */
}

static xmlSAXHandler cria_0_0_1_handler = {
	NULL, /* internalSubsetSAXFunc internalSubset;			*/
	NULL, /* isStandaloneSAXFunc isStandalone;			*/
	NULL, /* hasInternalSubsetSAXFunc hasInternalSubset;		*/
	NULL, /* hasExternalSubsetSAXFunc hasExternalSubset;		*/
	NULL, /* resolveEntitySAXFunc resolveEntity;			*/
	NULL, /* getEntitySAXFunc getEntity;				*/
	NULL, /* entityDeclSAXFunc entityDecl;				*/
	NULL, /* notationDeclSAXFunc notationDecl;			*/
	NULL, /* attributeDeclSAXFunc attributeDecl;			*/
	NULL, /* elementDeclSAXFunc elementDecl;			*/
	NULL, /* unparsedEntityDeclSAXFunc unparsedEntityDecl;		*/
	NULL, /* setDocumentLocatorSAXFunc setDocumentLocator;		*/
	cria_0_0_1_start_document,
	cria_0_0_1_end_document,
	(startElementSAXFunc)cria_0_0_1_start_element,
	(endElementSAXFunc)cria_0_0_1_end_element,
	NULL, /* referenceSAXFunc reference;				*/
	(charactersSAXFunc)cria_sax_0_0_1_characters,
	NULL, /* ignorableWhitespaceSAXFunc ignorableWhitespace;	*/
	NULL, /* processingInstructionSAXFunc processingInstruction;	*/
	NULL, /* commentSAXFunc comment;				*/
	NULL, /* warningSAXFunc warning;				*/
	NULL, /* errorSAXFunc error;					*/
	NULL  /* fatalErrorSAXFunc fatalError;				*/
};

