/* this file is part of criawips, a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2004 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef PRESENTATION_PARSER_H
#define PRESENTATION_PARSER_H

#include <glib-object.h>
#include <gsf/gsf.h>

#include <dom/cria-presentation.h>

G_BEGIN_DECLS

CriaPresentation* cria_presentation_populate_from_xml_input(GsfInput        * input,
							    GnomeVFSURI     * uri,
							    GError	    **error);

G_END_DECLS

#endif /* PRESENTATION_PARSER_H */
