/* this file is part of criawips a gnome presentation application
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *       Ezequiel P�rez       <eazel7@yahoo.com.ar>
 *
 * Copyright (C) 2004 Sven Herzberg
 * Copyright (C) 2004 Ezequiel P�rez
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef PRESENTATION_WRITER_H
#define PRESENTATION_WRITER_H

#include <dom/cria-presentation.h>

G_BEGIN_DECLS

void		cria_presentation_save_to_file	(CriaPresentation *self,
						 GnomeVFSURI *uri,
						 GError **err);

G_END_DECLS

#endif /* PRESENTATION_WRITER_H */
